# PI-Mobile-iOS

PI-Mobile iOS support based on Google's cross compiler j2objc. 

## Cross compilieren von Java nach iOS

XXX-iOS steht im folgenden für das IntelliJ-Projekt, in dem die alle für das Cross-Compilieren relevanten Java-Sourcen enthalten sind.     

1. **Im IntelliJ-Projekt "XXX-iOS"**
   - Java Sourcen updaten
   - Java Rebuild und ggf. Code lauffähig machen
   - Ordner "output" und "output-temp" von "pimobile/platform/ios" und in "XXX-iOS" leeren
   - Terminal öffnen und die Skripte j2objc-* laufen lassen (in "pimobile/platform/ios" und in "XXX-iOS")

1. **Im iOS Finder**
   - Ordner "output/de" von "pimobile/platform/ios" in "converted-src" des AppCode-Projekts kopieren (ersetzen)
   - Ordner "output/com" von "XXX-iOS" in "converted-src" des XCode-Projekts kopieren (ersetzen)

1. **In AppCode für alle in "src-temp" von "pimobile/platform/ios" geänderten Dateien (siehe Version Control -> Local Changes)**
   - Entsprechende Dateien in "native-src/de" des AppCode-Projekts mit Dateien in "output-temp/de" von "pimobile/platform/ios" mergen

1. **In AppCode für alle in "src-temp" von "StopStart-iOS" geänderten Dateien (siehe Version Control -> Local Changes)**
   - Entsprechende Dateien in "native-src/com" des AppCode-Projekts mit Dateien in "output-temp/de" von "StopStart-iOS" mergen

1. **Projektstruktur in AppCode anpassen:**
   - Beim Update von "XXX-iOS" gelöschte Dateien aus AppCode-Projekt entfernen
   - Beim Update von "XXX-iOS" hinzugefügte Dateien zu AppCode-Projekt hinzufügen (siehe in AppCode: Version Control -> Unversioned Files)

1. **In AppCode Run->Clean und dann Rund->Build**

## Setup XCode-Projekt

Siehe "XCode-Setup-PI-Mobile.odt" im Ordner "doc".
