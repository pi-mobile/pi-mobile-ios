//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/comm/pi-comm-basics/src/de/pidata/comm/client/base/DefaultConnectionController.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataCommClientBaseDefaultConnectionController")
#ifdef RESTRICT_DePidataCommClientBaseDefaultConnectionController
#define INCLUDE_ALL_DePidataCommClientBaseDefaultConnectionController 0
#else
#define INCLUDE_ALL_DePidataCommClientBaseDefaultConnectionController 1
#endif
#undef RESTRICT_DePidataCommClientBaseDefaultConnectionController

#if !defined (PIOCB_DefaultConnectionController_) && (INCLUDE_ALL_DePidataCommClientBaseDefaultConnectionController || defined(INCLUDE_PIOCB_DefaultConnectionController))
#define PIOCB_DefaultConnectionController_

#define RESTRICT_DePidataConnectBaseConnectionController 1
#define INCLUDE_PINB_ConnectionController 1
#include "de/pidata/connect/base/ConnectionController.h"

@interface PIOCB_DefaultConnectionController : NSObject < PINB_ConnectionController >

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithBoolean:(jboolean)clientIDset;

- (NSString *)connect;

- (void)disconnect;

- (jboolean)isConnected;

@end

J2OBJC_EMPTY_STATIC_INIT(PIOCB_DefaultConnectionController)

FOUNDATION_EXPORT void PIOCB_DefaultConnectionController_init(PIOCB_DefaultConnectionController *self);

FOUNDATION_EXPORT PIOCB_DefaultConnectionController *new_PIOCB_DefaultConnectionController_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIOCB_DefaultConnectionController *create_PIOCB_DefaultConnectionController_init(void);

FOUNDATION_EXPORT void PIOCB_DefaultConnectionController_initWithBoolean_(PIOCB_DefaultConnectionController *self, jboolean clientIDset);

FOUNDATION_EXPORT PIOCB_DefaultConnectionController *new_PIOCB_DefaultConnectionController_initWithBoolean_(jboolean clientIDset) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIOCB_DefaultConnectionController *create_PIOCB_DefaultConnectionController_initWithBoolean_(jboolean clientIDset);

J2OBJC_TYPE_LITERAL_HEADER(PIOCB_DefaultConnectionController)

@compatibility_alias DePidataCommClientBaseDefaultConnectionController PIOCB_DefaultConnectionController;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataCommClientBaseDefaultConnectionController")
