//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/connect/pi-connect-ip/src/de/pidata/connect/ip/IPHelper.java
//

#include "J2ObjC_source.h"
#include "de/pidata/connect/ip/IPHelper.h"
#include "java/lang/Exception.h"
#include "java/net/InetAddress.h"
#include "java/net/NetworkInterface.h"
#include "java/util/ArrayList.h"
#include "java/util/Collections.h"
#include "java/util/Enumeration.h"
#include "java/util/List.h"
#include "java/util/regex/Matcher.h"
#include "java/util/regex/Pattern.h"

#if !__has_feature(objc_arc)
#error "de/pidata/connect/ip/IPHelper must be compiled with ARC (-fobjc-arc)"
#endif

inline JavaUtilRegexPattern *PINI_IPHelper_get_IPV4_PATTERN(void);
static JavaUtilRegexPattern *PINI_IPHelper_IPV4_PATTERN;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PINI_IPHelper, IPV4_PATTERN, JavaUtilRegexPattern *)

J2OBJC_INITIALIZED_DEFN(PINI_IPHelper)

@implementation PINI_IPHelper

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PINI_IPHelper_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (jboolean)isIPv4AddressWithNSString:(NSString *)input {
  return PINI_IPHelper_isIPv4AddressWithNSString_(input);
}

+ (JavaNetInetAddress *)getMyIPWithBoolean:(jboolean)useIPv4 {
  return PINI_IPHelper_getMyIPWithBoolean_(useIPv4);
}

+ (NSString *)getMyIPAddressWithBoolean:(jboolean)useIPv4 {
  return PINI_IPHelper_getMyIPAddressWithBoolean_(useIPv4);
}

+ (NSString *)getMyIPAddressWithNSString:(NSString *)prefix {
  return PINI_IPHelper_getMyIPAddressWithNSString_(prefix);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LJavaNetInetAddress;", 0x9, 2, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 4, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 4, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(isIPv4AddressWithNSString:);
  methods[2].selector = @selector(getMyIPWithBoolean:);
  methods[3].selector = @selector(getMyIPAddressWithBoolean:);
  methods[4].selector = @selector(getMyIPAddressWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "IPV4_PATTERN", "LJavaUtilRegexPattern;", .constantValue.asLong = 0, 0x1a, -1, 5, -1, -1 },
  };
  static const void *ptrTable[] = { "isIPv4Address", "LNSString;", "getMyIP", "Z", "getMyIPAddress", &PINI_IPHelper_IPV4_PATTERN };
  static const J2ObjcClassInfo _PINI_IPHelper = { "IPHelper", "de.pidata.connect.ip", ptrTable, methods, fields, 7, 0x1, 5, 1, -1, -1, -1, -1, -1 };
  return &_PINI_IPHelper;
}

+ (void)initialize {
  if (self == [PINI_IPHelper class]) {
    PINI_IPHelper_IPV4_PATTERN = JavaUtilRegexPattern_compileWithNSString_(@"^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
    J2OBJC_SET_INITIALIZED(PINI_IPHelper)
  }
}

@end

void PINI_IPHelper_init(PINI_IPHelper *self) {
  NSObject_init(self);
}

PINI_IPHelper *new_PINI_IPHelper_init() {
  J2OBJC_NEW_IMPL(PINI_IPHelper, init)
}

PINI_IPHelper *create_PINI_IPHelper_init() {
  J2OBJC_CREATE_IMPL(PINI_IPHelper, init)
}

jboolean PINI_IPHelper_isIPv4AddressWithNSString_(NSString *input) {
  PINI_IPHelper_initialize();
  return [((JavaUtilRegexMatcher *) nil_chk([((JavaUtilRegexPattern *) nil_chk(PINI_IPHelper_IPV4_PATTERN)) matcherWithJavaLangCharSequence:input])) matches];
}

JavaNetInetAddress *PINI_IPHelper_getMyIPWithBoolean_(jboolean useIPv4) {
  PINI_IPHelper_initialize();
  @try {
    id<JavaUtilList> interfaces = JavaUtilCollections_listWithJavaUtilEnumeration_(JavaNetNetworkInterface_getNetworkInterfaces());
    for (JavaNetNetworkInterface * __strong intf in nil_chk(interfaces)) {
      id<JavaUtilList> addrs = JavaUtilCollections_listWithJavaUtilEnumeration_([((JavaNetNetworkInterface *) nil_chk(intf)) getInetAddresses]);
      for (JavaNetInetAddress * __strong addr in nil_chk(addrs)) {
        if (![((JavaNetInetAddress *) nil_chk(addr)) isLoopbackAddress]) {
          if (useIPv4) {
            NSString *sAddr = [((NSString *) nil_chk([addr getHostAddress])) uppercaseString];
            jboolean isIPv4 = PINI_IPHelper_isIPv4AddressWithNSString_(sAddr);
            if (isIPv4) {
              return addr;
            }
          }
          else {
            return addr;
          }
        }
      }
    }
  }
  @catch (JavaLangException *ex) {
  }
  return nil;
}

NSString *PINI_IPHelper_getMyIPAddressWithBoolean_(jboolean useIPv4) {
  PINI_IPHelper_initialize();
  JavaNetInetAddress *addr = PINI_IPHelper_getMyIPWithBoolean_(useIPv4);
  if (addr != nil) {
    return @"";
  }
  else {
    NSString *sAddr = [((NSString *) nil_chk([((JavaNetInetAddress *) nil_chk(addr)) getHostAddress])) uppercaseString];
    jboolean isIPv4 = PINI_IPHelper_isIPv4AddressWithNSString_(sAddr);
    if (isIPv4) {
      return sAddr;
    }
    else {
      jint delim = [((NSString *) nil_chk(sAddr)) java_indexOf:'%'];
      if (delim < 0) {
        return sAddr;
      }
      else {
        return [sAddr java_substring:0 endIndex:delim];
      }
    }
  }
}

NSString *PINI_IPHelper_getMyIPAddressWithNSString_(NSString *prefix) {
  PINI_IPHelper_initialize();
  @try {
    id<JavaUtilList> interfaces = JavaUtilCollections_listWithJavaUtilEnumeration_(JavaNetNetworkInterface_getNetworkInterfaces());
    for (JavaNetNetworkInterface * __strong intf in nil_chk(interfaces)) {
      id<JavaUtilList> addrs = JavaUtilCollections_listWithJavaUtilEnumeration_([((JavaNetNetworkInterface *) nil_chk(intf)) getInetAddresses]);
      for (JavaNetInetAddress * __strong addr in nil_chk(addrs)) {
        if (![((JavaNetInetAddress *) nil_chk(addr)) isLoopbackAddress]) {
          NSString *ip = [((NSString *) nil_chk([addr getHostAddress])) uppercaseString];
          if ((prefix == nil) || [((NSString *) nil_chk(ip)) java_hasPrefix:prefix]) {
            return [((NSString *) nil_chk([addr getHostName])) uppercaseString];
          }
        }
      }
    }
  }
  @catch (JavaLangException *ex) {
  }
  return nil;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PINI_IPHelper)

J2OBJC_NAME_MAPPING(PINI_IPHelper, "de.pidata.connect.ip", "PINI_")
