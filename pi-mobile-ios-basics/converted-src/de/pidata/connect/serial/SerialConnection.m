//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/connect/pi-connect-basics/src/de/pidata/connect/serial/SerialConnection.java
//

#include "J2ObjC_source.h"
#include "de/pidata/connect/serial/SerialConnection.h"

#if !__has_feature(objc_arc)
#error "de/pidata/connect/serial/SerialConnection must be compiled with ARC (-fobjc-arc)"
#endif

@interface PINE_SerialConnection : NSObject

@end

@implementation PINE_SerialConnection

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 1, 2, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 3, 2, -1, -1, -1 },
    { NULL, "V", 0x401, -1, -1, 2, -1, -1, -1 },
    { NULL, "Z", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(disconnect);
  methods[1].selector = @selector(sendWithChar:);
  methods[2].selector = @selector(sendWithJavaLangStringBuilder:);
  methods[3].selector = @selector(sendBreak);
  methods[4].selector = @selector(dataAvailable);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "send", "C", "LJavaIoIOException;", "LJavaLangStringBuilder;" };
  static const J2ObjcClassInfo _PINE_SerialConnection = { "SerialConnection", "de.pidata.connect.serial", ptrTable, methods, NULL, 7, 0x609, 5, 0, -1, -1, -1, -1, -1 };
  return &_PINE_SerialConnection;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PINE_SerialConnection)

J2OBJC_NAME_MAPPING(PINE_SerialConnection, "de.pidata.connect.serial", "PINE_")
