//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/geometry/Line.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGeometryLine")
#ifdef RESTRICT_DePidataGeometryLine
#define INCLUDE_ALL_DePidataGeometryLine 0
#else
#define INCLUDE_ALL_DePidataGeometryLine 1
#endif
#undef RESTRICT_DePidataGeometryLine

#if !defined (PICG_Line_) && (INCLUDE_ALL_DePidataGeometryLine || defined(INCLUDE_PICG_Line))
#define PICG_Line_

@class PICG_Point;

@interface PICG_Line : NSObject

#pragma mark Public

- (instancetype)initWithPICG_Point:(PICG_Point *)p1
                    withPICG_Point:(PICG_Point *)p2;

- (PICG_Point *)getIntersectionWithPICG_Line:(PICG_Line *)otherLine;

- (PICG_Point *)getP1;

- (PICG_Point *)getP2;

- (PICG_Line *)getParallelWithDouble:(jdouble)distance;

- (PICG_Point *)getPointAtWithDouble:(jdouble)distance;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PICG_Line)

FOUNDATION_EXPORT void PICG_Line_initWithPICG_Point_withPICG_Point_(PICG_Line *self, PICG_Point *p1, PICG_Point *p2);

FOUNDATION_EXPORT PICG_Line *new_PICG_Line_initWithPICG_Point_withPICG_Point_(PICG_Point *p1, PICG_Point *p2) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PICG_Line *create_PICG_Line_initWithPICG_Point_withPICG_Point_(PICG_Point *p1, PICG_Point *p2);

J2OBJC_TYPE_LITERAL_HEADER(PICG_Line)

@compatibility_alias DePidataGeometryLine PICG_Line;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGeometryLine")
