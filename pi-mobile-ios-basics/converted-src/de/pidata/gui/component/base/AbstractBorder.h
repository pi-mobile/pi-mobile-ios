//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/AbstractBorder.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentBaseAbstractBorder")
#ifdef RESTRICT_DePidataGuiComponentBaseAbstractBorder
#define INCLUDE_ALL_DePidataGuiComponentBaseAbstractBorder 0
#else
#define INCLUDE_ALL_DePidataGuiComponentBaseAbstractBorder 1
#endif
#undef RESTRICT_DePidataGuiComponentBaseAbstractBorder

#if !defined (PIGB_AbstractBorder_) && (INCLUDE_ALL_DePidataGuiComponentBaseAbstractBorder || defined(INCLUDE_PIGB_AbstractBorder))
#define PIGB_AbstractBorder_

#define RESTRICT_DePidataGuiComponentBaseBorder 1
#define INCLUDE_PIGB_Border 1
#include "de/pidata/gui/component/base/Border.h"

@class PIGB_Direction;

@interface PIGB_AbstractBorder : NSObject < PIGB_Border > {
 @public
  jshort top_;
  jshort left_;
  jshort right_;
  jshort bottom_;
}

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithShort:(jshort)top
                    withShort:(jshort)left
                    withShort:(jshort)right
                    withShort:(jshort)bottom;

- (jshort)get2BorderSizeWithPIGB_Direction:(PIGB_Direction *)direction;

- (jshort)getBottom;

- (jshort)getLeft;

- (jshort)getOffsetWithPIGB_Direction:(PIGB_Direction *)direction;

- (jshort)getRight;

- (jshort)getTop;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGB_AbstractBorder)

FOUNDATION_EXPORT void PIGB_AbstractBorder_init(PIGB_AbstractBorder *self);

FOUNDATION_EXPORT void PIGB_AbstractBorder_initWithShort_withShort_withShort_withShort_(PIGB_AbstractBorder *self, jshort top, jshort left, jshort right, jshort bottom);

J2OBJC_TYPE_LITERAL_HEADER(PIGB_AbstractBorder)

@compatibility_alias DePidataGuiComponentBaseAbstractBorder PIGB_AbstractBorder;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentBaseAbstractBorder")
