//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/CompSizeLimit.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentBaseCompSizeLimit")
#ifdef RESTRICT_DePidataGuiComponentBaseCompSizeLimit
#define INCLUDE_ALL_DePidataGuiComponentBaseCompSizeLimit 0
#else
#define INCLUDE_ALL_DePidataGuiComponentBaseCompSizeLimit 1
#endif
#undef RESTRICT_DePidataGuiComponentBaseCompSizeLimit

#if !defined (PIGB_CompSizeLimit_) && (INCLUDE_ALL_DePidataGuiComponentBaseCompSizeLimit || defined(INCLUDE_PIGB_CompSizeLimit))
#define PIGB_CompSizeLimit_

#define RESTRICT_DePidataGuiComponentBaseSizeLimit 1
#define INCLUDE_PIGB_SizeLimit 1
#include "de/pidata/gui/component/base/SizeLimit.h"

@class PIGB_Direction;
@protocol PIGB_Component;

@interface PIGB_CompSizeLimit : PIGB_SizeLimit

#pragma mark Public

- (instancetype)initWithPIGB_Component:(id<PIGB_Component>)owner
                    withPIGB_Direction:(PIGB_Direction *)direction;

- (instancetype)initWithPIGB_Component:(id<PIGB_Component>)owner
                    withPIGB_Direction:(PIGB_Direction *)direction
                             withShort:(jshort)min
                             withShort:(jshort)pref
                             withShort:(jshort)max;

- (jboolean)setCurrentWithShort:(jshort)current;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithShort:(jshort)arg0
                    withShort:(jshort)arg1
                    withShort:(jshort)arg2 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGB_CompSizeLimit)

FOUNDATION_EXPORT void PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_(PIGB_CompSizeLimit *self, id<PIGB_Component> owner, PIGB_Direction *direction);

FOUNDATION_EXPORT PIGB_CompSizeLimit *new_PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_(id<PIGB_Component> owner, PIGB_Direction *direction) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGB_CompSizeLimit *create_PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_(id<PIGB_Component> owner, PIGB_Direction *direction);

FOUNDATION_EXPORT void PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_withShort_withShort_withShort_(PIGB_CompSizeLimit *self, id<PIGB_Component> owner, PIGB_Direction *direction, jshort min, jshort pref, jshort max);

FOUNDATION_EXPORT PIGB_CompSizeLimit *new_PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_withShort_withShort_withShort_(id<PIGB_Component> owner, PIGB_Direction *direction, jshort min, jshort pref, jshort max) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGB_CompSizeLimit *create_PIGB_CompSizeLimit_initWithPIGB_Component_withPIGB_Direction_withShort_withShort_withShort_(id<PIGB_Component> owner, PIGB_Direction *direction, jshort min, jshort pref, jshort max);

J2OBJC_TYPE_LITERAL_HEADER(PIGB_CompSizeLimit)

@compatibility_alias DePidataGuiComponentBaseCompSizeLimit PIGB_CompSizeLimit;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentBaseCompSizeLimit")
