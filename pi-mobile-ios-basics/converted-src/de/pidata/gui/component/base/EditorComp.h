//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/EditorComp.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentBaseEditorComp")
#ifdef RESTRICT_DePidataGuiComponentBaseEditorComp
#define INCLUDE_ALL_DePidataGuiComponentBaseEditorComp 0
#else
#define INCLUDE_ALL_DePidataGuiComponentBaseEditorComp 1
#endif
#undef RESTRICT_DePidataGuiComponentBaseEditorComp

#if !defined (PIGB_EditorComp_) && (INCLUDE_ALL_DePidataGuiComponentBaseEditorComp || defined(INCLUDE_PIGB_EditorComp))
#define PIGB_EditorComp_

#define RESTRICT_DePidataGuiComponentBasePanel 1
#define INCLUDE_PIGB_Panel 1
#include "de/pidata/gui/component/base/Panel.h"

@protocol PIGL_Layouter;

@interface PIGB_EditorComp : PIGB_Panel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithBoolean:(jboolean)scrollHor
                    withBoolean:(jboolean)scrollVert;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGL_Layouter:(id<PIGL_Layouter>)arg0
                    withPIGL_Layouter:(id<PIGL_Layouter>)arg1
                          withBoolean:(jboolean)arg2
                          withBoolean:(jboolean)arg3
                            withShort:(jshort)arg4 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGB_EditorComp)

FOUNDATION_EXPORT void PIGB_EditorComp_init(PIGB_EditorComp *self);

FOUNDATION_EXPORT PIGB_EditorComp *new_PIGB_EditorComp_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGB_EditorComp *create_PIGB_EditorComp_init(void);

FOUNDATION_EXPORT void PIGB_EditorComp_initWithBoolean_withBoolean_(PIGB_EditorComp *self, jboolean scrollHor, jboolean scrollVert);

FOUNDATION_EXPORT PIGB_EditorComp *new_PIGB_EditorComp_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGB_EditorComp *create_PIGB_EditorComp_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert);

J2OBJC_TYPE_LITERAL_HEADER(PIGB_EditorComp)

@compatibility_alias DePidataGuiComponentBaseEditorComp PIGB_EditorComp;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentBaseEditorComp")
