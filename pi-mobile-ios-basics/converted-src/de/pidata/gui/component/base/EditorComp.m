//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/EditorComp.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentState.h"
#include "de/pidata/gui/component/base/Direction.h"
#include "de/pidata/gui/component/base/EditorComp.h"
#include "de/pidata/gui/component/base/Panel.h"
#include "de/pidata/gui/layout/FlowLayouter.h"
#include "de/pidata/gui/layout/StackLayouter.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/component/base/EditorComp must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIGB_EditorComp

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGB_EditorComp_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithBoolean:(jboolean)scrollHor
                    withBoolean:(jboolean)scrollVert {
  PIGB_EditorComp_initWithBoolean_withBoolean_(self, scrollHor, scrollVert);
  return self;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithBoolean:withBoolean:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "ZZ" };
  static const J2ObjcClassInfo _PIGB_EditorComp = { "EditorComp", "de.pidata.gui.component.base", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_PIGB_EditorComp;
}

@end

void PIGB_EditorComp_init(PIGB_EditorComp *self) {
  PIGB_EditorComp_initWithBoolean_withBoolean_(self, false, false);
}

PIGB_EditorComp *new_PIGB_EditorComp_init() {
  J2OBJC_NEW_IMPL(PIGB_EditorComp, init)
}

PIGB_EditorComp *create_PIGB_EditorComp_init() {
  J2OBJC_CREATE_IMPL(PIGB_EditorComp, init)
}

void PIGB_EditorComp_initWithBoolean_withBoolean_(PIGB_EditorComp *self, jboolean scrollHor, jboolean scrollVert) {
  PIGB_Panel_initWithPIGL_Layouter_withPIGL_Layouter_withBoolean_withBoolean_withShort_(self, new_PIGL_StackLayouter_initWithPIGB_Direction_(JreLoadStatic(PIGB_Direction, X)), new_PIGL_FlowLayouter_initWithPIGB_Direction_(JreLoadStatic(PIGB_Direction, Y)), scrollHor, scrollVert, (jshort) 1);
  [((PIGB_ComponentState *) nil_chk([self getComponentState])) setVisibleWithBoolean:false];
}

PIGB_EditorComp *new_PIGB_EditorComp_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert) {
  J2OBJC_NEW_IMPL(PIGB_EditorComp, initWithBoolean_withBoolean_, scrollHor, scrollVert)
}

PIGB_EditorComp *create_PIGB_EditorComp_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert) {
  J2OBJC_CREATE_IMPL(PIGB_EditorComp, initWithBoolean_withBoolean_, scrollHor, scrollVert)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGB_EditorComp)

J2OBJC_NAME_MAPPING(PIGB_EditorComp, "de.pidata.gui.component.base", "PIGB_")
