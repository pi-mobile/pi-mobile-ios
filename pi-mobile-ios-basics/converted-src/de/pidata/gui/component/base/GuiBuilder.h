//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/GuiBuilder.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentBaseGuiBuilder")
#ifdef RESTRICT_DePidataGuiComponentBaseGuiBuilder
#define INCLUDE_ALL_DePidataGuiComponentBaseGuiBuilder 0
#else
#define INCLUDE_ALL_DePidataGuiComponentBaseGuiBuilder 1
#endif
#undef RESTRICT_DePidataGuiComponentBaseGuiBuilder

#if !defined (PIGB_GuiBuilder_) && (INCLUDE_ALL_DePidataGuiComponentBaseGuiBuilder || defined(INCLUDE_PIGB_GuiBuilder))
#define PIGB_GuiBuilder_

@class IOSClass;
@class PIGG_DialogDef;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIGB_Dialog;
@protocol PIMR_Model;

@protocol PIGB_GuiBuilder < JavaObject >

- (id<PIGB_Dialog>)createDialogWithPIGG_DialogDef:(PIGG_DialogDef *)dialogDef;

- (void)setModuleContainerWithPIMR_Model:(id<PIMR_Model>)moduleContainer;

- (void)addShapeClassWithPIQ_QName:(PIQ_QName *)typeID
                      withIOSClass:(IOSClass *)shapeClass;

@end

@interface PIGB_GuiBuilder : NSObject

@end

J2OBJC_STATIC_INIT(PIGB_GuiBuilder)

inline PIQ_Namespace *PIGB_GuiBuilder_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGB_GuiBuilder_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_COLS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_COLS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_COLS, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ROWS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ROWS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ROWS, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_SCROLLNONE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_SCROLLNONE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_SCROLLNONE, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_SCROLLBOTH(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_SCROLLBOTH;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_SCROLLBOTH, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_SCROLLHOR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_SCROLLHOR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_SCROLLHOR, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_SCROLLVERT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_SCROLLVERT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_SCROLLVERT, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_GRIDLAYOUTER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_GRIDLAYOUTER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_GRIDLAYOUTER, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_STACKLAYOUTER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_STACKLAYOUTER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_STACKLAYOUTER, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_FLOWLAYOUTER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_FLOWLAYOUTER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_FLOWLAYOUTER, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNLEFT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNLEFT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNLEFT, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNRIGHT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNRIGHT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNRIGHT, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNTOP(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNTOP;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNTOP, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNBOTTOM(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNBOTTOM;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNBOTTOM, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNCENTER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNCENTER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNCENTER, PIQ_QName *)

inline PIQ_QName *PIGB_GuiBuilder_get_ID_ALIGNGROW(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_GuiBuilder_ID_ALIGNGROW;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_GuiBuilder, ID_ALIGNGROW, PIQ_QName *)

J2OBJC_TYPE_LITERAL_HEADER(PIGB_GuiBuilder)

#define DePidataGuiComponentBaseGuiBuilder PIGB_GuiBuilder

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentBaseGuiBuilder")
