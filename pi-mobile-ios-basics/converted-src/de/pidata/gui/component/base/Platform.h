//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/Platform.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentBasePlatform")
#ifdef RESTRICT_DePidataGuiComponentBasePlatform
#define INCLUDE_ALL_DePidataGuiComponentBasePlatform 0
#else
#define INCLUDE_ALL_DePidataGuiComponentBasePlatform 1
#endif
#undef RESTRICT_DePidataGuiComponentBasePlatform

#if !defined (PIGB_Platform_) && (INCLUDE_ALL_DePidataGuiComponentBasePlatform || defined(INCLUDE_PIGB_Platform))
#define PIGB_Platform_

#define RESTRICT_JavaLangThread 1
#define INCLUDE_JavaLangThread 1
#include "java/lang/Thread.h"

@class IOSObjectArray;
@class JavaIoInputStream;
@class JavaLangThreadGroup;
@class PIGB_ComponentFactory;
@class PIGG_ControllerBuilder;
@class PIMI_Configurator;
@class PIMI_DeviceTable;
@class PIMR_Context;
@class PINS_MessageSplitter;
@class PIQ_QName;
@class PISYSB_Storage;
@protocol JavaLangRunnable;
@protocol PIGB_Border;
@protocol PIGB_ComponentBitmap;
@protocol PIGB_ComponentColor;
@protocol PIGB_ComponentFont;
@protocol PIGB_Dialog;
@protocol PIGB_GuiBuilder;
@protocol PIGB_MediaInterface;
@protocol PIGB_NfcTool;
@protocol PIGB_PlatformScheduler;
@protocol PIGB_Screen;
@protocol PIGE_InputManager;
@protocol PIGL_Layouter;
@protocol PIGU_UIContainer;
@protocol PIGU_UIFactory;
@protocol PIGV_ViewPI;
@protocol PINL_SPPConnection;

@interface PIGB_Platform : JavaLangThread

#pragma mark Public

- (void)addToImageCacheWithPIQ_QName:(PIQ_QName *)imageID
            withPIGB_ComponentBitmap:(id<PIGB_ComponentBitmap>)bitmap;

- (id<PIGB_ComponentBitmap>)createBitmapWithInt:(jint)width
                                        withInt:(jint)height;

- (id<PINL_SPPConnection>)createBluetoothSPPConnectionWithNSString:(NSString *)serverAddress
                                                      withNSString:(NSString *)uuidString
                                          withPINS_MessageSplitter:(PINS_MessageSplitter *)messageSplitter;

- (id<PIGB_Dialog>)createDialogWithPIGL_Layouter:(id<PIGL_Layouter>)layouterX
                               withPIGL_Layouter:(id<PIGL_Layouter>)layouterY
                                       withShort:(jshort)x
                                       withShort:(jshort)y
                                       withShort:(jshort)width
                                       withShort:(jshort)height;

- (id<PIGB_ComponentFont>)createFontWithNSString:(NSString *)name
                                   withPIQ_QName:(PIQ_QName *)style
                                         withInt:(jint)size;

- (id<PIGB_PlatformScheduler>)createScheduler;

- (void)exitWithPIMR_Context:(PIMR_Context *)context;

- (id<PIGB_ComponentBitmap>)getBitmapWithPIQ_QName:(PIQ_QName *)bitmapID;

- (id<PIGB_Border>)getBorderWithPIQ_QName:(PIQ_QName *)borderID;

- (id<PIGB_ComponentColor>)getColorWithInt:(jint)red
                                   withInt:(jint)green
                                   withInt:(jint)blue;

- (id<PIGB_ComponentColor>)getColorWithInt:(jint)red
                                   withInt:(jint)green
                                   withInt:(jint)blue
                                withDouble:(jdouble)alpha;

- (id<PIGB_ComponentColor>)getColorWithPIQ_QName:(PIQ_QName *)colorID;

- (id<PIGB_ComponentColor>)getColorWithNSString:(NSString *)colorString;

- (PIGB_ComponentFactory *)getComponentFactory;

- (PIMI_Configurator *)getConfigurator;

- (PIGG_ControllerBuilder *)getControllerBuilder;

- (NSString *)getDevice;

- (id<PIGB_ComponentFont>)getFontWithPIQ_QName:(PIQ_QName *)fontID;

- (PISYSB_Storage *)getGuiStorage;

- (id<PIGE_InputManager>)getInputManager;

+ (PIGB_Platform *)getInstance;

- (id<PIGB_MediaInterface>)getMediaInterface;

- (PIQ_QName *)getNavigationKeys;

- (id<PIGB_NfcTool>)getNfcTool;

- (void)getPairedBluetoothDevicesWithPIMI_DeviceTable:(PIMI_DeviceTable *)deviceTable;

- (NSString *)getPlatformName;

- (id<PIGB_Screen>)getScreen;

- (NSString *)getStartupFileWithNSString:(NSString *)programName;

- (PISYSB_Storage *)getStartupFileStorage;

- (jint)getStartupProgress;

- (id<PIGU_UIFactory>)getUiFactory;

- (jboolean)hasTableFirstRowForEmptySelection;

- (void)initWindowWithId:(id)dialog OBJC_METHOD_FAMILY_NONE;

- (jboolean)isCharWidthUsable;

- (jboolean)isOnUiThread;

- (jboolean)isSingleWindow;

- (id<PIGB_ComponentBitmap>)loadBitmapWithJavaIoInputStream:(JavaIoInputStream *)imageStream;

- (id<PIGB_ComponentBitmap>)loadBitmapAssetWithPIQ_QName:(PIQ_QName *)bitmapID;

- (id<PIGB_ComponentBitmap>)loadBitmapFileWithNSString:(NSString *)path;

- (id<PIGB_ComponentBitmap>)loadBitmapResourceWithPIQ_QName:(PIQ_QName *)bitmapID;

- (id<PIGB_ComponentBitmap>)loadBitmapThumbnailWithPISYSB_Storage:(PISYSB_Storage *)imageStorage
                                                     withNSString:(NSString *)imageFileName
                                                          withInt:(jint)width
                                                          withInt:(jint)height;

- (void)loadGuiWithPIMR_Context:(PIMR_Context *)context
                   withNSString:(NSString *)filename;

- (void)loadServicesWithPIMR_Context:(PIMR_Context *)context;

- (jboolean)openBrowserWithNSString:(NSString *)url;

- (void)run;

- (void)runOnUiThreadWithJavaLangRunnable:(id<JavaLangRunnable>)runnable;

- (void)setBorderWithPIQ_QName:(PIQ_QName *)borderID
               withPIGB_Border:(id<PIGB_Border>)border;

- (id<PIGB_ComponentColor>)setColorWithPIQ_QName:(PIQ_QName *)name
                                         withInt:(jint)red
                                         withInt:(jint)green
                                         withInt:(jint)blue
                                      withDouble:(jdouble)alpha;

- (void)setColorWithPIQ_QName:(PIQ_QName *)colorID
                 withNSString:(NSString *)value;

- (void)setFontWithPIQ_QName:(PIQ_QName *)fontID
                withNSString:(NSString *)name
               withPIQ_QName:(PIQ_QName *)style
                     withInt:(jint)size;

- (void)setStartupProgressWithInt:(jint)percent;

- (void)startGuiWithPIMR_Context:(PIMR_Context *)context;

+ (void)switchFocusToWithPIGV_ViewPI:(id<PIGV_ViewPI>)toView;

- (void)toggleKeyboardWithBoolean:(jboolean)hasFocus;

- (void)updateLanguageWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer
                             withPIQ_QName:(PIQ_QName *)dialogID
                              withNSString:(NSString *)language;

- (jboolean)useCursor4Focus;

- (jboolean)useDoubleBuffering;

#pragma mark Protected

- (instancetype)init;

- (id<PIGB_GuiBuilder>)createGuiBuilder;

- (void)init__WithPIMR_Context:(PIMR_Context *)context
             withNSStringArray:(IOSObjectArray *)args OBJC_METHOD_FAMILY_NONE;

- (void)initCommWithPIMR_Context:(PIMR_Context *)context OBJC_METHOD_FAMILY_NONE;

- (void)loadConfigWithPIMR_Context:(PIMR_Context *)context;

- (void)loadGuiWithPIMR_Context:(PIMR_Context *)context
              withNSStringArray:(IOSObjectArray *)args;

- (void)openInitialDialogWithPIMR_Context:(PIMR_Context *)context
                            withPIQ_QName:(PIQ_QName *)opID;

- (void)setConfiguratorWithPIMI_Configurator:(PIMI_Configurator *)configurator;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithJavaLangRunnable:(id<JavaLangRunnable>)arg0 NS_UNAVAILABLE;

- (instancetype)initWithJavaLangRunnable:(id<JavaLangRunnable>)arg0
                            withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithJavaLangThreadGroup:(JavaLangThreadGroup *)arg0
                       withJavaLangRunnable:(id<JavaLangRunnable>)arg1 NS_UNAVAILABLE;

- (instancetype)initWithJavaLangThreadGroup:(JavaLangThreadGroup *)arg0
                       withJavaLangRunnable:(id<JavaLangRunnable>)arg1
                               withNSString:(NSString *)arg2 NS_UNAVAILABLE;

- (instancetype)initWithJavaLangThreadGroup:(JavaLangThreadGroup *)arg0
                       withJavaLangRunnable:(id<JavaLangRunnable>)arg1
                               withNSString:(NSString *)arg2
                                   withLong:(jlong)arg3 NS_UNAVAILABLE;

- (instancetype)initWithJavaLangThreadGroup:(JavaLangThreadGroup *)arg0
                               withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithNSString:(NSString *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGB_Platform)

inline PIQ_QName *PIGB_Platform_get_NAVKEYS_KEYBOARD(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_Platform_NAVKEYS_KEYBOARD;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_Platform, NAVKEYS_KEYBOARD, PIQ_QName *)

inline PIQ_QName *PIGB_Platform_get_NAVKEYS_2_WAY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_Platform_NAVKEYS_2_WAY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_Platform, NAVKEYS_2_WAY, PIQ_QName *)

inline PIQ_QName *PIGB_Platform_get_NAVKEYS_4_WAY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGB_Platform_NAVKEYS_4_WAY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGB_Platform, NAVKEYS_4_WAY, PIQ_QName *)

inline jboolean PIGB_Platform_get_loadServices(void);
inline jboolean PIGB_Platform_set_loadServices(jboolean value);
inline jboolean *PIGB_Platform_getRef_loadServices(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT jboolean PIGB_Platform_loadServices;
J2OBJC_STATIC_FIELD_PRIMITIVE(PIGB_Platform, loadServices, jboolean)

FOUNDATION_EXPORT PIGB_Platform *PIGB_Platform_getInstance(void);

FOUNDATION_EXPORT void PIGB_Platform_switchFocusToWithPIGV_ViewPI_(id<PIGV_ViewPI> toView);

FOUNDATION_EXPORT void PIGB_Platform_init(PIGB_Platform *self);

J2OBJC_TYPE_LITERAL_HEADER(PIGB_Platform)

@compatibility_alias DePidataGuiComponentBasePlatform PIGB_Platform;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentBasePlatform")
