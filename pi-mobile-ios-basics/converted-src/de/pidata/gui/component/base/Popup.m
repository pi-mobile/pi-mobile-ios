//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/Popup.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/AbstractContainer.h"
#include "de/pidata/gui/component/base/Component.h"
#include "de/pidata/gui/component/base/ComponentState.h"
#include "de/pidata/gui/component/base/Container.h"
#include "de/pidata/gui/component/base/Dialog.h"
#include "de/pidata/gui/component/base/Direction.h"
#include "de/pidata/gui/component/base/PaintManager.h"
#include "de/pidata/gui/component/base/PaintState.h"
#include "de/pidata/gui/component/base/Panel.h"
#include "de/pidata/gui/component/base/Popup.h"
#include "de/pidata/gui/component/base/Position.h"
#include "de/pidata/gui/component/base/Scroller.h"
#include "de/pidata/gui/component/base/SizeLimit.h"
#include "de/pidata/gui/layout/FlowLayouter.h"
#include "de/pidata/gui/layout/StackLayouter.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/component/base/Popup must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGB_Popup () {
 @public
  id<PIGB_Component> owner_;
  id<PIGB_Dialog> dialog_;
  PIGB_Popup *parentPopup_;
  PIGB_Popup *childPopup_;
}

- (jshort)calcPosXWithShort:(jshort)windowWidth
                  withShort:(jshort)x
                  withShort:(jshort)compWidth
                  withShort:(jshort)popupWidth;

- (jshort)calcPosYWithShort:(jshort)windowHeight
                  withShort:(jshort)y
                  withShort:(jshort)compHeigth
                  withShort:(jshort)popupHeight;

- (void)doShowWithPIGB_Position:(PIGB_Position *)position
                      withShort:(jshort)popupWidth
                      withShort:(jshort)popupHeight
             withPIGB_Component:(id<PIGB_Component>)oldOwner
             withPIGB_Component:(id<PIGB_Component>)owner
                 withPIGB_Popup:(PIGB_Popup *)oldPopup;

- (void)setChildPopupWithPIGB_Popup:(PIGB_Popup *)childPopup;

@end

J2OBJC_FIELD_SETTER(PIGB_Popup, owner_, id<PIGB_Component>)
J2OBJC_FIELD_SETTER(PIGB_Popup, dialog_, id<PIGB_Dialog>)
J2OBJC_FIELD_SETTER(PIGB_Popup, parentPopup_, PIGB_Popup *)
J2OBJC_FIELD_SETTER(PIGB_Popup, childPopup_, PIGB_Popup *)

__attribute__((unused)) static jshort PIGB_Popup_calcPosXWithShort_withShort_withShort_withShort_(PIGB_Popup *self, jshort windowWidth, jshort x, jshort compWidth, jshort popupWidth);

__attribute__((unused)) static jshort PIGB_Popup_calcPosYWithShort_withShort_withShort_withShort_(PIGB_Popup *self, jshort windowHeight, jshort y, jshort compHeigth, jshort popupHeight);

__attribute__((unused)) static void PIGB_Popup_doShowWithPIGB_Position_withShort_withShort_withPIGB_Component_withPIGB_Component_withPIGB_Popup_(PIGB_Popup *self, PIGB_Position *position, jshort popupWidth, jshort popupHeight, id<PIGB_Component> oldOwner, id<PIGB_Component> owner, PIGB_Popup *oldPopup);

__attribute__((unused)) static void PIGB_Popup_setChildPopupWithPIGB_Popup_(PIGB_Popup *self, PIGB_Popup *childPopup);

@implementation PIGB_Popup

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGB_Popup_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithBoolean:(jboolean)scrollHor
                    withBoolean:(jboolean)scrollVert {
  PIGB_Popup_initWithBoolean_withBoolean_(self, scrollHor, scrollVert);
  return self;
}

- (void)setVisibleWithBoolean:(jboolean)visible {
  [((PIGB_ComponentState *) nil_chk([self getComponentState])) setVisibleWithBoolean:visible];
  id<PIGB_Dialog> dlg = [self getDialog];
  if (dlg != nil) [dlg forceRepaint];
}

- (jboolean)isVisible {
  id<PIGB_Dialog> dlg = [self getDialog];
  if (dlg != nil) {
    return [((PIGB_ComponentState *) nil_chk([self getComponentState])) isVisible];
  }
  else {
    return false;
  }
}

- (void)showPopupWithPIGB_Component:(id<PIGB_Component>)owner
                  withPIGB_Position:(PIGB_Position *)position
                          withShort:(jshort)width
                          withShort:(jshort)height {
  jshort windowWidth;
  jshort windowHeight;
  jshort popupWidth;
  jshort popupHeight;
  jshort x;
  jshort y;
  jshort compHeigth = [((PIGB_SizeLimit *) nil_chk([((id<PIGB_Component>) nil_chk(owner)) getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getCurrent];
  jshort compWidth = [((PIGB_SizeLimit *) nil_chk([owner getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) getCurrent];
  PIGB_Position *newPos;
  id<PIGB_Component> oldOwner = self->owner_;
  PIGB_Popup *oldPopup = [((PIGB_PaintManager *) nil_chk(PIGB_PaintManager_getInstance())) getActivePopup];
  self->owner_ = owner;
  self->dialog_ = [owner getDialog];
  self->parentPopup_ = PIGB_Popup_findParentPopupWithPIGB_Component_(owner);
  windowWidth = [((PIGB_SizeLimit *) nil_chk([((id<PIGB_Dialog>) nil_chk(dialog_)) getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) getCurrent];
  windowHeight = [((PIGB_SizeLimit *) nil_chk([((id<PIGB_Dialog>) nil_chk(dialog_)) getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getCurrent];
  if (![((PIGB_PaintState *) nil_chk([self getPaintStateWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) hasPaintStateReachedWithInt:PIGB_PaintState_STATE_SIZES_CALC]) {
    [self calcSizesWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X) withPIGB_SizeLimit:[self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)]];
  }
  if (![((PIGB_PaintState *) nil_chk([self getPaintStateWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) hasPaintStateReachedWithInt:PIGB_PaintState_STATE_SIZES_CALC]) {
    [self calcSizesWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y) withPIGB_SizeLimit:[self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)]];
  }
  popupWidth = [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) getPref];
  popupHeight = [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getPref];
  if (position == nil && width == 0 && height == 0) {
    if (popupWidth < compWidth) {
      popupWidth = compWidth;
    }
    if (popupHeight > windowHeight) {
      popupWidth += [((PIGB_Scroller *) nil_chk([self getScrollerWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getMinScrollerWidth];
    }
    x = PIGB_Popup_calcPosXWithShort_withShort_withShort_withShort_(self, windowWidth, PIGB_AbstractContainer_calcPosInWindowWithPIGL_Layoutable_withPIGB_Direction_(owner, JreLoadStatic(PIGB_Direction, X)), compWidth, popupWidth);
    y = PIGB_Popup_calcPosYWithShort_withShort_withShort_withShort_(self, windowHeight, PIGB_AbstractContainer_calcPosInWindowWithPIGL_Layoutable_withPIGB_Direction_(owner, JreLoadStatic(PIGB_Direction, Y)), compHeigth, popupHeight);
  }
  else {
    x = [((PIGB_Position *) nil_chk(position)) getX];
    y = [position getY];
    if (width > 0) {
      popupWidth = width;
    }
    else {
      popupWidth = [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) getPref];
    }
    if (height > 0) {
      popupHeight = height;
    }
    else {
      popupHeight = [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getPref];
    }
  }
  if (popupHeight > windowHeight) {
    popupHeight = windowHeight;
  }
  if (popupWidth > windowWidth) {
    popupWidth = windowWidth;
  }
  newPos = new_PIGB_Position_initWithShort_withShort_(x, y);
  PIGB_Popup_doShowWithPIGB_Position_withShort_withShort_withPIGB_Component_withPIGB_Component_withPIGB_Popup_(self, newPos, popupWidth, popupHeight, oldOwner, owner, oldPopup);
}

+ (PIGB_Popup *)findParentPopupWithPIGB_Component:(id<PIGB_Component>)comp {
  return PIGB_Popup_findParentPopupWithPIGB_Component_(comp);
}

- (jshort)calcPosXWithShort:(jshort)windowWidth
                  withShort:(jshort)x
                  withShort:(jshort)compWidth
                  withShort:(jshort)popupWidth {
  return PIGB_Popup_calcPosXWithShort_withShort_withShort_withShort_(self, windowWidth, x, compWidth, popupWidth);
}

- (jshort)calcPosYWithShort:(jshort)windowHeight
                  withShort:(jshort)y
                  withShort:(jshort)compHeigth
                  withShort:(jshort)popupHeight {
  return PIGB_Popup_calcPosYWithShort_withShort_withShort_withShort_(self, windowHeight, y, compHeigth, popupHeight);
}

- (void)doShowWithPIGB_Position:(PIGB_Position *)position
                      withShort:(jshort)popupWidth
                      withShort:(jshort)popupHeight
             withPIGB_Component:(id<PIGB_Component>)oldOwner
             withPIGB_Component:(id<PIGB_Component>)owner
                 withPIGB_Popup:(PIGB_Popup *)oldPopup {
  PIGB_Popup_doShowWithPIGB_Position_withShort_withShort_withPIGB_Component_withPIGB_Component_withPIGB_Popup_(self, position, popupWidth, popupHeight, oldOwner, owner, oldPopup);
}

- (PIGB_Popup *)getParentPopup {
  return parentPopup_;
}

- (void)setChildPopupWithPIGB_Popup:(PIGB_Popup *)childPopup {
  PIGB_Popup_setChildPopupWithPIGB_Popup_(self, childPopup);
}

- (PIGB_Popup *)getChildPopup {
  return childPopup_;
}

- (void)closePopup {
  id<PIGB_Dialog> dialog = [self getDialog];
  if (childPopup_ != nil) {
    [childPopup_ closePopup];
  }
  if (parentPopup_ == nil) {
    PIGB_PaintManager *paintMgr = PIGB_PaintManager_getInstance();
    if (JreObjectEqualsEquals([((PIGB_PaintManager *) nil_chk(paintMgr)) getActivePopup], self)) {
      [((PIGB_PaintManager *) nil_chk(PIGB_PaintManager_getInstance())) setActivePopupWithPIGB_Popup:nil];
    }
  }
  else {
    PIGB_Popup_setChildPopupWithPIGB_Popup_(self->parentPopup_, nil);
  }
  [((PIGB_ComponentState *) nil_chk([self getComponentState])) setVisibleWithBoolean:false];
  [self setLayoutParentWithPIGB_Container:nil];
  if ((owner_ != nil) && [((PIGB_ComponentState *) nil_chk([self getComponentState])) hasFocus]) {
    [((id<PIGB_Dialog>) nil_chk([self getDialog])) switchFocusWithPIGB_Component:owner_];
  }
  self->owner_ = nil;
  self->parentPopup_ = nil;
  [((id<PIGB_Dialog>) nil_chk(dialog)) repaint];
  dialog = nil;
}

- (id<PIGB_Component>)getOwner {
  return owner_;
}

- (id<PIGB_Container>)getParentContainer {
  if (owner_ == nil) {
    return [self getDialog];
  }
  else {
    if ([PIGB_Container_class_() isInstance:owner_]) {
      return (id<PIGB_Container>) cast_check(owner_, PIGB_Container_class_());
    }
    else {
      return [owner_ getParentContainer];
    }
  }
}

- (id<PIGB_Container>)getLayoutParent {
  return [self getDialog];
}

- (id<PIGB_Dialog>)getDialog {
  return self->dialog_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIGB_Popup;", 0x9, 5, 6, -1, -1, -1, -1 },
    { NULL, "S", 0x2, 7, 8, -1, -1, -1, -1 },
    { NULL, "S", 0x2, 9, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 10, 11, -1, -1, -1, -1 },
    { NULL, "LPIGB_Popup;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 12, 13, -1, -1, -1, -1 },
    { NULL, "LPIGB_Popup;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGB_Component;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGB_Container;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGB_Container;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGB_Dialog;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithBoolean:withBoolean:);
  methods[2].selector = @selector(setVisibleWithBoolean:);
  methods[3].selector = @selector(isVisible);
  methods[4].selector = @selector(showPopupWithPIGB_Component:withPIGB_Position:withShort:withShort:);
  methods[5].selector = @selector(findParentPopupWithPIGB_Component:);
  methods[6].selector = @selector(calcPosXWithShort:withShort:withShort:withShort:);
  methods[7].selector = @selector(calcPosYWithShort:withShort:withShort:withShort:);
  methods[8].selector = @selector(doShowWithPIGB_Position:withShort:withShort:withPIGB_Component:withPIGB_Component:withPIGB_Popup:);
  methods[9].selector = @selector(getParentPopup);
  methods[10].selector = @selector(setChildPopupWithPIGB_Popup:);
  methods[11].selector = @selector(getChildPopup);
  methods[12].selector = @selector(closePopup);
  methods[13].selector = @selector(getOwner);
  methods[14].selector = @selector(getParentContainer);
  methods[15].selector = @selector(getLayoutParent);
  methods[16].selector = @selector(getDialog);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "MODE_FIT", "I", .constantValue.asInt = PIGB_Popup_MODE_FIT, 0x19, -1, -1, -1, -1 },
    { "MODE_STICK", "I", .constantValue.asInt = PIGB_Popup_MODE_STICK, 0x19, -1, -1, -1, -1 },
    { "MODE_CENTER", "I", .constantValue.asInt = PIGB_Popup_MODE_CENTER, 0x19, -1, -1, -1, -1 },
    { "owner_", "LPIGB_Component;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dialog_", "LPIGB_Dialog;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "parentPopup_", "LPIGB_Popup;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "childPopup_", "LPIGB_Popup;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "ZZ", "setVisible", "Z", "showPopup", "LPIGB_Component;LPIGB_Position;SS", "findParentPopup", "LPIGB_Component;", "calcPosX", "SSSS", "calcPosY", "doShow", "LPIGB_Position;SSLPIGB_Component;LPIGB_Component;LPIGB_Popup;", "setChildPopup", "LPIGB_Popup;" };
  static const J2ObjcClassInfo _PIGB_Popup = { "Popup", "de.pidata.gui.component.base", ptrTable, methods, fields, 7, 0x1, 17, 7, -1, -1, -1, -1, -1 };
  return &_PIGB_Popup;
}

@end

void PIGB_Popup_init(PIGB_Popup *self) {
  PIGB_Popup_initWithBoolean_withBoolean_(self, false, false);
}

PIGB_Popup *new_PIGB_Popup_init() {
  J2OBJC_NEW_IMPL(PIGB_Popup, init)
}

PIGB_Popup *create_PIGB_Popup_init() {
  J2OBJC_CREATE_IMPL(PIGB_Popup, init)
}

void PIGB_Popup_initWithBoolean_withBoolean_(PIGB_Popup *self, jboolean scrollHor, jboolean scrollVert) {
  PIGB_Panel_initWithPIGL_Layouter_withPIGL_Layouter_withBoolean_withBoolean_withShort_(self, new_PIGL_StackLayouter_initWithPIGB_Direction_(JreLoadStatic(PIGB_Direction, X)), new_PIGL_FlowLayouter_initWithPIGB_Direction_(JreLoadStatic(PIGB_Direction, Y)), scrollHor, scrollVert, (jshort) 0);
  [((PIGB_ComponentState *) nil_chk([self getComponentState])) setVisibleWithBoolean:false];
}

PIGB_Popup *new_PIGB_Popup_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert) {
  J2OBJC_NEW_IMPL(PIGB_Popup, initWithBoolean_withBoolean_, scrollHor, scrollVert)
}

PIGB_Popup *create_PIGB_Popup_initWithBoolean_withBoolean_(jboolean scrollHor, jboolean scrollVert) {
  J2OBJC_CREATE_IMPL(PIGB_Popup, initWithBoolean_withBoolean_, scrollHor, scrollVert)
}

PIGB_Popup *PIGB_Popup_findParentPopupWithPIGB_Component_(id<PIGB_Component> comp) {
  PIGB_Popup_initialize();
  id<PIGB_Container> container = [((id<PIGB_Component>) nil_chk(comp)) getParentContainer];
  while (container != nil) {
    if ([container isKindOfClass:[PIGB_Popup class]]) {
      return (PIGB_Popup *) container;
    }
    container = [container getParentContainer];
  }
  return nil;
}

jshort PIGB_Popup_calcPosXWithShort_withShort_withShort_withShort_(PIGB_Popup *self, jshort windowWidth, jshort x, jshort compWidth, jshort popupWidth) {
  if (compWidth < popupWidth) {
    if (x + compWidth > popupWidth) {
      x = (jshort) (x + compWidth - popupWidth);
    }
    else {
      x = 0;
    }
  }
  return x;
}

jshort PIGB_Popup_calcPosYWithShort_withShort_withShort_withShort_(PIGB_Popup *self, jshort windowHeight, jshort y, jshort compHeigth, jshort popupHeight) {
  if (windowHeight - y - compHeigth >= popupHeight) {
    y = (jshort) (y + compHeigth);
  }
  else {
    if (y > popupHeight) {
      y = (jshort) (y - popupHeight);
    }
    else {
      y = 0;
    }
  }
  return y;
}

void PIGB_Popup_doShowWithPIGB_Position_withShort_withShort_withPIGB_Component_withPIGB_Component_withPIGB_Popup_(PIGB_Popup *self, PIGB_Position *position, jshort popupWidth, jshort popupHeight, id<PIGB_Component> oldOwner, id<PIGB_Component> owner, PIGB_Popup *oldPopup) {
  [self setPosWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X) withShort:[((PIGB_Position *) nil_chk(position)) getX]];
  [self setPosWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y) withShort:[position getY]];
  [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) setCurrentFitWithShort:popupWidth];
  [((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) setCurrentFitWithShort:popupHeight];
  [self doLayoutWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)];
  [self doLayoutWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)];
  if (self->parentPopup_ == nil) {
    [((PIGB_PaintManager *) nil_chk(PIGB_PaintManager_getInstance())) setActivePopupWithPIGB_Popup:self];
  }
  else {
    PIGB_Popup_setChildPopupWithPIGB_Popup_(self->parentPopup_, self);
  }
  [((PIGB_ComponentState *) nil_chk([self getComponentState])) setVisibleWithBoolean:true];
  if (oldOwner != nil && !JreObjectEqualsEquals(oldOwner, owner)) {
    [((id<PIGB_Dialog>) nil_chk([oldOwner getDialog])) repaint];
  }
  else if (oldPopup != nil && !JreObjectEqualsEquals(oldPopup, self)) {
    [((id<PIGB_Dialog>) nil_chk([oldPopup getDialog])) repaint];
  }
  [((id<PIGB_Component>) nil_chk(owner)) repaint];
  [self repaint];
}

void PIGB_Popup_setChildPopupWithPIGB_Popup_(PIGB_Popup *self, PIGB_Popup *childPopup) {
  self->childPopup_ = childPopup;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGB_Popup)

J2OBJC_NAME_MAPPING(PIGB_Popup, "de.pidata.gui.component.base", "PIGB_")
