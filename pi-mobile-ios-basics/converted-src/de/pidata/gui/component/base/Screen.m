//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/Screen.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/Screen.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/component/base/Screen must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGB_Screen : NSObject

@end

@implementation PIGB_Screen

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPIGB_Rect;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "LPIGE_Dialog;", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getScreenBounds);
  methods[1].selector = @selector(isSingleWindow);
  methods[2].selector = @selector(setScreenSizeWithInt:withInt:);
  methods[3].selector = @selector(getFocusDialog);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "setScreenSize", "II" };
  static const J2ObjcClassInfo _PIGB_Screen = { "Screen", "de.pidata.gui.component.base", ptrTable, methods, NULL, 7, 0x609, 4, 0, -1, -1, -1, -1, -1 };
  return &_PIGB_Screen;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIGB_Screen)

J2OBJC_NAME_MAPPING(PIGB_Screen, "de.pidata.gui.component.base", "PIGB_")
