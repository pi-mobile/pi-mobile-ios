//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/base/SystemThreadAction.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/SystemThreadAction.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/component/base/SystemThreadAction must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGB_SystemThreadAction : NSObject

@end

@implementation PIGB_SystemThreadAction

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(doSystemThreadAction);
  #pragma clang diagnostic pop
  static const J2ObjcClassInfo _PIGB_SystemThreadAction = { "SystemThreadAction", "de.pidata.gui.component.base", NULL, methods, NULL, 7, 0x609, 1, 0, -1, -1, -1, -1, -1 };
  return &_PIGB_SystemThreadAction;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIGB_SystemThreadAction)

J2OBJC_NAME_MAPPING(PIGB_SystemThreadAction, "de.pidata.gui.component.base", "PIGB_")
