//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/general/AbstractInputManager.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiComponentGeneralAbstractInputManager")
#ifdef RESTRICT_DePidataGuiComponentGeneralAbstractInputManager
#define INCLUDE_ALL_DePidataGuiComponentGeneralAbstractInputManager 0
#else
#define INCLUDE_ALL_DePidataGuiComponentGeneralAbstractInputManager 1
#endif
#undef RESTRICT_DePidataGuiComponentGeneralAbstractInputManager

#if !defined (PIGN_AbstractInputManager_) && (INCLUDE_ALL_DePidataGuiComponentGeneralAbstractInputManager || defined(INCLUDE_PIGN_AbstractInputManager))
#define PIGN_AbstractInputManager_

#define RESTRICT_DePidataGuiEventInputManager 1
#define INCLUDE_PIGE_InputManager 1
#include "de/pidata/gui/event/InputManager.h"

@class PIQ_QName;
@protocol PIGB_Screen;

@interface PIGN_AbstractInputManager : NSObject < PIGE_InputManager >

#pragma mark Public

- (id<PIGB_Screen>)getScreen;

- (jboolean)isLetterWithChar:(jchar)keyChar;

#pragma mark Protected

- (instancetype)initWithPIGB_Screen:(id<PIGB_Screen>)screen;

- (jchar)getKeyCharWithInt:(jint)keyCode;

- (void)sendEventWithPIQ_QName:(PIQ_QName *)cmd
                      withChar:(jchar)keyChar
                       withInt:(jint)index;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGN_AbstractInputManager)

FOUNDATION_EXPORT void PIGN_AbstractInputManager_initWithPIGB_Screen_(PIGN_AbstractInputManager *self, id<PIGB_Screen> screen);

J2OBJC_TYPE_LITERAL_HEADER(PIGN_AbstractInputManager)

@compatibility_alias DePidataGuiComponentGeneralAbstractInputManager PIGN_AbstractInputManager;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiComponentGeneralAbstractInputManager")
