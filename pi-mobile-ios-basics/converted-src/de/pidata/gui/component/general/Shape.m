//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/component/general/Shape.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/component/base/ComponentFactory.h"
#include "de/pidata/gui/component/base/ComponentFont.h"
#include "de/pidata/gui/component/base/ComponentGraphics.h"
#include "de/pidata/gui/component/base/Container.h"
#include "de/pidata/gui/component/base/Direction.h"
#include "de/pidata/gui/component/base/PaintState.h"
#include "de/pidata/gui/component/base/SizeLimit.h"
#include "de/pidata/gui/component/general/Shape.h"
#include "de/pidata/gui/layout/LayoutInfo.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Double.h"
#include "java/lang/Exception.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/Short.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/component/general/Shape must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGN_Shape () {
 @public
  IOSObjectArray *sizeLimit_;
  IOSShortArray *pos_;
  IOSObjectArray *layoutInfo_;
}

- (void)init__ OBJC_METHOD_FAMILY_NONE;

@end

J2OBJC_FIELD_SETTER(PIGN_Shape, sizeLimit_, IOSObjectArray *)
J2OBJC_FIELD_SETTER(PIGN_Shape, pos_, IOSShortArray *)
J2OBJC_FIELD_SETTER(PIGN_Shape, layoutInfo_, IOSObjectArray *)

inline jint PIGN_Shape_get_SIZESHAPE_THICKNESS(void);
#define PIGN_Shape_SIZESHAPE_THICKNESS 10
J2OBJC_STATIC_FIELD_CONSTANT(PIGN_Shape, SIZESHAPE_THICKNESS, jint)

__attribute__((unused)) static void PIGN_Shape_init__(PIGN_Shape *self);

J2OBJC_INITIALIZED_DEFN(PIGN_Shape)

PIQ_QName *PIGN_Shape_SHAPE_RECT;
PIQ_QName *PIGN_Shape_SHAPE_LINE;
PIQ_QName *PIGN_Shape_SHAPE_ELLIPSE;
PIQ_QName *PIGN_Shape_SHAPE_VERT_SIZE;
PIQ_QName *PIGN_Shape_SHAPE_HOR_SIZE;
PIQ_QName *PIGN_Shape_ID_X;
PIQ_QName *PIGN_Shape_ID_Y;
PIQ_QName *PIGN_Shape_ID_WIDTH;
PIQ_QName *PIGN_Shape_ID_HEIGHT;
PIQ_QName *PIGN_Shape_ID_VISIBLE;

@implementation PIGN_Shape

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGN_Shape_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)init__WithPIQ_QName:(PIQ_QName *)shapeID
              withPIQ_QName:(PIQ_QName *)typeID
    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)color
    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)selectedColor
                  withShort:(jshort)x
                  withShort:(jshort)y
                  withShort:(jshort)width
                  withShort:(jshort)height {
  self->shapeKey_ = shapeID;
  self->typeID_ = typeID;
  self->color_ = color;
  if (selectedColor != nil) self->selectedColor_ = selectedColor;
  else self->selectedColor_ = color;
  self->x_ = x;
  self->y_ = y;
  self->width_ = width;
  self->height_ = height;
  PIGN_Shape_init__(self);
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)shapeKey
        withPIGB_ComponentColor:(id<PIGB_ComponentColor>)color
        withPIGB_ComponentColor:(id<PIGB_ComponentColor>)selectedColor
                   withNSString:(NSString *)coords {
  PIGN_Shape_initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_(self, shapeKey, color, selectedColor, coords);
  return self;
}

- (void)init__ {
  PIGN_Shape_init__(self);
}

- (void)setScaleWithFloat:(jfloat)scale_ {
  self->scale__ = scale_;
  [self layoutChanged];
}

- (jshort)getX {
  return x_;
}

- (jshort)getY {
  return y_;
}

- (jshort)getWidth {
  return width_;
}

- (jshort)getHeight {
  return height_;
}

- (void)calcSizes {
  jshort x;
  jshort y;
  jshort w;
  jshort h;
  jshort wMax;
  jshort hMax;
  if (scale__ == 1.0f) {
    x = self->x_;
    y = self->y_;
    w = self->width_;
    h = self->height_;
  }
  else {
    x = (jshort) JreFpToInt(((jfloat) self->x_ * scale__));
    y = (jshort) JreFpToInt(((jfloat) self->y_ * scale__));
    w = (jshort) JreFpToInt(((jfloat) self->width_ * scale__));
    h = (jshort) JreFpToInt(((jfloat) self->height_ * scale__));
  }
  if (w > 0) {
    w = (jshort) (x + w);
    wMax = w;
  }
  else {
    if (JreObjectEqualsEquals(typeID_, PIGN_Shape_SHAPE_VERT_SIZE)) {
      jshort labelWidth = 0;
      if (layoutParent_ != nil) labelWidth = (jshort) [((id<PIGB_ComponentFont>) nil_chk([((id<PIGB_Container>) nil_chk([self getLayoutParent])) getFont])) getStringWidthWithPIGB_Component:[self getLayoutParent] withNSString:JreStrcat("S", self->height_)];
      if (labelWidth > PIGN_Shape_SIZESHAPE_THICKNESS) w = labelWidth;
      else w = PIGN_Shape_SIZESHAPE_THICKNESS;
    }
    else w = (jshort) (x - w);
    wMax = JavaLangShort_MAX_VALUE;
  }
  if (h > 0) {
    h = (jshort) (y + h);
    hMax = h;
  }
  else {
    if (JreObjectEqualsEquals(typeID_, PIGN_Shape_SHAPE_HOR_SIZE)) {
      jshort labelheight = 0;
      if (layoutParent_ != nil) labelheight = (jshort) ([((id<PIGB_ComponentFont>) nil_chk([((id<PIGB_Container>) nil_chk([self getLayoutParent])) getFont])) getHeightWithPIGB_Component:[self getLayoutParent]] + 2);
      if (labelheight > PIGN_Shape_SIZESHAPE_THICKNESS) h = labelheight;
      else h = PIGN_Shape_SIZESHAPE_THICKNESS;
    }
    else h = (jshort) (y - h);
    hMax = JavaLangShort_MAX_VALUE;
  }
  (void) IOSObjectArray_SetAndConsume(nil_chk(sizeLimit_), [((PIGB_Direction *) nil_chk(JreLoadStatic(PIGB_Direction, X))) getIndex], new_PIGB_SizeLimit_initWithShort_withShort_withShort_(w, w, wMax));
  (void) IOSObjectArray_SetAndConsume(nil_chk(sizeLimit_), [((PIGB_Direction *) nil_chk(JreLoadStatic(PIGB_Direction, Y))) getIndex], new_PIGB_SizeLimit_initWithShort_withShort_withShort_(h, h, hMax));
  if (layoutParent_ != nil) {
    [((PIGB_PaintState *) nil_chk([layoutParent_ getPaintStateWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) undoPaintStateWithInt:PIGB_PaintState_STATE_SIZES_CALC];
    [((PIGB_PaintState *) nil_chk([((id<PIGB_Container>) nil_chk(layoutParent_)) getPaintStateWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) undoPaintStateWithInt:PIGB_PaintState_STATE_SIZES_CALC];
  }
}

- (id<PIQ_Key>)getKey {
  return self->shapeKey_;
}

- (PIQ_QName *)getTypeID {
  return typeID_;
}

- (id<PIGB_ComponentColor>)getColor {
  return color_;
}

- (jboolean)isSelected {
  return selected_;
}

- (void)setSelectedWithBoolean:(jboolean)selected {
  self->selected_ = selected;
}

- (void)doPaintWithPIGB_ComponentGraphics:(id<PIGB_ComponentGraphics>)graph {
  jshort w;
  jshort h;
  jshort x;
  jshort y;
  NSString *label;
  if (selected_) {
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) setColorWithPIGB_ComponentColor:selectedColor_];
  }
  else {
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) setColorWithPIGB_ComponentColor:color_];
  }
  if (scale__ == 1.0f) {
    x = self->x_;
    y = self->y_;
    w = self->width_;
    h = self->height_;
  }
  else {
    x = (jshort) JreFpToInt(((jfloat) self->x_ * scale__));
    y = (jshort) JreFpToInt(((jfloat) self->y_ * scale__));
    w = (jshort) JreFpToInt(((jfloat) self->width_ * scale__));
    h = (jshort) JreFpToInt(((jfloat) self->height_ * scale__));
  }
  if (w <= 0) {
    w = (jshort) ([((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, X)])) getCurrent] - x + w);
  }
  if (h <= 0) {
    h = (jshort) ([((PIGB_SizeLimit *) nil_chk([self getSizeLimitWithPIGB_Direction:JreLoadStatic(PIGB_Direction, Y)])) getCurrent] - y + h);
  }
  [self paintShapeWithPIGB_ComponentGraphics:graph withPIQ_QName:self->typeID_ withShort:x withShort:y withShort:w withShort:h];
}

- (void)paintShapeWithPIGB_ComponentGraphics:(id<PIGB_ComponentGraphics>)graph
                               withPIQ_QName:(PIQ_QName *)typeID
                                   withShort:(jshort)x
                                   withShort:(jshort)y
                                   withShort:(jshort)w
                                   withShort:(jshort)h {
  NSString *label;
  if (JreObjectEqualsEquals(typeID, PIGN_Shape_SHAPE_RECT)) {
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) paintRectWithShort:x withShort:y withShort:w withShort:h];
  }
  else if (JreObjectEqualsEquals(typeID, PIGN_Shape_SHAPE_LINE)) {
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) paintLineWithShort:x withShort:y withShort:w withShort:h];
  }
  else if (JreObjectEqualsEquals(typeID, PIGN_Shape_SHAPE_ELLIPSE)) {
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) paintEllipseWithShort:x withShort:y withShort:w withShort:h];
  }
  else if (JreObjectEqualsEquals(typeID, PIGN_Shape_SHAPE_VERT_SIZE)) {
    id<PIGB_ComponentFont> font = [((id<PIGB_Container>) nil_chk([self getLayoutParent])) getFont];
    if (self->height_ > 0) label = JreStrcat("S", self->height_);
    else label = JreStrcat("S", (jshort) JreFpToInt(((jfloat) h / self->scale__)));
    jshort xm = (jshort) (x - 1 + JreIntDiv(w, 2));
    jshort x1 = (jshort) (xm - JreIntDiv(PIGN_Shape_SIZESHAPE_THICKNESS, 2));
    jshort x2 = (jshort) (xm + JreIntDiv(PIGN_Shape_SIZESHAPE_THICKNESS, 2));
    jshort y2 = (jshort) (y - 1 + h);
    jshort ht = (jshort) [((id<PIGB_ComponentFont>) nil_chk(font)) getHeightWithPIGB_Component:[self getLayoutParent]];
    jshort yl = (jshort) (y - 1 + JreIntDiv((h - ht), 2));
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) paintLineWithShort:x1 withShort:y withShort:x2 withShort:y];
    [graph paintLineWithShort:x1 withShort:y2 withShort:x2 withShort:y2];
    [graph paintLineWithShort:xm withShort:y withShort:xm withShort:yl];
    [graph paintStringWithNSString:label withInt:x withInt:yl withPIGB_ComponentColor:color_ withPIGB_ComponentFont:font];
    [graph paintLineWithShort:xm withShort:(jshort) (yl + ht) withShort:xm withShort:y2];
  }
  else if (JreObjectEqualsEquals(typeID, PIGN_Shape_SHAPE_HOR_SIZE)) {
    id<PIGB_ComponentFont> font = [((id<PIGB_Container>) nil_chk([self getLayoutParent])) getFont];
    if (self->width_ > 0) label = JreStrcat("S", self->width_);
    else label = JreStrcat("S", (jshort) JreFpToInt(((jfloat) w / self->scale__)));
    jshort ym = (jshort) (y - 1 + JreIntDiv([((id<PIGB_ComponentFont>) nil_chk(font)) getHeightWithPIGB_Component:[self getLayoutParent]], 2));
    jshort y1 = (jshort) (y + 1);
    jshort y2 = (jshort) (y - 1 + PIGN_Shape_SIZESHAPE_THICKNESS);
    jshort x2 = (jshort) (x - 1 + w);
    jshort wt = (jshort) [font getStringWidthWithPIGB_Component:[self getLayoutParent] withNSString:label];
    jshort xl = (jshort) (x - 1 + JreIntDiv((w - wt), 2));
    jshort yl = (jshort) (y - 1 + JreIntDiv((h - [font getHeightWithPIGB_Component:[self getLayoutParent]]), 2));
    [((id<PIGB_ComponentGraphics>) nil_chk(graph)) paintLineWithShort:x withShort:y1 withShort:x withShort:y2];
    [graph paintLineWithShort:x2 withShort:y1 withShort:x2 withShort:y2];
    [graph paintLineWithShort:x withShort:ym withShort:xl withShort:ym];
    [graph paintStringWithNSString:label withInt:xl withInt:yl withPIGB_ComponentColor:color_ withPIGB_ComponentFont:font];
    [graph paintLineWithShort:(jshort) (xl + wt) withShort:ym withShort:x2 withShort:ym];
  }
  else {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@", @"Unsupported type ID=", self->typeID_));
  }
}

- (jboolean)containsPosWithShort:(jshort)posX
                       withShort:(jshort)posY {
  jshort w;
  jshort h;
  jshort x;
  jshort y;
  if (scale__ == 1.0f) {
    x = self->x_;
    y = self->y_;
    w = self->width_;
    h = self->height_;
  }
  else {
    x = (jshort) JreFpToInt(((jfloat) self->x_ * scale__));
    y = (jshort) JreFpToInt(((jfloat) self->y_ * scale__));
    w = (jshort) JreFpToInt(((jfloat) self->width_ * scale__));
    h = (jshort) JreFpToInt(((jfloat) self->height_ * scale__));
  }
  return ((posX >= x) && (posX < (x + w)) && (posY >= y) && (posY < y + h));
}

- (PIGB_SizeLimit *)getSizeLimitWithPIGB_Direction:(PIGB_Direction *)direction {
  return IOSObjectArray_Get(nil_chk(sizeLimit_), [((PIGB_Direction *) nil_chk(direction)) getIndex]);
}

- (PIGL_LayoutInfo *)getLayoutInfoWithPIGB_Direction:(PIGB_Direction *)direction {
  return IOSObjectArray_Get(nil_chk(layoutInfo_), [((PIGB_Direction *) nil_chk(direction)) getIndex]);
}

- (jshort)getPosWithPIGB_Direction:(PIGB_Direction *)direction {
  return IOSShortArray_Get(nil_chk(pos_), [((PIGB_Direction *) nil_chk(direction)) getIndex]);
}

- (void)setPosWithPIGB_Direction:(PIGB_Direction *)direction
                       withShort:(jshort)position {
  *IOSShortArray_GetRef(nil_chk(pos_), [((PIGB_Direction *) nil_chk(direction)) getIndex]) = position;
}

- (id<PIGB_Container>)getLayoutParent {
  return self->layoutParent_;
}

- (void)setLayoutParentWithPIGB_Container:(id<PIGB_Container>)parent {
  self->layoutParent_ = parent;
}

- (jboolean)isVisible {
  id<PIGB_Container> parent = [self getLayoutParent];
  if (parent != nil) {
    return (visible_ && [parent isVisible]);
  }
  return false;
}

- (void)layoutChanged {
  [self calcSizes];
  id<PIGB_Container> parent = [self getLayoutParent];
  if (parent != nil) {
    [parent contentChanged];
  }
}

- (void)setParamValueWithPIQ_QName:(PIQ_QName *)id_
                      withNSString:(NSString *)source
                withJavaLangDouble:(JavaLangDouble *)factor
                            withId:(id)compareTo {
  if (parameters_ == nil) {
    parameters_ = new_JavaUtilHashtable_init();
    factors_ = new_JavaUtilHashtable_init();
  }
  (void) [((JavaUtilHashtable *) nil_chk(parameters_)) putWithId:source withId:id_];
  if (factor != nil) (void) [((JavaUtilHashtable *) nil_chk(factors_)) putWithId:source withId:factor];
  else (void) [((JavaUtilHashtable *) nil_chk(factors_)) putWithId:source withId:compareTo];
}

- (void)parameterUpdatedWithNSString:(NSString *)source
                              withId:(id)value {
  if (parameters_ != nil) {
    PIQ_QName *id_ = (PIQ_QName *) cast_chk([parameters_ getWithId:source], [PIQ_QName class]);
    if ((id_ != nil) && (value != nil) && ([((NSString *) nil_chk(((NSString *) cast_chk(value, [NSString class])))) java_length] > 0)) {
      if (JreObjectEqualsEquals(id_, PIGN_Shape_ID_X)) {
        JavaLangDouble *factor = (JavaLangDouble *) cast_chk([((JavaUtilHashtable *) nil_chk(factors_)) getWithId:source], [JavaLangDouble class]);
        x_ = (jshort) JreFpToInt(((jdouble) (JavaLangShort_parseShortWithNSString_((NSString *) cast_chk(value, [NSString class])) * [((JavaLangDouble *) nil_chk(factor)) doubleValue])));
        [self layoutChanged];
      }
      else if (JreObjectEqualsEquals(id_, PIGN_Shape_ID_Y)) {
        JavaLangDouble *factor = (JavaLangDouble *) cast_chk([((JavaUtilHashtable *) nil_chk(factors_)) getWithId:source], [JavaLangDouble class]);
        y_ = (jshort) JreFpToInt(((jdouble) (JavaLangShort_parseShortWithNSString_((NSString *) cast_chk(value, [NSString class])) * [((JavaLangDouble *) nil_chk(factor)) doubleValue])));
        [self layoutChanged];
      }
      else if (JreObjectEqualsEquals(id_, PIGN_Shape_ID_WIDTH)) {
        JavaLangDouble *factor = (JavaLangDouble *) cast_chk([((JavaUtilHashtable *) nil_chk(factors_)) getWithId:source], [JavaLangDouble class]);
        width_ = (jshort) JreFpToInt(((jdouble) (JavaLangShort_parseShortWithNSString_((NSString *) cast_chk(value, [NSString class])) * [((JavaLangDouble *) nil_chk(factor)) doubleValue])));
        [self layoutChanged];
      }
      else if (JreObjectEqualsEquals(id_, PIGN_Shape_ID_HEIGHT)) {
        JavaLangDouble *factor = (JavaLangDouble *) cast_chk([((JavaUtilHashtable *) nil_chk(factors_)) getWithId:source], [JavaLangDouble class]);
        height_ = (jshort) JreFpToInt(((jdouble) (JavaLangShort_parseShortWithNSString_((NSString *) cast_chk(value, [NSString class])) * [((JavaLangDouble *) nil_chk(factor)) doubleValue])));
        [self layoutChanged];
      }
      else if (JreObjectEqualsEquals(id_, PIGN_Shape_ID_VISIBLE)) {
        id compareTo = [((JavaUtilHashtable *) nil_chk(factors_)) getWithId:source];
        if (compareTo == nil) {
          visible_ = true;
        }
        else {
          visible_ = [compareTo isEqual:value];
        }
        [self layoutChanged];
      }
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 0, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "S", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "S", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "S", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "S", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_Key;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGB_ComponentColor;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x11, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 9, 10, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "LPIGB_SizeLimit;", 0x1, 13, 14, -1, -1, -1, -1 },
    { NULL, "LPIGL_LayoutInfo;", 0x1, 15, 14, -1, -1, -1, -1 },
    { NULL, "S", 0x1, 16, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 17, 18, -1, -1, -1, -1 },
    { NULL, "LPIGB_Container;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 19, 20, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 21, 22, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 23, 24, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(init__WithPIQ_QName:withPIQ_QName:withPIGB_ComponentColor:withPIGB_ComponentColor:withShort:withShort:withShort:withShort:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIGB_ComponentColor:withPIGB_ComponentColor:withNSString:);
  methods[3].selector = @selector(init__);
  methods[4].selector = @selector(setScaleWithFloat:);
  methods[5].selector = @selector(getX);
  methods[6].selector = @selector(getY);
  methods[7].selector = @selector(getWidth);
  methods[8].selector = @selector(getHeight);
  methods[9].selector = @selector(calcSizes);
  methods[10].selector = @selector(getKey);
  methods[11].selector = @selector(getTypeID);
  methods[12].selector = @selector(getColor);
  methods[13].selector = @selector(isSelected);
  methods[14].selector = @selector(setSelectedWithBoolean:);
  methods[15].selector = @selector(doPaintWithPIGB_ComponentGraphics:);
  methods[16].selector = @selector(paintShapeWithPIGB_ComponentGraphics:withPIQ_QName:withShort:withShort:withShort:withShort:);
  methods[17].selector = @selector(containsPosWithShort:withShort:);
  methods[18].selector = @selector(getSizeLimitWithPIGB_Direction:);
  methods[19].selector = @selector(getLayoutInfoWithPIGB_Direction:);
  methods[20].selector = @selector(getPosWithPIGB_Direction:);
  methods[21].selector = @selector(setPosWithPIGB_Direction:withShort:);
  methods[22].selector = @selector(getLayoutParent);
  methods[23].selector = @selector(setLayoutParentWithPIGB_Container:);
  methods[24].selector = @selector(isVisible);
  methods[25].selector = @selector(layoutChanged);
  methods[26].selector = @selector(setParamValueWithPIQ_QName:withNSString:withJavaLangDouble:withId:);
  methods[27].selector = @selector(parameterUpdatedWithNSString:withId:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "SHAPE_RECT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "SHAPE_LINE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "SHAPE_ELLIPSE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 27, -1, -1 },
    { "SHAPE_VERT_SIZE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 28, -1, -1 },
    { "SHAPE_HOR_SIZE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 29, -1, -1 },
    { "ID_X", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 30, -1, -1 },
    { "ID_Y", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 31, -1, -1 },
    { "ID_WIDTH", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 32, -1, -1 },
    { "ID_HEIGHT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 33, -1, -1 },
    { "ID_VISIBLE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "shapeKey_", "LPIQ_Key;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "typeID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "color_", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "selectedColor_", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "x_", "S", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "y_", "S", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "width_", "S", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "height_", "S", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "scale__", "F", .constantValue.asLong = 0, 0x4, 35, -1, -1, -1 },
    { "selected_", "Z", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "visible_", "Z", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "layoutParent_", "LPIGB_Container;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "parameters_", "LJavaUtilHashtable;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "factors_", "LJavaUtilHashtable;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "sizeLimit_", "[LPIGB_SizeLimit;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "pos_", "[S", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "layoutInfo_", "[LPIGL_LayoutInfo;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "SIZESHAPE_THICKNESS", "I", .constantValue.asInt = PIGN_Shape_SIZESHAPE_THICKNESS, 0x1a, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "init", "LPIQ_QName;LPIQ_QName;LPIGB_ComponentColor;LPIGB_ComponentColor;SSSS", "LPIQ_Key;LPIGB_ComponentColor;LPIGB_ComponentColor;LNSString;", "setScale", "F", "setSelected", "Z", "doPaint", "LPIGB_ComponentGraphics;", "paintShape", "LPIGB_ComponentGraphics;LPIQ_QName;SSSS", "containsPos", "SS", "getSizeLimit", "LPIGB_Direction;", "getLayoutInfo", "getPos", "setPos", "LPIGB_Direction;S", "setLayoutParent", "LPIGB_Container;", "setParamValue", "LPIQ_QName;LNSString;LJavaLangDouble;LNSObject;", "parameterUpdated", "LNSString;LNSObject;", &PIGN_Shape_SHAPE_RECT, &PIGN_Shape_SHAPE_LINE, &PIGN_Shape_SHAPE_ELLIPSE, &PIGN_Shape_SHAPE_VERT_SIZE, &PIGN_Shape_SHAPE_HOR_SIZE, &PIGN_Shape_ID_X, &PIGN_Shape_ID_Y, &PIGN_Shape_ID_WIDTH, &PIGN_Shape_ID_HEIGHT, &PIGN_Shape_ID_VISIBLE, "scale" };
  static const J2ObjcClassInfo _PIGN_Shape = { "Shape", "de.pidata.gui.component.general", ptrTable, methods, fields, 7, 0x1, 28, 28, -1, -1, -1, -1, -1 };
  return &_PIGN_Shape;
}

+ (void)initialize {
  if (self == [PIGN_Shape class]) {
    PIGN_Shape_SHAPE_RECT = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_ComponentFactory, NAMESPACE))) getQNameWithNSString:@"rect"];
    PIGN_Shape_SHAPE_LINE = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"line"];
    PIGN_Shape_SHAPE_ELLIPSE = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"ellipse"];
    PIGN_Shape_SHAPE_VERT_SIZE = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"vertSize"];
    PIGN_Shape_SHAPE_HOR_SIZE = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"horSize"];
    PIGN_Shape_ID_X = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"x"];
    PIGN_Shape_ID_Y = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"y"];
    PIGN_Shape_ID_WIDTH = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"width"];
    PIGN_Shape_ID_HEIGHT = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"height"];
    PIGN_Shape_ID_VISIBLE = [JreLoadStatic(PIGB_ComponentFactory, NAMESPACE) getQNameWithNSString:@"visible"];
    J2OBJC_SET_INITIALIZED(PIGN_Shape)
  }
}

@end

void PIGN_Shape_init(PIGN_Shape *self) {
  NSObject_init(self);
  self->scale__ = 1.0f;
  self->selected_ = false;
  self->visible_ = true;
  self->sizeLimit_ = [IOSObjectArray newArrayWithLength:2 type:PIGB_SizeLimit_class_()];
  self->pos_ = [IOSShortArray newArrayWithLength:2];
  self->layoutInfo_ = [IOSObjectArray newArrayWithLength:2 type:PIGL_LayoutInfo_class_()];
}

PIGN_Shape *new_PIGN_Shape_init() {
  J2OBJC_NEW_IMPL(PIGN_Shape, init)
}

PIGN_Shape *create_PIGN_Shape_init() {
  J2OBJC_CREATE_IMPL(PIGN_Shape, init)
}

void PIGN_Shape_initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_(PIGN_Shape *self, id<PIQ_Key> shapeKey, id<PIGB_ComponentColor> color, id<PIGB_ComponentColor> selectedColor, NSString *coords) {
  NSObject_init(self);
  self->scale__ = 1.0f;
  self->selected_ = false;
  self->visible_ = true;
  self->sizeLimit_ = [IOSObjectArray newArrayWithLength:2 type:PIGB_SizeLimit_class_()];
  self->pos_ = [IOSShortArray newArrayWithLength:2];
  self->layoutInfo_ = [IOSObjectArray newArrayWithLength:2 type:PIGL_LayoutInfo_class_()];
  jint pos;
  jint start;
  self->shapeKey_ = shapeKey;
  self->color_ = color;
  if (selectedColor != nil) self->selectedColor_ = selectedColor;
  else self->selectedColor_ = color;
  @try {
    pos = [((NSString *) nil_chk(coords)) java_indexOf:' '];
    self->typeID_ = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_ComponentFactory, NAMESPACE))) getQNameWithNSString:[coords java_substring:0 endIndex:pos]];
    start = pos + 1;
    pos = [coords java_indexOf:','];
    self->x_ = JavaLangShort_parseShortWithNSString_([coords java_substring:start endIndex:pos]);
    start = pos + 1;
    pos = [coords java_indexOf:',' fromIndex:start];
    self->y_ = JavaLangShort_parseShortWithNSString_([coords java_substring:start endIndex:pos]);
    start = pos + 1;
    pos = [coords java_indexOf:',' fromIndex:start];
    self->width_ = (jshort) (JavaLangShort_parseShortWithNSString_([coords java_substring:start endIndex:pos]) - self->x_);
    self->height_ = (jshort) (JavaLangShort_parseShortWithNSString_([coords java_substring:pos + 1]) - self->y_);
  }
  @catch (JavaLangException *ex) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$$$@", @"Invalid coordinates: '", coords, @"', shapeKey=", shapeKey), ex);
  }
  PIGN_Shape_init__(self);
}

PIGN_Shape *new_PIGN_Shape_initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_(id<PIQ_Key> shapeKey, id<PIGB_ComponentColor> color, id<PIGB_ComponentColor> selectedColor, NSString *coords) {
  J2OBJC_NEW_IMPL(PIGN_Shape, initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_, shapeKey, color, selectedColor, coords)
}

PIGN_Shape *create_PIGN_Shape_initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_(id<PIQ_Key> shapeKey, id<PIGB_ComponentColor> color, id<PIGB_ComponentColor> selectedColor, NSString *coords) {
  J2OBJC_CREATE_IMPL(PIGN_Shape, initWithPIQ_Key_withPIGB_ComponentColor_withPIGB_ComponentColor_withNSString_, shapeKey, color, selectedColor, coords)
}

void PIGN_Shape_init__(PIGN_Shape *self) {
  [self calcSizes];
  (void) IOSObjectArray_SetAndConsume(nil_chk(self->layoutInfo_), 0, new_PIGL_LayoutInfo_init());
  (void) IOSObjectArray_SetAndConsume(nil_chk(self->layoutInfo_), 1, new_PIGL_LayoutInfo_init());
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGN_Shape)

J2OBJC_NAME_MAPPING(PIGN_Shape, "de.pidata.gui.component.general", "PIGN_")
