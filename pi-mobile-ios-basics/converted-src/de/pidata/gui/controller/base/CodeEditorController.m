//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/CodeEditorController.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/CodeEditorController.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/SearchListener.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/gui/guidef/InputMode.h"
#include "de/pidata/gui/view/base/CodeEditorViewPI.h"
#include "de/pidata/gui/view/base/TextViewPI.h"
#include "de/pidata/models/binding/Binding.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/CodeEditorController must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGC_CodeEditorController)

PIQ_QName *PIGC_CodeEditorController_SEARCH_NEXT;
PIQ_QName *PIGC_CodeEditorController_SEARCH_NEXT_SHORTCUT;
PIQ_QName *PIGC_CodeEditorController_SEARCH_PREV;
PIQ_QName *PIGC_CodeEditorController_SEARCH_PREV_SHORTCUT;
PIQ_QName *PIGC_CodeEditorController_REPLACE_NEXT;
PIQ_QName *PIGC_CodeEditorController_REPLACE_ALL;
PIQ_QName *PIGC_CodeEditorController_ACTIVATE_SEARCH;
PIQ_QName *PIGC_CodeEditorController_CLEAR_SEARCH;
PIQ_QName *PIGC_CodeEditorController_ACTIVATE_REPLACE;

@implementation PIGC_CodeEditorController

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_CodeEditorController_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
        withPIGC_Controller:(id<PIGC_Controller>)parent
        withPIGV_TextViewPI:(PIGV_TextViewPI *)textViewPI
                withBoolean:(jboolean)readOnly {
  codeEditorViewPI_ = (PIGV_CodeEditorViewPI *) cast_chk(textViewPI, [PIGV_CodeEditorViewPI class]);
  [super init__WithPIQ_QName:id_ withPIGC_Controller:parent withPIGV_TextViewPI:textViewPI withPIMT_SimpleType:PIME_StringType_getDefString() withPIGG_InputMode:JreLoadEnum(PIGG_InputMode, string) withId:nil withBoolean:readOnly];
}

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
        withPIGC_Controller:(id<PIGC_Controller>)parent
        withPIGV_TextViewPI:(PIGV_TextViewPI *)textViewPI
           withPIMB_Binding:(PIMB_Binding *)valueBinding
                withBoolean:(jboolean)readOnly {
  if (valueBinding == nil) {
    [self init__WithPIQ_QName:id_ withPIGC_Controller:parent withPIGV_TextViewPI:textViewPI withBoolean:readOnly];
  }
  else {
    codeEditorViewPI_ = (PIGV_CodeEditorViewPI *) cast_chk(textViewPI, [PIGV_CodeEditorViewPI class]);
    [self init__WithPIQ_QName:id_ withPIGC_Controller:parent withPIGV_TextViewPI:textViewPI withPIMB_Binding:valueBinding withPIGG_InputMode:JreLoadEnum(PIGG_InputMode, string) withId:nil withBoolean:readOnly];
  }
}

- (void)showLinenumbersWithBoolean:(jboolean)show {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ showLinenumbersWithBoolean:show];
  }
}

- (void)addLineMarkerWithNSString:(NSString *)marker
                 withJavaUtilList:(id<JavaUtilList>)markLinesList
          withPIGB_ComponentColor:(id<PIGB_ComponentColor>)markerColor {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ addLineMarkerWithNSString:marker withJavaUtilList:markLinesList withPIGB_ComponentColor:markerColor];
  }
}

- (void)setAutoindentWithBoolean:(jboolean)autoindent {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ setAutoindentWithBoolean:autoindent];
  }
}

- (void)searchAllWithNSString:(NSString *)searchStr {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ searchAllWithNSString:searchStr];
  }
}

- (void)searchNextWithNSString:(NSString *)searchStr {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ searchNextWithNSString:searchStr];
  }
}

- (void)searchPrevWithNSString:(NSString *)searchStr {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ searchPrevWithNSString:searchStr];
  }
}

- (void)refreshLineMarkers {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ refreshLineMarkers];
  }
}

- (void)setSearchRegexWithBoolean:(jboolean)searchRegex {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ setSearchRegexWithBoolean:searchRegex];
  }
}

- (void)initListeners {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ initListeners];
  }
}

- (void)addSearchListenerWithPIGC_SearchListener:(id<PIGC_SearchListener>)searchListener {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ addSearchListenerWithPIGC_SearchListener:searchListener];
  }
}

- (void)replaceNextWithNSString:(NSString *)replaceStr {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ replaceNextWithNSString:replaceStr];
  }
}

- (void)replaceAllWithNSString:(NSString *)replaceStr {
  if (codeEditorViewPI_ != nil) {
    [codeEditorViewPI_ replaceAllWithNSString:replaceStr];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, 7, -1, -1 },
    { NULL, "V", 0x1, 8, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 16, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 17, 10, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(init__WithPIQ_QName:withPIGC_Controller:withPIGV_TextViewPI:withBoolean:);
  methods[2].selector = @selector(init__WithPIQ_QName:withPIGC_Controller:withPIGV_TextViewPI:withPIMB_Binding:withBoolean:);
  methods[3].selector = @selector(showLinenumbersWithBoolean:);
  methods[4].selector = @selector(addLineMarkerWithNSString:withJavaUtilList:withPIGB_ComponentColor:);
  methods[5].selector = @selector(setAutoindentWithBoolean:);
  methods[6].selector = @selector(searchAllWithNSString:);
  methods[7].selector = @selector(searchNextWithNSString:);
  methods[8].selector = @selector(searchPrevWithNSString:);
  methods[9].selector = @selector(refreshLineMarkers);
  methods[10].selector = @selector(setSearchRegexWithBoolean:);
  methods[11].selector = @selector(initListeners);
  methods[12].selector = @selector(addSearchListenerWithPIGC_SearchListener:);
  methods[13].selector = @selector(replaceNextWithNSString:);
  methods[14].selector = @selector(replaceAllWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "SEARCH_NEXT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 18, -1, -1 },
    { "SEARCH_NEXT_SHORTCUT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 19, -1, -1 },
    { "SEARCH_PREV", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 20, -1, -1 },
    { "SEARCH_PREV_SHORTCUT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 21, -1, -1 },
    { "REPLACE_NEXT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 22, -1, -1 },
    { "REPLACE_ALL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
    { "ACTIVATE_SEARCH", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "CLEAR_SEARCH", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "ACTIVATE_REPLACE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "codeEditorViewPI_", "LPIGV_CodeEditorViewPI;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "init", "LPIQ_QName;LPIGC_Controller;LPIGV_TextViewPI;Z", "LPIQ_QName;LPIGC_Controller;LPIGV_TextViewPI;LPIMB_Binding;Z", "showLinenumbers", "Z", "addLineMarker", "LNSString;LJavaUtilList;LPIGB_ComponentColor;", "(Ljava/lang/String;Ljava/util/List<Ljava/lang/Integer;>;Lde/pidata/gui/component/base/ComponentColor;)V", "setAutoindent", "searchAll", "LNSString;", "searchNext", "searchPrev", "setSearchRegex", "addSearchListener", "LPIGC_SearchListener;", "replaceNext", "replaceAll", &PIGC_CodeEditorController_SEARCH_NEXT, &PIGC_CodeEditorController_SEARCH_NEXT_SHORTCUT, &PIGC_CodeEditorController_SEARCH_PREV, &PIGC_CodeEditorController_SEARCH_PREV_SHORTCUT, &PIGC_CodeEditorController_REPLACE_NEXT, &PIGC_CodeEditorController_REPLACE_ALL, &PIGC_CodeEditorController_ACTIVATE_SEARCH, &PIGC_CodeEditorController_CLEAR_SEARCH, &PIGC_CodeEditorController_ACTIVATE_REPLACE };
  static const J2ObjcClassInfo _PIGC_CodeEditorController = { "CodeEditorController", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 15, 10, -1, -1, -1, -1, -1 };
  return &_PIGC_CodeEditorController;
}

+ (void)initialize {
  if (self == [PIGC_CodeEditorController class]) {
    PIGC_CodeEditorController_SEARCH_NEXT = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"searchNext"];
    PIGC_CodeEditorController_SEARCH_NEXT_SHORTCUT = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"searchNextShortcut"];
    PIGC_CodeEditorController_SEARCH_PREV = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"searchPrev"];
    PIGC_CodeEditorController_SEARCH_PREV_SHORTCUT = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"searchPrevShortcut"];
    PIGC_CodeEditorController_REPLACE_NEXT = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"replaceNext"];
    PIGC_CodeEditorController_REPLACE_ALL = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"replaceAll"];
    PIGC_CodeEditorController_ACTIVATE_SEARCH = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"activateSearchText"];
    PIGC_CodeEditorController_CLEAR_SEARCH = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"clearSearchText"];
    PIGC_CodeEditorController_ACTIVATE_REPLACE = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"activateReplaceText"];
    J2OBJC_SET_INITIALIZED(PIGC_CodeEditorController)
  }
}

@end

void PIGC_CodeEditorController_init(PIGC_CodeEditorController *self) {
  PIGC_TextController_init(self);
}

PIGC_CodeEditorController *new_PIGC_CodeEditorController_init() {
  J2OBJC_NEW_IMPL(PIGC_CodeEditorController, init)
}

PIGC_CodeEditorController *create_PIGC_CodeEditorController_init() {
  J2OBJC_CREATE_IMPL(PIGC_CodeEditorController, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_CodeEditorController)

J2OBJC_NAME_MAPPING(PIGC_CodeEditorController, "de.pidata.gui.controller.base", "PIGC_")
