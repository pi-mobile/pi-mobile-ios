//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/ColumnInfo.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/ColumnInfo.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/guidef/AttrBindingType.h"
#include "de/pidata/gui/guidef/ColumnType.h"
#include "de/pidata/gui/view/base/ColumnView.h"
#include "de/pidata/models/tree/Context.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/XPath.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/ColumnInfo must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_ColumnInfo () {
 @public
  PIMR_Context *context_;
  PIQ_QName *colName_;
  PIQ_QName *headerCompID_;
  PIQ_QName *bodyCompID_;
  id<PIGC_Controller> renderCtrl_;
  id<PIGC_Controller> editCtrl_;
  PIQ_QName *valueID_;
  NSString *align_;
  PIQ_QName *iconValueID_;
  PIQ_QName *helpValueID_;
  PIQ_QName *backgroundValueID_;
  PIQ_QName *labelValueID_;
  NSString *modelRef_;
  jboolean readOnly_;
  jboolean sortable_;
  PIMR_XPath *path_;
  jint index_;
  id<PIGV_ColumnView> columnView_;
}

- (id)getAttributeWithPIMR_Model:(id<PIMR_Model>)row
                   withPIQ_QName:(PIQ_QName *)attrName;

@end

J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, context_, PIMR_Context *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, colName_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, headerCompID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, bodyCompID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, renderCtrl_, id<PIGC_Controller>)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, editCtrl_, id<PIGC_Controller>)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, valueID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, align_, NSString *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, iconValueID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, helpValueID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, backgroundValueID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, labelValueID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, modelRef_, NSString *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, path_, PIMR_XPath *)
J2OBJC_FIELD_SETTER(PIGC_ColumnInfo, columnView_, id<PIGV_ColumnView>)

__attribute__((unused)) static id PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(PIGC_ColumnInfo *self, id<PIMR_Model> row, PIQ_QName *attrName);

@implementation PIGC_ColumnInfo

- (instancetype)initWithPIMR_Context:(PIMR_Context *)context
                             withInt:(jint)index
              withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable
                 withPIGC_Controller:(id<PIGC_Controller>)renderCtrl
                 withPIGC_Controller:(id<PIGC_Controller>)editCtrl
                 withPIGG_ColumnType:(PIGG_ColumnType *)columnDef
            withPIGG_AttrBindingType:(PIGG_AttrBindingType *)columnBindingDef
            withPIGG_AttrBindingType:(PIGG_AttrBindingType *)displayBindingDef {
  PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_(self, context, index, namespaceTable, renderCtrl, editCtrl, columnDef, columnBindingDef, displayBindingDef);
  return self;
}

- (instancetype)initWithPIMR_Context:(PIMR_Context *)context
                             withInt:(jint)index
                       withPIQ_QName:(PIQ_QName *)colName
                        withNSString:(NSString *)modelRef
                        withNSString:(NSString *)valuePathRef
                       withPIQ_QName:(PIQ_QName *)valueID
                       withPIQ_QName:(PIQ_QName *)iconValueID
                       withPIQ_QName:(PIQ_QName *)helpValueID
                       withPIQ_QName:(PIQ_QName *)backgroundValueID
                       withPIQ_QName:(PIQ_QName *)headerCompID
                       withPIQ_QName:(PIQ_QName *)bodyCompID
                       withPIQ_QName:(PIQ_QName *)labelValueID
              withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable
                 withPIGC_Controller:(id<PIGC_Controller>)renderCtrl
                 withPIGC_Controller:(id<PIGC_Controller>)editCtrl
                         withBoolean:(jboolean)readOnly
                         withBoolean:(jboolean)sortable {
  PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_(self, context, index, colName, modelRef, valuePathRef, valueID, iconValueID, helpValueID, backgroundValueID, headerCompID, bodyCompID, labelValueID, namespaceTable, renderCtrl, editCtrl, readOnly, sortable);
  return self;
}

- (jint)getIndex {
  return index_;
}

- (PIMR_Context *)getContext {
  return context_;
}

- (PIQ_QName *)getColName {
  return colName_;
}

- (NSString *)getModelRef {
  return modelRef_;
}

- (NSString *)getValuePathRef {
  return modelRef_;
}

- (PIQ_QName *)getValueID {
  return valueID_;
}

- (PIQ_QName *)getIconValueID {
  return iconValueID_;
}

- (PIQ_QName *)getLabelValueID {
  return labelValueID_;
}

- (PIQ_QName *)getHelpValueID {
  return helpValueID_;
}

- (PIQ_QName *)getBackgroundValueID {
  return backgroundValueID_;
}

- (jboolean)isReadOnly {
  return readOnly_;
}

- (jboolean)isSortable {
  return sortable_;
}

- (PIQ_QName *)getHeaderCompID {
  return headerCompID_;
}

- (PIQ_QName *)getBodyCompID {
  return bodyCompID_;
}

- (id<PIGC_Controller>)getRenderCtrl {
  return renderCtrl_;
}

- (id<PIGC_Controller>)getEditCtrl {
  return editCtrl_;
}

- (PIMR_XPath *)getPath {
  return path_;
}

- (id)getAttributeWithPIMR_Model:(id<PIMR_Model>)row
                   withPIQ_QName:(PIQ_QName *)attrName {
  return PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, attrName);
}

- (id<PIMR_Model>)getValueModelWithPIMR_Model:(id<PIMR_Model>)row {
  id<PIMR_Model> model;
  if (path_ == nil) {
    model = row;
  }
  else {
    model = [path_ getModelWithPIMR_Model:row withPIMR_Context:context_];
  }
  return model;
}

- (id)getCellValueWithPIMR_Model:(id<PIMR_Model>)row {
  if (valueID_ == nil) {
    return row;
  }
  else {
    return PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, valueID_);
  }
}

- (id)getIconValueWithPIMR_Model:(id<PIMR_Model>)row {
  return PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, iconValueID_);
}

- (NSString *)getAlign {
  return align_;
}

- (NSString *)getHelpValueWithPIMR_Model:(id<PIMR_Model>)row {
  id value = PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, helpValueID_);
  if (value == nil) {
    return nil;
  }
  else {
    return [value description];
  }
}

- (NSString *)getLabelValueWithPIMR_Model:(id<PIMR_Model>)row {
  id value = PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, labelValueID_);
  if ([value isKindOfClass:[NSString class]]) {
    return (NSString *) value;
  }
  return @"";
}

- (PIQ_QName *)getBackgroundValueWithPIMR_Model:(id<PIMR_Model>)row {
  id value = PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(self, row, backgroundValueID_);
  if (value == nil) {
    return nil;
  }
  else {
    return (PIQ_QName *) cast_chk(value, [PIQ_QName class]);
  }
}

- (NSString *)description {
  return [((PIQ_QName *) nil_chk(colName_)) description];
}

- (void)setColNameWithPIQ_QName:(PIQ_QName *)newColName {
  self->colName_ = newColName;
  if (self->columnView_ != nil) {
    [columnView_ onColumnChangedWithPIGC_ColumnInfo:self];
  }
}

- (void)setViewWithPIGV_ColumnView:(id<PIGV_ColumnView>)columnView {
  self->columnView_ = columnView;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Context;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGC_Controller;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGC_Controller;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_XPath;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x2, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 6, 5, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 7, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 8, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 9, 5, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, 10, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 11, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIMR_Context:withInt:withPIQ_NamespaceTable:withPIGC_Controller:withPIGC_Controller:withPIGG_ColumnType:withPIGG_AttrBindingType:withPIGG_AttrBindingType:);
  methods[1].selector = @selector(initWithPIMR_Context:withInt:withPIQ_QName:withNSString:withNSString:withPIQ_QName:withPIQ_QName:withPIQ_QName:withPIQ_QName:withPIQ_QName:withPIQ_QName:withPIQ_QName:withPIQ_NamespaceTable:withPIGC_Controller:withPIGC_Controller:withBoolean:withBoolean:);
  methods[2].selector = @selector(getIndex);
  methods[3].selector = @selector(getContext);
  methods[4].selector = @selector(getColName);
  methods[5].selector = @selector(getModelRef);
  methods[6].selector = @selector(getValuePathRef);
  methods[7].selector = @selector(getValueID);
  methods[8].selector = @selector(getIconValueID);
  methods[9].selector = @selector(getLabelValueID);
  methods[10].selector = @selector(getHelpValueID);
  methods[11].selector = @selector(getBackgroundValueID);
  methods[12].selector = @selector(isReadOnly);
  methods[13].selector = @selector(isSortable);
  methods[14].selector = @selector(getHeaderCompID);
  methods[15].selector = @selector(getBodyCompID);
  methods[16].selector = @selector(getRenderCtrl);
  methods[17].selector = @selector(getEditCtrl);
  methods[18].selector = @selector(getPath);
  methods[19].selector = @selector(getAttributeWithPIMR_Model:withPIQ_QName:);
  methods[20].selector = @selector(getValueModelWithPIMR_Model:);
  methods[21].selector = @selector(getCellValueWithPIMR_Model:);
  methods[22].selector = @selector(getIconValueWithPIMR_Model:);
  methods[23].selector = @selector(getAlign);
  methods[24].selector = @selector(getHelpValueWithPIMR_Model:);
  methods[25].selector = @selector(getLabelValueWithPIMR_Model:);
  methods[26].selector = @selector(getBackgroundValueWithPIMR_Model:);
  methods[27].selector = @selector(description);
  methods[28].selector = @selector(setColNameWithPIQ_QName:);
  methods[29].selector = @selector(setViewWithPIGV_ColumnView:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "context_", "LPIMR_Context;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "colName_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "headerCompID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "bodyCompID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "renderCtrl_", "LPIGC_Controller;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "editCtrl_", "LPIGC_Controller;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "valueID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "align_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "iconValueID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "helpValueID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "backgroundValueID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "labelValueID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "modelRef_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "readOnly_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "sortable_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "path_", "LPIMR_XPath;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "index_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "columnView_", "LPIGV_ColumnView;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIMR_Context;ILPIQ_NamespaceTable;LPIGC_Controller;LPIGC_Controller;LPIGG_ColumnType;LPIGG_AttrBindingType;LPIGG_AttrBindingType;", "LPIMR_Context;ILPIQ_QName;LNSString;LNSString;LPIQ_QName;LPIQ_QName;LPIQ_QName;LPIQ_QName;LPIQ_QName;LPIQ_QName;LPIQ_QName;LPIQ_NamespaceTable;LPIGC_Controller;LPIGC_Controller;ZZ", "getAttribute", "LPIMR_Model;LPIQ_QName;", "getValueModel", "LPIMR_Model;", "getCellValue", "getIconValue", "getHelpValue", "getLabelValue", "getBackgroundValue", "toString", "setColName", "LPIQ_QName;", "setView", "LPIGV_ColumnView;" };
  static const J2ObjcClassInfo _PIGC_ColumnInfo = { "ColumnInfo", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 30, 18, -1, -1, -1, -1, -1 };
  return &_PIGC_ColumnInfo;
}

@end

void PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_(PIGC_ColumnInfo *self, PIMR_Context *context, jint index, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, PIGG_ColumnType *columnDef, PIGG_AttrBindingType *columnBindingDef, PIGG_AttrBindingType *displayBindingDef) {
  NSObject_init(self);
  self->path_ = nil;
  if (context == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"context must not be null!");
  if (renderCtrl == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"renderCtrl must not be null!");
  self->context_ = context;
  self->index_ = index;
  self->bodyCompID_ = [((PIGG_ColumnType *) nil_chk(columnDef)) getColumnCompID];
  self->colName_ = [columnDef getID];
  if (self->colName_ == nil) {
    self->colName_ = self->bodyCompID_;
  }
  if (self->colName_ == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"colName must not be null!");
  if (columnBindingDef != nil) {
    self->valueID_ = [columnBindingDef getAttrName];
    self->modelRef_ = [columnBindingDef getPath];
  }
  PIGG_AttrBindingType *iconBindingDef = [columnDef getIconAttrBinding];
  self->iconValueID_ = (iconBindingDef == nil) ? nil : [((PIGG_AttrBindingType *) nil_chk(iconBindingDef)) getAttrName];
  PIGG_AttrBindingType *helpBindingDef = [columnDef getHelpAttrBinding];
  self->helpValueID_ = (helpBindingDef == nil) ? nil : [((PIGG_AttrBindingType *) nil_chk(helpBindingDef)) getAttrName];
  PIGG_AttrBindingType *backgroundBindingDef = [columnDef getBackgroundAttrBinding];
  self->backgroundValueID_ = (backgroundBindingDef == nil) ? nil : [((PIGG_AttrBindingType *) nil_chk(backgroundBindingDef)) getAttrName];
  PIGG_AttrBindingType *headerBindingDef = [columnDef getHeaderAttrBinding];
  self->headerCompID_ = (headerBindingDef == nil) ? nil : [((PIGG_AttrBindingType *) nil_chk(headerBindingDef)) getAttrName];
  self->labelValueID_ = (displayBindingDef == nil) ? nil : [((PIGG_AttrBindingType *) nil_chk(displayBindingDef)) getAttrName];
  self->align_ = [columnDef getAlign];
  self->readOnly_ = [columnDef readOnly];
  self->sortable_ = [columnDef sortable];
  self->renderCtrl_ = renderCtrl;
  self->editCtrl_ = editCtrl;
  if (self->modelRef_ != nil && ([self->modelRef_ java_length] > 0) && ![((NSString *) nil_chk(self->modelRef_)) isEqual:@"."]) {
    if (self->path_ == nil) {
      self->path_ = new_PIMR_XPath_initWithPIQ_NamespaceTable_withNSString_(namespaceTable, self->modelRef_);
    }
    else {
      (void) [self->path_ appendWithPIMR_XPath:new_PIMR_XPath_initWithPIQ_NamespaceTable_withNSString_(namespaceTable, self->modelRef_)];
    }
  }
}

PIGC_ColumnInfo *new_PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_(PIMR_Context *context, jint index, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, PIGG_ColumnType *columnDef, PIGG_AttrBindingType *columnBindingDef, PIGG_AttrBindingType *displayBindingDef) {
  J2OBJC_NEW_IMPL(PIGC_ColumnInfo, initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_, context, index, namespaceTable, renderCtrl, editCtrl, columnDef, columnBindingDef, displayBindingDef)
}

PIGC_ColumnInfo *create_PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_(PIMR_Context *context, jint index, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, PIGG_ColumnType *columnDef, PIGG_AttrBindingType *columnBindingDef, PIGG_AttrBindingType *displayBindingDef) {
  J2OBJC_CREATE_IMPL(PIGC_ColumnInfo, initWithPIMR_Context_withInt_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withPIGG_ColumnType_withPIGG_AttrBindingType_withPIGG_AttrBindingType_, context, index, namespaceTable, renderCtrl, editCtrl, columnDef, columnBindingDef, displayBindingDef)
}

void PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_(PIGC_ColumnInfo *self, PIMR_Context *context, jint index, PIQ_QName *colName, NSString *modelRef, NSString *valuePathRef, PIQ_QName *valueID, PIQ_QName *iconValueID, PIQ_QName *helpValueID, PIQ_QName *backgroundValueID, PIQ_QName *headerCompID, PIQ_QName *bodyCompID, PIQ_QName *labelValueID, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, jboolean readOnly, jboolean sortable) {
  NSObject_init(self);
  self->path_ = nil;
  if (context == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"context must not be null!");
  if (colName == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"colName must not be null!");
  if (renderCtrl == nil) @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"renderCtrl must not be null!");
  self->context_ = context;
  self->index_ = index;
  self->colName_ = colName;
  self->modelRef_ = modelRef;
  self->valueID_ = valueID;
  self->iconValueID_ = iconValueID;
  self->helpValueID_ = helpValueID;
  self->backgroundValueID_ = backgroundValueID;
  self->labelValueID_ = labelValueID;
  self->readOnly_ = readOnly;
  self->sortable_ = sortable;
  self->headerCompID_ = headerCompID;
  self->bodyCompID_ = bodyCompID;
  self->renderCtrl_ = renderCtrl;
  self->editCtrl_ = editCtrl;
  if ((modelRef != nil) && ([((NSString *) nil_chk(modelRef)) java_length] > 0) && ![modelRef isEqual:@"."]) {
    self->path_ = new_PIMR_XPath_initWithPIQ_NamespaceTable_withNSString_(namespaceTable, modelRef);
  }
  if (valuePathRef != nil && ([valuePathRef java_length] > 0) && ![valuePathRef isEqual:@"."]) {
    if (self->path_ == nil) {
      self->path_ = new_PIMR_XPath_initWithPIQ_NamespaceTable_withNSString_(namespaceTable, valuePathRef);
    }
    else {
      (void) [self->path_ appendWithPIMR_XPath:new_PIMR_XPath_initWithPIQ_NamespaceTable_withNSString_(namespaceTable, valuePathRef)];
    }
  }
}

PIGC_ColumnInfo *new_PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_(PIMR_Context *context, jint index, PIQ_QName *colName, NSString *modelRef, NSString *valuePathRef, PIQ_QName *valueID, PIQ_QName *iconValueID, PIQ_QName *helpValueID, PIQ_QName *backgroundValueID, PIQ_QName *headerCompID, PIQ_QName *bodyCompID, PIQ_QName *labelValueID, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, jboolean readOnly, jboolean sortable) {
  J2OBJC_NEW_IMPL(PIGC_ColumnInfo, initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_, context, index, colName, modelRef, valuePathRef, valueID, iconValueID, helpValueID, backgroundValueID, headerCompID, bodyCompID, labelValueID, namespaceTable, renderCtrl, editCtrl, readOnly, sortable)
}

PIGC_ColumnInfo *create_PIGC_ColumnInfo_initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_(PIMR_Context *context, jint index, PIQ_QName *colName, NSString *modelRef, NSString *valuePathRef, PIQ_QName *valueID, PIQ_QName *iconValueID, PIQ_QName *helpValueID, PIQ_QName *backgroundValueID, PIQ_QName *headerCompID, PIQ_QName *bodyCompID, PIQ_QName *labelValueID, PIQ_NamespaceTable *namespaceTable, id<PIGC_Controller> renderCtrl, id<PIGC_Controller> editCtrl, jboolean readOnly, jboolean sortable) {
  J2OBJC_CREATE_IMPL(PIGC_ColumnInfo, initWithPIMR_Context_withInt_withPIQ_QName_withNSString_withNSString_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_QName_withPIQ_NamespaceTable_withPIGC_Controller_withPIGC_Controller_withBoolean_withBoolean_, context, index, colName, modelRef, valuePathRef, valueID, iconValueID, helpValueID, backgroundValueID, headerCompID, bodyCompID, labelValueID, namespaceTable, renderCtrl, editCtrl, readOnly, sortable)
}

id PIGC_ColumnInfo_getAttributeWithPIMR_Model_withPIQ_QName_(PIGC_ColumnInfo *self, id<PIMR_Model> row, PIQ_QName *attrName) {
  if (attrName == nil) {
    return nil;
  }
  id<PIMR_Model> model = [self getValueModelWithPIMR_Model:row];
  if (model == nil) {
    return nil;
  }
  else {
    return [model getWithPIQ_QName:attrName];
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_ColumnInfo)

J2OBJC_NAME_MAPPING(PIGC_ColumnInfo, "de.pidata.gui.controller.base", "PIGC_")
