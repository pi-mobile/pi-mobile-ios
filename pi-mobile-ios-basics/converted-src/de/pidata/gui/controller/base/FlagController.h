//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/FlagController.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiControllerBaseFlagController")
#ifdef RESTRICT_DePidataGuiControllerBaseFlagController
#define INCLUDE_ALL_DePidataGuiControllerBaseFlagController 0
#else
#define INCLUDE_ALL_DePidataGuiControllerBaseFlagController 1
#endif
#undef RESTRICT_DePidataGuiControllerBaseFlagController

#if !defined (PIGC_FlagController_) && (INCLUDE_ALL_DePidataGuiControllerBaseFlagController || defined(INCLUDE_PIGC_FlagController))
#define PIGC_FlagController_

#define RESTRICT_DePidataGuiControllerBaseAbstractValueController 1
#define INCLUDE_PIGC_AbstractValueController 1
#include "de/pidata/gui/controller/base/AbstractValueController.h"

@class JavaLangBoolean;
@class PIGC_GuiOperation;
@class PIGV_FlagViewPI;
@class PIMB_Binding;
@class PIMR_ValidationException;
@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIGC_FlagListener;
@protocol PIGU_UIContainer;
@protocol PIGV_ViewPI;
@protocol PIMR_Model;

@interface PIGC_FlagController : PIGC_AbstractValueController

#pragma mark Public

- (instancetype)init;

- (void)activateWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer;

- (void)addListenerWithPIGC_FlagListener:(id<PIGC_FlagListener>)listener;

- (JavaLangBoolean *)getBooleanValue;

- (jboolean)getBoolValue;

- (PIGC_GuiOperation *)getGuiOperationWithPIQ_QName:(PIQ_QName *)guiOpID;

- (PIMB_Binding *)getLabelBinding;

- (id)getValue;

- (id<PIGV_ViewPI>)getView;

- (PIMB_Binding *)getVisibilityBinding;

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
        withPIGC_Controller:(id<PIGC_Controller>)parent
           withPIMB_Binding:(PIMB_Binding *)valueBinding
           withPIMB_Binding:(PIMB_Binding *)labelBinding
        withPIGV_FlagViewPI:(PIGV_FlagViewPI *)flagViewPI
               withNSString:(NSString *)format
                withBoolean:(jboolean)readOnly OBJC_METHOD_FAMILY_NONE;

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
        withPIGC_Controller:(id<PIGC_Controller>)parent
           withPIMB_Binding:(PIMB_Binding *)valueBinding
        withPIGV_FlagViewPI:(PIGV_FlagViewPI *)flagViewPI
               withNSString:(NSString *)format
                withBoolean:(jboolean)readOnly OBJC_METHOD_FAMILY_NONE;

- (void)initLabelBindingWithPIMB_Binding:(PIMB_Binding *)labelBinding OBJC_METHOD_FAMILY_NONE;

- (void)onCheckChangedWithJavaLangBoolean:(JavaLangBoolean *)checkedValue
                           withPIMR_Model:(id<PIMR_Model>)dataContext;

- (void)removeListenerWithPIGC_FlagListener:(id<PIGC_FlagListener>)listener;

- (NSString *)renderWithId:(id)value;

- (void)valueChangedWithPIMB_Binding:(PIMB_Binding *)source
                              withId:(id)newValue;

#pragma mark Protected

- (void)fireFlagChangedWithJavaLangBoolean:(JavaLangBoolean *)newValue;

- (void)setValidationErrorWithPIMR_ValidationException:(PIMR_ValidationException *)vex;

- (void)viewSetValueWithId:(id)value;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGC_FlagController)

FOUNDATION_EXPORT void PIGC_FlagController_init(PIGC_FlagController *self);

FOUNDATION_EXPORT PIGC_FlagController *new_PIGC_FlagController_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGC_FlagController *create_PIGC_FlagController_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIGC_FlagController)

@compatibility_alias DePidataGuiControllerBaseFlagController PIGC_FlagController;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiControllerBaseFlagController")
