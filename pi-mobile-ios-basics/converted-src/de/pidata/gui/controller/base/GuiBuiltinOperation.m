//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/GuiBuiltinOperation.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/ControllerGroup.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/GuiBuiltinOperation.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/guidef/BuiltinOperation.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/service/base/ServiceException.h"
#include "java/lang/Exception.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/GuiBuiltinOperation must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_GuiBuiltinOperation () {
 @public
  PIGG_BuiltinOperation *operationID_;
  PIQ_QName *childDialogID_;
  id<PIMR_Model> parameter_;
}

@end

J2OBJC_FIELD_SETTER(PIGC_GuiBuiltinOperation, operationID_, PIGG_BuiltinOperation *)
J2OBJC_FIELD_SETTER(PIGC_GuiBuiltinOperation, childDialogID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIGC_GuiBuiltinOperation, parameter_, id<PIMR_Model>)

@implementation PIGC_GuiBuiltinOperation

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_GuiBuiltinOperation_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)init__WithPIQ_QName:(PIQ_QName *)eventID
  withPIGG_BuiltinOperation:(PIGG_BuiltinOperation *)operationID
             withPIMR_Model:(id<PIMR_Model>)parameter
   withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)controllerGroup {
  [super init__WithPIQ_QName:eventID withNSString:nil withPIGC_ControllerGroup:controllerGroup];
  self->operationID_ = operationID;
  self->parameter_ = parameter;
}

- (void)init__WithPIQ_QName:(PIQ_QName *)eventID
              withPIQ_QName:(PIQ_QName *)childDialogID
             withPIMR_Model:(id<PIMR_Model>)parameter
   withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)controllerGroup {
  [super init__WithPIQ_QName:eventID withNSString:nil withPIGC_ControllerGroup:controllerGroup];
  self->childDialogID_ = childDialogID;
  self->parameter_ = parameter;
}

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  @try {
    id<PIGC_DialogController> dlgController = [((id<PIGC_Controller>) nil_chk(sourceCtrl)) getDialogController];
    if (operationID_ != nil) {
      PIGC_GuiOperation_executeBuiltinWithPIGC_Controller_withPIMR_Model_withPIGG_BuiltinOperation_(dlgController, parameter_, operationID_);
    }
    else if (childDialogID_ != nil) {
      PIGC_GuiOperation_openChildDialogWithPIGC_Controller_withPIMR_Model_withPIQ_QName_(dlgController, parameter_, childDialogID_);
    }
  }
  @catch (JavaLangException *ex) {
    NSString *msg = JreStrcat("$@", @"Could execute builtin operationID=", operationID_);
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(msg, ex);
    @throw new_PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(PISB_ServiceException_SERVICE_FAILED, msg, ex);
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, 5, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(init__WithPIQ_QName:withPIGG_BuiltinOperation:withPIMR_Model:withPIGC_ControllerGroup:);
  methods[2].selector = @selector(init__WithPIQ_QName:withPIQ_QName:withPIMR_Model:withPIGC_ControllerGroup:);
  methods[3].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "operationID_", "LPIGG_BuiltinOperation;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "childDialogID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "parameter_", "LPIMR_Model;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "init", "LPIQ_QName;LPIGG_BuiltinOperation;LPIMR_Model;LPIGC_ControllerGroup;", "LPIQ_QName;LPIQ_QName;LPIMR_Model;LPIGC_ControllerGroup;", "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;" };
  static const J2ObjcClassInfo _PIGC_GuiBuiltinOperation = { "GuiBuiltinOperation", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 4, 3, -1, -1, -1, -1, -1 };
  return &_PIGC_GuiBuiltinOperation;
}

@end

void PIGC_GuiBuiltinOperation_init(PIGC_GuiBuiltinOperation *self) {
  PIGC_GuiOperation_init(self);
}

PIGC_GuiBuiltinOperation *new_PIGC_GuiBuiltinOperation_init() {
  J2OBJC_NEW_IMPL(PIGC_GuiBuiltinOperation, init)
}

PIGC_GuiBuiltinOperation *create_PIGC_GuiBuiltinOperation_init() {
  J2OBJC_CREATE_IMPL(PIGC_GuiBuiltinOperation, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_GuiBuiltinOperation)

J2OBJC_NAME_MAPPING(PIGC_GuiBuiltinOperation, "de.pidata.gui.controller.base", "PIGC_")
