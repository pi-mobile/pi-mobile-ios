//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/ModelParameterList.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/ModelParameterList.h"
#include "de/pidata/gui/guidef/GuiService.h"
#include "de/pidata/models/tree/CombinedKey.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelFactory.h"
#include "de/pidata/models/tree/ModelFactoryTable.h"
#include "de/pidata/models/tree/SimpleKey.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/SimpleType.h"
#include "de/pidata/models/types/Type.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/service/base/AbstractParameterList.h"
#include "de/pidata/service/base/ParameterType.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/ModelParameterList must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGC_ModelParameterList)

PIQ_QName *PIGC_ModelParameterList_PARAM_TYPENAME;
PIQ_QName *PIGC_ModelParameterList_PARAM_KEYCLASS;
PIQ_QName *PIGC_ModelParameterList_PARAM_KEYVALUE;
PIQ_QName *PIGC_ModelParameterList_PARAM_NSTABLE;

@implementation PIGC_ModelParameterList

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_ModelParameterList_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIMR_Model:(id<PIMR_Model>)model {
  PIGC_ModelParameterList_initWithPIMR_Model_(self, model);
  return self;
}

- (id<PIQ_Key>)getKey {
  id<PIQ_Key> key;
  PIQ_NamespaceTable *namespaceTable = [self getNamespaceTableWithPIQ_QName:PIGC_ModelParameterList_PARAM_NSTABLE];
  PIQ_QName *modelTypeName = [self getQNameWithPIQ_QName:PIGC_ModelParameterList_PARAM_TYPENAME];
  id<PIMT_ComplexType> modelType = (id<PIMT_ComplexType>) cast_check([((id<PIMR_ModelFactory>) nil_chk([((PIMR_ModelFactoryTable *) nil_chk(PIMR_ModelFactoryTable_getInstance())) getFactoryWithPIQ_Namespace:[((PIQ_QName *) nil_chk(modelTypeName)) getNamespace]])) getTypeWithPIQ_QName:modelTypeName], PIMT_ComplexType_class_());
  NSString *keyClassName = [self getStringWithPIQ_QName:PIGC_ModelParameterList_PARAM_KEYCLASS];
  NSString *keyString = [self getStringWithPIQ_QName:PIGC_ModelParameterList_PARAM_KEYVALUE];
  if ([((NSString *) nil_chk(keyClassName)) isEqual:[PIQ_QName_class_() getName]]) {
    key = PIQ_QName_fromKeyStringWithNSString_withPIQ_NamespaceTable_(keyString, namespaceTable);
  }
  else if ([keyClassName isEqual:[PIMR_SimpleKey_class_() getName]]) {
    key = PIMR_SimpleKey_fromKeyStringWithPIMT_SimpleType_withNSString_withPIQ_NamespaceTable_([((id<PIMT_ComplexType>) nil_chk(modelType)) getKeyAttributeTypeWithInt:0], keyString, nil);
  }
  else {
    key = PIMR_CombinedKey_fromKeyStringWithPIMT_ComplexType_withNSString_withPIQ_NamespaceTable_(modelType, keyString, nil);
  }
  return key;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LPIQ_Key;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIMR_Model:);
  methods[2].selector = @selector(getKey);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "PARAM_TYPENAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 1, -1, -1 },
    { "PARAM_KEYCLASS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 2, -1, -1 },
    { "PARAM_KEYVALUE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "PARAM_NSTABLE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIMR_Model;", &PIGC_ModelParameterList_PARAM_TYPENAME, &PIGC_ModelParameterList_PARAM_KEYCLASS, &PIGC_ModelParameterList_PARAM_KEYVALUE, &PIGC_ModelParameterList_PARAM_NSTABLE };
  static const J2ObjcClassInfo _PIGC_ModelParameterList = { "ModelParameterList", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 3, 4, -1, -1, -1, -1, -1 };
  return &_PIGC_ModelParameterList;
}

+ (void)initialize {
  if (self == [PIGC_ModelParameterList class]) {
    PIGC_ModelParameterList_PARAM_TYPENAME = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_GuiService, NAMESPACE))) getQNameWithNSString:@"TypeName"];
    PIGC_ModelParameterList_PARAM_KEYCLASS = [JreLoadStatic(PIGG_GuiService, NAMESPACE) getQNameWithNSString:@"KeyClass"];
    PIGC_ModelParameterList_PARAM_KEYVALUE = [JreLoadStatic(PIGG_GuiService, NAMESPACE) getQNameWithNSString:@"KeyValue"];
    PIGC_ModelParameterList_PARAM_NSTABLE = [JreLoadStatic(PIGG_GuiService, NAMESPACE) getQNameWithNSString:@"NsTable"];
    J2OBJC_SET_INITIALIZED(PIGC_ModelParameterList)
  }
}

@end

void PIGC_ModelParameterList_init(PIGC_ModelParameterList *self) {
  PISB_AbstractParameterList_initWithPISB_ParameterType_withPIQ_QName_withPISB_ParameterType_withPIQ_QName_withPISB_ParameterType_withPIQ_QName_(self, JreLoadEnum(PISB_ParameterType, QNameType), PIGC_ModelParameterList_PARAM_TYPENAME, JreLoadEnum(PISB_ParameterType, StringType), PIGC_ModelParameterList_PARAM_KEYCLASS, JreLoadEnum(PISB_ParameterType, StringType), PIGC_ModelParameterList_PARAM_KEYVALUE);
}

PIGC_ModelParameterList *new_PIGC_ModelParameterList_init() {
  J2OBJC_NEW_IMPL(PIGC_ModelParameterList, init)
}

PIGC_ModelParameterList *create_PIGC_ModelParameterList_init() {
  J2OBJC_CREATE_IMPL(PIGC_ModelParameterList, init)
}

void PIGC_ModelParameterList_initWithPIMR_Model_(PIGC_ModelParameterList *self, id<PIMR_Model> model) {
  PIGC_ModelParameterList_init(self);
  id<PIQ_Key> key = [((id<PIMR_Model>) nil_chk(model)) key];
  if (key == nil) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Cannot use model as parameter: model's key is null");
  }
  [self setQNameWithPIQ_QName:PIGC_ModelParameterList_PARAM_TYPENAME withPIQ_QName:[((id<PIMT_Type>) nil_chk([model type])) name]];
  [self setStringWithPIQ_QName:PIGC_ModelParameterList_PARAM_KEYCLASS withNSString:[[key java_getClass] getName]];
  [self setStringWithPIQ_QName:PIGC_ModelParameterList_PARAM_KEYVALUE withNSString:[key toKeyStringWithPIQ_NamespaceTable:[model namespaceTable]]];
  [self setNamespaceTableWithPIQ_QName:PIGC_ModelParameterList_PARAM_NSTABLE withPIQ_NamespaceTable:[model namespaceTable]];
}

PIGC_ModelParameterList *new_PIGC_ModelParameterList_initWithPIMR_Model_(id<PIMR_Model> model) {
  J2OBJC_NEW_IMPL(PIGC_ModelParameterList, initWithPIMR_Model_, model)
}

PIGC_ModelParameterList *create_PIGC_ModelParameterList_initWithPIMR_Model_(id<PIMR_Model> model) {
  J2OBJC_CREATE_IMPL(PIGC_ModelParameterList, initWithPIMR_Model_, model)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_ModelParameterList)

J2OBJC_NAME_MAPPING(PIGC_ModelParameterList, "de.pidata.gui.controller.base", "PIGC_")
