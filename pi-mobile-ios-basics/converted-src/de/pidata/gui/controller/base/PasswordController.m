//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/PasswordController.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/PasswordController.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/gui/view/base/TextViewPI.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/binding/Binding.h"
#include "de/pidata/models/crypt/DefaultSHA1Encrypter.h"
#include "de/pidata/models/crypt/Encrypter.h"
#include "de/pidata/models/crypt/PlainEncrypter.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/string/Helper.h"
#include "java/lang/Exception.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/PasswordController must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_PasswordController () {
 @public
  id<PIMC_Encrypter> encrypter_;
  jboolean hideInput_;
}

- (void)initEncrypterWithNSString:(NSString *)encryptFkt OBJC_METHOD_FAMILY_NONE;

@end

J2OBJC_FIELD_SETTER(PIGC_PasswordController, encrypter_, id<PIMC_Encrypter>)

__attribute__((unused)) static void PIGC_PasswordController_initEncrypterWithNSString_(PIGC_PasswordController *self, NSString *encryptFkt);

NSString *PIGC_PasswordController_ENCRYPT_SHA1 = @"SHA1";

@implementation PIGC_PasswordController

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_PasswordController_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
        withPIGC_Controller:(id<PIGC_Controller>)parent
        withPIGV_TextViewPI:(PIGV_TextViewPI *)textViewPI
           withPIMB_Binding:(PIMB_Binding *)binding
               withNSString:(NSString *)encryptor
                withBoolean:(jboolean)readOnly {
  PIGC_PasswordController_initEncrypterWithNSString_(self, encryptor);
  [super init__WithPIQ_QName:id_ withPIGC_Controller:parent withPIGV_TextViewPI:textViewPI withPIMB_Binding:binding withBoolean:readOnly];
  [((PIGV_TextViewPI *) nil_chk(textViewPI)) setHideInputWithBoolean:hideInput_];
}

- (void)initEncrypterWithNSString:(NSString *)encryptFkt {
  PIGC_PasswordController_initEncrypterWithNSString_(self, encryptFkt);
}

- (void)viewSetValueWithId:(id)value {
  [self viewSetTextWithNSString:@""];
}

- (id)getValue {
  NSString *value = [self viewGetText];
  return [((id<PIMC_Encrypter>) nil_chk(encrypter_)) encryptWithNSString:value];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(init__WithPIQ_QName:withPIGC_Controller:withPIGV_TextViewPI:withPIMB_Binding:withNSString:withBoolean:);
  methods[2].selector = @selector(initEncrypterWithNSString:);
  methods[3].selector = @selector(viewSetValueWithId:);
  methods[4].selector = @selector(getValue);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "encrypter_", "LPIMC_Encrypter;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "hideInput_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "ENCRYPT_SHA1", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 6, -1, -1 },
  };
  static const void *ptrTable[] = { "init", "LPIQ_QName;LPIGC_Controller;LPIGV_TextViewPI;LPIMB_Binding;LNSString;Z", "initEncrypter", "LNSString;", "viewSetValue", "LNSObject;", &PIGC_PasswordController_ENCRYPT_SHA1 };
  static const J2ObjcClassInfo _PIGC_PasswordController = { "PasswordController", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 5, 3, -1, -1, -1, -1, -1 };
  return &_PIGC_PasswordController;
}

@end

void PIGC_PasswordController_init(PIGC_PasswordController *self) {
  PIGC_TextController_init(self);
  self->hideInput_ = false;
}

PIGC_PasswordController *new_PIGC_PasswordController_init() {
  J2OBJC_NEW_IMPL(PIGC_PasswordController, init)
}

PIGC_PasswordController *create_PIGC_PasswordController_init() {
  J2OBJC_CREATE_IMPL(PIGC_PasswordController, init)
}

void PIGC_PasswordController_initEncrypterWithNSString_(PIGC_PasswordController *self, NSString *encryptFkt) {
  @try {
    if (PICS_Helper_isNullOrEmptyWithId_(encryptFkt)) {
      self->encrypter_ = new_PIMC_PlainEncrypter_init();
    }
    else {
      NSString *encryptFktName;
      if ([((NSString *) nil_chk(encryptFkt)) charAtWithInt:0] == '*') {
        self->hideInput_ = true;
        encryptFktName = [encryptFkt java_substring:1];
      }
      else {
        encryptFktName = encryptFkt;
      }
      if ([((NSString *) nil_chk(PIGC_PasswordController_ENCRYPT_SHA1)) isEqual:encryptFktName]) {
        self->encrypter_ = new_PIMC_DefaultSHA1Encrypter_init();
      }
      else {
        self->encrypter_ = (id<PIMC_Encrypter>) cast_check([((IOSClass *) nil_chk(IOSClass_forName_(encryptFktName))) newInstance], PIMC_Encrypter_class_());
      }
    }
  }
  @catch (JavaLangException *ex) {
    NSString *msg = @"Could not create encrypter";
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(msg, ex);
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(msg);
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_PasswordController)

J2OBJC_NAME_MAPPING(PIGC_PasswordController, "de.pidata.gui.controller.base", "PIGC_")
