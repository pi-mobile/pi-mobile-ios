//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/SapIdController.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/CharBuffer.h"
#include "de/pidata/gui/controller/base/SapIdController.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/models/types/Type.h"
#include "de/pidata/models/types/simple/QNameType.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/SapIdController must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIGC_SapIdController

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_SapIdController_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)viewSetValueWithId:(id)value {
  NSString *strValue = nil;
  if (value == nil) {
    [self viewSetTextWithNSString:nil];
  }
  else {
    if ([value isKindOfClass:[PIQ_QName class]]) {
      strValue = [((PIQ_QName *) value) getName];
    }
    else {
      strValue = [value description];
      jint pos = [((NSString *) nil_chk(strValue)) java_lastIndexOf:':'];
      if (pos > 0) {
        strValue = [strValue java_substring:pos + 1];
      }
    }
    strValue = PIME_StringType_removeLeadingZerosWithNSString_(strValue);
    [self viewSetTextWithNSString:strValue];
  }
}

- (id)getValue {
  NSString *text = [self viewGetText];
  PIGB_CharBuffer *buffer = new_PIGB_CharBuffer_initWithNSString_(nil);
  jint length;
  id<PIMT_Type> valueType = [self getValueType];
  if (valueType != nil) {
    length = [((PIME_StringType *) cast_chk(valueType, [PIME_StringType class])) getMaxLength];
  }
  else {
    length = [buffer length];
  }
  if ((valueType != nil) && ([valueType isKindOfClass:[PIME_QNameType class]])) {
    [buffer setValueWithPIQ_Namespace:[self getNamespace] withNSString:text];
    return [buffer toQNameFillWithInt:length withChar:'0' withBoolean:true];
  }
  else {
    [buffer setValueWithPIQ_Namespace:nil withNSString:text];
    return [buffer toStringFillWithInt:length withChar:'0' withBoolean:true];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(viewSetValueWithId:);
  methods[2].selector = @selector(getValue);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "viewSetValue", "LNSObject;" };
  static const J2ObjcClassInfo _PIGC_SapIdController = { "SapIdController", "de.pidata.gui.controller.base", ptrTable, methods, NULL, 7, 0x1, 3, 0, -1, -1, -1, -1, -1 };
  return &_PIGC_SapIdController;
}

@end

void PIGC_SapIdController_init(PIGC_SapIdController *self) {
  PIGC_TextController_init(self);
}

PIGC_SapIdController *new_PIGC_SapIdController_init() {
  J2OBJC_NEW_IMPL(PIGC_SapIdController, init)
}

PIGC_SapIdController *create_PIGC_SapIdController_init() {
  J2OBJC_CREATE_IMPL(PIGC_SapIdController, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_SapIdController)

J2OBJC_NAME_MAPPING(PIGC_SapIdController, "de.pidata.gui.controller.base", "PIGC_")
