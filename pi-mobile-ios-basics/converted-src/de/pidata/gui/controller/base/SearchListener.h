//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/SearchListener.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiControllerBaseSearchListener")
#ifdef RESTRICT_DePidataGuiControllerBaseSearchListener
#define INCLUDE_ALL_DePidataGuiControllerBaseSearchListener 0
#else
#define INCLUDE_ALL_DePidataGuiControllerBaseSearchListener 1
#endif
#undef RESTRICT_DePidataGuiControllerBaseSearchListener

#if !defined (PIGC_SearchListener_) && (INCLUDE_ALL_DePidataGuiControllerBaseSearchListener || defined(INCLUDE_PIGC_SearchListener))
#define PIGC_SearchListener_

@protocol PIGC_SearchListener < JavaObject >

- (void)searchChangedWithNSString:(NSString *)searchText
                          withInt:(jint)searchCount;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGC_SearchListener)

J2OBJC_TYPE_LITERAL_HEADER(PIGC_SearchListener)

#define DePidataGuiControllerBaseSearchListener PIGC_SearchListener

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiControllerBaseSearchListener")
