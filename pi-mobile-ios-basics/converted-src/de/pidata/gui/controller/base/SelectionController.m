//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/SelectionController.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/SelectionController.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/SelectionController must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_SelectionController : NSObject

@end

@implementation PIGC_SelectionController

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "S", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x401, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIMB_Selection;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGV_ListViewPI;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 6, 7, -1, -1, -1, -1 },
    { NULL, "LPIGC_Controller;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 8, 9, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(indexOfWithPIQ_Key:);
  methods[1].selector = @selector(rowCount);
  methods[2].selector = @selector(getSelectedRowWithInt:);
  methods[3].selector = @selector(getSelection);
  methods[4].selector = @selector(getListView);
  methods[5].selector = @selector(hasFirstRowForEmptySelection);
  methods[6].selector = @selector(onSelectionChangedWithPIMR_Model:withBoolean:);
  methods[7].selector = @selector(onPrevSelected);
  methods[8].selector = @selector(onNextSelected);
  methods[9].selector = @selector(initDetailControllerWithPIGC_Controller:);
  methods[10].selector = @selector(getDetailController);
  methods[11].selector = @selector(selectRowWithPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "indexOf", "LPIQ_Key;", "getSelectedRow", "I", "onSelectionChanged", "LPIMR_Model;Z", "initDetailController", "LPIGC_Controller;", "selectRow", "LPIMR_Model;" };
  static const J2ObjcClassInfo _PIGC_SelectionController = { "SelectionController", "de.pidata.gui.controller.base", ptrTable, methods, NULL, 7, 0x609, 12, 0, -1, -1, -1, -1, -1 };
  return &_PIGC_SelectionController;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIGC_SelectionController)

J2OBJC_NAME_MAPPING(PIGC_SelectionController, "de.pidata.gui.controller.base", "PIGC_")
