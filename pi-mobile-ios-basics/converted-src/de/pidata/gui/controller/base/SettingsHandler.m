//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/SettingsHandler.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/SettingsHandler.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/SettingsHandler must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_SettingsHandler : NSObject

@end

@implementation PIGC_SettingsHandler

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(loadSettings);
  methods[1].selector = @selector(saveSettings);
  #pragma clang diagnostic pop
  static const J2ObjcClassInfo _PIGC_SettingsHandler = { "SettingsHandler", "de.pidata.gui.controller.base", NULL, methods, NULL, 7, 0x609, 2, 0, -1, -1, -1, -1, -1 };
  return &_PIGC_SettingsHandler;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIGC_SettingsHandler)

J2OBJC_NAME_MAPPING(PIGC_SettingsHandler, "de.pidata.gui.controller.base", "PIGC_")
