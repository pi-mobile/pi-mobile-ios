//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/TabPaneController.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/AbstractValueController.h"
#include "de/pidata/gui/controller/base/ControllerGroup.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/controller/base/MutableControllerGroup.h"
#include "de/pidata/gui/controller/base/TabPaneController.h"
#include "de/pidata/gui/view/base/TabPaneViewPI.h"
#include "de/pidata/gui/view/base/ViewPI.h"
#include "de/pidata/models/binding/AttributeBinding.h"
#include "de/pidata/models/binding/Binding.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ValidationException.h"
#include "de/pidata/models/types/simple/BooleanType.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Boolean.h"
#include "java/lang/RuntimeException.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/controller/base/TabPaneController must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGC_TabPaneController () {
 @public
  PIGV_TabPaneViewPI *tabPaneViewPI_;
  PIGC_GuiOperation *onClickOperation_;
  jboolean enabled_;
  PIQ_Namespace *valueNS_;
}

@end

J2OBJC_FIELD_SETTER(PIGC_TabPaneController, tabPaneViewPI_, PIGV_TabPaneViewPI *)
J2OBJC_FIELD_SETTER(PIGC_TabPaneController, onClickOperation_, PIGC_GuiOperation *)
J2OBJC_FIELD_SETTER(PIGC_TabPaneController, valueNS_, PIQ_Namespace *)

@implementation PIGC_TabPaneController

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGC_TabPaneController_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
   withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)parent
     withPIGV_TabPaneViewPI:(PIGV_TabPaneViewPI *)tabPaneViewPI {
  [super init__WithPIQ_QName:id_ withPIGC_Controller:parent withPIMB_Binding:valueBinding_ withBoolean:true];
  self->tabPaneViewPI_ = tabPaneViewPI;
  [((PIGV_TabPaneViewPI *) nil_chk(tabPaneViewPI)) setControllerWithPIGC_TabPaneController:self];
  self->onClickOperation_ = onClickOperation_;
}

- (id<PIGV_ViewPI>)getView {
  return tabPaneViewPI_;
}

- (id)getValue {
  return @"";
}

- (void)viewSetValueWithId:(id)value {
  if (tabPaneViewPI_ != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
}

- (PIGC_GuiOperation *)getOnClickOperation {
  return onClickOperation_;
}

- (PIGC_GuiOperation *)getGuiOperationWithPIQ_QName:(PIQ_QName *)guiOpID {
  if ((onClickOperation_ != nil) && (JreObjectEqualsEquals(((PIGC_GuiOperation *) nil_chk(onClickOperation_))->eventID_, guiOpID))) {
    return onClickOperation_;
  }
  else {
    return nil;
  }
}

- (jboolean)isFocusable {
  return (enabled_ && (tabPaneViewPI_ != nil));
}

- (void)setEnabledWithBoolean:(jboolean)enabled {
  self->enabled_ = enabled;
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setStateWithShort:(jshort) -1 withShort:(jshort) -1 withJavaLangBoolean:nil withJavaLangBoolean:nil withJavaLangBoolean:PIME_BooleanType_valueOfWithBoolean_(enabled) withJavaLangBoolean:nil];
  }
}

- (void)viewSetEnabled {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setEnabledWithBoolean:enabled_];
  }
}

- (void)setVisibleWithBoolean:(jboolean)visible {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setStateWithShort:(jshort) -1 withShort:(jshort) -1 withJavaLangBoolean:nil withJavaLangBoolean:nil withJavaLangBoolean:nil withJavaLangBoolean:PIME_BooleanType_valueOfWithBoolean_(visible)];
  }
}

- (void)press {
  if (enabled_) {
    id<PIMR_Model> model = [((id<PIGC_MutableControllerGroup>) nil_chk([self getControllerGroup])) getModel];
    [((id<PIGC_DialogController>) nil_chk([self getDialogController])) subActionWithPIGC_GuiOperation:self->onClickOperation_ withPIQ_QName:[self getName] withPIGC_Controller:self withPIMR_Model:model];
  }
}

- (void)setValidationErrorWithPIMR_ValidationException:(PIMR_ValidationException *)vex {
}

- (NSString *)getSelectedTab {
  if (tabPaneViewPI_ != nil) {
    return [tabPaneViewPI_ getSelectedTab];
  }
  return nil;
}

- (void)setSelectedTabWithNSString:(NSString *)tabId {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setSelectedTabWithNSString:tabId];
  }
}

- (void)setSelectedTabWithInt:(jint)index {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setSelectedTabWithInt:index];
  }
}

- (id<JavaUtilList>)getTabs {
  if (tabPaneViewPI_ != nil) {
    return [tabPaneViewPI_ getTabs];
  }
  return nil;
}

- (void)setTabDisabledWithNSString:(NSString *)tabId
                       withBoolean:(jboolean)disabled {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setTabDisabledWithNSString:tabId withBoolean:disabled];
  }
}

- (void)tabChangedWithNSString:(NSString *)id_ {
  id<PIGC_MutableControllerGroup> ctrlGroup = [self getControllerGroup];
  (void) [((id<PIGC_MutableControllerGroup>) nil_chk(ctrlGroup)) description];
}

- (void)setTabTextWithNSString:(NSString *)tabId
                  withNSString:(NSString *)text {
  if (tabPaneViewPI_ != nil) {
    [tabPaneViewPI_ setTabTextWithNSString:tabId withNSString:text];
  }
}

- (void)setValueNSWithPIQ_Namespace:(PIQ_Namespace *)valueNS {
  self->valueNS_ = valueNS;
}

- (PIQ_Namespace *)getValueNS {
  if (valueNS_ == nil) {
    if (valueBinding_ != nil && [valueBinding_ isKindOfClass:[PIMB_AttributeBinding class]]) {
      valueNS_ = [((PIQ_QName *) nil_chk([((PIMB_AttributeBinding *) cast_chk(valueBinding_, [PIMB_AttributeBinding class])) getAttributeName])) getNamespace];
    }
    else {
      @throw new_JavaLangRuntimeException_initWithNSString_(@"no namespace given");
    }
  }
  return valueNS_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "LPIGV_ViewPI;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIGC_GuiOperation;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGC_GuiOperation;", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 9, 10, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 13, -1, -1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 14, -1, -1 },
    { NULL, "V", 0x1, 15, 16, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 17, 12, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 20, 21, -1, -1, -1, -1 },
    { NULL, "LPIQ_Namespace;", 0x4, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(init__WithPIQ_QName:withPIGC_ControllerGroup:withPIGV_TabPaneViewPI:);
  methods[2].selector = @selector(getView);
  methods[3].selector = @selector(getValue);
  methods[4].selector = @selector(viewSetValueWithId:);
  methods[5].selector = @selector(getOnClickOperation);
  methods[6].selector = @selector(getGuiOperationWithPIQ_QName:);
  methods[7].selector = @selector(isFocusable);
  methods[8].selector = @selector(setEnabledWithBoolean:);
  methods[9].selector = @selector(viewSetEnabled);
  methods[10].selector = @selector(setVisibleWithBoolean:);
  methods[11].selector = @selector(press);
  methods[12].selector = @selector(setValidationErrorWithPIMR_ValidationException:);
  methods[13].selector = @selector(getSelectedTab);
  methods[14].selector = @selector(setSelectedTabWithNSString:);
  methods[15].selector = @selector(setSelectedTabWithInt:);
  methods[16].selector = @selector(getTabs);
  methods[17].selector = @selector(setTabDisabledWithNSString:withBoolean:);
  methods[18].selector = @selector(tabChangedWithNSString:);
  methods[19].selector = @selector(setTabTextWithNSString:withNSString:);
  methods[20].selector = @selector(setValueNSWithPIQ_Namespace:);
  methods[21].selector = @selector(getValueNS);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "tabPaneViewPI_", "LPIGV_TabPaneViewPI;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "onClickOperation_", "LPIGC_GuiOperation;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "enabled_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "valueNS_", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "init", "LPIQ_QName;LPIGC_ControllerGroup;LPIGV_TabPaneViewPI;", "viewSetValue", "LNSObject;", "getGuiOperation", "LPIQ_QName;", "setEnabled", "Z", "setVisible", "setValidationError", "LPIMR_ValidationException;", "setSelectedTab", "LNSString;", "I", "()Ljava/util/List<Ljava/lang/String;>;", "setTabDisabled", "LNSString;Z", "tabChanged", "setTabText", "LNSString;LNSString;", "setValueNS", "LPIQ_Namespace;" };
  static const J2ObjcClassInfo _PIGC_TabPaneController = { "TabPaneController", "de.pidata.gui.controller.base", ptrTable, methods, fields, 7, 0x1, 22, 4, -1, -1, -1, -1, -1 };
  return &_PIGC_TabPaneController;
}

@end

void PIGC_TabPaneController_init(PIGC_TabPaneController *self) {
  PIGC_AbstractValueController_init(self);
  self->enabled_ = true;
}

PIGC_TabPaneController *new_PIGC_TabPaneController_init() {
  J2OBJC_NEW_IMPL(PIGC_TabPaneController, init)
}

PIGC_TabPaneController *create_PIGC_TabPaneController_init() {
  J2OBJC_CREATE_IMPL(PIGC_TabPaneController, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGC_TabPaneController)

J2OBJC_NAME_MAPPING(PIGC_TabPaneController, "de.pidata.gui.controller.base", "PIGC_")
