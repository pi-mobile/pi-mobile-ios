//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/TableController.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiControllerBaseTableController")
#ifdef RESTRICT_DePidataGuiControllerBaseTableController
#define INCLUDE_ALL_DePidataGuiControllerBaseTableController 0
#else
#define INCLUDE_ALL_DePidataGuiControllerBaseTableController 1
#endif
#undef RESTRICT_DePidataGuiControllerBaseTableController

#if !defined (PIGC_TableController_) && (INCLUDE_ALL_DePidataGuiControllerBaseTableController || defined(INCLUDE_PIGC_TableController))
#define PIGC_TableController_

#define RESTRICT_DePidataGuiControllerBaseSelectionController 1
#define INCLUDE_PIGC_SelectionController 1
#include "de/pidata/gui/controller/base/SelectionController.h"

@class PIGC_ColumnInfo;
@class PIGC_GuiOperation;
@class PIGG_InplaceEdit;
@class PIGV_TableViewPI;
@class PIMB_Binding;
@class PIMB_Selection;
@class PIQ_QName;
@protocol PIGC_ControllerGroup;
@protocol PIGC_TableDelegate;
@protocol PIMR_Model;

@protocol PIGC_TableController < PIGC_SelectionController, JavaObject >

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
   withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)parent
       withPIGV_TableViewPI:(PIGV_TableViewPI *)tableViewPI
           withPIMB_Binding:(PIMB_Binding *)binding
         withPIMB_Selection:(PIMB_Selection *)selection
                withBoolean:(jboolean)readOnly
                withBoolean:(jboolean)selectable
       withPIGG_InplaceEdit:(PIGG_InplaceEdit *)inplaceEdit
      withPIGC_GuiOperation:(PIGC_GuiOperation *)rowAction OBJC_METHOD_FAMILY_NONE;

- (PIGV_TableViewPI *)getTableView;

- (jboolean)isEditable;

- (jboolean)isSelectable;

- (void)onStartEditWithPIMR_Model:(id<PIMR_Model>)editRow
              withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)onStopEditWithPIMR_Model:(id<PIMR_Model>)editedRow
             withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo
                          withId:(id)newValue;

- (jint)columnCount;

- (PIGC_ColumnInfo *)getColumnWithInt:(jint)index;

- (PIGC_ColumnInfo *)getColumnByHeaderIDWithPIQ_QName:(PIQ_QName *)headerID;

- (PIGC_ColumnInfo *)getColumnByNameWithPIQ_QName:(PIQ_QName *)colName;

- (PIGC_ColumnInfo *)getColumnByBodyCompIDWithPIQ_QName:(PIQ_QName *)renderCompID;

- (void)addColumnWithPIGC_ColumnInfo:(PIGC_ColumnInfo *)column;

- (void)onSelectedCellWithPIMR_Model:(id<PIMR_Model>)displayRow
                 withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)onDoubleClickCellWithPIMR_Model:(id<PIMR_Model>)displayRow
                    withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)onRightClickCellWithPIMR_Model:(id<PIMR_Model>)displayRow
                   withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)setTableDelegateWithPIGC_TableDelegate:(id<PIGC_TableDelegate>)tableDelegate;

- (void)onFilteredWithBoolean:(jboolean)filterActive;

- (void)setRowDoubleClickActionWithPIGC_GuiOperation:(PIGC_GuiOperation *)rowDoubleClickAction;

- (void)setRowMenuActionWithPIGC_GuiOperation:(PIGC_GuiOperation *)rowDoubleClickAction;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGC_TableController)

J2OBJC_TYPE_LITERAL_HEADER(PIGC_TableController)

#define DePidataGuiControllerBaseTableController PIGC_TableController

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiControllerBaseTableController")
