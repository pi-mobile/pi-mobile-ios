//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/base/TreeTableController.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiControllerBaseTreeTableController")
#ifdef RESTRICT_DePidataGuiControllerBaseTreeTableController
#define INCLUDE_ALL_DePidataGuiControllerBaseTreeTableController 0
#else
#define INCLUDE_ALL_DePidataGuiControllerBaseTreeTableController 1
#endif
#undef RESTRICT_DePidataGuiControllerBaseTreeTableController

#if !defined (PIGC_TreeTableController_) && (INCLUDE_ALL_DePidataGuiControllerBaseTreeTableController || defined(INCLUDE_PIGC_TreeTableController))
#define PIGC_TreeTableController_

#define RESTRICT_DePidataGuiControllerBaseTreeController 1
#define INCLUDE_PIGC_TreeController 1
#include "de/pidata/gui/controller/base/TreeController.h"

@class PIGC_ColumnInfo;
@class PIGC_GuiOperation;
@class PIGV_TreeNodePI;
@class PIGV_TreeTableViewPI;
@class PIMB_Binding;
@class PIMB_ModelBinding;
@class PIQ_QName;
@protocol PIGC_ControllerGroup;

@protocol PIGC_TreeTableController < PIGC_TreeController, JavaObject >

- (void)init__WithPIQ_QName:(PIQ_QName *)id_
   withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)ctrlGroup
   withPIGV_TreeTableViewPI:(PIGV_TreeTableViewPI *)treeTableViewPI
           withPIMB_Binding:(PIMB_Binding *)rowBinding
      withPIMB_ModelBinding:(PIMB_ModelBinding *)treeBinding
      withPIGC_GuiOperation:(PIGC_GuiOperation *)nodeAction
                withBoolean:(jboolean)readOnly
                withBoolean:(jboolean)lazyLoading
                withBoolean:(jboolean)showRootNode OBJC_METHOD_FAMILY_NONE;

- (void)addColumnWithPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (jint)columnCount;

- (PIGC_ColumnInfo *)getColumnWithShort:(jshort)i;

- (PIGC_ColumnInfo *)getColumnByBodyCompIDWithPIQ_QName:(PIQ_QName *)columnID;

- (void)onSelectedCellWithPIGV_TreeNodePI:(PIGV_TreeNodePI *)selectedNode
                      withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)onDoubleClickCellWithPIGV_TreeNodePI:(PIGV_TreeNodePI *)selectedNode
                         withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

- (void)onRightClickCellWithPIGV_TreeNodePI:(PIGV_TreeNodePI *)selectedNode
                        withPIGC_ColumnInfo:(PIGC_ColumnInfo *)columnInfo;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGC_TreeTableController)

J2OBJC_TYPE_LITERAL_HEADER(PIGC_TreeTableController)

#define DePidataGuiControllerBaseTreeTableController PIGC_TreeTableController

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiControllerBaseTreeTableController")
