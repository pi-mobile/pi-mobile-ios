//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/controller/file/FileChooserResult.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiControllerFileFileChooserResult")
#ifdef RESTRICT_DePidataGuiControllerFileFileChooserResult
#define INCLUDE_ALL_DePidataGuiControllerFileFileChooserResult 0
#else
#define INCLUDE_ALL_DePidataGuiControllerFileFileChooserResult 1
#endif
#undef RESTRICT_DePidataGuiControllerFileFileChooserResult

#if !defined (PIGI_FileChooserResult_) && (INCLUDE_ALL_DePidataGuiControllerFileFileChooserResult || defined(INCLUDE_PIGI_FileChooserResult))
#define PIGI_FileChooserResult_

#define RESTRICT_DePidataServiceBaseAbstractParameterList 1
#define INCLUDE_PISB_AbstractParameterList 1
#include "de/pidata/service/base/AbstractParameterList.h"

@class IOSObjectArray;
@class PIQ_QName;
@class PISB_ParameterType;

@interface PIGI_FileChooserResult : PISB_AbstractParameterList

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithNSString:(NSString *)filePath;

- (NSString *)getFilePath;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithInt:(jint)arg0 NS_UNAVAILABLE;

- (instancetype)initWithNSString:(NSString *)arg0
     withPISB_ParameterTypeArray:(IOSObjectArray *)arg1
              withPIQ_QNameArray:(IOSObjectArray *)arg2
               withNSObjectArray:(IOSObjectArray *)arg3 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3
                    withPISB_ParameterType:(PISB_ParameterType *)arg4
                             withPIQ_QName:(PIQ_QName *)arg5 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3
                    withPISB_ParameterType:(PISB_ParameterType *)arg4
                             withPIQ_QName:(PIQ_QName *)arg5
                    withPISB_ParameterType:(PISB_ParameterType *)arg6
                             withPIQ_QName:(PIQ_QName *)arg7 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGI_FileChooserResult)

inline PIQ_QName *PIGI_FileChooserResult_get_PARAM_FILE_PATH(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGI_FileChooserResult_PARAM_FILE_PATH;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGI_FileChooserResult, PARAM_FILE_PATH, PIQ_QName *)

FOUNDATION_EXPORT void PIGI_FileChooserResult_init(PIGI_FileChooserResult *self);

FOUNDATION_EXPORT PIGI_FileChooserResult *new_PIGI_FileChooserResult_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGI_FileChooserResult *create_PIGI_FileChooserResult_init(void);

FOUNDATION_EXPORT void PIGI_FileChooserResult_initWithNSString_(PIGI_FileChooserResult *self, NSString *filePath);

FOUNDATION_EXPORT PIGI_FileChooserResult *new_PIGI_FileChooserResult_initWithNSString_(NSString *filePath) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGI_FileChooserResult *create_PIGI_FileChooserResult_initWithNSString_(NSString *filePath);

J2OBJC_TYPE_LITERAL_HEADER(PIGI_FileChooserResult)

@compatibility_alias DePidataGuiControllerFileFileChooserResult PIGI_FileChooserResult;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiControllerFileFileChooserResult")
