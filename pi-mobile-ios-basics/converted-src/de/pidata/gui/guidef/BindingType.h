//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/BindingType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefBindingType")
#ifdef RESTRICT_DePidataGuiGuidefBindingType
#define INCLUDE_ALL_DePidataGuiGuidefBindingType 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefBindingType 1
#endif
#undef RESTRICT_DePidataGuiGuidefBindingType

#if !defined (PIGG_BindingType_) && (INCLUDE_ALL_DePidataGuiGuidefBindingType || defined(INCLUDE_PIGG_BindingType))
#define PIGG_BindingType_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_BindingType : PIMR_SequenceModel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (NSString *)getAction;

- (PIQ_QName *)getDialogDef;

- (NSString *)getItemPath;

- (PIQ_QName *)getOperation;

- (PIQ_QName *)getPart;

- (void)setActionWithNSString:(NSString *)action;

- (void)setDialogDefWithPIQ_QName:(PIQ_QName *)dialogDef;

- (void)setItemPathWithNSString:(NSString *)itemPath;

- (void)setOperationWithPIQ_QName:(PIQ_QName *)operation;

- (void)setPartWithPIQ_QName:(PIQ_QName *)part;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGG_BindingType)

inline PIQ_Namespace *PIGG_BindingType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_BindingType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_BindingType_get_ID_ACTION(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_BindingType_ID_ACTION;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, ID_ACTION, PIQ_QName *)

inline PIQ_QName *PIGG_BindingType_get_ID_DIALOGDEF(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_BindingType_ID_DIALOGDEF;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, ID_DIALOGDEF, PIQ_QName *)

inline PIQ_QName *PIGG_BindingType_get_ID_ITEMPATH(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_BindingType_ID_ITEMPATH;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, ID_ITEMPATH, PIQ_QName *)

inline PIQ_QName *PIGG_BindingType_get_ID_OPERATION(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_BindingType_ID_OPERATION;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, ID_OPERATION, PIQ_QName *)

inline PIQ_QName *PIGG_BindingType_get_ID_PART(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_BindingType_ID_PART;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_BindingType, ID_PART, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_BindingType_init(PIGG_BindingType *self);

FOUNDATION_EXPORT PIGG_BindingType *new_PIGG_BindingType_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_BindingType *create_PIGG_BindingType_init(void);

FOUNDATION_EXPORT void PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_BindingType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_BindingType *new_PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_BindingType *create_PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_BindingType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_BindingType *new_PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_BindingType *create_PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_BindingType)

@compatibility_alias DePidataGuiGuidefBindingType PIGG_BindingType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefBindingType")
