//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/BindingType.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/BindingType.h"
#include "de/pidata/gui/guidef/ControllerFactory.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/BindingType must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGG_BindingType)

PIQ_Namespace *PIGG_BindingType_NAMESPACE;
PIQ_QName *PIGG_BindingType_ID_ACTION;
PIQ_QName *PIGG_BindingType_ID_DIALOGDEF;
PIQ_QName *PIGG_BindingType_ID_ITEMPATH;
PIQ_QName *PIGG_BindingType_ID_OPERATION;
PIQ_QName *PIGG_BindingType_ID_PART;

@implementation PIGG_BindingType

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGG_BindingType_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getOperation {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_BindingType_ID_OPERATION], [PIQ_QName class]);
}

- (void)setOperationWithPIQ_QName:(PIQ_QName *)operation {
  [self setWithPIQ_QName:PIGG_BindingType_ID_OPERATION withId:operation];
}

- (PIQ_QName *)getDialogDef {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_BindingType_ID_DIALOGDEF], [PIQ_QName class]);
}

- (void)setDialogDefWithPIQ_QName:(PIQ_QName *)dialogDef {
  [self setWithPIQ_QName:PIGG_BindingType_ID_DIALOGDEF withId:dialogDef];
}

- (PIQ_QName *)getPart {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_BindingType_ID_PART], [PIQ_QName class]);
}

- (void)setPartWithPIQ_QName:(PIQ_QName *)part {
  [self setWithPIQ_QName:PIGG_BindingType_ID_PART withId:part];
}

- (NSString *)getAction {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_BindingType_ID_ACTION], [NSString class]);
}

- (void)setActionWithNSString:(NSString *)action {
  [self setWithPIQ_QName:PIGG_BindingType_ID_ACTION withId:action];
}

- (NSString *)getItemPath {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_BindingType_ID_ITEMPATH], [NSString class]);
}

- (void)setItemPathWithNSString:(NSString *)itemPath {
  [self setWithPIQ_QName:PIGG_BindingType_ID_ITEMPATH withId:itemPath];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 3, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getOperation);
  methods[4].selector = @selector(setOperationWithPIQ_QName:);
  methods[5].selector = @selector(getDialogDef);
  methods[6].selector = @selector(setDialogDefWithPIQ_QName:);
  methods[7].selector = @selector(getPart);
  methods[8].selector = @selector(setPartWithPIQ_QName:);
  methods[9].selector = @selector(getAction);
  methods[10].selector = @selector(setActionWithNSString:);
  methods[11].selector = @selector(getItemPath);
  methods[12].selector = @selector(setItemPathWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
    { "ID_ACTION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 10, -1, -1 },
    { "ID_DIALOGDEF", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
    { "ID_ITEMPATH", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_OPERATION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "ID_PART", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setOperation", "LPIQ_QName;", "setDialogDef", "setPart", "setAction", "LNSString;", "setItemPath", &PIGG_BindingType_NAMESPACE, &PIGG_BindingType_ID_ACTION, &PIGG_BindingType_ID_DIALOGDEF, &PIGG_BindingType_ID_ITEMPATH, &PIGG_BindingType_ID_OPERATION, &PIGG_BindingType_ID_PART };
  static const J2ObjcClassInfo _PIGG_BindingType = { "BindingType", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x1, 13, 6, -1, -1, -1, -1, -1 };
  return &_PIGG_BindingType;
}

+ (void)initialize {
  if (self == [PIGG_BindingType class]) {
    PIGG_BindingType_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIGG_BindingType_ID_ACTION = [((PIQ_Namespace *) nil_chk(PIGG_BindingType_NAMESPACE)) getQNameWithNSString:@"action"];
    PIGG_BindingType_ID_DIALOGDEF = [PIGG_BindingType_NAMESPACE getQNameWithNSString:@"dialogDef"];
    PIGG_BindingType_ID_ITEMPATH = [PIGG_BindingType_NAMESPACE getQNameWithNSString:@"itemPath"];
    PIGG_BindingType_ID_OPERATION = [PIGG_BindingType_NAMESPACE getQNameWithNSString:@"operation"];
    PIGG_BindingType_ID_PART = [PIGG_BindingType_NAMESPACE getQNameWithNSString:@"part"];
    J2OBJC_SET_INITIALIZED(PIGG_BindingType)
  }
}

@end

void PIGG_BindingType_init(PIGG_BindingType *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIGG_ControllerFactory, BINDINGTYPE_TYPE), nil, nil, nil);
}

PIGG_BindingType *new_PIGG_BindingType_init() {
  J2OBJC_NEW_IMPL(PIGG_BindingType, init)
}

PIGG_BindingType *create_PIGG_BindingType_init() {
  J2OBJC_CREATE_IMPL(PIGG_BindingType, init)
}

void PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_BindingType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIGG_ControllerFactory, BINDINGTYPE_TYPE), attributeNames, anyAttribs, childNames);
}

PIGG_BindingType *new_PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_BindingType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIGG_BindingType *create_PIGG_BindingType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_BindingType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_BindingType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIGG_BindingType *new_PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_BindingType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIGG_BindingType *create_PIGG_BindingType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_BindingType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_BindingType)

J2OBJC_NAME_MAPPING(PIGG_BindingType, "de.pidata.gui.guidef", "PIGG_")
