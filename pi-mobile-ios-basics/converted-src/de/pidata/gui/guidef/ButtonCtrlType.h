//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/ButtonCtrlType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefButtonCtrlType")
#ifdef RESTRICT_DePidataGuiGuidefButtonCtrlType
#define INCLUDE_ALL_DePidataGuiGuidefButtonCtrlType 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefButtonCtrlType 1
#endif
#undef RESTRICT_DePidataGuiGuidefButtonCtrlType

#if !defined (PIGG_ButtonCtrlType_) && (INCLUDE_ALL_DePidataGuiGuidefButtonCtrlType || defined(INCLUDE_PIGG_ButtonCtrlType))
#define PIGG_ButtonCtrlType_

#define RESTRICT_DePidataGuiGuidefValueCtrlType 1
#define INCLUDE_PIGG_ValueCtrlType 1
#include "de/pidata/gui/guidef/ValueCtrlType.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIGC_ButtonController;
@class PIGG_AttrBindingType;
@class PIGG_ControllerBuilder;
@class PIGG_OperationType;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIGC_ControllerGroup;
@protocol PIGC_MutableControllerGroup;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_ButtonCtrlType : PIGG_ValueCtrlType

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (PIGC_ButtonController *)createControllerWithPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                                      withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup;

- (PIGG_OperationType *)getAction;

- (PIQ_QName *)getButtonCompID;

- (PIGG_AttrBindingType *)getIconAttrBinding;

- (PIGG_AttrBindingType *)getLabelAttrBinding;

- (void)initBindingsWithPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)ctrlGroup
                   withPIGC_ButtonController:(PIGC_ButtonController *)buttonController OBJC_METHOD_FAMILY_NONE;

- (void)setActionWithPIGG_OperationType:(PIGG_OperationType *)action;

- (void)setButtonCompIDWithPIQ_QName:(PIQ_QName *)buttonCompID;

- (void)setIconAttrBindingWithPIGG_AttrBindingType:(PIGG_AttrBindingType *)iconAttrBinding;

- (void)setLabelAttrBindingWithPIGG_AttrBindingType:(PIGG_AttrBindingType *)labelAttrBinding;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

@end

J2OBJC_STATIC_INIT(PIGG_ButtonCtrlType)

inline PIQ_Namespace *PIGG_ButtonCtrlType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_ButtonCtrlType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ButtonCtrlType, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_ButtonCtrlType_get_ID_ACTION(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ButtonCtrlType_ID_ACTION;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ButtonCtrlType, ID_ACTION, PIQ_QName *)

inline PIQ_QName *PIGG_ButtonCtrlType_get_ID_BUTTONCOMPID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ButtonCtrlType_ID_BUTTONCOMPID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ButtonCtrlType, ID_BUTTONCOMPID, PIQ_QName *)

inline PIQ_QName *PIGG_ButtonCtrlType_get_ID_ICONATTRBINDING(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ButtonCtrlType_ID_ICONATTRBINDING;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ButtonCtrlType, ID_ICONATTRBINDING, PIQ_QName *)

inline PIQ_QName *PIGG_ButtonCtrlType_get_ID_LABELATTRBINDING(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ButtonCtrlType_ID_LABELATTRBINDING;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ButtonCtrlType, ID_LABELATTRBINDING, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_ButtonCtrlType_initWithPIQ_Key_(PIGG_ButtonCtrlType *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIGG_ButtonCtrlType *new_PIGG_ButtonCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ButtonCtrlType *create_PIGG_ButtonCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIGG_ButtonCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_ButtonCtrlType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_ButtonCtrlType *new_PIGG_ButtonCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ButtonCtrlType *create_PIGG_ButtonCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_ButtonCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_ButtonCtrlType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_ButtonCtrlType *new_PIGG_ButtonCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ButtonCtrlType *create_PIGG_ButtonCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_ButtonCtrlType)

@compatibility_alias DePidataGuiGuidefButtonCtrlType PIGG_ButtonCtrlType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefButtonCtrlType")
