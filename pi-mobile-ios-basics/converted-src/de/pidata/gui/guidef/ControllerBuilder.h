//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/ControllerBuilder.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefControllerBuilder")
#ifdef RESTRICT_DePidataGuiGuidefControllerBuilder
#define INCLUDE_ALL_DePidataGuiGuidefControllerBuilder 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefControllerBuilder 1
#endif
#undef RESTRICT_DePidataGuiGuidefControllerBuilder

#if !defined (PIGG_ControllerBuilder_) && (INCLUDE_ALL_DePidataGuiGuidefControllerBuilder || defined(INCLUDE_PIGG_ControllerBuilder))
#define PIGG_ControllerBuilder_

@class PIGB_ProgressTask;
@class PIGC_GuiOperation;
@class PIGC_ModuleController;
@class PIGC_ModuleGroup;
@class PIGG_Application;
@class PIGG_CtrlContainerType;
@class PIGG_CtrlType;
@class PIGG_DialogDef;
@class PIGG_DialogType;
@class PIGG_Module;
@class PIGG_SelectionType;
@class PIGV_ViewFactory;
@class PIMB_Binding;
@class PIMB_Selection;
@class PIMR_Context;
@class PIQ_Namespace;
@class PIQ_NamespaceTable;
@class PIQ_QName;
@protocol PIGB_GuiBuilder;
@protocol PIGB_TaskHandler;
@protocol PIGC_Controller;
@protocol PIGC_ControllerGroup;
@protocol PIGC_DialogController;
@protocol PIGC_MutableControllerGroup;
@protocol PIGC_ProgressController;
@protocol PIGE_Dialog;
@protocol PIMR_Filter;
@protocol PIMR_Model;
@protocol PIMR_ModelIterator;
@protocol PIMR_ModelSource;

@interface PIGG_ControllerBuilder : NSObject {
 @public
  PIGG_Application *app_;
}

#pragma mark Public

- (instancetype)init;

- (void)addChildControllersWithPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrl
                                withPIGG_CtrlContainerType:(PIGG_CtrlContainerType *)moduleDef
                                               withBoolean:(jboolean)readOnly;

- (void)addChildControllersWithPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrl
                                           withPIGG_Module:(PIGG_Module *)moduleDef
                                               withBoolean:(jboolean)readOnly;

- (void)addControllerTypeWithPIQ_QName:(PIQ_QName *)controllerID
            withPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder;

- (PIMB_Binding *)createAttributeBindingWithPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)ctrlGroup
                                            withPIMR_ModelSource:(id<PIMR_ModelSource>)modelSource
                                                    withNSString:(NSString *)modelPath
                                                   withPIQ_QName:(PIQ_QName *)valueID
                                          withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable;

- (id<PIGC_Controller>)createControllerWithPIGG_CtrlType:(PIGG_CtrlType *)ctrlDef
                         withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup;

- (id<PIGC_DialogController>)createDialogControllerWithPIQ_QName:(PIQ_QName *)dialogID
                                                withPIMR_Context:(PIMR_Context *)context
                                                 withPIGE_Dialog:(id<PIGE_Dialog>)dialog
                                       withPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl;

- (PIGC_GuiOperation *)createGuiOpWithPIQ_QName:(PIQ_QName *)eventID
                                   withNSString:(NSString *)classname
                                   withNSString:(NSString *)command
                       withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)controllerGroup;

- (PIGC_ModuleGroup *)createModuleGroupWithPIQ_QName:(PIQ_QName *)moduleID
                           withPIGC_ModuleController:(PIGC_ModuleController *)moduleController
                                withPIMR_ModelSource:(id<PIMR_ModelSource>)modelSource
                                        withNSString:(NSString *)modelPath;

- (id<PIGC_ProgressController>)createProgressDialogWithNSString:(NSString *)title
                                                   withNSString:(NSString *)message
                                                        withInt:(jint)minValue
                                                        withInt:(jint)maxValue
                                                    withBoolean:(jboolean)cancelable
                                      withPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl;

- (PIMB_Selection *)createSelectionWithPIQ_QName:(PIQ_QName *)relationID
                                    withNSString:(NSString *)modelPath
                          withPIGG_SelectionType:(PIGG_SelectionType *)selType
                        withPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)ctrlGroup
                          withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable;

- (id<PIGB_TaskHandler>)createTaskHandlerWithPIGB_ProgressTask:(PIGB_ProgressTask *)progressTask;

- (PIGG_Application *)getApplication;

- (PIGG_DialogDef *)getDialogDefWithPIQ_QName:(PIQ_QName *)dialogID;

- (PIGG_DialogType *)getDialogTypeWithPIQ_QName:(PIQ_QName *)dialogID;

- (id<PIGB_GuiBuilder>)getGuiBuilder;

- (PIGV_ViewFactory *)getViewFactory;

- (void)init__WithPIGG_Application:(PIGG_Application *)app
               withPIGB_GuiBuilder:(id<PIGB_GuiBuilder>)guiBuilder OBJC_METHOD_FAMILY_NONE;

- (PIGC_ModuleGroup *)loadModuleWithPIGC_ModuleController:(PIGC_ModuleController *)moduleCtrl
                                            withPIQ_QName:(PIQ_QName *)moduleID
                                           withPIMR_Model:(id<PIMR_Model>)model;

- (PIGC_ModuleGroup *)loadModuleWithPIGC_ModuleController:(PIGC_ModuleController *)moduleCtrl
                                            withPIQ_QName:(PIQ_QName *)moduleID
                                     withPIMR_ModelSource:(id<PIMR_ModelSource>)modelSource
                                             withNSString:(NSString *)modelPath;

- (void)setGuiBuilderWithPIGB_GuiBuilder:(id<PIGB_GuiBuilder>)guiBuilder;

- (void)setViewFactoryWithPIGV_ViewFactory:(PIGV_ViewFactory *)viewFactory;

#pragma mark Protected

- (void)createDlgControllersWithPIMR_ModelIterator:(id<PIMR_ModelIterator>)iter
                         withPIGC_DialogController:(id<PIGC_DialogController>)dlgController;

- (PIMB_Binding *)createModelListBindingWithPIGC_ControllerGroup:(id<PIGC_ControllerGroup>)ctrlGroup
                                                   withPIQ_QName:(PIQ_QName *)dataSourceID
                                                    withNSString:(NSString *)modelPath
                                                   withPIQ_QName:(PIQ_QName *)relationID
                                                 withPIMR_Filter:(id<PIMR_Filter>)filter
                                          withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable;

- (id<PIGC_Controller>)createOneControllerWithPIMR_Model:(id<PIMR_Model>)model
                         withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup;

@end

J2OBJC_STATIC_INIT(PIGG_ControllerBuilder)

J2OBJC_FIELD_SETTER(PIGG_ControllerBuilder, app_, PIGG_Application *)

inline PIQ_Namespace *PIGG_ControllerBuilder_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_ControllerBuilder_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_TEXTCONTROLLER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_TEXTCONTROLLER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_TEXTCONTROLLER, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_DATECONTROLLER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_DATECONTROLLER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_DATECONTROLLER, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_SELECTIONCONTROLLER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_SELECTIONCONTROLLER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_SELECTIONCONTROLLER, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_TEXT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_TEXT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_TEXT, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_LABEL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_LABEL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_LABEL, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_SELECTOR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_SELECTOR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_SELECTOR, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_ID_BUTTON(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_ID_BUTTON;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, ID_BUTTON, PIQ_QName *)

inline PIQ_QName *PIGG_ControllerBuilder_get_SELECTION_MAX_ONE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ControllerBuilder_SELECTION_MAX_ONE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ControllerBuilder, SELECTION_MAX_ONE, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_ControllerBuilder_init(PIGG_ControllerBuilder *self);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_ControllerBuilder)

@compatibility_alias DePidataGuiGuidefControllerBuilder PIGG_ControllerBuilder;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefControllerBuilder")
