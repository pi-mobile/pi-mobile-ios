//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/CtrlContainerType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefCtrlContainerType")
#ifdef RESTRICT_DePidataGuiGuidefCtrlContainerType
#define INCLUDE_ALL_DePidataGuiGuidefCtrlContainerType 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefCtrlContainerType 1
#endif
#undef RESTRICT_DePidataGuiGuidefCtrlContainerType

#if !defined (PIGG_CtrlContainerType_) && (INCLUDE_ALL_DePidataGuiGuidefCtrlContainerType || defined(INCLUDE_PIGG_CtrlContainerType))
#define PIGG_CtrlContainerType_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIGG_ButtonCtrlType;
@class PIGG_ChoiceCtrlType;
@class PIGG_CodeEditorCtrlType;
@class PIGG_ColorType;
@class PIGG_CustomCtrlType;
@class PIGG_DateCtrlType;
@class PIGG_EnumCtrlType;
@class PIGG_FlagCtrlType;
@class PIGG_GroupCtrlType;
@class PIGG_HtmlEditorCtrlType;
@class PIGG_ImageCtrlType;
@class PIGG_IntegerCtrlType;
@class PIGG_ModuleCtrlType;
@class PIGG_NumberCtrlType;
@class PIGG_PaintCtrlType;
@class PIGG_PasswordCtrlType;
@class PIGG_ProgressCtrlType;
@class PIGG_TabPaneCtrlType;
@class PIGG_TableCtrlType;
@class PIGG_TextCtrlType;
@class PIGG_TextEditorCtrlType;
@class PIGG_TreeCtrlType;
@class PIGG_TreeTableCtrlType;
@class PIGG_WebViewCtrlType;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol JavaUtilCollection;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_CtrlContainerType : PIMR_SequenceModel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (void)addButtonCtrlWithPIGG_ButtonCtrlType:(PIGG_ButtonCtrlType *)buttonCtrl;

- (void)addCheckBoxCtrlWithPIGG_FlagCtrlType:(PIGG_FlagCtrlType *)checkBoxCtrl;

- (void)addChoiceCtrlWithPIGG_ChoiceCtrlType:(PIGG_ChoiceCtrlType *)choiceCtrl;

- (void)addCodeEditorCtrlWithPIGG_CodeEditorCtrlType:(PIGG_CodeEditorCtrlType *)codeEditorCtrl;

- (void)addColorWithPIGG_ColorType:(PIGG_ColorType *)color;

- (void)addCustomCtrlWithPIGG_CustomCtrlType:(PIGG_CustomCtrlType *)customCtrl;

- (void)addDateCtrlWithPIGG_DateCtrlType:(PIGG_DateCtrlType *)dateCtrl;

- (void)addEnumCtrlWithPIGG_EnumCtrlType:(PIGG_EnumCtrlType *)enumCtrl;

- (void)addGroupCtrlWithPIGG_GroupCtrlType:(PIGG_GroupCtrlType *)groupCtrl;

- (void)addHtmlEditorCtrlWithPIGG_HtmlEditorCtrlType:(PIGG_HtmlEditorCtrlType *)htmlEditorCtrl;

- (void)addImageCtrlWithPIGG_ImageCtrlType:(PIGG_ImageCtrlType *)imageCtrl;

- (void)addIntegerCtrlWithPIGG_IntegerCtrlType:(PIGG_IntegerCtrlType *)integerCtrl;

- (void)addModuleCtrlWithPIGG_ModuleCtrlType:(PIGG_ModuleCtrlType *)moduleCtrl;

- (void)addNumberCtrlWithPIGG_NumberCtrlType:(PIGG_NumberCtrlType *)numberCtrl;

- (void)addPaintCtrlWithPIGG_PaintCtrlType:(PIGG_PaintCtrlType *)paintCtrl;

- (void)addPasswordCtrlWithPIGG_PasswordCtrlType:(PIGG_PasswordCtrlType *)passwordCtrl;

- (void)addProgressCtrlWithPIGG_ProgressCtrlType:(PIGG_ProgressCtrlType *)progressCtrl;

- (void)addTableCtrlWithPIGG_TableCtrlType:(PIGG_TableCtrlType *)tableCtrl;

- (void)addTabPaneCtrlWithPIGG_TabPaneCtrlType:(PIGG_TabPaneCtrlType *)tabPaneCtrl;

- (void)addTextCtrlWithPIGG_TextCtrlType:(PIGG_TextCtrlType *)textCtrl;

- (void)addTextEditorCtrlWithPIGG_TextEditorCtrlType:(PIGG_TextEditorCtrlType *)textEditorCtrl;

- (void)addTreeCtrlWithPIGG_TreeCtrlType:(PIGG_TreeCtrlType *)treeCtrl;

- (void)addTreeTableCtrlWithPIGG_TreeTableCtrlType:(PIGG_TreeTableCtrlType *)treeTableCtrl;

- (void)addWebViewCtrlWithPIGG_WebViewCtrlType:(PIGG_WebViewCtrlType *)webViewCtrl;

- (jint)buttonCtrlCount;

- (id<PIMR_ModelIterator>)buttonCtrlIter;

- (jint)checkBoxCtrlCount;

- (id<PIMR_ModelIterator>)checkBoxCtrlIter;

- (jint)choiceCtrlCount;

- (id<PIMR_ModelIterator>)choiceCtrlIter;

- (jint)codeEditorCtrlCount;

- (id<PIMR_ModelIterator>)codeEditorCtrlIter;

- (jint)colorCount;

- (id<PIMR_ModelIterator>)colorIter;

- (jint)ctrlCount;

- (jint)customCtrlCount;

- (id<PIMR_ModelIterator>)customCtrlIter;

- (jint)dateCtrlCount;

- (id<PIMR_ModelIterator>)dateCtrlIter;

- (jint)enumCtrlCount;

- (id<PIMR_ModelIterator>)enumCtrlIter;

- (PIGG_ButtonCtrlType *)getButtonCtrlWithPIQ_Key:(id<PIQ_Key>)buttonCtrlID;

- (id<JavaUtilCollection>)getButtonCtrls;

- (PIGG_FlagCtrlType *)getCheckBoxCtrlWithPIQ_Key:(id<PIQ_Key>)checkBoxCtrlID;

- (id<JavaUtilCollection>)getCheckBoxCtrls;

- (PIGG_ChoiceCtrlType *)getChoiceCtrlWithPIQ_Key:(id<PIQ_Key>)choiceCtrlID;

- (id<JavaUtilCollection>)getChoiceCtrls;

- (PIGG_CodeEditorCtrlType *)getCodeEditorCtrlWithPIQ_Key:(id<PIQ_Key>)codeEditorCtrlID;

- (id<JavaUtilCollection>)getCodeEditorCtrls;

- (PIGG_ColorType *)getColorWithPIQ_Key:(id<PIQ_Key>)colorID;

- (id<JavaUtilCollection>)getColors;

- (PIGG_CustomCtrlType *)getCustomCtrlWithPIQ_Key:(id<PIQ_Key>)customCtrlID;

- (id<JavaUtilCollection>)getCustomCtrls;

- (PIGG_DateCtrlType *)getDateCtrlWithPIQ_Key:(id<PIQ_Key>)dateCtrlID;

- (id<JavaUtilCollection>)getDateCtrls;

- (PIGG_EnumCtrlType *)getEnumCtrlWithPIQ_Key:(id<PIQ_Key>)enumCtrlID;

- (id<JavaUtilCollection>)getEnumCtrls;

- (PIGG_GroupCtrlType *)getGroupCtrlWithPIQ_Key:(id<PIQ_Key>)groupCtrlID;

- (id<JavaUtilCollection>)getGroupCtrls;

- (PIGG_HtmlEditorCtrlType *)getHtmlEditorCtrlWithPIQ_Key:(id<PIQ_Key>)htmlEditorCtrlID;

- (id<JavaUtilCollection>)getHtmlEditorCtrls;

- (PIGG_ImageCtrlType *)getImageCtrlWithPIQ_Key:(id<PIQ_Key>)imageCtrlID;

- (id<JavaUtilCollection>)getImageCtrls;

- (PIGG_IntegerCtrlType *)getIntegerCtrlWithPIQ_Key:(id<PIQ_Key>)integerCtrlID;

- (id<JavaUtilCollection>)getIntegerCtrls;

- (PIGG_ModuleCtrlType *)getModuleCtrlWithPIQ_Key:(id<PIQ_Key>)moduleCtrlID;

- (id<JavaUtilCollection>)getModuleCtrls;

- (PIGG_NumberCtrlType *)getNumberCtrlWithPIQ_Key:(id<PIQ_Key>)numberCtrlID;

- (id<JavaUtilCollection>)getNumberCtrls;

- (PIGG_PaintCtrlType *)getPaintCtrlWithPIQ_Key:(id<PIQ_Key>)paintCtrlID;

- (id<JavaUtilCollection>)getPaintCtrls;

- (PIGG_PasswordCtrlType *)getPasswordCtrlWithPIQ_Key:(id<PIQ_Key>)passwordCtrlID;

- (id<JavaUtilCollection>)getPasswordCtrls;

- (PIGG_ProgressCtrlType *)getProgressCtrlWithPIQ_Key:(id<PIQ_Key>)progressCtrlID;

- (id<JavaUtilCollection>)getProgressCtrls;

- (PIGG_TableCtrlType *)getTableCtrlWithPIQ_Key:(id<PIQ_Key>)tableCtrlID;

- (id<JavaUtilCollection>)getTableCtrls;

- (PIGG_TabPaneCtrlType *)getTabPaneCtrlWithPIQ_Key:(id<PIQ_Key>)tabPaneCtrlID;

- (id<JavaUtilCollection>)getTabPaneCtrls;

- (PIGG_TextCtrlType *)getTextCtrlWithPIQ_Key:(id<PIQ_Key>)textCtrlID;

- (id<JavaUtilCollection>)getTextCtrls;

- (PIGG_TextEditorCtrlType *)getTextEditorCtrlWithPIQ_Key:(id<PIQ_Key>)textEditorCtrlID;

- (id<JavaUtilCollection>)getTextEditorCtrls;

- (PIGG_TreeCtrlType *)getTreeCtrlWithPIQ_Key:(id<PIQ_Key>)treeCtrlID;

- (id<JavaUtilCollection>)getTreeCtrls;

- (PIGG_TreeTableCtrlType *)getTreeTableCtrlWithPIQ_Key:(id<PIQ_Key>)treeTableCtrlID;

- (id<JavaUtilCollection>)getTreeTableCtrls;

- (PIGG_WebViewCtrlType *)getWebViewCtrlWithPIQ_Key:(id<PIQ_Key>)webViewCtrlID;

- (id<JavaUtilCollection>)getWebViewCtrls;

- (jint)groupCtrlCount;

- (id<PIMR_ModelIterator>)groupCtrlIter;

- (jint)htmlEditorCtrlCount;

- (id<PIMR_ModelIterator>)htmlEditorCtrlIter;

- (jint)imageCtrlCount;

- (id<PIMR_ModelIterator>)imageCtrlIter;

- (jint)integerCtrlCount;

- (id<PIMR_ModelIterator>)integerCtrlIter;

- (jint)moduleCtrlCount;

- (id<PIMR_ModelIterator>)moduleCtrlIter;

- (jint)numberCtrlCount;

- (id<PIMR_ModelIterator>)numberCtrlIter;

- (jint)paintCtrlCount;

- (id<PIMR_ModelIterator>)paintCtrlIter;

- (jint)passwordCtrlCount;

- (id<PIMR_ModelIterator>)passwordCtrlIter;

- (jint)progressCtrlCount;

- (id<PIMR_ModelIterator>)progressCtrlIter;

- (void)removeButtonCtrlWithPIGG_ButtonCtrlType:(PIGG_ButtonCtrlType *)buttonCtrl;

- (void)removeCheckBoxCtrlWithPIGG_FlagCtrlType:(PIGG_FlagCtrlType *)checkBoxCtrl;

- (void)removeChoiceCtrlWithPIGG_ChoiceCtrlType:(PIGG_ChoiceCtrlType *)choiceCtrl;

- (void)removeCodeEditorCtrlWithPIGG_CodeEditorCtrlType:(PIGG_CodeEditorCtrlType *)codeEditorCtrl;

- (void)removeColorWithPIGG_ColorType:(PIGG_ColorType *)color;

- (void)removeCustomCtrlWithPIGG_CustomCtrlType:(PIGG_CustomCtrlType *)customCtrl;

- (void)removeDateCtrlWithPIGG_DateCtrlType:(PIGG_DateCtrlType *)dateCtrl;

- (void)removeEnumCtrlWithPIGG_EnumCtrlType:(PIGG_EnumCtrlType *)enumCtrl;

- (void)removeGroupCtrlWithPIGG_GroupCtrlType:(PIGG_GroupCtrlType *)groupCtrl;

- (void)removeHtmlEditorCtrlWithPIGG_HtmlEditorCtrlType:(PIGG_HtmlEditorCtrlType *)htmlEditorCtrl;

- (void)removeImageCtrlWithPIGG_ImageCtrlType:(PIGG_ImageCtrlType *)imageCtrl;

- (void)removeIntegerCtrlWithPIGG_IntegerCtrlType:(PIGG_IntegerCtrlType *)integerCtrl;

- (void)removeModuleCtrlWithPIGG_ModuleCtrlType:(PIGG_ModuleCtrlType *)moduleCtrl;

- (void)removeNumberCtrlWithPIGG_NumberCtrlType:(PIGG_NumberCtrlType *)numberCtrl;

- (void)removePaintCtrlWithPIGG_PaintCtrlType:(PIGG_PaintCtrlType *)paintCtrl;

- (void)removePasswordCtrlWithPIGG_PasswordCtrlType:(PIGG_PasswordCtrlType *)passwordCtrl;

- (void)removeProgressCtrlWithPIGG_ProgressCtrlType:(PIGG_ProgressCtrlType *)progressCtrl;

- (void)removeTableCtrlWithPIGG_TableCtrlType:(PIGG_TableCtrlType *)tableCtrl;

- (void)removeTabPaneCtrlWithPIGG_TabPaneCtrlType:(PIGG_TabPaneCtrlType *)tabPaneCtrl;

- (void)removeTextCtrlWithPIGG_TextCtrlType:(PIGG_TextCtrlType *)textCtrl;

- (void)removeTextEditorCtrlWithPIGG_TextEditorCtrlType:(PIGG_TextEditorCtrlType *)textEditorCtrl;

- (void)removeTreeCtrlWithPIGG_TreeCtrlType:(PIGG_TreeCtrlType *)treeCtrl;

- (void)removeTreeTableCtrlWithPIGG_TreeTableCtrlType:(PIGG_TreeTableCtrlType *)treeTableCtrl;

- (void)removeWebViewCtrlWithPIGG_WebViewCtrlType:(PIGG_WebViewCtrlType *)webViewCtrl;

- (jint)tableCtrlCount;

- (id<PIMR_ModelIterator>)tableCtrlIter;

- (jint)tabPaneCtrlCount;

- (id<PIMR_ModelIterator>)tabPaneCtrlIter;

- (jint)textCtrlCount;

- (id<PIMR_ModelIterator>)textCtrlIter;

- (jint)textEditorCtrlCount;

- (id<PIMR_ModelIterator>)textEditorCtrlIter;

- (jint)treeCtrlCount;

- (id<PIMR_ModelIterator>)treeCtrlIter;

- (jint)treeTableCtrlCount;

- (id<PIMR_ModelIterator>)treeTableCtrlIter;

- (jint)webViewCtrlCount;

- (id<PIMR_ModelIterator>)webViewCtrlIter;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGG_CtrlContainerType)

inline PIQ_Namespace *PIGG_CtrlContainerType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_CtrlContainerType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_BUTTONCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_BUTTONCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_BUTTONCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_CHECKBOXCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_CHECKBOXCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_CHECKBOXCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_CHOICECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_CHOICECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_CHOICECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_CODEEDITORCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_CODEEDITORCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_CODEEDITORCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_COLOR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_COLOR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_COLOR, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_CUSTOMCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_CUSTOMCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_CUSTOMCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_DATECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_DATECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_DATECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_ENUMCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_ENUMCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_ENUMCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_GROUPCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_GROUPCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_GROUPCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_HTMLEDITORCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_HTMLEDITORCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_HTMLEDITORCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_IMAGECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_IMAGECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_IMAGECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_INTEGERCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_INTEGERCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_INTEGERCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_MODULECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_MODULECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_MODULECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_NUMBERCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_NUMBERCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_NUMBERCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_PAINTCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_PAINTCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_PAINTCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_PASSWORDCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_PASSWORDCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_PASSWORDCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_PROGRESSCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_PROGRESSCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_PROGRESSCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TABLECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TABLECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TABLECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TABPANECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TABPANECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TABPANECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TEXTCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TEXTCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TEXTCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TEXTEDITORCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TEXTEDITORCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TEXTEDITORCTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TREECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TREECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TREECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_TREETABLECTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_TREETABLECTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_TREETABLECTRL, PIQ_QName *)

inline PIQ_QName *PIGG_CtrlContainerType_get_ID_WEBVIEWCTRL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_CtrlContainerType_ID_WEBVIEWCTRL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_CtrlContainerType, ID_WEBVIEWCTRL, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_CtrlContainerType_init(PIGG_CtrlContainerType *self);

FOUNDATION_EXPORT void PIGG_CtrlContainerType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_CtrlContainerType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_CtrlContainerType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_CtrlContainerType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_CtrlContainerType)

@compatibility_alias DePidataGuiGuidefCtrlContainerType PIGG_CtrlContainerType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefCtrlContainerType")
