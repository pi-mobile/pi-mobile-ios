//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/EnumCtrlType.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/DefaultListController.h"
#include "de/pidata/gui/controller/base/ListController.h"
#include "de/pidata/gui/controller/base/MutableControllerGroup.h"
#include "de/pidata/gui/controller/base/ValueController.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/gui/guidef/ControllerFactory.h"
#include "de/pidata/gui/guidef/EnumCtrlType.h"
#include "de/pidata/gui/guidef/ValueCtrlType.h"
#include "de/pidata/gui/view/base/ListViewPI.h"
#include "de/pidata/gui/view/base/ViewFactory.h"
#include "de/pidata/models/binding/Binding.h"
#include "de/pidata/models/binding/EnumSelection.h"
#include "de/pidata/models/binding/Selection.h"
#include "de/pidata/models/binding/SimpleModelSource.h"
#include "de/pidata/models/simpleModels/SimpleStringTable.h"
#include "de/pidata/models/simpleModels/StringMap.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Context.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/EnumCtrlType must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGG_EnumCtrlType)

PIQ_Namespace *PIGG_EnumCtrlType_NAMESPACE;
PIQ_QName *PIGG_EnumCtrlType_ID_ENUMCOMPID;
PIQ_QName *PIGG_EnumCtrlType_ID_ROWCOMPID;

@implementation PIGG_EnumCtrlType

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIGG_EnumCtrlType_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_EnumCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_EnumCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getEnumCompID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_EnumCtrlType_ID_ENUMCOMPID], [PIQ_QName class]);
}

- (void)setEnumCompIDWithPIQ_QName:(PIQ_QName *)enumCompID {
  [self setWithPIQ_QName:PIGG_EnumCtrlType_ID_ENUMCOMPID withId:enumCompID];
}

- (PIQ_QName *)getRowCompID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_EnumCtrlType_ID_ROWCOMPID], [PIQ_QName class]);
}

- (void)setRowCompIDWithPIQ_QName:(PIQ_QName *)rowCompID {
  [self setWithPIQ_QName:PIGG_EnumCtrlType_ID_ROWCOMPID withId:rowCompID];
}

- (id<PIGC_ValueController>)createControllerWithPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                                       withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup {
  id<PIGC_ListController> listController;
  NSString *classname = [self getClassname];
  if ((classname == nil) || ([((NSString *) nil_chk(classname)) java_length] == 0)) {
    listController = new_PIGC_DefaultListController_init();
  }
  else {
    listController = (id<PIGC_ListController>) cast_check([((IOSClass *) nil_chk(IOSClass_forName_(classname))) newInstance], PIGC_ListController_class_());
  }
  PIQ_QName *enumCompID = [self getEnumCompID];
  if (enumCompID == nil) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@$", @"Enum controller ID=", [self getID], @": comp is missing"));
  }
  PIGV_ListViewPI *listViewPI = [((PIGV_ViewFactory *) nil_chk([((PIGG_ControllerBuilder *) nil_chk(builder)) getViewFactory])) createListViewWithPIQ_QName:enumCompID withPIQ_QName:[self getRowCompID] withPIGC_ListController:listController];
  PIMB_Binding *binding = [self createBindingWithPIGG_ControllerBuilder:builder withPIGC_MutableControllerGroup:ctrlGroup];
  id<PIMR_Model> model = new_PIMSM_SimpleStringTable_init();
  PIQ_QName *keyAttrID = JreLoadStatic(PIMSM_StringMap, ID_ID);
  PIQ_QName *valueID = JreLoadStatic(PIMSM_StringMap, ID_VALUE);
  PIMB_SimpleModelSource *modelSource = new_PIMB_SimpleModelSource_initWithPIMR_Context_withPIMR_Model_([((id<PIGC_MutableControllerGroup>) nil_chk(ctrlGroup)) getContext], model);
  PIQ_QName *tableRelationID = JreLoadStatic(PIMSM_SimpleStringTable, ID_STRINGMAPENTRY);
  PIMB_Selection *selection = new_PIMB_EnumSelection_initWithPIMR_Context_withPIQ_NamespaceTable_withPIMR_ModelSource_withPIQ_QName_withPIMB_Binding_withPIMR_Filter_withBoolean_([ctrlGroup getContext], [self namespaceTable], modelSource, tableRelationID, binding, nil, true);
  [((id<PIGC_ListController>) nil_chk(listController)) init__WithPIQ_QName:[self getID] withPIGC_Controller:ctrlGroup withPIGV_ListViewPI:listViewPI withPIMB_Binding:binding withPIMB_Selection:selection withPIQ_QName:keyAttrID withPIMR_XPath:nil withPIQ_QName:valueID withBoolean:[self readOnly]];
  [self initBindingsWithPIGC_ControllerGroup:ctrlGroup withPIGC_ValueController:listController];
  return listController;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "LPIGC_ValueController;", 0x1, 6, 7, 8, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getEnumCompID);
  methods[4].selector = @selector(setEnumCompIDWithPIQ_QName:);
  methods[5].selector = @selector(getRowCompID);
  methods[6].selector = @selector(setRowCompIDWithPIQ_QName:);
  methods[7].selector = @selector(createControllerWithPIGG_ControllerBuilder:withPIGC_MutableControllerGroup:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
    { "ID_ENUMCOMPID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 10, -1, -1 },
    { "ID_ROWCOMPID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setEnumCompID", "LPIQ_QName;", "setRowCompID", "createController", "LPIGG_ControllerBuilder;LPIGC_MutableControllerGroup;", "LJavaLangException;", &PIGG_EnumCtrlType_NAMESPACE, &PIGG_EnumCtrlType_ID_ENUMCOMPID, &PIGG_EnumCtrlType_ID_ROWCOMPID };
  static const J2ObjcClassInfo _PIGG_EnumCtrlType = { "EnumCtrlType", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x1, 8, 3, -1, -1, -1, -1, -1 };
  return &_PIGG_EnumCtrlType;
}

+ (void)initialize {
  if (self == [PIGG_EnumCtrlType class]) {
    PIGG_EnumCtrlType_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIGG_EnumCtrlType_ID_ENUMCOMPID = [((PIQ_Namespace *) nil_chk(PIGG_EnumCtrlType_NAMESPACE)) getQNameWithNSString:@"enumCompID"];
    PIGG_EnumCtrlType_ID_ROWCOMPID = [PIGG_EnumCtrlType_NAMESPACE getQNameWithNSString:@"rowCompID"];
    J2OBJC_SET_INITIALIZED(PIGG_EnumCtrlType)
  }
}

@end

void PIGG_EnumCtrlType_initWithPIQ_Key_(PIGG_EnumCtrlType *self, id<PIQ_Key> id_) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIGG_ControllerFactory, ENUMCTRLTYPE_TYPE), nil, nil, nil);
}

PIGG_EnumCtrlType *new_PIGG_EnumCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_, id_)
}

PIGG_EnumCtrlType *create_PIGG_EnumCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_, id_)
}

void PIGG_EnumCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_EnumCtrlType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIGG_ControllerFactory, ENUMCTRLTYPE_TYPE), attributeNames, anyAttribs, childNames);
}

PIGG_EnumCtrlType *new_PIGG_EnumCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIGG_EnumCtrlType *create_PIGG_EnumCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIGG_EnumCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_EnumCtrlType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIGG_EnumCtrlType *new_PIGG_EnumCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIGG_EnumCtrlType *create_PIGG_EnumCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_EnumCtrlType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_EnumCtrlType)

J2OBJC_NAME_MAPPING(PIGG_EnumCtrlType, "de.pidata.gui.guidef", "PIGG_")
