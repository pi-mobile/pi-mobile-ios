//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/ImageCtrlType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefImageCtrlType")
#ifdef RESTRICT_DePidataGuiGuidefImageCtrlType
#define INCLUDE_ALL_DePidataGuiGuidefImageCtrlType 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefImageCtrlType 1
#endif
#undef RESTRICT_DePidataGuiGuidefImageCtrlType

#if !defined (PIGG_ImageCtrlType_) && (INCLUDE_ALL_DePidataGuiGuidefImageCtrlType || defined(INCLUDE_PIGG_ImageCtrlType))
#define PIGG_ImageCtrlType_

#define RESTRICT_DePidataGuiGuidefValueCtrlType 1
#define INCLUDE_PIGG_ValueCtrlType 1
#include "de/pidata/gui/guidef/ValueCtrlType.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIGG_ControllerBuilder;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIGC_MutableControllerGroup;
@protocol PIGC_ValueController;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_ImageCtrlType : PIGG_ValueCtrlType

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (id<PIGC_ValueController>)createControllerWithPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                                       withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup;

- (PIQ_QName *)getImageCompID;

- (void)setImageCompIDWithPIQ_QName:(PIQ_QName *)imageCompID;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

@end

J2OBJC_STATIC_INIT(PIGG_ImageCtrlType)

inline PIQ_Namespace *PIGG_ImageCtrlType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_ImageCtrlType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ImageCtrlType, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_ImageCtrlType_get_ID_IMAGECOMPID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_ImageCtrlType_ID_IMAGECOMPID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_ImageCtrlType, ID_IMAGECOMPID, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_ImageCtrlType_initWithPIQ_Key_(PIGG_ImageCtrlType *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIGG_ImageCtrlType *new_PIGG_ImageCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ImageCtrlType *create_PIGG_ImageCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIGG_ImageCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_ImageCtrlType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_ImageCtrlType *new_PIGG_ImageCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ImageCtrlType *create_PIGG_ImageCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_ImageCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_ImageCtrlType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_ImageCtrlType *new_PIGG_ImageCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_ImageCtrlType *create_PIGG_ImageCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_ImageCtrlType)

@compatibility_alias DePidataGuiGuidefImageCtrlType PIGG_ImageCtrlType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefImageCtrlType")
