//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/InplaceEdit.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/InplaceEdit.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/InplaceEdit must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGG_InplaceEdit () {
 @public
  NSString *value_;
}

@end

J2OBJC_FIELD_SETTER(PIGG_InplaceEdit, value_, NSString *)

__attribute__((unused)) static void PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(PIGG_InplaceEdit *self, NSString *value, NSString *__name, jint __ordinal);

__attribute__((unused)) static PIGG_InplaceEdit *new_PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) NS_RETURNS_RETAINED;

J2OBJC_INITIALIZED_DEFN(PIGG_InplaceEdit)

PIGG_InplaceEdit *PIGG_InplaceEdit_values_[3];

@implementation PIGG_InplaceEdit

- (NSString *)getValue {
  return self->value_;
}

+ (PIGG_InplaceEdit *)fromStringWithNSString:(NSString *)stringValue {
  return PIGG_InplaceEdit_fromStringWithNSString_(stringValue);
}

- (NSString *)description {
  return value_;
}

+ (IOSObjectArray *)values {
  return PIGG_InplaceEdit_values();
}

+ (PIGG_InplaceEdit *)valueOfWithNSString:(NSString *)name {
  return PIGG_InplaceEdit_valueOfWithNSString_(name);
}

- (PIGG_InplaceEdit_Enum)toNSEnum {
  return (PIGG_InplaceEdit_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGG_InplaceEdit;", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
    { NULL, "[LPIGG_InplaceEdit;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGG_InplaceEdit;", 0x9, 3, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getValue);
  methods[1].selector = @selector(fromStringWithNSString:);
  methods[2].selector = @selector(description);
  methods[3].selector = @selector(values);
  methods[4].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "none", "LPIGG_InplaceEdit;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
    { "singleclick", "LPIGG_InplaceEdit;", .constantValue.asLong = 0, 0x4019, -1, 5, -1, -1 },
    { "doubleclick", "LPIGG_InplaceEdit;", .constantValue.asLong = 0, 0x4019, -1, 6, -1, -1 },
    { "value_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "fromString", "LNSString;", "toString", "valueOf", &JreEnum(PIGG_InplaceEdit, none), &JreEnum(PIGG_InplaceEdit, singleclick), &JreEnum(PIGG_InplaceEdit, doubleclick), "Ljava/lang/Enum<Lde/pidata/gui/guidef/InplaceEdit;>;" };
  static const J2ObjcClassInfo _PIGG_InplaceEdit = { "InplaceEdit", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x4011, 5, 4, -1, -1, -1, 7, -1 };
  return &_PIGG_InplaceEdit;
}

+ (void)initialize {
  if (self == [PIGG_InplaceEdit class]) {
    JreEnum(PIGG_InplaceEdit, none) = new_PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(@"none", JreEnumConstantName(PIGG_InplaceEdit_class_(), 0), 0);
    JreEnum(PIGG_InplaceEdit, singleclick) = new_PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(@"singleclick", JreEnumConstantName(PIGG_InplaceEdit_class_(), 1), 1);
    JreEnum(PIGG_InplaceEdit, doubleclick) = new_PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(@"doubleclick", JreEnumConstantName(PIGG_InplaceEdit_class_(), 2), 2);
    J2OBJC_SET_INITIALIZED(PIGG_InplaceEdit)
  }
}

@end

void PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(PIGG_InplaceEdit *self, NSString *value, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
  self->value_ = value;
}

PIGG_InplaceEdit *new_PIGG_InplaceEdit_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) {
  J2OBJC_NEW_IMPL(PIGG_InplaceEdit, initWithNSString_withNSString_withInt_, value, __name, __ordinal)
}

PIGG_InplaceEdit *PIGG_InplaceEdit_fromStringWithNSString_(NSString *stringValue) {
  PIGG_InplaceEdit_initialize();
  if (stringValue == nil) {
    return nil;
  }
  {
    IOSObjectArray *a__ = PIGG_InplaceEdit_values();
    PIGG_InplaceEdit * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
    PIGG_InplaceEdit * const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      PIGG_InplaceEdit *entry_ = *b__++;
      if ([((NSString *) nil_chk(((PIGG_InplaceEdit *) nil_chk(entry_))->value_)) isEqual:stringValue]) {
        return entry_;
      }
    }
  }
  return nil;
}

IOSObjectArray *PIGG_InplaceEdit_values() {
  PIGG_InplaceEdit_initialize();
  return [IOSObjectArray arrayWithObjects:PIGG_InplaceEdit_values_ count:3 type:PIGG_InplaceEdit_class_()];
}

PIGG_InplaceEdit *PIGG_InplaceEdit_valueOfWithNSString_(NSString *name) {
  PIGG_InplaceEdit_initialize();
  for (int i = 0; i < 3; i++) {
    PIGG_InplaceEdit *e = PIGG_InplaceEdit_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

PIGG_InplaceEdit *PIGG_InplaceEdit_fromOrdinal(NSUInteger ordinal) {
  PIGG_InplaceEdit_initialize();
  if (ordinal >= 3) {
    return nil;
  }
  return PIGG_InplaceEdit_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_InplaceEdit)

J2OBJC_NAME_MAPPING(PIGG_InplaceEdit, "de.pidata.gui.guidef", "PIGG_")
