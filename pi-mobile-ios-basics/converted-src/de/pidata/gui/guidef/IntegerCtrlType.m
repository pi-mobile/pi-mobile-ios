//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/IntegerCtrlType.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/IntegerController.h"
#include "de/pidata/gui/controller/base/MutableControllerGroup.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/gui/guidef/ControllerFactory.h"
#include "de/pidata/gui/guidef/IntegerCtrlType.h"
#include "de/pidata/gui/guidef/ValueCtrlType.h"
#include "de/pidata/gui/view/base/TextViewPI.h"
#include "de/pidata/gui/view/base/ViewFactory.h"
#include "de/pidata/models/binding/Binding.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/IntegerCtrlType must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGG_IntegerCtrlType)

PIQ_Namespace *PIGG_IntegerCtrlType_NAMESPACE;
PIQ_QName *PIGG_IntegerCtrlType_ID_INTEGERCOMPID;

@implementation PIGG_IntegerCtrlType

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIGG_IntegerCtrlType_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_IntegerCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_IntegerCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getIntegerCompID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_IntegerCtrlType_ID_INTEGERCOMPID], [PIQ_QName class]);
}

- (void)setIntegerCompIDWithPIQ_QName:(PIQ_QName *)integerCompID {
  [self setWithPIQ_QName:PIGG_IntegerCtrlType_ID_INTEGERCOMPID withId:integerCompID];
}

- (PIGC_IntegerController *)createControllerWithPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                                       withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup {
  PIGC_IntegerController *integerController;
  NSString *classname = [self getClassname];
  if ((classname == nil) || ([((NSString *) nil_chk(classname)) java_length] == 0)) {
    integerController = new_PIGC_IntegerController_init();
  }
  else {
    integerController = (PIGC_IntegerController *) cast_chk([((IOSClass *) nil_chk(IOSClass_forName_(classname))) newInstance], [PIGC_IntegerController class]);
  }
  PIQ_QName *integerCompID = [self getIntegerCompID];
  if (integerCompID == nil) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@$", @"Integer controller ID=", [self getID], @": comp is missing"));
  }
  PIGV_TextViewPI *textView = [((PIGV_ViewFactory *) nil_chk([((PIGG_ControllerBuilder *) nil_chk(builder)) getViewFactory])) createTextViewWithPIQ_QName:integerCompID];
  PIQ_Namespace *namespace_ = [self namespace__];
  if (namespace_ != nil) {
    [((PIGC_IntegerController *) nil_chk(integerController)) setNamespaceWithPIQ_Namespace:namespace_];
  }
  PIMB_Binding *binding = [self createBindingWithPIGG_ControllerBuilder:builder withPIGC_MutableControllerGroup:ctrlGroup];
  [((PIGC_IntegerController *) nil_chk(integerController)) init__WithPIQ_QName:[self getID] withPIGC_Controller:ctrlGroup withPIGV_TextViewPI:textView withPIMB_Binding:binding withBoolean:[self readOnly]];
  [self initBindingsWithPIGC_ControllerGroup:ctrlGroup withPIGC_ValueController:integerController];
  return integerController;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIGC_IntegerController;", 0x1, 5, 6, 7, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getIntegerCompID);
  methods[4].selector = @selector(setIntegerCompIDWithPIQ_QName:);
  methods[5].selector = @selector(createControllerWithPIGG_ControllerBuilder:withPIGC_MutableControllerGroup:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 8, -1, -1 },
    { "ID_INTEGERCOMPID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setIntegerCompID", "LPIQ_QName;", "createController", "LPIGG_ControllerBuilder;LPIGC_MutableControllerGroup;", "LJavaLangException;", &PIGG_IntegerCtrlType_NAMESPACE, &PIGG_IntegerCtrlType_ID_INTEGERCOMPID };
  static const J2ObjcClassInfo _PIGG_IntegerCtrlType = { "IntegerCtrlType", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x1, 6, 2, -1, -1, -1, -1, -1 };
  return &_PIGG_IntegerCtrlType;
}

+ (void)initialize {
  if (self == [PIGG_IntegerCtrlType class]) {
    PIGG_IntegerCtrlType_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIGG_IntegerCtrlType_ID_INTEGERCOMPID = [((PIQ_Namespace *) nil_chk(PIGG_IntegerCtrlType_NAMESPACE)) getQNameWithNSString:@"integerCompID"];
    J2OBJC_SET_INITIALIZED(PIGG_IntegerCtrlType)
  }
}

@end

void PIGG_IntegerCtrlType_initWithPIQ_Key_(PIGG_IntegerCtrlType *self, id<PIQ_Key> id_) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIGG_ControllerFactory, INTEGERCTRLTYPE_TYPE), nil, nil, nil);
}

PIGG_IntegerCtrlType *new_PIGG_IntegerCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_, id_)
}

PIGG_IntegerCtrlType *create_PIGG_IntegerCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_, id_)
}

void PIGG_IntegerCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_IntegerCtrlType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIGG_ControllerFactory, INTEGERCTRLTYPE_TYPE), attributeNames, anyAttribs, childNames);
}

PIGG_IntegerCtrlType *new_PIGG_IntegerCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIGG_IntegerCtrlType *create_PIGG_IntegerCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIGG_IntegerCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_IntegerCtrlType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIGG_ValueCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIGG_IntegerCtrlType *new_PIGG_IntegerCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIGG_IntegerCtrlType *create_PIGG_IntegerCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_IntegerCtrlType, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_IntegerCtrlType)

J2OBJC_NAME_MAPPING(PIGG_IntegerCtrlType, "de.pidata.gui.guidef", "PIGG_")
