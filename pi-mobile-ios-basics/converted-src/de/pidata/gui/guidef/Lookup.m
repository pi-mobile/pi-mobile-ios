//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/Lookup.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/ControllerFactory.h"
#include "de/pidata/gui/guidef/FilterDef.h"
#include "de/pidata/gui/guidef/Lookup.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/Lookup must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIGG_Lookup)

PIQ_Namespace *PIGG_Lookup_NAMESPACE;
PIQ_QName *PIGG_Lookup_ID_DATASOURCE;
PIQ_QName *PIGG_Lookup_ID_FILTERDEF;
PIQ_QName *PIGG_Lookup_ID_ID;
PIQ_QName *PIGG_Lookup_ID_KEYATTR;
PIQ_QName *PIGG_Lookup_ID_MODEL;
PIQ_QName *PIGG_Lookup_ID_RELATIONID;
PIQ_QName *PIGG_Lookup_ID_SELTYPE;
PIQ_QName *PIGG_Lookup_ID_VALUEID;
PIQ_QName *PIGG_Lookup_ID_VALUEPATH;

@implementation PIGG_Lookup

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGG_Lookup_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_Lookup_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_Lookup_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_ID], [PIQ_QName class]);
}

- (void)setIDWithPIQ_QName:(PIQ_QName *)iD {
  [self setWithPIQ_QName:PIGG_Lookup_ID_ID withId:iD];
}

- (PIQ_QName *)getSelType {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_SELTYPE], [PIQ_QName class]);
}

- (void)setSelTypeWithPIQ_QName:(PIQ_QName *)selType {
  [self setWithPIQ_QName:PIGG_Lookup_ID_SELTYPE withId:selType];
}

- (PIQ_QName *)getDataSource {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_DATASOURCE], [PIQ_QName class]);
}

- (void)setDataSourceWithPIQ_QName:(PIQ_QName *)dataSource {
  [self setWithPIQ_QName:PIGG_Lookup_ID_DATASOURCE withId:dataSource];
}

- (NSString *)getModel {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_MODEL], [NSString class]);
}

- (void)setModelWithNSString:(NSString *)model {
  [self setWithPIQ_QName:PIGG_Lookup_ID_MODEL withId:model];
}

- (PIQ_QName *)getRelationID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_RELATIONID], [PIQ_QName class]);
}

- (void)setRelationIDWithPIQ_QName:(PIQ_QName *)relationID {
  [self setWithPIQ_QName:PIGG_Lookup_ID_RELATIONID withId:relationID];
}

- (NSString *)getValuePath {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_VALUEPATH], [NSString class]);
}

- (void)setValuePathWithNSString:(NSString *)valuePath {
  [self setWithPIQ_QName:PIGG_Lookup_ID_VALUEPATH withId:valuePath];
}

- (PIQ_QName *)getValueID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_VALUEID], [PIQ_QName class]);
}

- (void)setValueIDWithPIQ_QName:(PIQ_QName *)valueID {
  [self setWithPIQ_QName:PIGG_Lookup_ID_VALUEID withId:valueID];
}

- (PIQ_QName *)getKeyAttr {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_KEYATTR], [PIQ_QName class]);
}

- (void)setKeyAttrWithPIQ_QName:(PIQ_QName *)keyAttr {
  [self setWithPIQ_QName:PIGG_Lookup_ID_KEYATTR withId:keyAttr];
}

- (PIGG_FilterDef *)getFilterDef {
  return (PIGG_FilterDef *) cast_chk([self getWithPIQ_QName:PIGG_Lookup_ID_FILTERDEF withPIQ_Key:nil], [PIGG_FilterDef class]);
}

- (void)setFilterDefWithPIGG_FilterDef:(PIGG_FilterDef *)filterDef {
  [self setChildWithPIQ_QName:PIGG_Lookup_ID_FILTERDEF withPIMR_Model:filterDef];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 3, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 3, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 7, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 3, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 3, -1, -1, -1, -1 },
    { NULL, "LPIGG_FilterDef;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getID);
  methods[4].selector = @selector(setIDWithPIQ_QName:);
  methods[5].selector = @selector(getSelType);
  methods[6].selector = @selector(setSelTypeWithPIQ_QName:);
  methods[7].selector = @selector(getDataSource);
  methods[8].selector = @selector(setDataSourceWithPIQ_QName:);
  methods[9].selector = @selector(getModel);
  methods[10].selector = @selector(setModelWithNSString:);
  methods[11].selector = @selector(getRelationID);
  methods[12].selector = @selector(setRelationIDWithPIQ_QName:);
  methods[13].selector = @selector(getValuePath);
  methods[14].selector = @selector(setValuePathWithNSString:);
  methods[15].selector = @selector(getValueID);
  methods[16].selector = @selector(setValueIDWithPIQ_QName:);
  methods[17].selector = @selector(getKeyAttr);
  methods[18].selector = @selector(setKeyAttrWithPIQ_QName:);
  methods[19].selector = @selector(getFilterDef);
  methods[20].selector = @selector(setFilterDefWithPIGG_FilterDef:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
    { "ID_DATASOURCE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "ID_FILTERDEF", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 16, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 17, -1, -1 },
    { "ID_KEYATTR", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 18, -1, -1 },
    { "ID_MODEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 19, -1, -1 },
    { "ID_RELATIONID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 20, -1, -1 },
    { "ID_SELTYPE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 21, -1, -1 },
    { "ID_VALUEID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 22, -1, -1 },
    { "ID_VALUEPATH", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setID", "LPIQ_QName;", "setSelType", "setDataSource", "setModel", "LNSString;", "setRelationID", "setValuePath", "setValueID", "setKeyAttr", "setFilterDef", "LPIGG_FilterDef;", &PIGG_Lookup_NAMESPACE, &PIGG_Lookup_ID_DATASOURCE, &PIGG_Lookup_ID_FILTERDEF, &PIGG_Lookup_ID_ID, &PIGG_Lookup_ID_KEYATTR, &PIGG_Lookup_ID_MODEL, &PIGG_Lookup_ID_RELATIONID, &PIGG_Lookup_ID_SELTYPE, &PIGG_Lookup_ID_VALUEID, &PIGG_Lookup_ID_VALUEPATH };
  static const J2ObjcClassInfo _PIGG_Lookup = { "Lookup", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x1, 21, 10, -1, -1, -1, -1, -1 };
  return &_PIGG_Lookup;
}

+ (void)initialize {
  if (self == [PIGG_Lookup class]) {
    PIGG_Lookup_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIGG_Lookup_ID_DATASOURCE = [((PIQ_Namespace *) nil_chk(PIGG_Lookup_NAMESPACE)) getQNameWithNSString:@"dataSource"];
    PIGG_Lookup_ID_FILTERDEF = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"filterDef"];
    PIGG_Lookup_ID_ID = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"ID"];
    PIGG_Lookup_ID_KEYATTR = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"keyAttr"];
    PIGG_Lookup_ID_MODEL = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"model"];
    PIGG_Lookup_ID_RELATIONID = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"relationID"];
    PIGG_Lookup_ID_SELTYPE = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"selType"];
    PIGG_Lookup_ID_VALUEID = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"valueID"];
    PIGG_Lookup_ID_VALUEPATH = [PIGG_Lookup_NAMESPACE getQNameWithNSString:@"valuePath"];
    J2OBJC_SET_INITIALIZED(PIGG_Lookup)
  }
}

@end

void PIGG_Lookup_init(PIGG_Lookup *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIGG_ControllerFactory, LOOKUP_TYPE), nil, nil, nil);
}

PIGG_Lookup *new_PIGG_Lookup_init() {
  J2OBJC_NEW_IMPL(PIGG_Lookup, init)
}

PIGG_Lookup *create_PIGG_Lookup_init() {
  J2OBJC_CREATE_IMPL(PIGG_Lookup, init)
}

void PIGG_Lookup_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_Lookup *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIGG_ControllerFactory, LOOKUP_TYPE), attributeNames, anyAttribs, childNames);
}

PIGG_Lookup *new_PIGG_Lookup_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_Lookup, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIGG_Lookup *create_PIGG_Lookup_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_Lookup, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIGG_Lookup_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_Lookup *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIGG_Lookup *new_PIGG_Lookup_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_Lookup, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIGG_Lookup *create_PIGG_Lookup_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_Lookup, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_Lookup)

J2OBJC_NAME_MAPPING(PIGG_Lookup, "de.pidata.gui.guidef", "PIGG_")
