//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/SchemaDialog.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefSchemaDialog")
#ifdef RESTRICT_DePidataGuiGuidefSchemaDialog
#define INCLUDE_ALL_DePidataGuiGuidefSchemaDialog 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefSchemaDialog 1
#endif
#undef RESTRICT_DePidataGuiGuidefSchemaDialog

#if !defined (PIGG_SchemaDialog_) && (INCLUDE_ALL_DePidataGuiGuidefSchemaDialog || defined(INCLUDE_PIGG_SchemaDialog))
#define PIGG_SchemaDialog_

@class PIGG_BuiltinOperation;
@class PIGG_ControllerBuilder;
@class PIGG_DialogDef;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PIMT_Type;

@interface PIGG_SchemaDialog : NSObject

#pragma mark Public

- (instancetype)initWithNSString:(NSString *)dialogName
      withPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                   withPIMT_Type:(id<PIMT_Type>)type
                   withPIQ_QName:(PIQ_QName *)tableRelation;

- (void)addActionWithPIQ_QName:(PIQ_QName *)serviceName
     withPIGG_BuiltinOperation:(PIGG_BuiltinOperation *)opID
                 withPIQ_QName:(PIQ_QName *)partName;

- (PIGG_DialogDef *)getDialogDef;

- (void)showWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                       withPIMR_Model:(id<PIMR_Model>)parameter;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGG_SchemaDialog)

inline PIQ_Namespace *PIGG_SchemaDialog_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_SchemaDialog_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_SchemaDialog, NAMESPACE, PIQ_Namespace *)

FOUNDATION_EXPORT void PIGG_SchemaDialog_initWithNSString_withPIGG_ControllerBuilder_withPIMT_Type_withPIQ_QName_(PIGG_SchemaDialog *self, NSString *dialogName, PIGG_ControllerBuilder *builder, id<PIMT_Type> type, PIQ_QName *tableRelation);

FOUNDATION_EXPORT PIGG_SchemaDialog *new_PIGG_SchemaDialog_initWithNSString_withPIGG_ControllerBuilder_withPIMT_Type_withPIQ_QName_(NSString *dialogName, PIGG_ControllerBuilder *builder, id<PIMT_Type> type, PIQ_QName *tableRelation) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_SchemaDialog *create_PIGG_SchemaDialog_initWithNSString_withPIGG_ControllerBuilder_withPIMT_Type_withPIQ_QName_(NSString *dialogName, PIGG_ControllerBuilder *builder, id<PIMT_Type> type, PIQ_QName *tableRelation);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_SchemaDialog)

@compatibility_alias DePidataGuiGuidefSchemaDialog PIGG_SchemaDialog;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefSchemaDialog")
