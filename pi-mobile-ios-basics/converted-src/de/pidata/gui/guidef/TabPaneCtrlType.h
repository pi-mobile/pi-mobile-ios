//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/TabPaneCtrlType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefTabPaneCtrlType")
#ifdef RESTRICT_DePidataGuiGuidefTabPaneCtrlType
#define INCLUDE_ALL_DePidataGuiGuidefTabPaneCtrlType 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefTabPaneCtrlType 1
#endif
#undef RESTRICT_DePidataGuiGuidefTabPaneCtrlType

#if !defined (PIGG_TabPaneCtrlType_) && (INCLUDE_ALL_DePidataGuiGuidefTabPaneCtrlType || defined(INCLUDE_PIGG_TabPaneCtrlType))
#define PIGG_TabPaneCtrlType_

#define RESTRICT_DePidataGuiGuidefValueCtrlType 1
#define INCLUDE_PIGG_ValueCtrlType 1
#include "de/pidata/gui/guidef/ValueCtrlType.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIGC_TabPaneController;
@class PIGG_ControllerBuilder;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIGC_MutableControllerGroup;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_TabPaneCtrlType : PIGG_ValueCtrlType

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (PIGC_TabPaneController *)createControllerWithPIGG_ControllerBuilder:(PIGG_ControllerBuilder *)builder
                                       withPIGC_MutableControllerGroup:(id<PIGC_MutableControllerGroup>)ctrlGroup;

- (PIQ_QName *)getTabPaneCompID;

- (void)setTabPaneCompIDWithPIQ_QName:(PIQ_QName *)tabPaneCompID;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

@end

J2OBJC_STATIC_INIT(PIGG_TabPaneCtrlType)

inline PIQ_Namespace *PIGG_TabPaneCtrlType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_TabPaneCtrlType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TabPaneCtrlType, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_TabPaneCtrlType_get_ID_TABPANECOMPID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_TabPaneCtrlType_ID_TABPANECOMPID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TabPaneCtrlType, ID_TABPANECOMPID, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_TabPaneCtrlType_initWithPIQ_Key_(PIGG_TabPaneCtrlType *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *new_PIGG_TabPaneCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *create_PIGG_TabPaneCtrlType_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIGG_TabPaneCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TabPaneCtrlType *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *new_PIGG_TabPaneCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *create_PIGG_TabPaneCtrlType_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_TabPaneCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TabPaneCtrlType *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *new_PIGG_TabPaneCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TabPaneCtrlType *create_PIGG_TabPaneCtrlType_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_TabPaneCtrlType)

@compatibility_alias DePidataGuiGuidefTabPaneCtrlType PIGG_TabPaneCtrlType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefTabPaneCtrlType")
