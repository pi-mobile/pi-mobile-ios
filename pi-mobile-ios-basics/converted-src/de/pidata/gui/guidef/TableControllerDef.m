//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/TableControllerDef.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/ActionDef.h"
#include "de/pidata/gui/guidef/Column.h"
#include "de/pidata/gui/guidef/Comp.h"
#include "de/pidata/gui/guidef/ControllerFactory.h"
#include "de/pidata/gui/guidef/Lookup.h"
#include "de/pidata/gui/guidef/TableControllerDef.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelCollection.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Boolean.h"
#include "java/lang/Deprecated.h"
#include "java/lang/annotation/Annotation.h"
#include "java/util/Collection.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/guidef/TableControllerDef must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGG_TableControllerDef () {
 @public
  id<JavaUtilCollection> comps_;
  id<JavaUtilCollection> columns_;
}

@end

J2OBJC_FIELD_SETTER(PIGG_TableControllerDef, comps_, id<JavaUtilCollection>)
J2OBJC_FIELD_SETTER(PIGG_TableControllerDef, columns_, id<JavaUtilCollection>)

__attribute__((unused)) static IOSObjectArray *PIGG_TableControllerDef__Annotations$0(void);

J2OBJC_INITIALIZED_DEFN(PIGG_TableControllerDef)

PIQ_Namespace *PIGG_TableControllerDef_NAMESPACE;
PIQ_QName *PIGG_TableControllerDef_ID_CLASSNAME;
PIQ_QName *PIGG_TableControllerDef_ID_COLORCOLUMNID;
PIQ_QName *PIGG_TableControllerDef_ID_COLUMN;
PIQ_QName *PIGG_TableControllerDef_ID_COMP;
PIQ_QName *PIGG_TableControllerDef_ID_DATASOURCE;
PIQ_QName *PIGG_TableControllerDef_ID_ID;
PIQ_QName *PIGG_TableControllerDef_ID_INPLACEEDIT;
PIQ_QName *PIGG_TableControllerDef_ID_LOOKUP;
PIQ_QName *PIGG_TableControllerDef_ID_MODEL;
PIQ_QName *PIGG_TableControllerDef_ID_MODELTYPE;
PIQ_QName *PIGG_TableControllerDef_ID_MODULEID;
PIQ_QName *PIGG_TableControllerDef_ID_READONLY;
PIQ_QName *PIGG_TableControllerDef_ID_ROWACTION;
PIQ_QName *PIGG_TableControllerDef_ID_ROWCOLORID;
PIQ_QName *PIGG_TableControllerDef_ID_ROWDOUBLECLICKACTION;
PIQ_QName *PIGG_TableControllerDef_ID_ROWMENUACTION;
PIQ_QName *PIGG_TableControllerDef_ID_SELECTABLE;
PIQ_QName *PIGG_TableControllerDef_ID_TABLEID;
PIQ_QName *PIGG_TableControllerDef_ID_VALUEID;

@implementation PIGG_TableControllerDef

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIGG_TableControllerDef_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_TableControllerDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIGG_TableControllerDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_ID], [PIQ_QName class]);
}

- (PIQ_QName *)getTableID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_TABLEID], [PIQ_QName class]);
}

- (void)setTableIDWithPIQ_QName:(PIQ_QName *)tableID {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_TABLEID withId:tableID];
}

- (PIQ_QName *)getModuleID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_MODULEID], [PIQ_QName class]);
}

- (void)setModuleIDWithPIQ_QName:(PIQ_QName *)moduleID {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_MODULEID withId:moduleID];
}

- (PIQ_QName *)getDataSource {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_DATASOURCE], [PIQ_QName class]);
}

- (void)setDataSourceWithPIQ_QName:(PIQ_QName *)dataSource {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_DATASOURCE withId:dataSource];
}

- (PIQ_QName *)getModelType {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_MODELTYPE], [PIQ_QName class]);
}

- (void)setModelTypeWithPIQ_QName:(PIQ_QName *)modelType {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_MODELTYPE withId:modelType];
}

- (NSString *)getModel {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_MODEL], [NSString class]);
}

- (void)setModelWithNSString:(NSString *)model {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_MODEL withId:model];
}

- (PIQ_QName *)getValueID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_VALUEID], [PIQ_QName class]);
}

- (void)setValueIDWithPIQ_QName:(PIQ_QName *)valueID {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_VALUEID withId:valueID];
}

- (JavaLangBoolean *)getSelectable {
  return (JavaLangBoolean *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_SELECTABLE], [JavaLangBoolean class]);
}

- (void)setSelectableWithJavaLangBoolean:(JavaLangBoolean *)selectable {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_SELECTABLE withId:selectable];
}

- (JavaLangBoolean *)getReadOnly {
  return (JavaLangBoolean *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_READONLY], [JavaLangBoolean class]);
}

- (void)setReadOnlyWithJavaLangBoolean:(JavaLangBoolean *)readOnly {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_READONLY withId:readOnly];
}

- (PIQ_QName *)getInplaceEdit {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_INPLACEEDIT], [PIQ_QName class]);
}

- (void)setInplaceEditWithPIQ_QName:(PIQ_QName *)inplaceEdit {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_INPLACEEDIT withId:inplaceEdit];
}

- (PIQ_QName *)getRowColorID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_ROWCOLORID], [PIQ_QName class]);
}

- (void)setRowColorIDWithPIQ_QName:(PIQ_QName *)rowColorID {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_ROWCOLORID withId:rowColorID];
}

- (PIQ_QName *)getColorColumnID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_COLORCOLUMNID], [PIQ_QName class]);
}

- (void)setColorColumnIDWithPIQ_QName:(PIQ_QName *)colorColumnID {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_COLORCOLUMNID withId:colorColumnID];
}

- (NSString *)getClassname {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_CLASSNAME], [NSString class]);
}

- (void)setClassnameWithNSString:(NSString *)classname {
  [self setWithPIQ_QName:PIGG_TableControllerDef_ID_CLASSNAME withId:classname];
}

- (PIGG_Comp *)getCompWithPIQ_Key:(id<PIQ_Key>)compID {
  return (PIGG_Comp *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_COMP withPIQ_Key:compID], [PIGG_Comp class]);
}

- (void)addCompWithPIGG_Comp:(PIGG_Comp *)comp {
  [self addWithPIQ_QName:PIGG_TableControllerDef_ID_COMP withPIMR_Model:comp];
}

- (void)removeCompWithPIGG_Comp:(PIGG_Comp *)comp {
  [self removeWithPIQ_QName:PIGG_TableControllerDef_ID_COMP withPIMR_Model:comp];
}

- (id<PIMR_ModelIterator>)compIter {
  return [self iteratorWithPIQ_QName:PIGG_TableControllerDef_ID_COMP withPIMR_Filter:nil];
}

- (jint)compCount {
  return [self childCountWithPIQ_QName:PIGG_TableControllerDef_ID_COMP];
}

- (id<JavaUtilCollection>)getComps {
  return comps_;
}

- (PIGG_ActionDef *)getRowAction {
  return (PIGG_ActionDef *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_ROWACTION withPIQ_Key:nil], [PIGG_ActionDef class]);
}

- (void)setRowActionWithPIGG_ActionDef:(PIGG_ActionDef *)rowAction {
  [self setChildWithPIQ_QName:PIGG_TableControllerDef_ID_ROWACTION withPIMR_Model:rowAction];
}

- (PIGG_ActionDef *)getRowDoubleClickAction {
  return (PIGG_ActionDef *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_ROWDOUBLECLICKACTION withPIQ_Key:nil], [PIGG_ActionDef class]);
}

- (void)setRowDoubleClickActionWithPIGG_ActionDef:(PIGG_ActionDef *)rowDoubleClickAction {
  [self setChildWithPIQ_QName:PIGG_TableControllerDef_ID_ROWDOUBLECLICKACTION withPIMR_Model:rowDoubleClickAction];
}

- (PIGG_ActionDef *)getRowMenuAction {
  return (PIGG_ActionDef *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_ROWMENUACTION withPIQ_Key:nil], [PIGG_ActionDef class]);
}

- (void)setRowMenuActionWithPIGG_ActionDef:(PIGG_ActionDef *)rowMenuAction {
  [self setChildWithPIQ_QName:PIGG_TableControllerDef_ID_ROWMENUACTION withPIMR_Model:rowMenuAction];
}

- (PIGG_Column *)getColumnWithPIQ_Key:(id<PIQ_Key>)columnID {
  return (PIGG_Column *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_COLUMN withPIQ_Key:columnID], [PIGG_Column class]);
}

- (void)addColumnWithPIGG_Column:(PIGG_Column *)column {
  [self addWithPIQ_QName:PIGG_TableControllerDef_ID_COLUMN withPIMR_Model:column];
}

- (void)removeColumnWithPIGG_Column:(PIGG_Column *)column {
  [self removeWithPIQ_QName:PIGG_TableControllerDef_ID_COLUMN withPIMR_Model:column];
}

- (id<PIMR_ModelIterator>)columnIter {
  return [self iteratorWithPIQ_QName:PIGG_TableControllerDef_ID_COLUMN withPIMR_Filter:nil];
}

- (jint)columnCount {
  return [self childCountWithPIQ_QName:PIGG_TableControllerDef_ID_COLUMN];
}

- (id<JavaUtilCollection>)getColumns {
  return columns_;
}

- (PIGG_Lookup *)getLookup {
  return (PIGG_Lookup *) cast_chk([self getWithPIQ_QName:PIGG_TableControllerDef_ID_LOOKUP withPIQ_Key:nil], [PIGG_Lookup class]);
}

- (void)setLookupWithPIGG_Lookup:(PIGG_Lookup *)lookup {
  [self setChildWithPIQ_QName:PIGG_TableControllerDef_ID_LOOKUP withPIMR_Model:lookup];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 4, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 4, -1, -1, -1, -1 },
    { NULL, "LJavaLangBoolean;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "LJavaLangBoolean;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 12, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 16, 4, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 17, 9, -1, -1, -1, -1 },
    { NULL, "LPIGG_Comp;", 0x1, 18, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 19, 20, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 21, 20, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 22, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 23, -1, -1 },
    { NULL, "LPIGG_ActionDef;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 24, 25, -1, -1, -1, -1 },
    { NULL, "LPIGG_ActionDef;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 26, 25, -1, -1, -1, -1 },
    { NULL, "LPIGG_ActionDef;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 27, 25, -1, -1, -1, -1 },
    { NULL, "LPIGG_Column;", 0x1, 28, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 29, 30, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 31, 30, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 32, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 33, -1, -1 },
    { NULL, "LPIGG_Lookup;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 34, 35, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getID);
  methods[4].selector = @selector(getTableID);
  methods[5].selector = @selector(setTableIDWithPIQ_QName:);
  methods[6].selector = @selector(getModuleID);
  methods[7].selector = @selector(setModuleIDWithPIQ_QName:);
  methods[8].selector = @selector(getDataSource);
  methods[9].selector = @selector(setDataSourceWithPIQ_QName:);
  methods[10].selector = @selector(getModelType);
  methods[11].selector = @selector(setModelTypeWithPIQ_QName:);
  methods[12].selector = @selector(getModel);
  methods[13].selector = @selector(setModelWithNSString:);
  methods[14].selector = @selector(getValueID);
  methods[15].selector = @selector(setValueIDWithPIQ_QName:);
  methods[16].selector = @selector(getSelectable);
  methods[17].selector = @selector(setSelectableWithJavaLangBoolean:);
  methods[18].selector = @selector(getReadOnly);
  methods[19].selector = @selector(setReadOnlyWithJavaLangBoolean:);
  methods[20].selector = @selector(getInplaceEdit);
  methods[21].selector = @selector(setInplaceEditWithPIQ_QName:);
  methods[22].selector = @selector(getRowColorID);
  methods[23].selector = @selector(setRowColorIDWithPIQ_QName:);
  methods[24].selector = @selector(getColorColumnID);
  methods[25].selector = @selector(setColorColumnIDWithPIQ_QName:);
  methods[26].selector = @selector(getClassname);
  methods[27].selector = @selector(setClassnameWithNSString:);
  methods[28].selector = @selector(getCompWithPIQ_Key:);
  methods[29].selector = @selector(addCompWithPIGG_Comp:);
  methods[30].selector = @selector(removeCompWithPIGG_Comp:);
  methods[31].selector = @selector(compIter);
  methods[32].selector = @selector(compCount);
  methods[33].selector = @selector(getComps);
  methods[34].selector = @selector(getRowAction);
  methods[35].selector = @selector(setRowActionWithPIGG_ActionDef:);
  methods[36].selector = @selector(getRowDoubleClickAction);
  methods[37].selector = @selector(setRowDoubleClickActionWithPIGG_ActionDef:);
  methods[38].selector = @selector(getRowMenuAction);
  methods[39].selector = @selector(setRowMenuActionWithPIGG_ActionDef:);
  methods[40].selector = @selector(getColumnWithPIQ_Key:);
  methods[41].selector = @selector(addColumnWithPIGG_Column:);
  methods[42].selector = @selector(removeColumnWithPIGG_Column:);
  methods[43].selector = @selector(columnIter);
  methods[44].selector = @selector(columnCount);
  methods[45].selector = @selector(getColumns);
  methods[46].selector = @selector(getLookup);
  methods[47].selector = @selector(setLookupWithPIGG_Lookup:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "ID_CLASSNAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "ID_COLORCOLUMNID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "ID_COLUMN", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 39, -1, -1 },
    { "ID_COMP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 40, -1, -1 },
    { "ID_DATASOURCE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 41, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 42, -1, -1 },
    { "ID_INPLACEEDIT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 43, -1, -1 },
    { "ID_LOOKUP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 44, -1, -1 },
    { "ID_MODEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 45, -1, -1 },
    { "ID_MODELTYPE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 46, -1, -1 },
    { "ID_MODULEID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 47, -1, -1 },
    { "ID_READONLY", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 48, -1, -1 },
    { "ID_ROWACTION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 49, -1, -1 },
    { "ID_ROWCOLORID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 50, -1, -1 },
    { "ID_ROWDOUBLECLICKACTION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 51, -1, -1 },
    { "ID_ROWMENUACTION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 52, -1, -1 },
    { "ID_SELECTABLE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 53, -1, -1 },
    { "ID_TABLEID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 54, -1, -1 },
    { "ID_VALUEID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 55, -1, -1 },
    { "comps_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x2, -1, -1, 56, -1 },
    { "columns_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x2, -1, -1, 57, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setTableID", "LPIQ_QName;", "setModuleID", "setDataSource", "setModelType", "setModel", "LNSString;", "setValueID", "setSelectable", "LJavaLangBoolean;", "setReadOnly", "setInplaceEdit", "setRowColorID", "setColorColumnID", "setClassname", "getComp", "addComp", "LPIGG_Comp;", "removeComp", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/gui/guidef/Comp;>;", "()Ljava/util/Collection<Lde/pidata/gui/guidef/Comp;>;", "setRowAction", "LPIGG_ActionDef;", "setRowDoubleClickAction", "setRowMenuAction", "getColumn", "addColumn", "LPIGG_Column;", "removeColumn", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/gui/guidef/Column;>;", "()Ljava/util/Collection<Lde/pidata/gui/guidef/Column;>;", "setLookup", "LPIGG_Lookup;", &PIGG_TableControllerDef_NAMESPACE, &PIGG_TableControllerDef_ID_CLASSNAME, &PIGG_TableControllerDef_ID_COLORCOLUMNID, &PIGG_TableControllerDef_ID_COLUMN, &PIGG_TableControllerDef_ID_COMP, &PIGG_TableControllerDef_ID_DATASOURCE, &PIGG_TableControllerDef_ID_ID, &PIGG_TableControllerDef_ID_INPLACEEDIT, &PIGG_TableControllerDef_ID_LOOKUP, &PIGG_TableControllerDef_ID_MODEL, &PIGG_TableControllerDef_ID_MODELTYPE, &PIGG_TableControllerDef_ID_MODULEID, &PIGG_TableControllerDef_ID_READONLY, &PIGG_TableControllerDef_ID_ROWACTION, &PIGG_TableControllerDef_ID_ROWCOLORID, &PIGG_TableControllerDef_ID_ROWDOUBLECLICKACTION, &PIGG_TableControllerDef_ID_ROWMENUACTION, &PIGG_TableControllerDef_ID_SELECTABLE, &PIGG_TableControllerDef_ID_TABLEID, &PIGG_TableControllerDef_ID_VALUEID, "Ljava/util/Collection<Lde/pidata/gui/guidef/Comp;>;", "Ljava/util/Collection<Lde/pidata/gui/guidef/Column;>;", (void *)&PIGG_TableControllerDef__Annotations$0 };
  static const J2ObjcClassInfo _PIGG_TableControllerDef = { "TableControllerDef", "de.pidata.gui.guidef", ptrTable, methods, fields, 7, 0x1, 48, 22, -1, -1, -1, -1, 58 };
  return &_PIGG_TableControllerDef;
}

+ (void)initialize {
  if (self == [PIGG_TableControllerDef class]) {
    PIGG_TableControllerDef_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIGG_TableControllerDef_ID_CLASSNAME = [((PIQ_Namespace *) nil_chk(PIGG_TableControllerDef_NAMESPACE)) getQNameWithNSString:@"classname"];
    PIGG_TableControllerDef_ID_COLORCOLUMNID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"colorColumnID"];
    PIGG_TableControllerDef_ID_COLUMN = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"column"];
    PIGG_TableControllerDef_ID_COMP = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"comp"];
    PIGG_TableControllerDef_ID_DATASOURCE = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"dataSource"];
    PIGG_TableControllerDef_ID_ID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"ID"];
    PIGG_TableControllerDef_ID_INPLACEEDIT = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"inplaceEdit"];
    PIGG_TableControllerDef_ID_LOOKUP = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"lookup"];
    PIGG_TableControllerDef_ID_MODEL = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"model"];
    PIGG_TableControllerDef_ID_MODELTYPE = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"modelType"];
    PIGG_TableControllerDef_ID_MODULEID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"moduleID"];
    PIGG_TableControllerDef_ID_READONLY = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"readOnly"];
    PIGG_TableControllerDef_ID_ROWACTION = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"rowAction"];
    PIGG_TableControllerDef_ID_ROWCOLORID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"rowColorID"];
    PIGG_TableControllerDef_ID_ROWDOUBLECLICKACTION = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"rowDoubleClickAction"];
    PIGG_TableControllerDef_ID_ROWMENUACTION = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"rowMenuAction"];
    PIGG_TableControllerDef_ID_SELECTABLE = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"selectable"];
    PIGG_TableControllerDef_ID_TABLEID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"tableID"];
    PIGG_TableControllerDef_ID_VALUEID = [PIGG_TableControllerDef_NAMESPACE getQNameWithNSString:@"valueID"];
    J2OBJC_SET_INITIALIZED(PIGG_TableControllerDef)
  }
}

@end

void PIGG_TableControllerDef_initWithPIQ_Key_(PIGG_TableControllerDef *self, id<PIQ_Key> id_) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIGG_ControllerFactory, TABLECONTROLLERDEF_TYPE), nil, nil, nil);
  self->comps_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COMP, self->children_);
  self->columns_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COLUMN, self->children_);
}

PIGG_TableControllerDef *new_PIGG_TableControllerDef_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_, id_)
}

PIGG_TableControllerDef *create_PIGG_TableControllerDef_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_, id_)
}

void PIGG_TableControllerDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TableControllerDef *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIGG_ControllerFactory, TABLECONTROLLERDEF_TYPE), attributeNames, anyAttribs, childNames);
  self->comps_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COMP, self->children_);
  self->columns_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COLUMN, self->children_);
}

PIGG_TableControllerDef *new_PIGG_TableControllerDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIGG_TableControllerDef *create_PIGG_TableControllerDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIGG_TableControllerDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TableControllerDef *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  self->comps_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COMP, self->children_);
  self->columns_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIGG_TableControllerDef_ID_COLUMN, self->children_);
}

PIGG_TableControllerDef *new_PIGG_TableControllerDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIGG_TableControllerDef *create_PIGG_TableControllerDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIGG_TableControllerDef, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

IOSObjectArray *PIGG_TableControllerDef__Annotations$0() {
  return [IOSObjectArray newArrayWithObjects:(id[]){ create_JavaLangDeprecated() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGG_TableControllerDef)

J2OBJC_NAME_MAPPING(PIGG_TableControllerDef, "de.pidata.gui.guidef", "PIGG_")
