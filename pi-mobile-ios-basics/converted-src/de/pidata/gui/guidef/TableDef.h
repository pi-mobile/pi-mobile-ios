//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/guidef/TableDef.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiGuidefTableDef")
#ifdef RESTRICT_DePidataGuiGuidefTableDef
#define INCLUDE_ALL_DePidataGuiGuidefTableDef 0
#else
#define INCLUDE_ALL_DePidataGuiGuidefTableDef 1
#endif
#undef RESTRICT_DePidataGuiGuidefTableDef

#if !defined (PIGG_TableDef_) && (INCLUDE_ALL_DePidataGuiGuidefTableDef || defined(INCLUDE_PIGG_TableDef))
#define PIGG_TableDef_

#define RESTRICT_DePidataGuiGuidefCompDef 1
#define INCLUDE_PIGG_CompDef 1
#include "de/pidata/gui/guidef/CompDef.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIGG_Layout;
@class PIGG_PanelType;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol JavaUtilCollection;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIGG_TableDef : PIGG_CompDef

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (void)addLayoutWithPIGG_Layout:(PIGG_Layout *)layout;

- (id)getWithPIQ_QName:(PIQ_QName *)attrName;

- (PIGG_PanelType *)getBody;

- (PIGG_PanelType *)getEdit;

- (PIGG_PanelType *)getHeader;

- (PIGG_Layout *)getLayoutWithPIQ_Key:(id<PIQ_Key>)layoutID;

- (id<JavaUtilCollection>)getLayouts;

- (jint)layoutCount;

- (id<PIMR_ModelIterator>)layoutIter;

- (void)removeLayoutWithPIGG_Layout:(PIGG_Layout *)layout;

- (void)setWithPIQ_QName:(PIQ_QName *)attributeName
                  withId:(id)value;

- (void)setBodyWithPIGG_PanelType:(PIGG_PanelType *)body;

- (void)setEditWithPIGG_PanelType:(PIGG_PanelType *)edit;

- (void)setHeaderWithPIGG_PanelType:(PIGG_PanelType *)header;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

@end

J2OBJC_STATIC_INIT(PIGG_TableDef)

inline PIQ_Namespace *PIGG_TableDef_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIGG_TableDef_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TableDef, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIGG_TableDef_get_ID_BODY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_TableDef_ID_BODY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TableDef, ID_BODY, PIQ_QName *)

inline PIQ_QName *PIGG_TableDef_get_ID_EDIT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_TableDef_ID_EDIT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TableDef, ID_EDIT, PIQ_QName *)

inline PIQ_QName *PIGG_TableDef_get_ID_HEADER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_TableDef_ID_HEADER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TableDef, ID_HEADER, PIQ_QName *)

inline PIQ_QName *PIGG_TableDef_get_ID_LAYOUT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIGG_TableDef_ID_LAYOUT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGG_TableDef, ID_LAYOUT, PIQ_QName *)

FOUNDATION_EXPORT void PIGG_TableDef_initWithPIQ_Key_(PIGG_TableDef *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIGG_TableDef *new_PIGG_TableDef_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TableDef *create_PIGG_TableDef_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIGG_TableDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TableDef *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_TableDef *new_PIGG_TableDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TableDef *create_PIGG_TableDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIGG_TableDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIGG_TableDef *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIGG_TableDef *new_PIGG_TableDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGG_TableDef *create_PIGG_TableDef_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIGG_TableDef)

@compatibility_alias DePidataGuiGuidefTableDef PIGG_TableDef;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiGuidefTableDef")
