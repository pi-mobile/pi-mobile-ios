//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/renderer/Renderer.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiRendererRenderer")
#ifdef RESTRICT_DePidataGuiRendererRenderer
#define INCLUDE_ALL_DePidataGuiRendererRenderer 0
#else
#define INCLUDE_ALL_DePidataGuiRendererRenderer 1
#endif
#undef RESTRICT_DePidataGuiRendererRenderer

#if !defined (PIGR_Renderer_) && (INCLUDE_ALL_DePidataGuiRendererRenderer || defined(INCLUDE_PIGR_Renderer))
#define PIGR_Renderer_

@protocol PIGR_Renderer < JavaObject >

- (NSString *)renderWithId:(id)value;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGR_Renderer)

J2OBJC_TYPE_LITERAL_HEADER(PIGR_Renderer)

#define DePidataGuiRendererRenderer PIGR_Renderer

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiRendererRenderer")
