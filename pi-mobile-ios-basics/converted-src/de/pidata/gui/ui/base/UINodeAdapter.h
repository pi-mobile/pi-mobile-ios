//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/ui/base/UINodeAdapter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiUiBaseUINodeAdapter")
#ifdef RESTRICT_DePidataGuiUiBaseUINodeAdapter
#define INCLUDE_ALL_DePidataGuiUiBaseUINodeAdapter 0
#else
#define INCLUDE_ALL_DePidataGuiUiBaseUINodeAdapter 1
#endif
#undef RESTRICT_DePidataGuiUiBaseUINodeAdapter

#if !defined (PIGU_UINodeAdapter_) && (INCLUDE_ALL_DePidataGuiUiBaseUINodeAdapter || defined(INCLUDE_PIGU_UINodeAdapter))
#define PIGU_UINodeAdapter_

#define RESTRICT_DePidataGuiUiBaseUIAdapter 1
#define INCLUDE_PIGU_UIAdapter 1
#include "de/pidata/gui/ui/base/UIAdapter.h"

@protocol PIGU_UINodeAdapter < PIGU_UIAdapter, JavaObject >

@end

J2OBJC_EMPTY_STATIC_INIT(PIGU_UINodeAdapter)

J2OBJC_TYPE_LITERAL_HEADER(PIGU_UINodeAdapter)

#define DePidataGuiUiBaseUINodeAdapter PIGU_UINodeAdapter

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiUiBaseUINodeAdapter")
