//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/base/ModuleViewPI.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/ModuleController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/ui/base/UIAdapter.h"
#include "de/pidata/gui/ui/base/UIContainer.h"
#include "de/pidata/gui/ui/base/UIFactory.h"
#include "de/pidata/gui/ui/base/UIFragmentAdapter.h"
#include "de/pidata/gui/view/base/AbstractViewPI.h"
#include "de/pidata/gui/view/base/ModuleViewPI.h"
#include "de/pidata/gui/view/base/ViewPI.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Iterable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/base/ModuleViewPI must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGV_ModuleViewPI () {
 @public
  id<PIGU_UIFragmentAdapter> uiFragmentAdapter_;
  PIGC_ModuleController *moduleController_;
}

@end

J2OBJC_FIELD_SETTER(PIGV_ModuleViewPI, uiFragmentAdapter_, id<PIGU_UIFragmentAdapter>)
J2OBJC_FIELD_SETTER(PIGV_ModuleViewPI, moduleController_, PIGC_ModuleController *)

@implementation PIGV_ModuleViewPI

- (instancetype)initWithPIQ_QName:(PIQ_QName *)componentID
                    withPIQ_QName:(PIQ_QName *)moduleID {
  PIGV_ModuleViewPI_initWithPIQ_QName_withPIQ_QName_(self, componentID, moduleID);
  return self;
}

- (id<PIGC_Controller>)getController {
  return moduleController_;
}

- (void)setControllerWithPIGC_ModuleController:(PIGC_ModuleController *)moduleGroup {
  self->moduleController_ = moduleGroup;
}

- (id<PIGU_UIAdapter>)getUIAdapter {
  return uiFragmentAdapter_;
}

- (id<PIGU_UIContainer>)getUIContainer {
  return uiFragmentAdapter_;
}

- (void)attachUIComponentWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer {
  @synchronized(self) {
    if (uiFragmentAdapter_ == nil) {
      uiFragmentAdapter_ = [((id<PIGU_UIFactory>) nil_chk([((id<PIGU_UIContainer>) nil_chk(uiContainer)) getUIFactory])) createFragmentAdapterWithPIGU_UIContainer:uiContainer withPIGV_ModuleViewPI:self];
      PIGC_ModuleGroup *moduleGroup = [((PIGC_ModuleController *) nil_chk(moduleController_)) getModuleGroup];
      [((id<PIGU_UIFragmentAdapter>) nil_chk(uiFragmentAdapter_)) moduleChangedWithPIGC_ModuleGroup:moduleGroup];
    }
  }
}

- (id<PIGU_UIAdapter>)attachUIRenderComponentWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer {
  PIGC_ModuleGroup *module = [((PIGC_ModuleController *) nil_chk(moduleController_)) getCurrentModule];
  for (id<PIGC_Controller> __strong childCtrl in nil_chk([((PIGC_ModuleGroup *) nil_chk(module)) controllerIterator])) {
    id<PIGV_ViewPI> viewPI = [((id<PIGC_Controller>) nil_chk(childCtrl)) getView];
    (void) [((id<PIGV_ViewPI>) nil_chk(viewPI)) attachUIRenderComponentWithPIGU_UIContainer:uiContainer];
  }
  return nil;
}

- (void)setFragmentAdapterWithPIGU_UIFragmentAdapter:(id<PIGU_UIFragmentAdapter>)uiFragmentAdapter {
  self->uiFragmentAdapter_ = uiFragmentAdapter;
}

- (void)detachUIComponent {
  @synchronized(self) {
    if (uiFragmentAdapter_ != nil) {
      [uiFragmentAdapter_ detach];
      uiFragmentAdapter_ = nil;
    }
  }
}

- (void)moduleChangedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)moduleGroup {
  @synchronized(self) {
    if (uiFragmentAdapter_ != nil) {
      [uiFragmentAdapter_ moduleChangedWithPIGC_ModuleGroup:moduleGroup];
    }
  }
}

- (void)onFragmentLoadedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)moduleGroup {
  [((PIGC_ModuleGroup *) nil_chk(moduleGroup)) activateModuleWithPIGU_UIContainer:[self getUIContainer]];
}

- (void)onFragmentDestroyedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)moduleGroup {
  [((PIGC_ModuleGroup *) nil_chk(moduleGroup)) deactivateModule];
  [((PIGC_ModuleController *) nil_chk(moduleController_)) moduleDeactivatedWithPIGC_ModuleGroup:moduleGroup];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LPIGC_Controller;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LPIGU_UIAdapter;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGU_UIContainer;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIGU_UIAdapter;", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 9, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_QName:withPIQ_QName:);
  methods[1].selector = @selector(getController);
  methods[2].selector = @selector(setControllerWithPIGC_ModuleController:);
  methods[3].selector = @selector(getUIAdapter);
  methods[4].selector = @selector(getUIContainer);
  methods[5].selector = @selector(attachUIComponentWithPIGU_UIContainer:);
  methods[6].selector = @selector(attachUIRenderComponentWithPIGU_UIContainer:);
  methods[7].selector = @selector(setFragmentAdapterWithPIGU_UIFragmentAdapter:);
  methods[8].selector = @selector(detachUIComponent);
  methods[9].selector = @selector(moduleChangedWithPIGC_ModuleGroup:);
  methods[10].selector = @selector(onFragmentLoadedWithPIGC_ModuleGroup:);
  methods[11].selector = @selector(onFragmentDestroyedWithPIGC_ModuleGroup:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "uiFragmentAdapter_", "LPIGU_UIFragmentAdapter;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "moduleController_", "LPIGC_ModuleController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_QName;LPIQ_QName;", "setController", "LPIGC_ModuleController;", "attachUIComponent", "LPIGU_UIContainer;", "attachUIRenderComponent", "setFragmentAdapter", "LPIGU_UIFragmentAdapter;", "moduleChanged", "LPIGC_ModuleGroup;", "onFragmentLoaded", "onFragmentDestroyed" };
  static const J2ObjcClassInfo _PIGV_ModuleViewPI = { "ModuleViewPI", "de.pidata.gui.view.base", ptrTable, methods, fields, 7, 0x1, 12, 2, -1, -1, -1, -1, -1 };
  return &_PIGV_ModuleViewPI;
}

@end

void PIGV_ModuleViewPI_initWithPIQ_QName_withPIQ_QName_(PIGV_ModuleViewPI *self, PIQ_QName *componentID, PIQ_QName *moduleID) {
  PIGV_AbstractViewPI_initWithPIQ_QName_withPIQ_QName_(self, componentID, moduleID);
}

PIGV_ModuleViewPI *new_PIGV_ModuleViewPI_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *componentID, PIQ_QName *moduleID) {
  J2OBJC_NEW_IMPL(PIGV_ModuleViewPI, initWithPIQ_QName_withPIQ_QName_, componentID, moduleID)
}

PIGV_ModuleViewPI *create_PIGV_ModuleViewPI_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *componentID, PIQ_QName *moduleID) {
  J2OBJC_CREATE_IMPL(PIGV_ModuleViewPI, initWithPIQ_QName_withPIQ_QName_, componentID, moduleID)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGV_ModuleViewPI)

J2OBJC_NAME_MAPPING(PIGV_ModuleViewPI, "de.pidata.gui.view.base", "PIGV_")
