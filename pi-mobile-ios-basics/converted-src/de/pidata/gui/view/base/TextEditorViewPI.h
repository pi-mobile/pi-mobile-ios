//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/base/TextEditorViewPI.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiViewBaseTextEditorViewPI")
#ifdef RESTRICT_DePidataGuiViewBaseTextEditorViewPI
#define INCLUDE_ALL_DePidataGuiViewBaseTextEditorViewPI 0
#else
#define INCLUDE_ALL_DePidataGuiViewBaseTextEditorViewPI 1
#endif
#undef RESTRICT_DePidataGuiViewBaseTextEditorViewPI

#if !defined (PIGV_TextEditorViewPI_) && (INCLUDE_ALL_DePidataGuiViewBaseTextEditorViewPI || defined(INCLUDE_PIGV_TextEditorViewPI))
#define PIGV_TextEditorViewPI_

#define RESTRICT_DePidataGuiViewBaseTextViewPI 1
#define INCLUDE_PIGV_TextViewPI 1
#include "de/pidata/gui/view/base/TextViewPI.h"

@class PIQ_QName;
@protocol PIGU_UIContainer;
@protocol PIGU_UITextEditorAdapter;

@interface PIGV_TextEditorViewPI : PIGV_TextViewPI

#pragma mark Public

- (instancetype)initWithPIQ_QName:(PIQ_QName *)componentID
                    withPIQ_QName:(PIQ_QName *)moduleID;

#pragma mark Protected

- (id<PIGU_UITextEditorAdapter>)createTextAdapterWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGV_TextEditorViewPI)

FOUNDATION_EXPORT void PIGV_TextEditorViewPI_initWithPIQ_QName_withPIQ_QName_(PIGV_TextEditorViewPI *self, PIQ_QName *componentID, PIQ_QName *moduleID);

FOUNDATION_EXPORT PIGV_TextEditorViewPI *new_PIGV_TextEditorViewPI_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *componentID, PIQ_QName *moduleID) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGV_TextEditorViewPI *create_PIGV_TextEditorViewPI_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *componentID, PIQ_QName *moduleID);

J2OBJC_TYPE_LITERAL_HEADER(PIGV_TextEditorViewPI)

@compatibility_alias DePidataGuiViewBaseTextEditorViewPI PIGV_TextEditorViewPI;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiViewBaseTextEditorViewPI")
