//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/AbstractConnector.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiViewFigureAbstractConnector")
#ifdef RESTRICT_DePidataGuiViewFigureAbstractConnector
#define INCLUDE_ALL_DePidataGuiViewFigureAbstractConnector 0
#else
#define INCLUDE_ALL_DePidataGuiViewFigureAbstractConnector 1
#endif
#undef RESTRICT_DePidataGuiViewFigureAbstractConnector

#if !defined (PIGF_AbstractConnector_) && (INCLUDE_ALL_DePidataGuiViewFigureAbstractConnector || defined(INCLUDE_PIGF_AbstractConnector))
#define PIGF_AbstractConnector_

#define RESTRICT_DePidataGuiViewFigureFigureConnector 1
#define INCLUDE_PIGF_FigureConnector 1
#include "de/pidata/gui/view/figure/FigureConnector.h"

@class IOSClass;
@protocol JavaUtilList;
@protocol PICR_PosDir;
@protocol PIGF_Figure;
@protocol PIGU_UIShapeAdapter;

@interface PIGF_AbstractConnector : NSObject < PIGF_FigureConnector > {
 @public
  id<PIGF_Figure> owner_;
  id<PIGU_UIShapeAdapter> uiShapeAdapter_;
  id<PICR_PosDir> anchor_;
  id<JavaUtilList> connectionList_;
}

#pragma mark Public

- (instancetype)initWithPICR_PosDir:(id<PICR_PosDir>)anchor
                    withPIGF_Figure:(id<PIGF_Figure>)owner;

- (void)attachUIWithPIGU_UIShapeAdapter:(id<PIGU_UIShapeAdapter>)uiShapeAdapter;

- (void)detachUI;

- (id<PICR_PosDir>)getAnchorWithIOSClass:(IOSClass *)connectionClass;

- (id<JavaUtilList>)getConnectionList;

- (id<PIGF_Figure>)getFigure;

- (id<PIGU_UIShapeAdapter>)getUIAdapter;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGF_AbstractConnector)

J2OBJC_FIELD_SETTER(PIGF_AbstractConnector, owner_, id<PIGF_Figure>)
J2OBJC_FIELD_SETTER(PIGF_AbstractConnector, uiShapeAdapter_, id<PIGU_UIShapeAdapter>)
J2OBJC_FIELD_SETTER(PIGF_AbstractConnector, anchor_, id<PICR_PosDir>)
J2OBJC_FIELD_SETTER(PIGF_AbstractConnector, connectionList_, id<JavaUtilList>)

FOUNDATION_EXPORT void PIGF_AbstractConnector_initWithPICR_PosDir_withPIGF_Figure_(PIGF_AbstractConnector *self, id<PICR_PosDir> anchor, id<PIGF_Figure> owner);

J2OBJC_TYPE_LITERAL_HEADER(PIGF_AbstractConnector)

@compatibility_alias DePidataGuiViewFigureAbstractConnector PIGF_AbstractConnector;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiViewFigureAbstractConnector")
