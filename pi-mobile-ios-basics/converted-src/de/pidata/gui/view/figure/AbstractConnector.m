//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/AbstractConnector.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/ui/base/UIShapeAdapter.h"
#include "de/pidata/gui/view/figure/AbstractConnector.h"
#include "de/pidata/gui/view/figure/Figure.h"
#include "de/pidata/rect/PosDir.h"
#include "java/util/LinkedList.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/AbstractConnector must be compiled with ARC (-fobjc-arc)"
#endif

#pragma clang diagnostic ignored "-Wprotocol"

@implementation PIGF_AbstractConnector

- (instancetype)initWithPICR_PosDir:(id<PICR_PosDir>)anchor
                    withPIGF_Figure:(id<PIGF_Figure>)owner {
  PIGF_AbstractConnector_initWithPICR_PosDir_withPIGF_Figure_(self, anchor, owner);
  return self;
}

- (id<PIGF_Figure>)getFigure {
  return owner_;
}

- (void)attachUIWithPIGU_UIShapeAdapter:(id<PIGU_UIShapeAdapter>)uiShapeAdapter {
  self->uiShapeAdapter_ = uiShapeAdapter;
}

- (void)detachUI {
  self->uiShapeAdapter_ = nil;
}

- (id<PIGU_UIShapeAdapter>)getUIAdapter {
  return uiShapeAdapter_;
}

- (id<PICR_PosDir>)getAnchorWithIOSClass:(IOSClass *)connectionClass {
  return anchor_;
}

- (id<JavaUtilList>)getConnectionList {
  return connectionList_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LPIGF_Figure;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGU_UIShapeAdapter;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPICR_PosDir;", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 5, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPICR_PosDir:withPIGF_Figure:);
  methods[1].selector = @selector(getFigure);
  methods[2].selector = @selector(attachUIWithPIGU_UIShapeAdapter:);
  methods[3].selector = @selector(detachUI);
  methods[4].selector = @selector(getUIAdapter);
  methods[5].selector = @selector(getAnchorWithIOSClass:);
  methods[6].selector = @selector(getConnectionList);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "owner_", "LPIGF_Figure;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "uiShapeAdapter_", "LPIGU_UIShapeAdapter;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "anchor_", "LPICR_PosDir;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "connectionList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x4, -1, -1, 6, -1 },
  };
  static const void *ptrTable[] = { "LPICR_PosDir;LPIGF_Figure;", "attachUI", "LPIGU_UIShapeAdapter;", "getAnchor", "LIOSClass;", "()Ljava/util/List<Lde/pidata/gui/view/figure/Figure;>;", "Ljava/util/List<Lde/pidata/gui/view/figure/Figure;>;" };
  static const J2ObjcClassInfo _PIGF_AbstractConnector = { "AbstractConnector", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x401, 7, 4, -1, -1, -1, -1, -1 };
  return &_PIGF_AbstractConnector;
}

@end

void PIGF_AbstractConnector_initWithPICR_PosDir_withPIGF_Figure_(PIGF_AbstractConnector *self, id<PICR_PosDir> anchor, id<PIGF_Figure> owner) {
  NSObject_init(self);
  self->connectionList_ = new_JavaUtilLinkedList_init();
  self->anchor_ = anchor;
  self->owner_ = owner;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_AbstractConnector)

J2OBJC_NAME_MAPPING(PIGF_AbstractConnector, "de.pidata.gui.view.figure", "PIGF_")
