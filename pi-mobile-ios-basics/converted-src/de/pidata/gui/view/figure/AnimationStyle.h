//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/AnimationStyle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiViewFigureAnimationStyle")
#ifdef RESTRICT_DePidataGuiViewFigureAnimationStyle
#define INCLUDE_ALL_DePidataGuiViewFigureAnimationStyle 0
#else
#define INCLUDE_ALL_DePidataGuiViewFigureAnimationStyle 1
#endif
#undef RESTRICT_DePidataGuiViewFigureAnimationStyle

#if !defined (PIGF_AnimationStyle_) && (INCLUDE_ALL_DePidataGuiViewFigureAnimationStyle || defined(INCLUDE_PIGF_AnimationStyle))
#define PIGF_AnimationStyle_

#define RESTRICT_DePidataGuiViewFigureShapeStyle 1
#define INCLUDE_PIGF_ShapeStyle 1
#include "de/pidata/gui/view/figure/ShapeStyle.h"

@class PIGF_TextAlign;
@class PIQ_QName;
@protocol PIGB_ComponentColor;

@interface PIGF_AnimationStyle : PIGF_ShapeStyle

#pragma mark Public

- (instancetype)initWithInt:(jint)animationDelay
                 withDouble:(jdouble)offsetRatio;

- (jint)getAnimationDelay;

- (jdouble)getOffsetRatio;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                                 withDouble:(jdouble)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                               withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                               withNSString:(NSString *)arg1
                         withPIGF_TextAlign:(PIGF_TextAlign *)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg1
                                 withDouble:(jdouble)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                       withDouble:(jdouble)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
               withPIGF_TextAlign:(PIGF_TextAlign *)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                    withPIQ_QName:(PIQ_QName *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                    withPIQ_QName:(PIQ_QName *)arg1
                       withDouble:(jdouble)arg2 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIGF_AnimationStyle)

inline PIGF_AnimationStyle *PIGF_AnimationStyle_get_DEFAULT_ANIMATION_STYLE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIGF_AnimationStyle *PIGF_AnimationStyle_DEFAULT_ANIMATION_STYLE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIGF_AnimationStyle, DEFAULT_ANIMATION_STYLE, PIGF_AnimationStyle *)

FOUNDATION_EXPORT void PIGF_AnimationStyle_initWithInt_withDouble_(PIGF_AnimationStyle *self, jint animationDelay, jdouble offsetRatio);

FOUNDATION_EXPORT PIGF_AnimationStyle *new_PIGF_AnimationStyle_initWithInt_withDouble_(jint animationDelay, jdouble offsetRatio) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGF_AnimationStyle *create_PIGF_AnimationStyle_initWithInt_withDouble_(jint animationDelay, jdouble offsetRatio);

J2OBJC_TYPE_LITERAL_HEADER(PIGF_AnimationStyle)

@compatibility_alias DePidataGuiViewFigureAnimationStyle PIGF_AnimationStyle;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiViewFigureAnimationStyle")
