//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/CircleGradientStyle.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiViewFigureCircleGradientStyle")
#ifdef RESTRICT_DePidataGuiViewFigureCircleGradientStyle
#define INCLUDE_ALL_DePidataGuiViewFigureCircleGradientStyle 0
#else
#define INCLUDE_ALL_DePidataGuiViewFigureCircleGradientStyle 1
#endif
#undef RESTRICT_DePidataGuiViewFigureCircleGradientStyle

#if !defined (PIGF_CircleGradientStyle_) && (INCLUDE_ALL_DePidataGuiViewFigureCircleGradientStyle || defined(INCLUDE_PIGF_CircleGradientStyle))
#define PIGF_CircleGradientStyle_

#define RESTRICT_DePidataGuiViewFigureShapeStyle 1
#define INCLUDE_PIGF_ShapeStyle 1
#include "de/pidata/gui/view/figure/ShapeStyle.h"

#define RESTRICT_DePidataRectPosSizeEventListener 1
#define INCLUDE_PICR_PosSizeEventListener 1
#include "de/pidata/rect/PosSizeEventListener.h"

@class PICR_Rotation;
@class PIGF_GradientMode;
@class PIGF_TextAlign;
@class PIQ_QName;
@protocol JavaUtilList;
@protocol PICR_Pos;
@protocol PICR_PosDir;
@protocol PICR_Rect;
@protocol PIGB_ComponentColor;

@interface PIGF_CircleGradientStyle : PIGF_ShapeStyle < PICR_PosSizeEventListener >

#pragma mark Public

- (instancetype)initWithPICR_Pos:(id<PICR_Pos>)centerPos
                      withDouble:(jdouble)radius
                withJavaUtilList:(id<JavaUtilList>)colorList
                withJavaUtilList:(id<JavaUtilList>)offsetList
           withPIGF_GradientMode:(PIGF_GradientMode *)gradientMode;

- (id<PICR_Pos>)getCenterPos;

- (id<JavaUtilList>)getColorList;

- (PIGF_GradientMode *)getGradientMode;

- (id<JavaUtilList>)getOffsetList;

- (jdouble)getRadius;

- (void)posChangedWithPICR_Pos:(id<PICR_Pos>)pos
                    withDouble:(jdouble)oldX
                    withDouble:(jdouble)oldY;

- (void)rotationChangedWithPICR_PosDir:(id<PICR_PosDir>)posDir
                     withPICR_Rotation:(PICR_Rotation *)oldRotation;

- (void)setColorWithInt:(jint)index
withPIGB_ComponentColor:(id<PIGB_ComponentColor>)color;

- (void)setRadiusWithDouble:(jdouble)radius;

- (void)sizeChangedWithPICR_Rect:(id<PICR_Rect>)rect
                      withDouble:(jdouble)oldWidth
                      withDouble:(jdouble)oldHeight;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                                 withDouble:(jdouble)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                               withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                               withNSString:(NSString *)arg1
                         withPIGF_TextAlign:(PIGF_TextAlign *)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg0
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)arg1
                                 withDouble:(jdouble)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                       withDouble:(jdouble)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
               withPIGF_TextAlign:(PIGF_TextAlign *)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                    withPIQ_QName:(PIQ_QName *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                    withPIQ_QName:(PIQ_QName *)arg1
                       withDouble:(jdouble)arg2 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIGF_CircleGradientStyle)

FOUNDATION_EXPORT void PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(PIGF_CircleGradientStyle *self, id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode);

FOUNDATION_EXPORT PIGF_CircleGradientStyle *new_PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIGF_CircleGradientStyle *create_PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode);

J2OBJC_TYPE_LITERAL_HEADER(PIGF_CircleGradientStyle)

@compatibility_alias DePidataGuiViewFigureCircleGradientStyle PIGF_CircleGradientStyle;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiViewFigureCircleGradientStyle")
