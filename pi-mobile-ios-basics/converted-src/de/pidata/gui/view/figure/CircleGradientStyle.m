//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/CircleGradientStyle.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/view/figure/CircleGradientStyle.h"
#include "de/pidata/gui/view/figure/GradientMode.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/rect/Pos.h"
#include "de/pidata/rect/PosDir.h"
#include "de/pidata/rect/PosSizeEventSender.h"
#include "de/pidata/rect/Rect.h"
#include "de/pidata/rect/Rotation.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/CircleGradientStyle must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGF_CircleGradientStyle () {
 @public
  id<PICR_Pos> centerPos_;
  jdouble radius_;
  id<JavaUtilList> colorList_;
  id<JavaUtilList> offsetList_;
  PIGF_GradientMode *gradientMode_;
}

@end

J2OBJC_FIELD_SETTER(PIGF_CircleGradientStyle, centerPos_, id<PICR_Pos>)
J2OBJC_FIELD_SETTER(PIGF_CircleGradientStyle, colorList_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(PIGF_CircleGradientStyle, offsetList_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(PIGF_CircleGradientStyle, gradientMode_, PIGF_GradientMode *)

@implementation PIGF_CircleGradientStyle

- (instancetype)initWithPICR_Pos:(id<PICR_Pos>)centerPos
                      withDouble:(jdouble)radius
                withJavaUtilList:(id<JavaUtilList>)colorList
                withJavaUtilList:(id<JavaUtilList>)offsetList
           withPIGF_GradientMode:(PIGF_GradientMode *)gradientMode {
  PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(self, centerPos, radius, colorList, offsetList, gradientMode);
  return self;
}

- (id<PICR_Pos>)getCenterPos {
  return centerPos_;
}

- (jdouble)getRadius {
  return radius_;
}

- (void)setRadiusWithDouble:(jdouble)radius {
  if (radius != self->radius_) {
    self->radius_ = radius;
    [self fireStyleChanged];
  }
}

- (id<JavaUtilList>)getColorList {
  return colorList_;
}

- (void)setColorWithInt:(jint)index
withPIGB_ComponentColor:(id<PIGB_ComponentColor>)color {
  id<PIGB_ComponentColor> oldColor = [((id<JavaUtilList>) nil_chk(colorList_)) getWithInt:index];
  if (!JreObjectEqualsEquals(color, oldColor)) {
    (void) [((id<JavaUtilList>) nil_chk(colorList_)) setWithInt:index withId:color];
    [self fireStyleChanged];
  }
}

- (id<JavaUtilList>)getOffsetList {
  return offsetList_;
}

- (PIGF_GradientMode *)getGradientMode {
  return gradientMode_;
}

- (void)posChangedWithPICR_Pos:(id<PICR_Pos>)pos
                    withDouble:(jdouble)oldX
                    withDouble:(jdouble)oldY {
  [self fireStyleChanged];
}

- (void)sizeChangedWithPICR_Rect:(id<PICR_Rect>)rect
                      withDouble:(jdouble)oldWidth
                      withDouble:(jdouble)oldHeight {
  [self fireStyleChanged];
}

- (void)rotationChangedWithPICR_PosDir:(id<PICR_PosDir>)posDir
                     withPICR_Rotation:(PICR_Rotation *)oldRotation {
  [self fireStyleChanged];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, "LPICR_Pos;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "D", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 4, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LJavaUtilList;", 0x1, -1, -1, -1, 7, -1, -1 },
    { NULL, "LPIGF_GradientMode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPICR_Pos:withDouble:withJavaUtilList:withJavaUtilList:withPIGF_GradientMode:);
  methods[1].selector = @selector(getCenterPos);
  methods[2].selector = @selector(getRadius);
  methods[3].selector = @selector(setRadiusWithDouble:);
  methods[4].selector = @selector(getColorList);
  methods[5].selector = @selector(setColorWithInt:withPIGB_ComponentColor:);
  methods[6].selector = @selector(getOffsetList);
  methods[7].selector = @selector(getGradientMode);
  methods[8].selector = @selector(posChangedWithPICR_Pos:withDouble:withDouble:);
  methods[9].selector = @selector(sizeChangedWithPICR_Rect:withDouble:withDouble:);
  methods[10].selector = @selector(rotationChangedWithPICR_PosDir:withPICR_Rotation:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "centerPos_", "LPICR_Pos;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "radius_", "D", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "colorList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 14, -1 },
    { "offsetList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 15, -1 },
    { "gradientMode_", "LPIGF_GradientMode;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPICR_Pos;DLJavaUtilList;LJavaUtilList;LPIGF_GradientMode;", "(Lde/pidata/rect/Pos;DLjava/util/List<Lde/pidata/gui/component/base/ComponentColor;>;Ljava/util/List<Ljava/lang/Double;>;Lde/pidata/gui/view/figure/GradientMode;)V", "setRadius", "D", "()Ljava/util/List<Lde/pidata/gui/component/base/ComponentColor;>;", "setColor", "ILPIGB_ComponentColor;", "()Ljava/util/List<Ljava/lang/Double;>;", "posChanged", "LPICR_Pos;DD", "sizeChanged", "LPICR_Rect;DD", "rotationChanged", "LPICR_PosDir;LPICR_Rotation;", "Ljava/util/List<Lde/pidata/gui/component/base/ComponentColor;>;", "Ljava/util/List<Ljava/lang/Double;>;" };
  static const J2ObjcClassInfo _PIGF_CircleGradientStyle = { "CircleGradientStyle", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x1, 11, 5, -1, -1, -1, -1, -1 };
  return &_PIGF_CircleGradientStyle;
}

@end

void PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(PIGF_CircleGradientStyle *self, id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode) {
  PIGF_ShapeStyle_init(self);
  if ([((id<JavaUtilList>) nil_chk(colorList)) size] != [((id<JavaUtilList>) nil_chk(offsetList)) size]) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"ColorList and offsetList must have same size");
  }
  self->centerPos_ = centerPos;
  [((PICR_PosSizeEventSender *) nil_chk([((id<PICR_Pos>) nil_chk(centerPos)) getEventSender])) addListenerWithPICR_PosSizeEventListener:self];
  self->radius_ = radius;
  self->colorList_ = colorList;
  self->offsetList_ = offsetList;
  self->gradientMode_ = gradientMode;
}

PIGF_CircleGradientStyle *new_PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode) {
  J2OBJC_NEW_IMPL(PIGF_CircleGradientStyle, initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_, centerPos, radius, colorList, offsetList, gradientMode)
}

PIGF_CircleGradientStyle *create_PIGF_CircleGradientStyle_initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_(id<PICR_Pos> centerPos, jdouble radius, id<JavaUtilList> colorList, id<JavaUtilList> offsetList, PIGF_GradientMode *gradientMode) {
  J2OBJC_CREATE_IMPL(PIGF_CircleGradientStyle, initWithPICR_Pos_withDouble_withJavaUtilList_withJavaUtilList_withPIGF_GradientMode_, centerPos, radius, colorList, offsetList, gradientMode)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_CircleGradientStyle)

J2OBJC_NAME_MAPPING(PIGF_CircleGradientStyle, "de.pidata.gui.view.figure", "PIGF_")
