//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/FigureHandle.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/view/figure/FigureHandle.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/FigureHandle must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGF_FigureHandle : NSObject

@end

@implementation PIGF_FigureHandle

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPIGF_Figure;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGF_HandlePosition;", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getOwner);
  methods[1].selector = @selector(getHandlePosition);
  #pragma clang diagnostic pop
  static const J2ObjcClassInfo _PIGF_FigureHandle = { "FigureHandle", "de.pidata.gui.view.figure", NULL, methods, NULL, 7, 0x609, 2, 0, -1, -1, -1, -1, -1 };
  return &_PIGF_FigureHandle;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIGF_FigureHandle)

J2OBJC_NAME_MAPPING(PIGF_FigureHandle, "de.pidata.gui.view.figure", "PIGF_")
