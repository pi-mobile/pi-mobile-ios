//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/GradientMode.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/view/figure/GradientMode.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/GradientMode must be compiled with ARC (-fobjc-arc)"
#endif

__attribute__((unused)) static void PIGF_GradientMode_initWithNSString_withInt_(PIGF_GradientMode *self, NSString *__name, jint __ordinal);

__attribute__((unused)) static PIGF_GradientMode *new_PIGF_GradientMode_initWithNSString_withInt_(NSString *__name, jint __ordinal) NS_RETURNS_RETAINED;

J2OBJC_INITIALIZED_DEFN(PIGF_GradientMode)

PIGF_GradientMode *PIGF_GradientMode_values_[3];

@implementation PIGF_GradientMode

+ (IOSObjectArray *)values {
  return PIGF_GradientMode_values();
}

+ (PIGF_GradientMode *)valueOfWithNSString:(NSString *)name {
  return PIGF_GradientMode_valueOfWithNSString_(name);
}

- (PIGF_GradientMode_Enum)toNSEnum {
  return (PIGF_GradientMode_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "[LPIGF_GradientMode;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGF_GradientMode;", 0x9, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(values);
  methods[1].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "CLAMP", "LPIGF_GradientMode;", .constantValue.asLong = 0, 0x4019, -1, 2, -1, -1 },
    { "MIRROR", "LPIGF_GradientMode;", .constantValue.asLong = 0, 0x4019, -1, 3, -1, -1 },
    { "REPEAT", "LPIGF_GradientMode;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
  };
  static const void *ptrTable[] = { "valueOf", "LNSString;", &JreEnum(PIGF_GradientMode, CLAMP), &JreEnum(PIGF_GradientMode, MIRROR), &JreEnum(PIGF_GradientMode, REPEAT), "Ljava/lang/Enum<Lde/pidata/gui/view/figure/GradientMode;>;" };
  static const J2ObjcClassInfo _PIGF_GradientMode = { "GradientMode", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x4011, 2, 3, -1, -1, -1, 5, -1 };
  return &_PIGF_GradientMode;
}

+ (void)initialize {
  if (self == [PIGF_GradientMode class]) {
    JreEnum(PIGF_GradientMode, CLAMP) = new_PIGF_GradientMode_initWithNSString_withInt_(JreEnumConstantName(PIGF_GradientMode_class_(), 0), 0);
    JreEnum(PIGF_GradientMode, MIRROR) = new_PIGF_GradientMode_initWithNSString_withInt_(JreEnumConstantName(PIGF_GradientMode_class_(), 1), 1);
    JreEnum(PIGF_GradientMode, REPEAT) = new_PIGF_GradientMode_initWithNSString_withInt_(JreEnumConstantName(PIGF_GradientMode_class_(), 2), 2);
    J2OBJC_SET_INITIALIZED(PIGF_GradientMode)
  }
}

@end

void PIGF_GradientMode_initWithNSString_withInt_(PIGF_GradientMode *self, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
}

PIGF_GradientMode *new_PIGF_GradientMode_initWithNSString_withInt_(NSString *__name, jint __ordinal) {
  J2OBJC_NEW_IMPL(PIGF_GradientMode, initWithNSString_withInt_, __name, __ordinal)
}

IOSObjectArray *PIGF_GradientMode_values() {
  PIGF_GradientMode_initialize();
  return [IOSObjectArray arrayWithObjects:PIGF_GradientMode_values_ count:3 type:PIGF_GradientMode_class_()];
}

PIGF_GradientMode *PIGF_GradientMode_valueOfWithNSString_(NSString *name) {
  PIGF_GradientMode_initialize();
  for (int i = 0; i < 3; i++) {
    PIGF_GradientMode *e = PIGF_GradientMode_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

PIGF_GradientMode *PIGF_GradientMode_fromOrdinal(NSUInteger ordinal) {
  PIGF_GradientMode_initialize();
  if (ordinal >= 3) {
    return nil;
  }
  return PIGF_GradientMode_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_GradientMode)

J2OBJC_NAME_MAPPING(PIGF_GradientMode, "de.pidata.gui.view.figure", "PIGF_")
