//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/LineShapePI.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/view/figure/AbstractShapePI.h"
#include "de/pidata/gui/view/figure/Figure.h"
#include "de/pidata/gui/view/figure/LineShapePI.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/gui/view/figure/ShapeType.h"
#include "de/pidata/rect/LineRect.h"
#include "de/pidata/rect/Pos.h"
#include "java/lang/IndexOutOfBoundsException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/LineShapePI must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGF_LineShapePI () {
 @public
  id<PICR_Pos> startPos_;
  id<PICR_Pos> endPos_;
}

@end

J2OBJC_FIELD_SETTER(PIGF_LineShapePI, startPos_, id<PICR_Pos>)
J2OBJC_FIELD_SETTER(PIGF_LineShapePI, endPos_, id<PICR_Pos>)

@implementation PIGF_LineShapePI

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)figure
                       withPICR_Pos:(id<PICR_Pos>)startPos
                       withPICR_Pos:(id<PICR_Pos>)endPos {
  PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_(self, figure, startPos, endPos);
  return self;
}

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)figure
                       withPICR_Pos:(id<PICR_Pos>)startPos
                       withPICR_Pos:(id<PICR_Pos>)endPos
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)style {
  PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_(self, figure, startPos, endPos, style);
  return self;
}

- (PIGF_ShapeType *)getShapeType {
  return JreLoadEnum(PIGF_ShapeType, line);
}

- (id<PICR_Pos>)getStartPos {
  return startPos_;
}

- (id<PICR_Pos>)getEndPos {
  return endPos_;
}

- (jint)posCount {
  return 2;
}

- (id<PICR_Pos>)getPosWithInt:(jint)index {
  switch (index) {
    case 0:
    return [self getStartPos];
    case 1:
    return [self getEndPos];
    default:
    @throw new_JavaLangIndexOutOfBoundsException_initWithNSString_(JreStrcat("$I", @"Index must be 0 or 1, but is=", index));
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, "LPIGF_ShapeType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPICR_Pos;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPICR_Pos;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPICR_Pos;", 0x1, 2, 3, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGF_Figure:withPICR_Pos:withPICR_Pos:);
  methods[1].selector = @selector(initWithPIGF_Figure:withPICR_Pos:withPICR_Pos:withPIGF_ShapeStyle:);
  methods[2].selector = @selector(getShapeType);
  methods[3].selector = @selector(getStartPos);
  methods[4].selector = @selector(getEndPos);
  methods[5].selector = @selector(posCount);
  methods[6].selector = @selector(getPosWithInt:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "startPos_", "LPICR_Pos;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "endPos_", "LPICR_Pos;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIGF_Figure;LPICR_Pos;LPICR_Pos;", "LPIGF_Figure;LPICR_Pos;LPICR_Pos;LPIGF_ShapeStyle;", "getPos", "I" };
  static const J2ObjcClassInfo _PIGF_LineShapePI = { "LineShapePI", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x1, 7, 2, -1, -1, -1, -1, -1 };
  return &_PIGF_LineShapePI;
}

@end

void PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_(PIGF_LineShapePI *self, id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos) {
  PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_(self, figure, startPos, endPos, JreLoadStatic(PIGF_ShapeStyle, STYLE_BLACK));
}

PIGF_LineShapePI *new_PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_(id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos) {
  J2OBJC_NEW_IMPL(PIGF_LineShapePI, initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_, figure, startPos, endPos)
}

PIGF_LineShapePI *create_PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_(id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos) {
  J2OBJC_CREATE_IMPL(PIGF_LineShapePI, initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_, figure, startPos, endPos)
}

void PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_(PIGF_LineShapePI *self, id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos, PIGF_ShapeStyle *style) {
  PIGF_AbstractShapePI_initWithPIGF_Figure_withPICR_Rect_withPIGF_ShapeStyle_(self, figure, new_PICR_LineRect_initWithPICR_Pos_withPICR_Pos_(startPos, endPos), style);
  self->startPos_ = startPos;
  self->endPos_ = endPos;
}

PIGF_LineShapePI *new_PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_(id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos, PIGF_ShapeStyle *style) {
  J2OBJC_NEW_IMPL(PIGF_LineShapePI, initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_, figure, startPos, endPos, style)
}

PIGF_LineShapePI *create_PIGF_LineShapePI_initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_(id<PIGF_Figure> figure, id<PICR_Pos> startPos, id<PICR_Pos> endPos, PIGF_ShapeStyle *style) {
  J2OBJC_CREATE_IMPL(PIGF_LineShapePI, initWithPIGF_Figure_withPICR_Pos_withPICR_Pos_withPIGF_ShapeStyle_, figure, startPos, endPos, style)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_LineShapePI)

J2OBJC_NAME_MAPPING(PIGF_LineShapePI, "de.pidata.gui.view.figure", "PIGF_")
