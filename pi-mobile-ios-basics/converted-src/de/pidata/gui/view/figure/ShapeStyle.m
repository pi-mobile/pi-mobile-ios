//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/ShapeStyle.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/component/base/Platform.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/gui/view/figure/StrokeType.h"
#include "de/pidata/gui/view/figure/StyleEventSender.h"
#include "de/pidata/gui/view/figure/TextAlign.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Double.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/ShapeStyle must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGF_ShapeStyle () {
 @public
  jboolean isFillColorTransparent_;
  id<PIGB_ComponentColor> borderColor_;
  id<PIGB_ComponentColor> fillColor_;
  id<PIGB_ComponentColor> shadowColor_;
  NSString *fontName_;
  PIGF_TextAlign *textAlign_;
  jdouble strokeWidth_;
  PIGF_StrokeType *strokeType_;
  jdouble shadowRadius_;
}

- (jboolean)equalColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldColor
                      withPIGB_ComponentColor:(id<PIGB_ComponentColor>)newColor;

@end

J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, borderColor_, id<PIGB_ComponentColor>)
J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, fillColor_, id<PIGB_ComponentColor>)
J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, shadowColor_, id<PIGB_ComponentColor>)
J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, fontName_, NSString *)
J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, textAlign_, PIGF_TextAlign *)
J2OBJC_FIELD_SETTER(PIGF_ShapeStyle, strokeType_, PIGF_StrokeType *)

__attribute__((unused)) static jboolean PIGF_ShapeStyle_equalColorWithPIGB_ComponentColor_withPIGB_ComponentColor_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> oldColor, id<PIGB_ComponentColor> newColor);

J2OBJC_INITIALIZED_DEFN(PIGF_ShapeStyle)

PIGF_StrokeType *PIGF_ShapeStyle_DEFAULT_STROKE_TYPE;
PIGF_TextAlign *PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN;
id<PIGB_ComponentColor> PIGF_ShapeStyle_COLOR_TRANSPARENT;
id<PIGB_ComponentColor> PIGF_ShapeStyle_COLOR_TEMP_TRANSPARENT;
PIGF_ShapeStyle *PIGF_ShapeStyle_STYLE_WHITE;
PIGF_ShapeStyle *PIGF_ShapeStyle_STYLE_BLACK;

@implementation PIGF_ShapeStyle

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIGF_ShapeStyle_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_(self, color);
  return self;
}

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color
                                 withDouble:(jdouble)strokeWidth {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withDouble_(self, color, strokeWidth);
  return self;
}

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)borderColor
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)fillColor {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, borderColor, fillColor);
  return self;
}

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)borderColor
                    withPIGB_ComponentColor:(id<PIGB_ComponentColor>)fillColor
                                 withDouble:(jdouble)strokeWidth {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(self, borderColor, fillColor, strokeWidth);
  return self;
}

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color
                               withNSString:(NSString *)fontName {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_(self, color, fontName);
  return self;
}

- (instancetype)initWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color
                               withNSString:(NSString *)fontName
                         withPIGF_TextAlign:(PIGF_TextAlign *)textAlign {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_(self, color, fontName, textAlign);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)colorID
                       withDouble:(jdouble)strokeWidth {
  PIGF_ShapeStyle_initWithPIQ_QName_withDouble_(self, colorID, strokeWidth);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)borderColorID
                    withPIQ_QName:(PIQ_QName *)fillColorID {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(self, borderColorID, fillColorID);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)colorID {
  PIGF_ShapeStyle_initWithPIQ_QName_(self, colorID);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)borderColorID
                    withPIQ_QName:(PIQ_QName *)fillColorID
                       withDouble:(jdouble)strokeWidth {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(self, borderColorID, fillColorID, strokeWidth);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)colorID
                     withNSString:(NSString *)fontName {
  PIGF_ShapeStyle_initWithPIQ_QName_withNSString_(self, colorID, fontName);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)colorID
                     withNSString:(NSString *)fontName
               withPIGF_TextAlign:(PIGF_TextAlign *)textAlign {
  PIGF_ShapeStyle_initWithPIQ_QName_withNSString_withPIGF_TextAlign_(self, colorID, fontName, textAlign);
  return self;
}

- (void)addShadowWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)shadowColor
                              withDouble:(jdouble)shadowRadius {
  self->shadowColor_ = shadowColor;
  self->shadowRadius_ = shadowRadius;
}

- (void)addShadowWithPIQ_QName:(PIQ_QName *)shadowColorID
                    withDouble:(jdouble)shadowRadius {
  self->shadowColor_ = [((PIGB_Platform *) nil_chk(PIGB_Platform_getInstance())) getColorWithPIQ_QName:shadowColorID];
  self->shadowRadius_ = shadowRadius;
}

- (id<PIGB_ComponentColor>)getBorderColor {
  return borderColor_;
}

- (void)setBorderColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)borderColor {
  id<PIGB_ComponentColor> oldBorderColor = self->borderColor_;
  self->borderColor_ = borderColor;
  [self fireColorChangedWithPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:fillColor_ withBoolean:isFillColorTransparent_];
}

- (void)setBorderColorWithPIQ_QName:(PIQ_QName *)borderColorID {
  id<PIGB_ComponentColor> oldBorderColor = self->borderColor_;
  PIGB_Platform *platform = PIGB_Platform_getInstance();
  self->borderColor_ = [((PIGB_Platform *) nil_chk(platform)) getColorWithPIQ_QName:borderColorID];
  [self fireColorChangedWithPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:fillColor_ withBoolean:isFillColorTransparent_];
}

- (id<PIGB_ComponentColor>)getFillColor {
  return fillColor_;
}

- (void)setFillColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)fillColor {
  id<PIGB_ComponentColor> oldFillColor = self->fillColor_;
  jboolean oldIsTransparent = isFillColorTransparent_;
  self->fillColor_ = fillColor;
  isFillColorTransparent_ = ([((id<PIGB_ComponentColor>) nil_chk(PIGF_ShapeStyle_COLOR_TRANSPARENT)) isEqual:fillColor]);
  [self fireColorChangedWithPIGB_ComponentColor:borderColor_ withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
}

- (void)setFillColorWithPIQ_QName:(PIQ_QName *)fillColorID {
  id<PIGB_ComponentColor> oldFillColor = self->fillColor_;
  PIGB_Platform *platform = PIGB_Platform_getInstance();
  self->fillColor_ = [((PIGB_Platform *) nil_chk(platform)) getColorWithPIQ_QName:fillColorID];
  jboolean oldIsTransparent = isFillColorTransparent_;
  isFillColorTransparent_ = ([((id<PIGB_ComponentColor>) nil_chk(PIGF_ShapeStyle_COLOR_TRANSPARENT)) isEqual:fillColor_]);
  [self fireColorChangedWithPIGB_ComponentColor:borderColor_ withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
}

- (void)setColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color {
  id<PIGB_ComponentColor> oldBorderColor = self->borderColor_;
  id<PIGB_ComponentColor> oldFillColor = self->fillColor_;
  jboolean oldIsTransparent = isFillColorTransparent_;
  self->borderColor_ = color;
  self->fillColor_ = color;
  [self fireColorChangedWithPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
}

- (void)setColorWithPIQ_QName:(PIQ_QName *)colorID {
  id<PIGB_ComponentColor> oldBorderColor = self->borderColor_;
  id<PIGB_ComponentColor> oldFillColor = self->fillColor_;
  jboolean oldIsTransparent = isFillColorTransparent_;
  PIGB_Platform *platform = PIGB_Platform_getInstance();
  self->borderColor_ = [((PIGB_Platform *) nil_chk(platform)) getColorWithPIQ_QName:colorID];
  self->fillColor_ = [platform getColorWithPIQ_QName:colorID];
  [self fireColorChangedWithPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
}

- (jboolean)isFillColorTransparent {
  return isFillColorTransparent_;
}

- (NSString *)getFontName {
  return fontName_;
}

- (void)setFontNameWithNSString:(NSString *)fontName {
  NSString *oldFontName = self->fontName_;
  self->fontName_ = fontName;
  [self fireFontChangedWithNSString:oldFontName withPIGF_TextAlign:textAlign_];
}

- (PIGF_TextAlign *)getTextAlign {
  return textAlign_;
}

- (void)setTextAlignWithPIGF_TextAlign:(PIGF_TextAlign *)textAlign {
  PIGF_TextAlign *oldTextAlign = self->textAlign_;
  self->textAlign_ = textAlign;
  [self fireFontChangedWithNSString:fontName_ withPIGF_TextAlign:oldTextAlign];
}

- (jdouble)getStrokeWidth {
  return strokeWidth_;
}

- (void)setStrokeWidthWithDouble:(jdouble)strokeWidth {
  jdouble oldStrokeWidth = self->strokeWidth_;
  self->strokeWidth_ = strokeWidth;
  [self fireStrokeChangedWithPIGF_StrokeType:strokeType_ withDouble:oldStrokeWidth];
}

- (PIGF_StrokeType *)getStrokeType {
  return strokeType_;
}

- (void)setStrokeTypeWithPIGF_StrokeType:(PIGF_StrokeType *)strokeType {
  PIGF_StrokeType *oldStrokeType = self->strokeType_;
  self->strokeType_ = strokeType;
  [self fireStrokeChangedWithPIGF_StrokeType:oldStrokeType withDouble:strokeWidth_];
}

- (id<PIGB_ComponentColor>)getShadowColor {
  return shadowColor_;
}

- (void)setShadowColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)shadowColor {
  if (!JreObjectEqualsEquals(shadowColor, self->shadowColor_)) {
    self->shadowColor_ = shadowColor;
    [self fireStyleChanged];
  }
}

- (void)setShadowColorWithPIQ_QName:(PIQ_QName *)colorID {
  id<PIGB_ComponentColor> newColor = [((PIGB_Platform *) nil_chk(PIGB_Platform_getInstance())) getColorWithPIQ_QName:colorID];
  if (!JreObjectEqualsEquals(newColor, shadowColor_)) {
    self->shadowColor_ = newColor;
    [self fireStyleChanged];
  }
}

- (jdouble)getShadowRadius {
  return shadowRadius_;
}

- (void)setShadowRadiusWithDouble:(jdouble)shadowRadius {
  if (shadowRadius != self->shadowRadius_) {
    self->shadowRadius_ = shadowRadius;
    [self fireStyleChanged];
  }
}

- (void)removeShadow {
  if (shadowColor_ != nil || shadowRadius_ > 0) {
    self->shadowColor_ = nil;
    self->shadowRadius_ = 0;
    [self fireStyleChanged];
  }
}

- (PIGF_StyleEventSender *)getEventSender {
  if (eventSender_ == nil) {
    eventSender_ = new_PIGF_StyleEventSender_initWithPIGF_ShapeStyle_(self);
  }
  return eventSender_;
}

- (jboolean)equalColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldColor
                      withPIGB_ComponentColor:(id<PIGB_ComponentColor>)newColor {
  return PIGF_ShapeStyle_equalColorWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, oldColor, newColor);
}

- (void)fireColorChangedWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldBorderColor
                        withPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldFillColor
                                    withBoolean:(jboolean)oldIsTransparent {
  if (eventSender_ != nil) {
    if (!PIGF_ShapeStyle_equalColorWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, oldBorderColor, borderColor_) || !PIGF_ShapeStyle_equalColorWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, oldFillColor, fillColor_) || (oldIsTransparent != isFillColorTransparent_)) {
      [((PIGF_StyleEventSender *) nil_chk(eventSender_)) fireColorChangedWithPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
    }
  }
}

- (void)fireFontChangedWithNSString:(NSString *)oldFontName
                 withPIGF_TextAlign:(PIGF_TextAlign *)oldTextAlign {
  if (eventSender_ != nil) {
    if (![((NSString *) nil_chk(fontName_)) isEqual:oldFontName] || oldTextAlign != textAlign_) {
      [((PIGF_StyleEventSender *) nil_chk(eventSender_)) fireFontChangedWithNSString:oldFontName withPIGF_TextAlign:oldTextAlign];
    }
  }
}

- (void)fireStrokeChangedWithPIGF_StrokeType:(PIGF_StrokeType *)oldStrokeType
                                  withDouble:(jdouble)oldStrokeWidth {
  if (eventSender_ != nil) {
    if (oldStrokeType != strokeType_ || (JavaLangDouble_compareWithDouble_withDouble_(oldStrokeWidth, strokeWidth_) != 0)) {
      [((PIGF_StyleEventSender *) nil_chk(eventSender_)) fireStrokeChangedWithPIGF_StrokeType:oldStrokeType withDouble:oldStrokeWidth];
    }
  }
}

- (void)fireStyleChanged {
  if (eventSender_ != nil) {
    [eventSender_ fireStyleChanged];
  }
}

+ (PIGF_ShapeStyle *)createCopyWithPIGF_ShapeStyle:(PIGF_ShapeStyle *)shapeStyle {
  return PIGF_ShapeStyle_createCopyWithPIGF_ShapeStyle_(shapeStyle);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 2, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 3, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 4, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 5, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 6, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 7, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 8, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 9, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 10, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 6, -1, -1, -1, -1 },
    { NULL, "LPIGB_ComponentColor;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 8, -1, -1, -1, -1 },
    { NULL, "LPIGB_ComponentColor;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 8, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 16, 17, -1, -1, -1, -1 },
    { NULL, "LPIGF_TextAlign;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "D", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 20, 21, -1, -1, -1, -1 },
    { NULL, "LPIGF_StrokeType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 22, 23, -1, -1, -1, -1 },
    { NULL, "LPIGB_ComponentColor;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 24, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 24, 8, -1, -1, -1, -1 },
    { NULL, "D", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 25, 21, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGF_StyleEventSender;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x2, 26, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 27, 28, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 29, 30, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 31, 32, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGF_ShapeStyle;", 0x9, 33, 34, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIGB_ComponentColor:);
  methods[2].selector = @selector(initWithPIGB_ComponentColor:withDouble:);
  methods[3].selector = @selector(initWithPIGB_ComponentColor:withPIGB_ComponentColor:);
  methods[4].selector = @selector(initWithPIGB_ComponentColor:withPIGB_ComponentColor:withDouble:);
  methods[5].selector = @selector(initWithPIGB_ComponentColor:withNSString:);
  methods[6].selector = @selector(initWithPIGB_ComponentColor:withNSString:withPIGF_TextAlign:);
  methods[7].selector = @selector(initWithPIQ_QName:withDouble:);
  methods[8].selector = @selector(initWithPIQ_QName:withPIQ_QName:);
  methods[9].selector = @selector(initWithPIQ_QName:);
  methods[10].selector = @selector(initWithPIQ_QName:withPIQ_QName:withDouble:);
  methods[11].selector = @selector(initWithPIQ_QName:withNSString:);
  methods[12].selector = @selector(initWithPIQ_QName:withNSString:withPIGF_TextAlign:);
  methods[13].selector = @selector(addShadowWithPIGB_ComponentColor:withDouble:);
  methods[14].selector = @selector(addShadowWithPIQ_QName:withDouble:);
  methods[15].selector = @selector(getBorderColor);
  methods[16].selector = @selector(setBorderColorWithPIGB_ComponentColor:);
  methods[17].selector = @selector(setBorderColorWithPIQ_QName:);
  methods[18].selector = @selector(getFillColor);
  methods[19].selector = @selector(setFillColorWithPIGB_ComponentColor:);
  methods[20].selector = @selector(setFillColorWithPIQ_QName:);
  methods[21].selector = @selector(setColorWithPIGB_ComponentColor:);
  methods[22].selector = @selector(setColorWithPIQ_QName:);
  methods[23].selector = @selector(isFillColorTransparent);
  methods[24].selector = @selector(getFontName);
  methods[25].selector = @selector(setFontNameWithNSString:);
  methods[26].selector = @selector(getTextAlign);
  methods[27].selector = @selector(setTextAlignWithPIGF_TextAlign:);
  methods[28].selector = @selector(getStrokeWidth);
  methods[29].selector = @selector(setStrokeWidthWithDouble:);
  methods[30].selector = @selector(getStrokeType);
  methods[31].selector = @selector(setStrokeTypeWithPIGF_StrokeType:);
  methods[32].selector = @selector(getShadowColor);
  methods[33].selector = @selector(setShadowColorWithPIGB_ComponentColor:);
  methods[34].selector = @selector(setShadowColorWithPIQ_QName:);
  methods[35].selector = @selector(getShadowRadius);
  methods[36].selector = @selector(setShadowRadiusWithDouble:);
  methods[37].selector = @selector(removeShadow);
  methods[38].selector = @selector(getEventSender);
  methods[39].selector = @selector(equalColorWithPIGB_ComponentColor:withPIGB_ComponentColor:);
  methods[40].selector = @selector(fireColorChangedWithPIGB_ComponentColor:withPIGB_ComponentColor:withBoolean:);
  methods[41].selector = @selector(fireFontChangedWithNSString:withPIGF_TextAlign:);
  methods[42].selector = @selector(fireStrokeChangedWithPIGF_StrokeType:withDouble:);
  methods[43].selector = @selector(fireStyleChanged);
  methods[44].selector = @selector(createCopyWithPIGF_ShapeStyle:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "DEFAULT_STROKE_WIDTH", "D", .constantValue.asDouble = PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH, 0x19, -1, -1, -1, -1 },
    { "DEFAULT_STROKE_TYPE", "LPIGF_StrokeType;", .constantValue.asLong = 0, 0x19, -1, 35, -1, -1 },
    { "DEFAULT_TEXT_ALIGN", "LPIGF_TextAlign;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "COLOR_TRANSPARENT", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "COLOR_TEMP_TRANSPARENT", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "isFillColorTransparent_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "STYLE_WHITE", "LPIGF_ShapeStyle;", .constantValue.asLong = 0, 0x19, -1, 39, -1, -1 },
    { "STYLE_BLACK", "LPIGF_ShapeStyle;", .constantValue.asLong = 0, 0x19, -1, 40, -1, -1 },
    { "eventSender_", "LPIGF_StyleEventSender;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "borderColor_", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "fillColor_", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "shadowColor_", "LPIGB_ComponentColor;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "fontName_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "textAlign_", "LPIGF_TextAlign;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "strokeWidth_", "D", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "strokeType_", "LPIGF_StrokeType;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "shadowRadius_", "D", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIGB_ComponentColor;", "LPIGB_ComponentColor;D", "LPIGB_ComponentColor;LPIGB_ComponentColor;", "LPIGB_ComponentColor;LPIGB_ComponentColor;D", "LPIGB_ComponentColor;LNSString;", "LPIGB_ComponentColor;LNSString;LPIGF_TextAlign;", "LPIQ_QName;D", "LPIQ_QName;LPIQ_QName;", "LPIQ_QName;", "LPIQ_QName;LPIQ_QName;D", "LPIQ_QName;LNSString;", "LPIQ_QName;LNSString;LPIGF_TextAlign;", "addShadow", "setBorderColor", "setFillColor", "setColor", "setFontName", "LNSString;", "setTextAlign", "LPIGF_TextAlign;", "setStrokeWidth", "D", "setStrokeType", "LPIGF_StrokeType;", "setShadowColor", "setShadowRadius", "equalColor", "fireColorChanged", "LPIGB_ComponentColor;LPIGB_ComponentColor;Z", "fireFontChanged", "LNSString;LPIGF_TextAlign;", "fireStrokeChanged", "LPIGF_StrokeType;D", "createCopy", "LPIGF_ShapeStyle;", &PIGF_ShapeStyle_DEFAULT_STROKE_TYPE, &PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN, &PIGF_ShapeStyle_COLOR_TRANSPARENT, &PIGF_ShapeStyle_COLOR_TEMP_TRANSPARENT, &PIGF_ShapeStyle_STYLE_WHITE, &PIGF_ShapeStyle_STYLE_BLACK };
  static const J2ObjcClassInfo _PIGF_ShapeStyle = { "ShapeStyle", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x1, 45, 17, -1, -1, -1, -1, -1 };
  return &_PIGF_ShapeStyle;
}

+ (void)initialize {
  if (self == [PIGF_ShapeStyle class]) {
    PIGF_ShapeStyle_DEFAULT_STROKE_TYPE = JreLoadEnum(PIGF_StrokeType, CENTERED);
    PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN = JreLoadEnum(PIGF_TextAlign, LEFT);
    PIGF_ShapeStyle_COLOR_TRANSPARENT = [((PIGB_Platform *) nil_chk(PIGB_Platform_getInstance())) getColorWithPIQ_QName:JreLoadStatic(PIGB_ComponentColor, TRANSPARENT)];
    PIGF_ShapeStyle_COLOR_TEMP_TRANSPARENT = [((PIGB_Platform *) nil_chk(PIGB_Platform_getInstance())) getColorWithPIQ_QName:JreLoadStatic(PIGB_ComponentColor, BLACK)];
    PIGF_ShapeStyle_STYLE_WHITE = new_PIGF_ShapeStyle_initWithPIQ_QName_(JreLoadStatic(PIGB_ComponentColor, WHITE));
    PIGF_ShapeStyle_STYLE_BLACK = new_PIGF_ShapeStyle_initWithPIQ_QName_(JreLoadStatic(PIGB_ComponentColor, BLACK));
    J2OBJC_SET_INITIALIZED(PIGF_ShapeStyle)
  }
}

@end

void PIGF_ShapeStyle_init(PIGF_ShapeStyle *self) {
  NSObject_init(self);
  self->textAlign_ = PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN;
  self->strokeWidth_ = PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH;
  self->strokeType_ = PIGF_ShapeStyle_DEFAULT_STROKE_TYPE;
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_init() {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, init)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_init() {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, init)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> color) {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, color, color);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_(id<PIGB_ComponentColor> color) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_, color)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_(id<PIGB_ComponentColor> color) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_, color)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_withDouble_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> color, jdouble strokeWidth) {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(self, color, color, strokeWidth);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withDouble_(id<PIGB_ComponentColor> color, jdouble strokeWidth) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withDouble_, color, strokeWidth)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withDouble_(id<PIGB_ComponentColor> color, jdouble strokeWidth) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withDouble_, color, strokeWidth)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor) {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(self, borderColor, fillColor, PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withPIGB_ComponentColor_, borderColor, fillColor)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withPIGB_ComponentColor_, borderColor, fillColor)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor, jdouble strokeWidth) {
  NSObject_init(self);
  self->textAlign_ = PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN;
  self->strokeWidth_ = PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH;
  self->strokeType_ = PIGF_ShapeStyle_DEFAULT_STROKE_TYPE;
  self->borderColor_ = borderColor;
  self->fillColor_ = fillColor;
  if ([((id<PIGB_ComponentColor>) nil_chk(PIGF_ShapeStyle_COLOR_TRANSPARENT)) isEqual:fillColor]) {
    self->isFillColorTransparent_ = true;
  }
  self->strokeWidth_ = strokeWidth;
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor, jdouble strokeWidth) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_, borderColor, fillColor, strokeWidth)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_(id<PIGB_ComponentColor> borderColor, id<PIGB_ComponentColor> fillColor, jdouble strokeWidth) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withPIGB_ComponentColor_withDouble_, borderColor, fillColor, strokeWidth)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> color, NSString *fontName) {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_(self, color, fontName, PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_(id<PIGB_ComponentColor> color, NSString *fontName) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withNSString_, color, fontName)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_(id<PIGB_ComponentColor> color, NSString *fontName) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withNSString_, color, fontName)
}

void PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> color, NSString *fontName, PIGF_TextAlign *textAlign) {
  PIGF_ShapeStyle_initWithPIGB_ComponentColor_withPIGB_ComponentColor_(self, color, color);
  self->fontName_ = fontName;
  self->textAlign_ = textAlign;
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_(id<PIGB_ComponentColor> color, NSString *fontName, PIGF_TextAlign *textAlign) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_, color, fontName, textAlign)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_(id<PIGB_ComponentColor> color, NSString *fontName, PIGF_TextAlign *textAlign) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIGB_ComponentColor_withNSString_withPIGF_TextAlign_, color, fontName, textAlign)
}

void PIGF_ShapeStyle_initWithPIQ_QName_withDouble_(PIGF_ShapeStyle *self, PIQ_QName *colorID, jdouble strokeWidth) {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(self, colorID, colorID, strokeWidth);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_withDouble_(PIQ_QName *colorID, jdouble strokeWidth) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withDouble_, colorID, strokeWidth)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_withDouble_(PIQ_QName *colorID, jdouble strokeWidth) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withDouble_, colorID, strokeWidth)
}

void PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(PIGF_ShapeStyle *self, PIQ_QName *borderColorID, PIQ_QName *fillColorID) {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(self, borderColorID, fillColorID, PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *borderColorID, PIQ_QName *fillColorID) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withPIQ_QName_, borderColorID, fillColorID)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *borderColorID, PIQ_QName *fillColorID) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withPIQ_QName_, borderColorID, fillColorID)
}

void PIGF_ShapeStyle_initWithPIQ_QName_(PIGF_ShapeStyle *self, PIQ_QName *colorID) {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(self, colorID, colorID);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_(PIQ_QName *colorID) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_, colorID)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_(PIQ_QName *colorID) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_, colorID)
}

void PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(PIGF_ShapeStyle *self, PIQ_QName *borderColorID, PIQ_QName *fillColorID, jdouble strokeWidth) {
  NSObject_init(self);
  self->textAlign_ = PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN;
  self->strokeWidth_ = PIGF_ShapeStyle_DEFAULT_STROKE_WIDTH;
  self->strokeType_ = PIGF_ShapeStyle_DEFAULT_STROKE_TYPE;
  PIGB_Platform *platform = PIGB_Platform_getInstance();
  if (borderColorID == nil) {
    self->borderColor_ = nil;
  }
  else {
    self->borderColor_ = [((PIGB_Platform *) nil_chk(platform)) getColorWithPIQ_QName:borderColorID];
  }
  if (fillColorID == nil) {
    self->fillColor_ = nil;
  }
  else {
    self->fillColor_ = [((PIGB_Platform *) nil_chk(platform)) getColorWithPIQ_QName:fillColorID];
  }
  if ([((id<PIGB_ComponentColor>) nil_chk(PIGF_ShapeStyle_COLOR_TRANSPARENT)) isEqual:self->fillColor_]) {
    self->isFillColorTransparent_ = true;
  }
  self->strokeWidth_ = strokeWidth;
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(PIQ_QName *borderColorID, PIQ_QName *fillColorID, jdouble strokeWidth) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withPIQ_QName_withDouble_, borderColorID, fillColorID, strokeWidth)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_withDouble_(PIQ_QName *borderColorID, PIQ_QName *fillColorID, jdouble strokeWidth) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withPIQ_QName_withDouble_, borderColorID, fillColorID, strokeWidth)
}

void PIGF_ShapeStyle_initWithPIQ_QName_withNSString_(PIGF_ShapeStyle *self, PIQ_QName *colorID, NSString *fontName) {
  PIGF_ShapeStyle_initWithPIQ_QName_withNSString_withPIGF_TextAlign_(self, colorID, fontName, PIGF_ShapeStyle_DEFAULT_TEXT_ALIGN);
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_withNSString_(PIQ_QName *colorID, NSString *fontName) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withNSString_, colorID, fontName)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_withNSString_(PIQ_QName *colorID, NSString *fontName) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withNSString_, colorID, fontName)
}

void PIGF_ShapeStyle_initWithPIQ_QName_withNSString_withPIGF_TextAlign_(PIGF_ShapeStyle *self, PIQ_QName *colorID, NSString *fontName, PIGF_TextAlign *textAlign) {
  PIGF_ShapeStyle_initWithPIQ_QName_withPIQ_QName_(self, colorID, colorID);
  self->fontName_ = fontName;
  self->textAlign_ = textAlign;
}

PIGF_ShapeStyle *new_PIGF_ShapeStyle_initWithPIQ_QName_withNSString_withPIGF_TextAlign_(PIQ_QName *colorID, NSString *fontName, PIGF_TextAlign *textAlign) {
  J2OBJC_NEW_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withNSString_withPIGF_TextAlign_, colorID, fontName, textAlign)
}

PIGF_ShapeStyle *create_PIGF_ShapeStyle_initWithPIQ_QName_withNSString_withPIGF_TextAlign_(PIQ_QName *colorID, NSString *fontName, PIGF_TextAlign *textAlign) {
  J2OBJC_CREATE_IMPL(PIGF_ShapeStyle, initWithPIQ_QName_withNSString_withPIGF_TextAlign_, colorID, fontName, textAlign)
}

jboolean PIGF_ShapeStyle_equalColorWithPIGB_ComponentColor_withPIGB_ComponentColor_(PIGF_ShapeStyle *self, id<PIGB_ComponentColor> oldColor, id<PIGB_ComponentColor> newColor) {
  if (oldColor == nil) {
    return newColor == nil;
  }
  else {
    return [oldColor isEqual:newColor];
  }
}

PIGF_ShapeStyle *PIGF_ShapeStyle_createCopyWithPIGF_ShapeStyle_(PIGF_ShapeStyle *shapeStyle) {
  PIGF_ShapeStyle_initialize();
  PIGF_ShapeStyle *newShapeStyle = new_PIGF_ShapeStyle_init();
  newShapeStyle->borderColor_ = ((PIGF_ShapeStyle *) nil_chk(shapeStyle))->borderColor_;
  newShapeStyle->fillColor_ = shapeStyle->fillColor_;
  newShapeStyle->isFillColorTransparent_ = shapeStyle->isFillColorTransparent_;
  newShapeStyle->shadowColor_ = shapeStyle->shadowColor_;
  newShapeStyle->fontName_ = shapeStyle->fontName_;
  newShapeStyle->textAlign_ = shapeStyle->textAlign_;
  newShapeStyle->strokeWidth_ = shapeStyle->strokeWidth_;
  newShapeStyle->strokeType_ = shapeStyle->strokeType_;
  newShapeStyle->shadowRadius_ = shapeStyle->shadowRadius_;
  return newShapeStyle;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_ShapeStyle)

J2OBJC_NAME_MAPPING(PIGF_ShapeStyle, "de.pidata.gui.view.figure", "PIGF_")
