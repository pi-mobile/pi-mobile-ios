//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/StrokeType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiViewFigureStrokeType")
#ifdef RESTRICT_DePidataGuiViewFigureStrokeType
#define INCLUDE_ALL_DePidataGuiViewFigureStrokeType 0
#else
#define INCLUDE_ALL_DePidataGuiViewFigureStrokeType 1
#endif
#undef RESTRICT_DePidataGuiViewFigureStrokeType

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (PIGF_StrokeType_) && (INCLUDE_ALL_DePidataGuiViewFigureStrokeType || defined(INCLUDE_PIGF_StrokeType))
#define PIGF_StrokeType_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, PIGF_StrokeType_Enum) {
  PIGF_StrokeType_Enum_CENTERED = 0,
  PIGF_StrokeType_Enum_INSIDE = 1,
  PIGF_StrokeType_Enum_OUTSIDE = 2,
};

@interface PIGF_StrokeType : JavaLangEnum

#pragma mark Public

+ (PIGF_StrokeType *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (PIGF_StrokeType_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(PIGF_StrokeType)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT PIGF_StrokeType *PIGF_StrokeType_values_[];

inline PIGF_StrokeType *PIGF_StrokeType_get_CENTERED(void);
J2OBJC_ENUM_CONSTANT(PIGF_StrokeType, CENTERED)

inline PIGF_StrokeType *PIGF_StrokeType_get_INSIDE(void);
J2OBJC_ENUM_CONSTANT(PIGF_StrokeType, INSIDE)

inline PIGF_StrokeType *PIGF_StrokeType_get_OUTSIDE(void);
J2OBJC_ENUM_CONSTANT(PIGF_StrokeType, OUTSIDE)

FOUNDATION_EXPORT IOSObjectArray *PIGF_StrokeType_values(void);

FOUNDATION_EXPORT PIGF_StrokeType *PIGF_StrokeType_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT PIGF_StrokeType *PIGF_StrokeType_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(PIGF_StrokeType)

@compatibility_alias DePidataGuiViewFigureStrokeType PIGF_StrokeType;

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_DePidataGuiViewFigureStrokeType")
