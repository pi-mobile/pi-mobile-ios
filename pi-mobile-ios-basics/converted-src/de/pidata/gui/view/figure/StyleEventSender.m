//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/gui/pi-gui-basics/src/de/pidata/gui/view/figure/StyleEventSender.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/gui/view/figure/StrokeType.h"
#include "de/pidata/gui/view/figure/StyleEventListener.h"
#include "de/pidata/gui/view/figure/StyleEventSender.h"
#include "de/pidata/gui/view/figure/TextAlign.h"
#include "de/pidata/log/Logger.h"
#include "java/lang/Exception.h"
#include "java/util/LinkedList.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/gui/view/figure/StyleEventSender must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIGF_StyleEventSender () {
 @public
  PIGF_ShapeStyle *owner_;
  id<JavaUtilList> listenerList_;
}

@end

J2OBJC_FIELD_SETTER(PIGF_StyleEventSender, owner_, PIGF_ShapeStyle *)
J2OBJC_FIELD_SETTER(PIGF_StyleEventSender, listenerList_, id<JavaUtilList>)

@implementation PIGF_StyleEventSender

- (instancetype)initWithPIGF_ShapeStyle:(PIGF_ShapeStyle *)owner {
  PIGF_StyleEventSender_initWithPIGF_ShapeStyle_(self, owner);
  return self;
}

- (id)getOwner {
  return owner_;
}

- (void)addListenerWithPIGF_StyleEventListener:(id<PIGF_StyleEventListener>)listener {
  if (self->listenerList_ == nil) {
    self->listenerList_ = new_JavaUtilLinkedList_init();
  }
  [self->listenerList_ addWithId:listener];
}

- (void)removeListenerWithPIGF_StyleEventListener:(id<PIGF_StyleEventListener>)listener {
  if (self->listenerList_ != nil) {
    [self->listenerList_ removeWithId:listener];
  }
}

- (void)fireColorChangedWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldBorderColor
                        withPIGB_ComponentColor:(id<PIGB_ComponentColor>)oldFillColor
                                    withBoolean:(jboolean)oldIsTransparent {
  if (self->listenerList_ != nil) {
    IOSObjectArray *listenerArr = [IOSObjectArray newArrayWithLength:[listenerList_ size] type:PIGF_StyleEventListener_class_()];
    (void) [((id<JavaUtilList>) nil_chk(listenerList_)) toArrayWithNSObjectArray:listenerArr];
    id<PIGF_StyleEventListener> listener;
    for (jint i = 0; i < listenerArr->size_; i++) {
      listener = IOSObjectArray_Get(listenerArr, i);
      @try {
        [((id<PIGF_StyleEventListener>) nil_chk(listener)) colorChangedWithPIGF_ShapeStyle:owner_ withPIGB_ComponentColor:oldBorderColor withPIGB_ComponentColor:oldFillColor withBoolean:oldIsTransparent];
      }
      @catch (JavaLangException *ex) {
        PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error while event processing", ex);
      }
    }
  }
}

- (void)fireFontChangedWithNSString:(NSString *)oldFontName
                 withPIGF_TextAlign:(PIGF_TextAlign *)oldTextAlign {
  if (self->listenerList_ != nil) {
    IOSObjectArray *listenerArr = [IOSObjectArray newArrayWithLength:[listenerList_ size] type:PIGF_StyleEventListener_class_()];
    (void) [((id<JavaUtilList>) nil_chk(listenerList_)) toArrayWithNSObjectArray:listenerArr];
    id<PIGF_StyleEventListener> listener;
    for (jint i = 0; i < listenerArr->size_; i++) {
      listener = IOSObjectArray_Get(listenerArr, i);
      @try {
        [((id<PIGF_StyleEventListener>) nil_chk(listener)) fontChangedWithPIGF_ShapeStyle:owner_ withNSString:oldFontName withPIGF_TextAlign:oldTextAlign];
      }
      @catch (JavaLangException *ex) {
        PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error while event processing", ex);
      }
    }
  }
}

- (void)fireStrokeChangedWithPIGF_StrokeType:(PIGF_StrokeType *)oldStrokeType
                                  withDouble:(jdouble)oldStrokeWidth {
  if (self->listenerList_ != nil) {
    IOSObjectArray *listenerArr = [IOSObjectArray newArrayWithLength:[listenerList_ size] type:PIGF_StyleEventListener_class_()];
    (void) [((id<JavaUtilList>) nil_chk(listenerList_)) toArrayWithNSObjectArray:listenerArr];
    id<PIGF_StyleEventListener> listener;
    for (jint i = 0; i < listenerArr->size_; i++) {
      listener = IOSObjectArray_Get(listenerArr, i);
      @try {
        [((id<PIGF_StyleEventListener>) nil_chk(listener)) strokeChangedWithPIGF_ShapeStyle:owner_ withPIGF_StrokeType:oldStrokeType withDouble:oldStrokeWidth];
      }
      @catch (JavaLangException *ex) {
        PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error while event processing", ex);
      }
    }
  }
}

- (void)fireStyleChanged {
  if (self->listenerList_ != nil) {
    IOSObjectArray *listenerArr = [IOSObjectArray newArrayWithLength:[listenerList_ size] type:PIGF_StyleEventListener_class_()];
    (void) [((id<JavaUtilList>) nil_chk(listenerList_)) toArrayWithNSObjectArray:listenerArr];
    id<PIGF_StyleEventListener> listener;
    for (jint i = 0; i < listenerArr->size_; i++) {
      listener = IOSObjectArray_Get(listenerArr, i);
      @try {
        [((id<PIGF_StyleEventListener>) nil_chk(listener)) styleChangedWithPIGF_ShapeStyle:owner_];
      }
      @catch (JavaLangException *ex) {
        PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error while event processing", ex);
      }
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGF_ShapeStyle:);
  methods[1].selector = @selector(getOwner);
  methods[2].selector = @selector(addListenerWithPIGF_StyleEventListener:);
  methods[3].selector = @selector(removeListenerWithPIGF_StyleEventListener:);
  methods[4].selector = @selector(fireColorChangedWithPIGB_ComponentColor:withPIGB_ComponentColor:withBoolean:);
  methods[5].selector = @selector(fireFontChangedWithNSString:withPIGF_TextAlign:);
  methods[6].selector = @selector(fireStrokeChangedWithPIGF_StrokeType:withDouble:);
  methods[7].selector = @selector(fireStyleChanged);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "owner_", "LPIGF_ShapeStyle;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "listenerList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 10, -1 },
  };
  static const void *ptrTable[] = { "LPIGF_ShapeStyle;", "addListener", "LPIGF_StyleEventListener;", "removeListener", "fireColorChanged", "LPIGB_ComponentColor;LPIGB_ComponentColor;Z", "fireFontChanged", "LNSString;LPIGF_TextAlign;", "fireStrokeChanged", "LPIGF_StrokeType;D", "Ljava/util/List<Lde/pidata/gui/view/figure/StyleEventListener;>;" };
  static const J2ObjcClassInfo _PIGF_StyleEventSender = { "StyleEventSender", "de.pidata.gui.view.figure", ptrTable, methods, fields, 7, 0x1, 8, 2, -1, -1, -1, -1, -1 };
  return &_PIGF_StyleEventSender;
}

@end

void PIGF_StyleEventSender_initWithPIGF_ShapeStyle_(PIGF_StyleEventSender *self, PIGF_ShapeStyle *owner) {
  NSObject_init(self);
  self->owner_ = owner;
}

PIGF_StyleEventSender *new_PIGF_StyleEventSender_initWithPIGF_ShapeStyle_(PIGF_ShapeStyle *owner) {
  J2OBJC_NEW_IMPL(PIGF_StyleEventSender, initWithPIGF_ShapeStyle_, owner)
}

PIGF_StyleEventSender *create_PIGF_StyleEventSender_initWithPIGF_ShapeStyle_(PIGF_ShapeStyle *owner) {
  J2OBJC_CREATE_IMPL(PIGF_StyleEventSender, initWithPIGF_ShapeStyle_, owner)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIGF_StyleEventSender)

J2OBJC_NAME_MAPPING(PIGF_StyleEventSender, "de.pidata.gui.view.figure", "PIGF_")
