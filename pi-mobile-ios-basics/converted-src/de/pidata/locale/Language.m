//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/locale/Language.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/locale/Language.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/locale/Language must be compiled with ARC (-fobjc-arc)"
#endif

__attribute__((unused)) static void DePidataLocaleLanguage_initWithNSString_withInt_(DePidataLocaleLanguage *self, NSString *__name, jint __ordinal);

__attribute__((unused)) static DePidataLocaleLanguage *new_DePidataLocaleLanguage_initWithNSString_withInt_(NSString *__name, jint __ordinal) NS_RETURNS_RETAINED;

J2OBJC_INITIALIZED_DEFN(DePidataLocaleLanguage)

DePidataLocaleLanguage *DePidataLocaleLanguage_values_[21];

@implementation DePidataLocaleLanguage

- (NSString *)description {
  return JreStrcat("C$", [((NSString *) nil_chk([self name])) charAtWithInt:0], [((NSString *) nil_chk([((NSString *) nil_chk([self name])) java_substring:1])) lowercaseString]);
}

+ (IOSObjectArray *)values {
  return DePidataLocaleLanguage_values();
}

+ (DePidataLocaleLanguage *)valueOfWithNSString:(NSString *)name {
  return DePidataLocaleLanguage_valueOfWithNSString_(name);
}

- (DePidataLocaleLanguage_Enum)toNSEnum {
  return (DePidataLocaleLanguage_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSString;", 0x1, 0, -1, -1, -1, -1, -1 },
    { NULL, "[LDePidataLocaleLanguage;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LDePidataLocaleLanguage;", 0x9, 1, 2, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(description);
  methods[1].selector = @selector(values);
  methods[2].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ARABIC", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 3, -1, -1 },
    { "BENGALI", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
    { "CHINESE", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 5, -1, -1 },
    { "DUTCH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 6, -1, -1 },
    { "ENGLISH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 7, -1, -1 },
    { "FRENCH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 8, -1, -1 },
    { "GERMAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 9, -1, -1 },
    { "HINDI", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 10, -1, -1 },
    { "INDONESIAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 11, -1, -1 },
    { "ITALIAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 12, -1, -1 },
    { "JAPANESE", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 13, -1, -1 },
    { "KOREAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 14, -1, -1 },
    { "MANDARIN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 15, -1, -1 },
    { "POLISH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 16, -1, -1 },
    { "PORTUGUESE", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 17, -1, -1 },
    { "ROMANIAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 18, -1, -1 },
    { "SPANISH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 19, -1, -1 },
    { "TURKISH", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 20, -1, -1 },
    { "UKRAINIAN", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 21, -1, -1 },
    { "URDU", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 22, -1, -1 },
    { "VIETNAMESE", "LDePidataLocaleLanguage;", .constantValue.asLong = 0, 0x4019, -1, 23, -1, -1 },
  };
  static const void *ptrTable[] = { "toString", "valueOf", "LNSString;", &JreEnum(DePidataLocaleLanguage, ARABIC), &JreEnum(DePidataLocaleLanguage, BENGALI), &JreEnum(DePidataLocaleLanguage, CHINESE), &JreEnum(DePidataLocaleLanguage, DUTCH), &JreEnum(DePidataLocaleLanguage, ENGLISH), &JreEnum(DePidataLocaleLanguage, FRENCH), &JreEnum(DePidataLocaleLanguage, GERMAN), &JreEnum(DePidataLocaleLanguage, HINDI), &JreEnum(DePidataLocaleLanguage, INDONESIAN), &JreEnum(DePidataLocaleLanguage, ITALIAN), &JreEnum(DePidataLocaleLanguage, JAPANESE), &JreEnum(DePidataLocaleLanguage, KOREAN), &JreEnum(DePidataLocaleLanguage, MANDARIN), &JreEnum(DePidataLocaleLanguage, POLISH), &JreEnum(DePidataLocaleLanguage, PORTUGUESE), &JreEnum(DePidataLocaleLanguage, ROMANIAN), &JreEnum(DePidataLocaleLanguage, SPANISH), &JreEnum(DePidataLocaleLanguage, TURKISH), &JreEnum(DePidataLocaleLanguage, UKRAINIAN), &JreEnum(DePidataLocaleLanguage, URDU), &JreEnum(DePidataLocaleLanguage, VIETNAMESE), "Ljava/lang/Enum<Lde/pidata/locale/Language;>;" };
  static const J2ObjcClassInfo _DePidataLocaleLanguage = { "Language", "de.pidata.locale", ptrTable, methods, fields, 7, 0x4011, 3, 21, -1, -1, -1, 24, -1 };
  return &_DePidataLocaleLanguage;
}

+ (void)initialize {
  if (self == [DePidataLocaleLanguage class]) {
    JreEnum(DePidataLocaleLanguage, ARABIC) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 0), 0);
    JreEnum(DePidataLocaleLanguage, BENGALI) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 1), 1);
    JreEnum(DePidataLocaleLanguage, CHINESE) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 2), 2);
    JreEnum(DePidataLocaleLanguage, DUTCH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 3), 3);
    JreEnum(DePidataLocaleLanguage, ENGLISH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 4), 4);
    JreEnum(DePidataLocaleLanguage, FRENCH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 5), 5);
    JreEnum(DePidataLocaleLanguage, GERMAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 6), 6);
    JreEnum(DePidataLocaleLanguage, HINDI) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 7), 7);
    JreEnum(DePidataLocaleLanguage, INDONESIAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 8), 8);
    JreEnum(DePidataLocaleLanguage, ITALIAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 9), 9);
    JreEnum(DePidataLocaleLanguage, JAPANESE) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 10), 10);
    JreEnum(DePidataLocaleLanguage, KOREAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 11), 11);
    JreEnum(DePidataLocaleLanguage, MANDARIN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 12), 12);
    JreEnum(DePidataLocaleLanguage, POLISH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 13), 13);
    JreEnum(DePidataLocaleLanguage, PORTUGUESE) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 14), 14);
    JreEnum(DePidataLocaleLanguage, ROMANIAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 15), 15);
    JreEnum(DePidataLocaleLanguage, SPANISH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 16), 16);
    JreEnum(DePidataLocaleLanguage, TURKISH) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 17), 17);
    JreEnum(DePidataLocaleLanguage, UKRAINIAN) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 18), 18);
    JreEnum(DePidataLocaleLanguage, URDU) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 19), 19);
    JreEnum(DePidataLocaleLanguage, VIETNAMESE) = new_DePidataLocaleLanguage_initWithNSString_withInt_(JreEnumConstantName(DePidataLocaleLanguage_class_(), 20), 20);
    J2OBJC_SET_INITIALIZED(DePidataLocaleLanguage)
  }
}

@end

void DePidataLocaleLanguage_initWithNSString_withInt_(DePidataLocaleLanguage *self, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
}

DePidataLocaleLanguage *new_DePidataLocaleLanguage_initWithNSString_withInt_(NSString *__name, jint __ordinal) {
  J2OBJC_NEW_IMPL(DePidataLocaleLanguage, initWithNSString_withInt_, __name, __ordinal)
}

IOSObjectArray *DePidataLocaleLanguage_values() {
  DePidataLocaleLanguage_initialize();
  return [IOSObjectArray arrayWithObjects:DePidataLocaleLanguage_values_ count:21 type:DePidataLocaleLanguage_class_()];
}

DePidataLocaleLanguage *DePidataLocaleLanguage_valueOfWithNSString_(NSString *name) {
  DePidataLocaleLanguage_initialize();
  for (int i = 0; i < 21; i++) {
    DePidataLocaleLanguage *e = DePidataLocaleLanguage_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

DePidataLocaleLanguage *DePidataLocaleLanguage_fromOrdinal(NSUInteger ordinal) {
  DePidataLocaleLanguage_initialize();
  if (ordinal >= 21) {
    return nil;
  }
  return DePidataLocaleLanguage_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataLocaleLanguage)
