//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/log/DefaultFormatter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataLogDefaultFormatter")
#ifdef RESTRICT_DePidataLogDefaultFormatter
#define INCLUDE_ALL_DePidataLogDefaultFormatter 0
#else
#define INCLUDE_ALL_DePidataLogDefaultFormatter 1
#endif
#undef RESTRICT_DePidataLogDefaultFormatter

#if !defined (PICL_DefaultFormatter_) && (INCLUDE_ALL_DePidataLogDefaultFormatter || defined(INCLUDE_PICL_DefaultFormatter))
#define PICL_DefaultFormatter_

#define RESTRICT_JavaUtilLoggingFormatter 1
#define INCLUDE_JavaUtilLoggingFormatter 1
#include "java/util/logging/Formatter.h"

@class JavaUtilLoggingLogRecord;

@interface PICL_DefaultFormatter : JavaUtilLoggingFormatter

#pragma mark Public

- (instancetype)init;

- (NSString *)formatWithJavaUtilLoggingLogRecord:(JavaUtilLoggingLogRecord *)record;

@end

J2OBJC_EMPTY_STATIC_INIT(PICL_DefaultFormatter)

FOUNDATION_EXPORT void PICL_DefaultFormatter_init(PICL_DefaultFormatter *self);

FOUNDATION_EXPORT PICL_DefaultFormatter *new_PICL_DefaultFormatter_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PICL_DefaultFormatter *create_PICL_DefaultFormatter_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PICL_DefaultFormatter)

@compatibility_alias DePidataLogDefaultFormatter PICL_DefaultFormatter;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataLogDefaultFormatter")
