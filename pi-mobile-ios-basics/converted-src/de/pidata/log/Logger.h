//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/log/Logger.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataLogLogger")
#ifdef RESTRICT_DePidataLogLogger
#define INCLUDE_ALL_DePidataLogLogger 0
#else
#define INCLUDE_ALL_DePidataLogLogger 1
#endif
#undef RESTRICT_DePidataLogLogger

#if !defined (PICL_Logger_) && (INCLUDE_ALL_DePidataLogLogger || defined(INCLUDE_PICL_Logger))
#define PICL_Logger_

@class JavaLangThrowable;
@class PICL_Level;
@protocol PICL_LoggerInterface;

@interface PICL_Logger : NSObject

#pragma mark Public

- (instancetype)init;

+ (void)addLoggerWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)newLogger;

+ (void)debugWithNSString:(NSString *)message;

+ (void)errorWithNSString:(NSString *)message;

+ (void)errorWithNSString:(NSString *)message
    withJavaLangThrowable:(JavaLangThrowable *)ex;

+ (void)fatalWithNSString:(NSString *)message
    withJavaLangThrowable:(JavaLangThrowable *)ex;

+ (id<PICL_LoggerInterface>)getLogger;

+ (PICL_Level *)getLogLevel;

+ (void)infoWithNSString:(NSString *)message;

+ (void)removeLoggerWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)loggerToRemove;

+ (void)reset;

+ (void)setLoggerWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)newLogger;

+ (void)setLogLevelWithPICL_Level:(PICL_Level *)logLevel;

+ (void)warnWithNSString:(NSString *)message;

#pragma mark Package-Private

+ (void)logWithPICL_Level:(PICL_Level *)loglevel
             withNSString:(NSString *)message;

+ (void)logWithPICL_Level:(PICL_Level *)logLevel
             withNSString:(NSString *)message
    withJavaLangThrowable:(JavaLangThrowable *)ex;

@end

J2OBJC_STATIC_INIT(PICL_Logger)

inline NSString *PICL_Logger_get_KEY_LOGLEVEL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PICL_Logger_KEY_LOGLEVEL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICL_Logger, KEY_LOGLEVEL, NSString *)

inline NSString *PICL_Logger_get_KEY_ATTACH_JAVA_LOGGING(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PICL_Logger_KEY_ATTACH_JAVA_LOGGING;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICL_Logger, KEY_ATTACH_JAVA_LOGGING, NSString *)

FOUNDATION_EXPORT void PICL_Logger_init(PICL_Logger *self);

FOUNDATION_EXPORT PICL_Logger *new_PICL_Logger_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PICL_Logger *create_PICL_Logger_init(void);

FOUNDATION_EXPORT void PICL_Logger_setLogLevelWithPICL_Level_(PICL_Level *logLevel);

FOUNDATION_EXPORT void PICL_Logger_setLoggerWithPICL_LoggerInterface_(id<PICL_LoggerInterface> newLogger);

FOUNDATION_EXPORT void PICL_Logger_reset(void);

FOUNDATION_EXPORT void PICL_Logger_addLoggerWithPICL_LoggerInterface_(id<PICL_LoggerInterface> newLogger);

FOUNDATION_EXPORT void PICL_Logger_removeLoggerWithPICL_LoggerInterface_(id<PICL_LoggerInterface> loggerToRemove);

FOUNDATION_EXPORT id<PICL_LoggerInterface> PICL_Logger_getLogger(void);

FOUNDATION_EXPORT PICL_Level *PICL_Logger_getLogLevel(void);

FOUNDATION_EXPORT void PICL_Logger_logWithPICL_Level_withNSString_(PICL_Level *loglevel, NSString *message);

FOUNDATION_EXPORT void PICL_Logger_logWithPICL_Level_withNSString_withJavaLangThrowable_(PICL_Level *logLevel, NSString *message, JavaLangThrowable *ex);

FOUNDATION_EXPORT void PICL_Logger_debugWithNSString_(NSString *message);

FOUNDATION_EXPORT void PICL_Logger_infoWithNSString_(NSString *message);

FOUNDATION_EXPORT void PICL_Logger_warnWithNSString_(NSString *message);

FOUNDATION_EXPORT void PICL_Logger_errorWithNSString_(NSString *message);

FOUNDATION_EXPORT void PICL_Logger_errorWithNSString_withJavaLangThrowable_(NSString *message, JavaLangThrowable *ex);

FOUNDATION_EXPORT void PICL_Logger_fatalWithNSString_withJavaLangThrowable_(NSString *message, JavaLangThrowable *ex);

J2OBJC_TYPE_LITERAL_HEADER(PICL_Logger)

@compatibility_alias DePidataLogLogger PICL_Logger;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataLogLogger")
