//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/log/MultiLogger.java
//

#include "J2ObjC_source.h"
#include "de/pidata/log/Level.h"
#include "de/pidata/log/LoggerInterface.h"
#include "de/pidata/log/MultiLogger.h"
#include "java/lang/Throwable.h"
#include "java/util/ArrayList.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/log/MultiLogger must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PICL_MultiLogger

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PICL_MultiLogger_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)defaultLogger {
  PICL_MultiLogger_initWithPICL_LoggerInterface_(self, defaultLogger);
  return self;
}

- (void)addLoggerWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)newLogger {
  if (![((id<JavaUtilList>) nil_chk(loggerList_)) containsWithId:newLogger]) {
    [((id<JavaUtilList>) nil_chk(loggerList_)) addWithId:newLogger];
  }
}

- (void)removeLoggerWithPICL_LoggerInterface:(id<PICL_LoggerInterface>)loggerToRemove {
  [((id<JavaUtilList>) nil_chk(loggerList_)) removeWithId:loggerToRemove];
}

- (void)logWithPICL_Level:(PICL_Level *)logLevel
             withNSString:(NSString *)message
    withJavaLangThrowable:(JavaLangThrowable *)ex {
  for (id<PICL_LoggerInterface> __strong logger in nil_chk(loggerList_)) {
    [((id<PICL_LoggerInterface>) nil_chk(logger)) logWithPICL_Level:logLevel withNSString:message withJavaLangThrowable:ex];
  }
}

- (void)close {
  for (id<PICL_LoggerInterface> __strong logger in nil_chk(loggerList_)) {
    [((id<PICL_LoggerInterface>) nil_chk(logger)) close];
  }
}

- (void)setLogLevelWithPICL_Level:(PICL_Level *)loglevel {
  for (id<PICL_LoggerInterface> __strong logger in nil_chk(loggerList_)) {
    [((id<PICL_LoggerInterface>) nil_chk(logger)) setLogLevelWithPICL_Level:loglevel];
  }
}

- (PICL_Level *)getLogLevel {
  PICL_Level *maxLoglevel = JreLoadEnum(PICL_Level, OFF);
  for (id<PICL_LoggerInterface> __strong logger in nil_chk(loggerList_)) {
    PICL_Level *loglevel = [((id<PICL_LoggerInterface>) nil_chk(logger)) getLogLevel];
    if ([((PICL_Level *) nil_chk(loglevel)) getLevelValue] < [((PICL_Level *) nil_chk(maxLoglevel)) getLevelValue]) {
      maxLoglevel = loglevel;
    }
  }
  return maxLoglevel;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LPICL_Level;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPICL_LoggerInterface:);
  methods[2].selector = @selector(addLoggerWithPICL_LoggerInterface:);
  methods[3].selector = @selector(removeLoggerWithPICL_LoggerInterface:);
  methods[4].selector = @selector(logWithPICL_Level:withNSString:withJavaLangThrowable:);
  methods[5].selector = @selector(close);
  methods[6].selector = @selector(setLogLevelWithPICL_Level:);
  methods[7].selector = @selector(getLogLevel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "loggerList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x1, -1, -1, 7, -1 },
  };
  static const void *ptrTable[] = { "LPICL_LoggerInterface;", "addLogger", "removeLogger", "log", "LPICL_Level;LNSString;LJavaLangThrowable;", "setLogLevel", "LPICL_Level;", "Ljava/util/List<Lde/pidata/log/LoggerInterface;>;" };
  static const J2ObjcClassInfo _PICL_MultiLogger = { "MultiLogger", "de.pidata.log", ptrTable, methods, fields, 7, 0x1, 8, 1, -1, -1, -1, -1, -1 };
  return &_PICL_MultiLogger;
}

@end

void PICL_MultiLogger_init(PICL_MultiLogger *self) {
  NSObject_init(self);
  self->loggerList_ = new_JavaUtilArrayList_init();
}

PICL_MultiLogger *new_PICL_MultiLogger_init() {
  J2OBJC_NEW_IMPL(PICL_MultiLogger, init)
}

PICL_MultiLogger *create_PICL_MultiLogger_init() {
  J2OBJC_CREATE_IMPL(PICL_MultiLogger, init)
}

void PICL_MultiLogger_initWithPICL_LoggerInterface_(PICL_MultiLogger *self, id<PICL_LoggerInterface> defaultLogger) {
  NSObject_init(self);
  self->loggerList_ = new_JavaUtilArrayList_init();
  [self->loggerList_ addWithId:defaultLogger];
}

PICL_MultiLogger *new_PICL_MultiLogger_initWithPICL_LoggerInterface_(id<PICL_LoggerInterface> defaultLogger) {
  J2OBJC_NEW_IMPL(PICL_MultiLogger, initWithPICL_LoggerInterface_, defaultLogger)
}

PICL_MultiLogger *create_PICL_MultiLogger_initWithPICL_LoggerInterface_(id<PICL_LoggerInterface> defaultLogger) {
  J2OBJC_CREATE_IMPL(PICL_MultiLogger, initWithPICL_LoggerInterface_, defaultLogger)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PICL_MultiLogger)

J2OBJC_NAME_MAPPING(PICL_MultiLogger, "de.pidata.log", "PICL_")
