//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/ModelHelper.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/ModelHelper.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/SimpleModel.h"
#include "java/lang/StringBuilder.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/ModelHelper must be compiled with ARC (-fobjc-arc)"
#endif

@implementation DePidataModelsModelHelper

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataModelsModelHelper_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (NSString *)getContentWithPIMR_Model:(id<PIMR_Model>)model {
  return DePidataModelsModelHelper_getContentWithPIMR_Model_(model);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getContentWithPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "getContent", "LPIMR_Model;" };
  static const J2ObjcClassInfo _DePidataModelsModelHelper = { "ModelHelper", "de.pidata.models", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_DePidataModelsModelHelper;
}

@end

void DePidataModelsModelHelper_init(DePidataModelsModelHelper *self) {
  NSObject_init(self);
}

DePidataModelsModelHelper *new_DePidataModelsModelHelper_init() {
  J2OBJC_NEW_IMPL(DePidataModelsModelHelper, init)
}

DePidataModelsModelHelper *create_DePidataModelsModelHelper_init() {
  J2OBJC_CREATE_IMPL(DePidataModelsModelHelper, init)
}

NSString *DePidataModelsModelHelper_getContentWithPIMR_Model_(id<PIMR_Model> model) {
  DePidataModelsModelHelper_initialize();
  JavaLangStringBuilder *sb = new_JavaLangStringBuilder_init();
  id<PIMR_ModelIterator> valuePartIterator = [((id<PIMR_Model>) nil_chk(model)) iteratorWithPIQ_QName:nil withPIMR_Filter:nil];
  while ([((id<PIMR_ModelIterator>) nil_chk(valuePartIterator)) hasNext]) {
    id<PIMR_Model> next = [valuePartIterator next];
    if ([next isKindOfClass:[PIMR_SimpleModel class]]) {
      id content = [((id<PIMR_Model>) nil_chk(next)) getContent];
      if (content != nil) {
        (void) [sb appendWithNSString:[content description]];
      }
    }
    else {
      (void) [sb appendWithNSString:DePidataModelsModelHelper_getContentWithPIMR_Model_(next)];
    }
  }
  return [sb description];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataModelsModelHelper)
