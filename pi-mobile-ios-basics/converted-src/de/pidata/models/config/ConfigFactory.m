//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-service/src/de/pidata/models/config/ConfigFactory.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/config/Binding.h"
#include "de/pidata/models/config/BluetoothDevice.h"
#include "de/pidata/models/config/BusinessObjectDef.h"
#include "de/pidata/models/config/Config.h"
#include "de/pidata/models/config/ConfigFactory.h"
#include "de/pidata/models/config/DeviceTable.h"
#include "de/pidata/models/config/Instance.h"
#include "de/pidata/models/config/Parameter.h"
#include "de/pidata/models/tree/AbstractModelFactory.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/Relation.h"
#include "de/pidata/models/types/Type.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/models/types/simple/BooleanType.h"
#include "de/pidata/models/types/simple/QNameType.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Integer.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/config/ConfigFactory must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIMI_ConfigFactory)

PIQ_Namespace *PIMI_ConfigFactory_NAMESPACE;
PIQ_QName *PIMI_ConfigFactory_ID_BLUETOOTHDEVICE;
PIQ_QName *PIMI_ConfigFactory_ID_DEVICETABLE;
PIQ_QName *PIMI_ConfigFactory_ID_BINDING;
PIQ_QName *PIMI_ConfigFactory_ID_CONFIG;
PIQ_QName *PIMI_ConfigFactory_ID_PARAMETER;
PIQ_QName *PIMI_ConfigFactory_ID_BUSINESSOBJECTDEF;
PIQ_QName *PIMI_ConfigFactory_ID_INSTANCE;
id<PIMT_ComplexType> PIMI_ConfigFactory_PARAMETER_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_INSTANCE_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_BINDING_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_CONFIG_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE;
id<PIMT_ComplexType> PIMI_ConfigFactory_DEVICETABLE_TYPE;

@implementation PIMI_ConfigFactory

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIMI_ConfigFactory_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (id<PIMR_Model>)createInstanceWithPIQ_Key:(id<PIQ_Key>)key
                              withPIMT_Type:(id<PIMT_Type>)typeDef
                          withNSObjectArray:(IOSObjectArray *)attributes
                      withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
                         withPIMR_ChildList:(PIMR_ChildList *)children {
  IOSClass *modelClass = [((id<PIMT_Type>) nil_chk(typeDef)) getValueClass];
  if (JreObjectEqualsEquals(modelClass, PIMI_Parameter_class_())) {
    return new_PIMI_Parameter_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_Instance_class_())) {
    return new_PIMI_Instance_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_Binding_class_())) {
    return new_PIMI_Binding_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_BusinessObjectDef_class_())) {
    return new_PIMI_BusinessObjectDef_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_Config_class_())) {
    return new_PIMI_Config_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_BluetoothDevice_class_())) {
    return new_PIMI_BluetoothDevice_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  if (JreObjectEqualsEquals(modelClass, PIMI_DeviceTable_class_())) {
    return new_PIMI_DeviceTable_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(key, attributes, anyAttribs, children);
  }
  return [super createInstanceWithPIQ_Key:key withPIMT_Type:typeDef withNSObjectArray:attributes withJavaUtilHashtable:anyAttribs withPIMR_ChildList:children];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(createInstanceWithPIQ_Key:withPIMT_Type:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 2, -1, -1 },
    { "ID_BLUETOOTHDEVICE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "ID_DEVICETABLE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
    { "ID_BINDING", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 5, -1, -1 },
    { "ID_CONFIG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 6, -1, -1 },
    { "ID_PARAMETER", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 7, -1, -1 },
    { "ID_BUSINESSOBJECTDEF", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 8, -1, -1 },
    { "ID_INSTANCE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
    { "PARAMETER_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 10, -1, -1 },
    { "INSTANCE_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
    { "BINDING_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "BUSINESSOBJECTDEF_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "CONFIG_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
    { "BLUETOOTHDEVICE_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "DEVICETABLE_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 16, -1, -1 },
  };
  static const void *ptrTable[] = { "createInstance", "LPIQ_Key;LPIMT_Type;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", &PIMI_ConfigFactory_NAMESPACE, &PIMI_ConfigFactory_ID_BLUETOOTHDEVICE, &PIMI_ConfigFactory_ID_DEVICETABLE, &PIMI_ConfigFactory_ID_BINDING, &PIMI_ConfigFactory_ID_CONFIG, &PIMI_ConfigFactory_ID_PARAMETER, &PIMI_ConfigFactory_ID_BUSINESSOBJECTDEF, &PIMI_ConfigFactory_ID_INSTANCE, &PIMI_ConfigFactory_PARAMETER_TYPE, &PIMI_ConfigFactory_INSTANCE_TYPE, &PIMI_ConfigFactory_BINDING_TYPE, &PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE, &PIMI_ConfigFactory_CONFIG_TYPE, &PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE, &PIMI_ConfigFactory_DEVICETABLE_TYPE };
  static const J2ObjcClassInfo _PIMI_ConfigFactory = { "ConfigFactory", "de.pidata.models.config", ptrTable, methods, fields, 7, 0x1, 2, 15, -1, -1, -1, -1, -1 };
  return &_PIMI_ConfigFactory;
}

+ (void)initialize {
  if (self == [PIMI_ConfigFactory class]) {
    PIMI_ConfigFactory_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://schema.pidata.de/config");
    PIMI_ConfigFactory_ID_BLUETOOTHDEVICE = [((PIQ_Namespace *) nil_chk(PIMI_ConfigFactory_NAMESPACE)) getQNameWithNSString:@"bluetoothDevice"];
    PIMI_ConfigFactory_ID_DEVICETABLE = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"deviceTable"];
    PIMI_ConfigFactory_ID_BINDING = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"binding"];
    PIMI_ConfigFactory_ID_CONFIG = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"config"];
    PIMI_ConfigFactory_ID_PARAMETER = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"parameter"];
    PIMI_ConfigFactory_ID_BUSINESSOBJECTDEF = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"businessObjectDef"];
    PIMI_ConfigFactory_ID_INSTANCE = [PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"instance"];
    PIMI_ConfigFactory_PARAMETER_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"parameter"], [PIMI_Parameter_class_() getName], 1);
    PIMI_ConfigFactory_INSTANCE_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"instance"], [PIMI_Instance_class_() getName], 0);
    PIMI_ConfigFactory_BINDING_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"binding"], [PIMI_Binding_class_() getName], 0);
    PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"businessObjectDef"], [PIMI_BusinessObjectDef_class_() getName], 0);
    PIMI_ConfigFactory_CONFIG_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"config"], [PIMI_Config_class_() getName], 0);
    PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"bluetoothDevice"], [PIMI_BluetoothDevice_class_() getName], 1);
    PIMI_ConfigFactory_DEVICETABLE_TYPE = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIMI_ConfigFactory_NAMESPACE getQNameWithNSString:@"deviceTable"], [PIMI_DeviceTable_class_() getName], 0);
    {
      PIMO_DefaultComplexType *type;
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_PARAMETER_TYPE, [PIMO_DefaultComplexType class]);
      [type addKeyAttributeTypeWithInt:0 withPIQ_QName:JreLoadStatic(PIMI_Parameter, ID_NAME) withPIMT_SimpleType:PIME_QNameType_getInstance()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Parameter, ID_VALUE) withPIMT_SimpleType:PIME_StringType_getDefString()];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_INSTANCE_TYPE, [PIMO_DefaultComplexType class]);
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Instance, ID_CLASSNAME) withPIMT_SimpleType:PIME_StringType_getDefString()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Instance, ID_NAME) withPIMT_SimpleType:PIME_QNameType_getInstance()];
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_Instance, ID_PARAMETER) withPIMT_Type:PIMI_ConfigFactory_PARAMETER_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_BINDING_TYPE, [PIMO_DefaultComplexType class]);
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Binding, ID_SERVICENAME) withPIMT_SimpleType:PIME_StringType_getDefString()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Binding, ID_PORTTYPE) withPIMT_SimpleType:PIME_QNameType_getInstance()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Binding, ID_CLASSNAME) withPIMT_SimpleType:PIME_StringType_getDefString()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_Binding, ID_CLIENTSERVICE) withPIMT_SimpleType:PIME_BooleanType_getDefault()];
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_Binding, ID_PARAMETER) withPIMT_Type:PIMI_ConfigFactory_PARAMETER_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE, [PIMO_DefaultComplexType class]);
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_BusinessObjectDef, ID_PORTTYPE) withPIMT_SimpleType:PIME_QNameType_getInstance()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_BusinessObjectDef, ID_CLASSNAME) withPIMT_SimpleType:PIME_StringType_getDefString()];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_CONFIG_TYPE, [PIMO_DefaultComplexType class]);
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_Config, ID_INSTANCE) withPIMT_Type:PIMI_ConfigFactory_INSTANCE_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_Config, ID_BUSINESSOBJECTDEF) withPIMT_Type:PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_Config, ID_BINDING) withPIMT_Type:PIMI_ConfigFactory_BINDING_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE, [PIMO_DefaultComplexType class]);
      [type addKeyAttributeTypeWithInt:0 withPIQ_QName:JreLoadStatic(PIMI_BluetoothDevice, ID_ID) withPIMT_SimpleType:PIME_QNameType_getInstance()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_BluetoothDevice, ID_NAME) withPIMT_SimpleType:PIME_StringType_getDefString()];
      [type addAttributeTypeWithPIQ_QName:JreLoadStatic(PIMI_BluetoothDevice, ID_ALIAS) withPIMT_SimpleType:PIME_StringType_getDefString()];
      type = (PIMO_DefaultComplexType *) cast_chk(PIMI_ConfigFactory_DEVICETABLE_TYPE, [PIMO_DefaultComplexType class]);
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMI_DeviceTable, ID_BLUETOOTHDEVICE) withPIMT_Type:PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE withInt:0 withInt:JavaLangInteger_MAX_VALUE];
    }
    J2OBJC_SET_INITIALIZED(PIMI_ConfigFactory)
  }
}

@end

void PIMI_ConfigFactory_init(PIMI_ConfigFactory *self) {
  PIMR_AbstractModelFactory_initWithPIQ_Namespace_withNSString_withNSString_(self, PIMI_ConfigFactory_NAMESPACE, @"http://schema.pidata.de/config", @"3.0");
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_PARAMETER_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_INSTANCE_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_BINDING_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_CONFIG_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE];
  [self addTypeWithPIMT_Type:PIMI_ConfigFactory_DEVICETABLE_TYPE];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_PARAMETER withPIMT_Type:PIMI_ConfigFactory_PARAMETER_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_INSTANCE withPIMT_Type:PIMI_ConfigFactory_INSTANCE_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_BINDING withPIMT_Type:PIMI_ConfigFactory_BINDING_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_BUSINESSOBJECTDEF withPIMT_Type:PIMI_ConfigFactory_BUSINESSOBJECTDEF_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_CONFIG withPIMT_Type:PIMI_ConfigFactory_CONFIG_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_BLUETOOTHDEVICE withPIMT_Type:PIMI_ConfigFactory_BLUETOOTHDEVICE_TYPE withInt:1 withInt:1];
  (void) [self addRootRelationWithPIQ_QName:PIMI_ConfigFactory_ID_DEVICETABLE withPIMT_Type:PIMI_ConfigFactory_DEVICETABLE_TYPE withInt:1 withInt:1];
}

PIMI_ConfigFactory *new_PIMI_ConfigFactory_init() {
  J2OBJC_NEW_IMPL(PIMI_ConfigFactory, init)
}

PIMI_ConfigFactory *create_PIMI_ConfigFactory_init() {
  J2OBJC_CREATE_IMPL(PIMI_ConfigFactory, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMI_ConfigFactory)

J2OBJC_NAME_MAPPING(PIMI_ConfigFactory, "de.pidata.models.config", "PIMI_")
