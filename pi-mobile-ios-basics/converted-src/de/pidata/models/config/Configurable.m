//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-service/src/de/pidata/models/config/Configurable.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/config/Configurable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/config/Configurable must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMI_Configurable : NSObject

@end

@implementation PIMI_Configurable

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init__WithPIMI_Configurator:withPIMI_Instance:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "init", "LPIMI_Configurator;LPIMI_Instance;", "LJavaLangException;" };
  static const J2ObjcClassInfo _PIMI_Configurable = { "Configurable", "de.pidata.models.config", ptrTable, methods, NULL, 7, 0x609, 1, 0, -1, -1, -1, -1, -1 };
  return &_PIMI_Configurable;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIMI_Configurable)

J2OBJC_NAME_MAPPING(PIMI_Configurable, "de.pidata.models.config", "PIMI_")
