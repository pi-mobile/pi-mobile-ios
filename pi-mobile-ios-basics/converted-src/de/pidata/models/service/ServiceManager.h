//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-service/src/de/pidata/models/service/ServiceManager.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsServiceServiceManager")
#ifdef RESTRICT_DePidataModelsServiceServiceManager
#define INCLUDE_ALL_DePidataModelsServiceServiceManager 0
#else
#define INCLUDE_ALL_DePidataModelsServiceServiceManager 1
#endif
#undef RESTRICT_DePidataModelsServiceServiceManager

#if !defined (PIMS_ServiceManager_) && (INCLUDE_ALL_DePidataModelsServiceServiceManager || defined(INCLUDE_PIMS_ServiceManager))
#define PIMS_ServiceManager_

@class IOSClass;
@class JavaUtilHashtable;
@class PIMR_Context;
@class PIQ_QName;
@protocol PIMR_Model;
@protocol PIMS_Service;

@interface PIMS_ServiceManager : NSObject {
 @public
  JavaUtilHashtable *services_;
  NSString *targetID_;
  jboolean active_;
}

#pragma mark Public

- (instancetype)init;

+ (PIMS_ServiceManager *)getInstance;

+ (PIMS_ServiceManager *)getInstanceWithNSString:(NSString *)targetID;

- (id<PIMS_Service>)getServiceWithNSString:(NSString *)serviceName;

- (NSString *)getTargetID;

- (id<PIMR_Model>)invokeServiceWithPIMR_Context:(PIMR_Context *)caller
                                  withPIQ_QName:(PIQ_QName *)portTypeName
                                  withPIQ_QName:(PIQ_QName *)operation
                                 withPIMR_Model:(id<PIMR_Model>)input;

- (id<PIMR_Model>)invokeServiceWithPIMR_Context:(PIMR_Context *)caller
                                   withNSString:(NSString *)serviceName
                                   withNSString:(NSString *)opName
                                 withPIMR_Model:(id<PIMR_Model>)input;

- (void)registerServiceWithPIQ_QName:(PIQ_QName *)portTypName
                    withPIMS_Service:(id<PIMS_Service>)service;

- (void)registerServiceWithNSString:(NSString *)serviceName
                       withIOSClass:(IOSClass *)serviceClass;

- (void)registerServiceWithNSString:(NSString *)serviceName
                   withPIMS_Service:(id<PIMS_Service>)service;

- (void)removeServiceWithNSString:(NSString *)serviceName;

- (void)setActiveWithBoolean:(jboolean)active;

- (void)shutdown;

#pragma mark Protected

- (instancetype)initWithNSString:(NSString *)targetID;

- (PIMS_ServiceManager *)createInstanceWithNSString:(NSString *)targetID;

- (void)waitActiveWithNSString:(NSString *)opName;

@end

J2OBJC_STATIC_INIT(PIMS_ServiceManager)

J2OBJC_FIELD_SETTER(PIMS_ServiceManager, services_, JavaUtilHashtable *)
J2OBJC_FIELD_SETTER(PIMS_ServiceManager, targetID_, NSString *)

inline jint PIMS_ServiceManager_get_SERVICETYPE_APPLICATION(void);
#define PIMS_ServiceManager_SERVICETYPE_APPLICATION 10
J2OBJC_STATIC_FIELD_CONSTANT(PIMS_ServiceManager, SERVICETYPE_APPLICATION, jint)

inline jint PIMS_ServiceManager_get_SERVICETYPE_BUSINESSLOGIC(void);
#define PIMS_ServiceManager_SERVICETYPE_BUSINESSLOGIC 11
J2OBJC_STATIC_FIELD_CONSTANT(PIMS_ServiceManager, SERVICETYPE_BUSINESSLOGIC, jint)

inline jint PIMS_ServiceManager_get_SERVICETYPE_UNSPECIFIED(void);
#define PIMS_ServiceManager_SERVICETYPE_UNSPECIFIED 12
J2OBJC_STATIC_FIELD_CONSTANT(PIMS_ServiceManager, SERVICETYPE_UNSPECIFIED, jint)

FOUNDATION_EXPORT PIMS_ServiceManager *PIMS_ServiceManager_getInstanceWithNSString_(NSString *targetID);

FOUNDATION_EXPORT PIMS_ServiceManager *PIMS_ServiceManager_getInstance(void);

FOUNDATION_EXPORT void PIMS_ServiceManager_init(PIMS_ServiceManager *self);

FOUNDATION_EXPORT PIMS_ServiceManager *new_PIMS_ServiceManager_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMS_ServiceManager *create_PIMS_ServiceManager_init(void);

FOUNDATION_EXPORT void PIMS_ServiceManager_initWithNSString_(PIMS_ServiceManager *self, NSString *targetID);

FOUNDATION_EXPORT PIMS_ServiceManager *new_PIMS_ServiceManager_initWithNSString_(NSString *targetID) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMS_ServiceManager *create_PIMS_ServiceManager_initWithNSString_(NSString *targetID);

J2OBJC_TYPE_LITERAL_HEADER(PIMS_ServiceManager)

@compatibility_alias DePidataModelsServiceServiceManager PIMS_ServiceManager;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsServiceServiceManager")
