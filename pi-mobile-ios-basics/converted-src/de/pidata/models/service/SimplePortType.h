//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-service/src/de/pidata/models/service/SimplePortType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsServiceSimplePortType")
#ifdef RESTRICT_DePidataModelsServiceSimplePortType
#define INCLUDE_ALL_DePidataModelsServiceSimplePortType 0
#else
#define INCLUDE_ALL_DePidataModelsServiceSimplePortType 1
#endif
#undef RESTRICT_DePidataModelsServiceSimplePortType

#if !defined (PIMS_SimplePortType_) && (INCLUDE_ALL_DePidataModelsServiceSimplePortType || defined(INCLUDE_PIMS_SimplePortType))
#define PIMS_SimplePortType_

#define RESTRICT_DePidataModelsServicePortType 1
#define INCLUDE_PIMS_PortType 1
#include "de/pidata/models/service/PortType.h"

@class PIQ_QName;
@protocol PIMS_Operation;

@interface PIMS_SimplePortType : NSObject < PIMS_PortType >

#pragma mark Public

- (instancetype)initWithPIQ_QName:(PIQ_QName *)name;

- (void)addOperationWithPIMS_Operation:(id<PIMS_Operation>)op;

- (PIQ_QName *)getName;

- (id<PIMS_Operation>)getOperationWithPIQ_QName:(PIQ_QName *)opName;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMS_SimplePortType)

FOUNDATION_EXPORT void PIMS_SimplePortType_initWithPIQ_QName_(PIMS_SimplePortType *self, PIQ_QName *name);

FOUNDATION_EXPORT PIMS_SimplePortType *new_PIMS_SimplePortType_initWithPIQ_QName_(PIQ_QName *name) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMS_SimplePortType *create_PIMS_SimplePortType_initWithPIQ_QName_(PIQ_QName *name);

J2OBJC_TYPE_LITERAL_HEADER(PIMS_SimplePortType)

@compatibility_alias DePidataModelsServiceSimplePortType PIMS_SimplePortType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsServiceSimplePortType")
