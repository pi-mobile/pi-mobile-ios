//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-binding/src/de/pidata/models/simpleModels/SimpleStringTable.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/simpleModels/SimpleModelsFactory.h"
#include "de/pidata/models/simpleModels/SimpleStringTable.h"
#include "de/pidata/models/simpleModels/StringMap.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelCollection.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "java/util/Collection.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/simpleModels/SimpleStringTable must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMSM_SimpleStringTable () {
 @public
  id<JavaUtilCollection> stringMapEntrys_;
}

@end

J2OBJC_FIELD_SETTER(PIMSM_SimpleStringTable, stringMapEntrys_, id<JavaUtilCollection>)

J2OBJC_INITIALIZED_DEFN(PIMSM_SimpleStringTable)

PIQ_Namespace *PIMSM_SimpleStringTable_NAMESPACE;
PIQ_QName *PIMSM_SimpleStringTable_ID_ID;
PIQ_QName *PIMSM_SimpleStringTable_ID_STRINGMAPENTRY;

@implementation PIMSM_SimpleStringTable

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIMSM_SimpleStringTable_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIMSM_SimpleStringTable_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIMSM_SimpleStringTable_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIMSM_SimpleStringTable_ID_ID], [PIQ_QName class]);
}

- (void)setIDWithPIQ_QName:(PIQ_QName *)iD {
  [self setWithPIQ_QName:PIMSM_SimpleStringTable_ID_ID withId:iD];
}

- (PIMSM_StringMap *)getStringMapEntryWithPIQ_Key:(id<PIQ_Key>)stringMapEntryID {
  return (PIMSM_StringMap *) cast_chk([self getWithPIQ_QName:PIMSM_SimpleStringTable_ID_STRINGMAPENTRY withPIQ_Key:stringMapEntryID], [PIMSM_StringMap class]);
}

- (void)addStringMapEntryWithPIMSM_StringMap:(PIMSM_StringMap *)stringMapEntry {
  [self addWithPIQ_QName:PIMSM_SimpleStringTable_ID_STRINGMAPENTRY withPIMR_Model:stringMapEntry];
}

- (void)removeStringMapEntryWithPIMSM_StringMap:(PIMSM_StringMap *)stringMapEntry {
  [self removeWithPIQ_QName:PIMSM_SimpleStringTable_ID_STRINGMAPENTRY withPIMR_Model:stringMapEntry];
}

- (id<PIMR_ModelIterator>)stringMapEntryIter {
  return [self iteratorWithPIQ_QName:PIMSM_SimpleStringTable_ID_STRINGMAPENTRY withPIMR_Filter:nil];
}

- (jint)stringMapEntryCount {
  return [self childCountWithPIQ_QName:PIMSM_SimpleStringTable_ID_STRINGMAPENTRY];
}

- (id<JavaUtilCollection>)getStringMapEntrys {
  return stringMapEntrys_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "LPIMSM_StringMap;", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 9, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 10, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getID);
  methods[4].selector = @selector(setIDWithPIQ_QName:);
  methods[5].selector = @selector(getStringMapEntryWithPIQ_Key:);
  methods[6].selector = @selector(addStringMapEntryWithPIMSM_StringMap:);
  methods[7].selector = @selector(removeStringMapEntryWithPIMSM_StringMap:);
  methods[8].selector = @selector(stringMapEntryIter);
  methods[9].selector = @selector(stringMapEntryCount);
  methods[10].selector = @selector(getStringMapEntrys);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_STRINGMAPENTRY", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "stringMapEntrys_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x2, -1, -1, 14, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setID", "LPIQ_QName;", "getStringMapEntry", "LPIQ_Key;", "addStringMapEntry", "LPIMSM_StringMap;", "removeStringMapEntry", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/models/simpleModels/StringMap;>;", "()Ljava/util/Collection<Lde/pidata/models/simpleModels/StringMap;>;", &PIMSM_SimpleStringTable_NAMESPACE, &PIMSM_SimpleStringTable_ID_ID, &PIMSM_SimpleStringTable_ID_STRINGMAPENTRY, "Ljava/util/Collection<Lde/pidata/models/simpleModels/StringMap;>;" };
  static const J2ObjcClassInfo _PIMSM_SimpleStringTable = { "SimpleStringTable", "de.pidata.models.simpleModels", ptrTable, methods, fields, 7, 0x1, 11, 4, -1, -1, -1, -1, -1 };
  return &_PIMSM_SimpleStringTable;
}

+ (void)initialize {
  if (self == [PIMSM_SimpleStringTable class]) {
    PIMSM_SimpleStringTable_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://www.pidata.de/res/simpleModels.xsd");
    PIMSM_SimpleStringTable_ID_ID = [((PIQ_Namespace *) nil_chk(PIMSM_SimpleStringTable_NAMESPACE)) getQNameWithNSString:@"ID"];
    PIMSM_SimpleStringTable_ID_STRINGMAPENTRY = [PIMSM_SimpleStringTable_NAMESPACE getQNameWithNSString:@"stringMapEntry"];
    J2OBJC_SET_INITIALIZED(PIMSM_SimpleStringTable)
  }
}

@end

void PIMSM_SimpleStringTable_init(PIMSM_SimpleStringTable *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIMSM_SimpleModelsFactory, SIMPLESTRINGTABLE_TYPE), nil, nil, nil);
  self->stringMapEntrys_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIMSM_SimpleStringTable_ID_STRINGMAPENTRY, self->children_);
}

PIMSM_SimpleStringTable *new_PIMSM_SimpleStringTable_init() {
  J2OBJC_NEW_IMPL(PIMSM_SimpleStringTable, init)
}

PIMSM_SimpleStringTable *create_PIMSM_SimpleStringTable_init() {
  J2OBJC_CREATE_IMPL(PIMSM_SimpleStringTable, init)
}

void PIMSM_SimpleStringTable_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIMSM_SimpleStringTable *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIMSM_SimpleModelsFactory, SIMPLESTRINGTABLE_TYPE), attributeNames, anyAttribs, childNames);
  self->stringMapEntrys_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIMSM_SimpleStringTable_ID_STRINGMAPENTRY, self->children_);
}

PIMSM_SimpleStringTable *new_PIMSM_SimpleStringTable_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIMSM_SimpleStringTable, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIMSM_SimpleStringTable *create_PIMSM_SimpleStringTable_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIMSM_SimpleStringTable, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIMSM_SimpleStringTable_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIMSM_SimpleStringTable *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  self->stringMapEntrys_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_ChildList_(PIMSM_SimpleStringTable_ID_STRINGMAPENTRY, self->children_);
}

PIMSM_SimpleStringTable *new_PIMSM_SimpleStringTable_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIMSM_SimpleStringTable, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIMSM_SimpleStringTable *create_PIMSM_SimpleStringTable_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIMSM_SimpleStringTable, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMSM_SimpleStringTable)

J2OBJC_NAME_MAPPING(PIMSM_SimpleStringTable, "de.pidata.models.simpleModels", "PIMSM_")
