//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/AbstractModel.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTreeAbstractModel")
#ifdef RESTRICT_DePidataModelsTreeAbstractModel
#define INCLUDE_ALL_DePidataModelsTreeAbstractModel 0
#else
#define INCLUDE_ALL_DePidataModelsTreeAbstractModel 1
#endif
#undef RESTRICT_DePidataModelsTreeAbstractModel

#if !defined (PIMR_AbstractModel_) && (INCLUDE_ALL_DePidataModelsTreeAbstractModel || defined(INCLUDE_PIMR_AbstractModel))
#define PIMR_AbstractModel_

#define RESTRICT_DePidataModelsTreeModel 1
#define INCLUDE_PIMR_Model 1
#include "de/pidata/models/tree/Model.h"

@class IOSObjectArray;
@class JavaLangStringBuffer;
@class JavaUtilHashtable;
@class JavaUtilVector;
@class PIQ_NamespaceTable;
@class PIQ_QName;
@protocol JavaUtilEnumeration;
@protocol PIMR_Comparator;
@protocol PIMR_EventListener;
@protocol PIMR_ModelFactory;
@protocol PIMT_Type;
@protocol PIQ_Key;

@interface PIMR_AbstractModel : NSObject < PIMR_Model > {
 @public
  id<PIQ_Key> key_;
  id<PIMT_Type> type_;
  id content_;
  PIQ_NamespaceTable *namespaceTable_;
  jboolean dirty_;
}

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
                  withPIMT_Type:(id<PIMT_Type>)type;

- (void)addListenerWithPIMR_EventListener:(id<PIMR_EventListener>)listener;

- (id<JavaUtilEnumeration>)anyAttributeNames;

- (void)changeSiblingWithInt:(jint)siblingType
              withPIMR_Model:(id<PIMR_Model>)sibling;

- (id<PIMR_Model>)cloneWithPIQ_Key:(id<PIQ_Key>)newKey
                       withBoolean:(jboolean)deep
                       withBoolean:(jboolean)linked;

- (void)clonedFromWithPIMR_Model:(id<PIMR_Model>)cloneSource;

- (id<PIMR_Model>)cloneSource;

- (JavaLangStringBuffer *)createPathWithPIMR_Model:(id<PIMR_Model>)fromAncestor;

- (jboolean)isEqual:(id)object;

- (id<PIMR_Model>)firstChildWithPIQ_QName:(PIQ_QName *)relationID;

- (id<PIMR_Model>)getWithPIQ_QName:(PIQ_QName *)relationID
                       withPIQ_Key:(id<PIQ_Key>)childKey;

- (IOSObjectArray *)getAllWithPIQ_QName:(PIQ_QName *)relationID;

- (id)getContent;

- (id<PIMR_ModelFactory>)getFactory;

- (id<PIMR_Model>)getParentWithBoolean:(jboolean)useCloneSource;

- (PIQ_QName *)getParentRelationID;

- (jboolean)isDirty;

- (jboolean)isReadOnlyWithPIQ_QName:(PIQ_QName *)attributeName;

- (id<PIQ_Key>)key;

- (id<PIMR_Model>)lastChildWithPIQ_QName:(PIQ_QName *)relationID;

- (PIQ_NamespaceTable *)namespaceTable;

- (id<PIMR_Model>)nextSiblingWithPIQ_QName:(PIQ_QName *)relationID;

- (void)ownNamespaceTableWithPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaceTable;

- (id<PIMR_Model>)prevSiblingWithPIQ_QName:(PIQ_QName *)relationID;

- (void)removeListenerWithPIMR_EventListener:(id<PIMR_EventListener>)listener;

- (void)setContentWithId:(id)content;

- (void)setParentWithPIMR_Model:(id<PIMR_Model>)parent
                  withPIQ_QName:(PIQ_QName *)parentRelationID;

- (jboolean)sortWithPIQ_QName:(PIQ_QName *)relationName
          withPIMR_Comparator:(id<PIMR_Comparator>)comparator;

- (id<PIMT_Type>)type;

#pragma mark Protected

- (void)addAllElementsWithJavaUtilHashtable:(JavaUtilHashtable *)elemTable
                         withJavaUtilVector:(JavaUtilVector *)children;

- (void)addAllElementsWithJavaUtilVector:(JavaUtilVector *)elemList
                      withJavaUtilVector:(JavaUtilVector *)children;

- (void)fireDataAddedWithPIQ_QName:(PIQ_QName *)elementID
                            withId:(id)newValue;

- (void)fireDataChangedWithPIQ_QName:(PIQ_QName *)elementID
                              withId:(id)oldValue
                              withId:(id)newValue;

- (void)fireDataRemovedWithPIQ_QName:(PIQ_QName *)relationID
                              withId:(id)oldValue
                      withPIMR_Model:(id<PIMR_Model>)nextValue;

- (void)fireEventWithInt:(jint)eventID
                  withId:(id)source
           withPIQ_QName:(PIQ_QName *)elementID
                  withId:(id)oldValue
                  withId:(id)newValue;

- (jint)indexOfWithPIQ_QName:(PIQ_QName *)elementID
          withJavaUtilVector:(JavaUtilVector *)elemList;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMR_AbstractModel)

J2OBJC_FIELD_SETTER(PIMR_AbstractModel, key_, id<PIQ_Key>)
J2OBJC_FIELD_SETTER(PIMR_AbstractModel, type_, id<PIMT_Type>)
J2OBJC_FIELD_SETTER(PIMR_AbstractModel, content_, id)
J2OBJC_FIELD_SETTER(PIMR_AbstractModel, namespaceTable_, PIQ_NamespaceTable *)

FOUNDATION_EXPORT void PIMR_AbstractModel_initWithPIQ_Key_withPIMT_Type_(PIMR_AbstractModel *self, id<PIQ_Key> key, id<PIMT_Type> type);

J2OBJC_TYPE_LITERAL_HEADER(PIMR_AbstractModel)

@compatibility_alias DePidataModelsTreeAbstractModel PIMR_AbstractModel;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTreeAbstractModel")
