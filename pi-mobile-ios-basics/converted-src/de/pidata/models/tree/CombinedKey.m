//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/CombinedKey.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/CombinedKey.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/SimpleType.h"
#include "de/pidata/qnames/Base32.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/StringBuffer.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/tree/CombinedKey must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMR_CombinedKey () {
 @public
  IOSObjectArray *values_;
  jint hashCode_;
  NSString *keyString_;
}

@end

J2OBJC_FIELD_SETTER(PIMR_CombinedKey, values_, IOSObjectArray *)
J2OBJC_FIELD_SETTER(PIMR_CombinedKey, keyString_, NSString *)

@implementation PIMR_CombinedKey

- (instancetype)initWithNSObjectArray:(IOSObjectArray *)values {
  PIMR_CombinedKey_initWithNSObjectArray_(self, values);
  return self;
}

+ (PIMR_CombinedKey *)fromKeyStringWithPIMT_ComplexType:(id<PIMT_ComplexType>)type
                                           withNSString:(NSString *)keyString
                                 withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaces {
  return PIMR_CombinedKey_fromKeyStringWithPIMT_ComplexType_withNSString_withPIQ_NamespaceTable_(type, keyString, namespaces);
}

- (NSString *)toKeyStringWithPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaces {
  if (keyString_ == nil) {
    JavaLangStringBuffer *buf = new_JavaLangStringBuffer_init();
    for (jint i = 0; i < ((IOSObjectArray *) nil_chk(values_))->size_; i++) {
      IOSByteArray *bytes;
      if ([IOSObjectArray_Get(values_, i) isKindOfClass:[PIQ_QName class]]) {
        bytes = [((NSString *) nil_chk([((PIQ_QName *) nil_chk(((PIQ_QName *) cast_chk(IOSObjectArray_Get(values_, i), [PIQ_QName class])))) toStringWithPIQ_NamespaceTable:namespaces])) java_getBytes];
      }
      else {
        bytes = [((NSString *) nil_chk([nil_chk(IOSObjectArray_Get(values_, i)) description])) java_getBytes];
      }
      if (i > 0) (void) [buf appendWithChar:'_'];
      (void) [buf appendWithNSString:PIQ_Base32_encodeWithByteArray_(bytes)];
    }
    keyString_ = [buf description];
  }
  return keyString_;
}

- (id)getKeyValueWithInt:(jint)keyValueIndex {
  return IOSObjectArray_Get(nil_chk(self->values_), keyValueIndex);
}

- (jint)keyValueCount {
  return ((IOSObjectArray *) nil_chk(values_))->size_;
}

- (NSUInteger)hash {
  return self->hashCode_;
}

- (jboolean)isEqual:(id)obj {
  if ([obj isKindOfClass:[PIMR_CombinedKey class]]) {
    PIMR_CombinedKey *otherKey = (PIMR_CombinedKey *) obj;
    if (((IOSObjectArray *) nil_chk(((PIMR_CombinedKey *) nil_chk(otherKey))->values_))->size_ != values_->size_) {
      return false;
    }
    for (jint i = 0; i < ((IOSObjectArray *) nil_chk(values_))->size_; i++) {
      id myValue = IOSObjectArray_Get(values_, i);
      id otherValue = IOSObjectArray_Get(otherKey->values_, i);
      if (otherValue == nil) {
        if (myValue != nil) return false;
      }
      else {
        if (![otherValue isEqual:myValue]) return false;
      }
    }
    return true;
  }
  else {
    return false;
  }
}

- (NSString *)description {
  JavaLangStringBuffer *buf = new_JavaLangStringBuffer_initWithNSString_(@"CombinedKey[");
  for (jint i = 0; i < ((IOSObjectArray *) nil_chk(values_))->size_; i++) {
    if (i > 0) (void) [buf appendWithNSString:@","];
    (void) [buf appendWithId:IOSObjectArray_Get(nil_chk(values_), i)];
  }
  (void) [buf appendWithNSString:@"]"];
  return [buf description];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LPIMR_CombinedKey;", 0x9, 1, 2, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 7, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 10, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithNSObjectArray:);
  methods[1].selector = @selector(fromKeyStringWithPIMT_ComplexType:withNSString:withPIQ_NamespaceTable:);
  methods[2].selector = @selector(toKeyStringWithPIQ_NamespaceTable:);
  methods[3].selector = @selector(getKeyValueWithInt:);
  methods[4].selector = @selector(keyValueCount);
  methods[5].selector = @selector(hash);
  methods[6].selector = @selector(isEqual:);
  methods[7].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "values_", "[LNSObject;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "hashCode_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "keyString_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "[LNSObject;", "fromKeyString", "LPIMT_ComplexType;LNSString;LPIQ_NamespaceTable;", "toKeyString", "LPIQ_NamespaceTable;", "getKeyValue", "I", "hashCode", "equals", "LNSObject;", "toString" };
  static const J2ObjcClassInfo _PIMR_CombinedKey = { "CombinedKey", "de.pidata.models.tree", ptrTable, methods, fields, 7, 0x1, 8, 3, -1, -1, -1, -1, -1 };
  return &_PIMR_CombinedKey;
}

@end

void PIMR_CombinedKey_initWithNSObjectArray_(PIMR_CombinedKey *self, IOSObjectArray *values) {
  NSObject_init(self);
  self->hashCode_ = 0;
  self->keyString_ = nil;
  self->values_ = values;
  for (jint i = 0; i < ((IOSObjectArray *) nil_chk(values))->size_; i++) {
    if (IOSObjectArray_Get(values, i) != nil) {
      self->hashCode_ += ((jint) [nil_chk(IOSObjectArray_Get(values, i)) hash]);
    }
  }
}

PIMR_CombinedKey *new_PIMR_CombinedKey_initWithNSObjectArray_(IOSObjectArray *values) {
  J2OBJC_NEW_IMPL(PIMR_CombinedKey, initWithNSObjectArray_, values)
}

PIMR_CombinedKey *create_PIMR_CombinedKey_initWithNSObjectArray_(IOSObjectArray *values) {
  J2OBJC_CREATE_IMPL(PIMR_CombinedKey, initWithNSObjectArray_, values)
}

PIMR_CombinedKey *PIMR_CombinedKey_fromKeyStringWithPIMT_ComplexType_withNSString_withPIQ_NamespaceTable_(id<PIMT_ComplexType> type, NSString *keyString, PIQ_NamespaceTable *namespaces) {
  PIMR_CombinedKey_initialize();
  jint start = 0;
  jint pos;
  jint i = 0;
  IOSObjectArray *values = [IOSObjectArray newArrayWithLength:[((id<PIMT_ComplexType>) nil_chk(type)) keyAttributeCount] type:NSObject_class_()];
  do {
    pos = [((NSString *) nil_chk(keyString)) java_indexOf:'_' fromIndex:start];
    IOSByteArray *bytes;
    if (pos >= 0) {
      bytes = PIQ_Base32_decodeWithNSString_([keyString java_substring:start endIndex:pos]);
    }
    else {
      bytes = PIQ_Base32_decodeWithNSString_([keyString java_substring:start]);
    }
    start = pos + 1;
    id<PIMT_SimpleType> keyValueType = [type getKeyAttributeTypeWithInt:i];
    (void) IOSObjectArray_Set(values, i, [((id<PIMT_SimpleType>) nil_chk(keyValueType)) createValueWithNSString:[NSString java_stringWithBytes:bytes] withPIQ_NamespaceTable:namespaces]);
  }
  while (start > 0);
  PIMR_CombinedKey *key = new_PIMR_CombinedKey_initWithNSObjectArray_(values);
  key->keyString_ = keyString;
  return key;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMR_CombinedKey)

J2OBJC_NAME_MAPPING(PIMR_CombinedKey, "de.pidata.models.tree", "PIMR_")
