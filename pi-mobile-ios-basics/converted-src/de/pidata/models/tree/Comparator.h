//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/Comparator.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTreeComparator")
#ifdef RESTRICT_DePidataModelsTreeComparator
#define INCLUDE_ALL_DePidataModelsTreeComparator 0
#else
#define INCLUDE_ALL_DePidataModelsTreeComparator 1
#endif
#undef RESTRICT_DePidataModelsTreeComparator

#if !defined (PIMR_Comparator_) && (INCLUDE_ALL_DePidataModelsTreeComparator || defined(INCLUDE_PIMR_Comparator))
#define PIMR_Comparator_

@protocol PIMR_Model;

@protocol PIMR_Comparator < JavaObject >

- (jint)compareWithPIMR_Model:(id<PIMR_Model>)m1
               withPIMR_Model:(id<PIMR_Model>)m2;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMR_Comparator)

J2OBJC_TYPE_LITERAL_HEADER(PIMR_Comparator)

#define DePidataModelsTreeComparator PIMR_Comparator

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTreeComparator")
