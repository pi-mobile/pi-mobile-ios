//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/ComparatorNumber.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ComparatorNumber.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/XPath.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Byte.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/lang/Short.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/tree/ComparatorNumber must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMR_ComparatorNumber () {
 @public
  PIQ_QName *attributeName_;
  PIMR_XPath *modelPath_;
}

@end

J2OBJC_FIELD_SETTER(PIMR_ComparatorNumber, attributeName_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIMR_ComparatorNumber, modelPath_, PIMR_XPath *)

@implementation PIMR_ComparatorNumber

- (instancetype)initWithPIQ_QName:(PIQ_QName *)attributeName {
  PIMR_ComparatorNumber_initWithPIQ_QName_(self, attributeName);
  return self;
}

- (instancetype)initWithPIMR_XPath:(PIMR_XPath *)modelPath
                     withPIQ_QName:(PIQ_QName *)attributeName {
  PIMR_ComparatorNumber_initWithPIMR_XPath_withPIQ_QName_(self, modelPath, attributeName);
  return self;
}

- (jint)compareWithPIMR_Model:(id<PIMR_Model>)m1
               withPIMR_Model:(id<PIMR_Model>)m2 {
  jlong value1 = [self fetchValueWithPIMR_Model:m1];
  jlong value2 = [self fetchValueWithPIMR_Model:m2];
  if (value1 == value2) return 0;
  else if (value1 > value2) return 1;
  else return -1;
}

- (jlong)fetchValueWithPIMR_Model:(id<PIMR_Model>)model {
  if (modelPath_ != nil) {
    model = [modelPath_ getModelWithPIMR_Model:model withPIMR_Context:nil];
    if (model == nil) return 0;
  }
  id attr1 = [((id<PIMR_Model>) nil_chk(model)) getWithPIQ_QName:attributeName_];
  if (attr1 == nil) return 0;
  else if ([attr1 isKindOfClass:[JavaLangLong class]]) return [((JavaLangLong *) attr1) longLongValue];
  else if ([attr1 isKindOfClass:[JavaLangInteger class]]) return [((JavaLangInteger *) attr1) intValue];
  else if ([attr1 isKindOfClass:[JavaLangShort class]]) return [((JavaLangShort *) attr1) shortValue];
  else if ([attr1 isKindOfClass:[JavaLangByte class]]) return [((JavaLangByte *) attr1) charValue];
  @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@", @"Unsupported value type, value=", attr1));
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 2, 3, -1, -1, -1, -1 },
    { NULL, "J", 0x4, 4, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_QName:);
  methods[1].selector = @selector(initWithPIMR_XPath:withPIQ_QName:);
  methods[2].selector = @selector(compareWithPIMR_Model:withPIMR_Model:);
  methods[3].selector = @selector(fetchValueWithPIMR_Model:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "attributeName_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "modelPath_", "LPIMR_XPath;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_QName;", "LPIMR_XPath;LPIQ_QName;", "compare", "LPIMR_Model;LPIMR_Model;", "fetchValue", "LPIMR_Model;", "Ljava/lang/Object;Lde/pidata/models/tree/Comparator<Lde/pidata/models/tree/Model;>;" };
  static const J2ObjcClassInfo _PIMR_ComparatorNumber = { "ComparatorNumber", "de.pidata.models.tree", ptrTable, methods, fields, 7, 0x1, 4, 2, -1, -1, -1, 6, -1 };
  return &_PIMR_ComparatorNumber;
}

@end

void PIMR_ComparatorNumber_initWithPIQ_QName_(PIMR_ComparatorNumber *self, PIQ_QName *attributeName) {
  PIMR_ComparatorNumber_initWithPIMR_XPath_withPIQ_QName_(self, nil, attributeName);
}

PIMR_ComparatorNumber *new_PIMR_ComparatorNumber_initWithPIQ_QName_(PIQ_QName *attributeName) {
  J2OBJC_NEW_IMPL(PIMR_ComparatorNumber, initWithPIQ_QName_, attributeName)
}

PIMR_ComparatorNumber *create_PIMR_ComparatorNumber_initWithPIQ_QName_(PIQ_QName *attributeName) {
  J2OBJC_CREATE_IMPL(PIMR_ComparatorNumber, initWithPIQ_QName_, attributeName)
}

void PIMR_ComparatorNumber_initWithPIMR_XPath_withPIQ_QName_(PIMR_ComparatorNumber *self, PIMR_XPath *modelPath, PIQ_QName *attributeName) {
  NSObject_init(self);
  if (attributeName == nil) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"attributeName must not be null");
  }
  self->attributeName_ = attributeName;
  self->modelPath_ = modelPath;
}

PIMR_ComparatorNumber *new_PIMR_ComparatorNumber_initWithPIMR_XPath_withPIQ_QName_(PIMR_XPath *modelPath, PIQ_QName *attributeName) {
  J2OBJC_NEW_IMPL(PIMR_ComparatorNumber, initWithPIMR_XPath_withPIQ_QName_, modelPath, attributeName)
}

PIMR_ComparatorNumber *create_PIMR_ComparatorNumber_initWithPIMR_XPath_withPIQ_QName_(PIMR_XPath *modelPath, PIQ_QName *attributeName) {
  J2OBJC_CREATE_IMPL(PIMR_ComparatorNumber, initWithPIMR_XPath_withPIQ_QName_, modelPath, attributeName)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMR_ComparatorNumber)

J2OBJC_NAME_MAPPING(PIMR_ComparatorNumber, "de.pidata.models.tree", "PIMR_")
