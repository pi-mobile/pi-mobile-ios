//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/EmptyEnumerator.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/tree/EmptyEnumerator.h"
#include "java/util/NoSuchElementException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/tree/EmptyEnumerator must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMR_EmptyEnumerator ()

- (instancetype)init;

@end

inline PIMR_EmptyEnumerator *PIMR_EmptyEnumerator_get_instance(void);
inline PIMR_EmptyEnumerator *PIMR_EmptyEnumerator_set_instance(PIMR_EmptyEnumerator *value);
static PIMR_EmptyEnumerator *PIMR_EmptyEnumerator_instance;
J2OBJC_STATIC_FIELD_OBJ(PIMR_EmptyEnumerator, instance, PIMR_EmptyEnumerator *)

__attribute__((unused)) static void PIMR_EmptyEnumerator_init(PIMR_EmptyEnumerator *self);

__attribute__((unused)) static PIMR_EmptyEnumerator *new_PIMR_EmptyEnumerator_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static PIMR_EmptyEnumerator *create_PIMR_EmptyEnumerator_init(void);

@implementation PIMR_EmptyEnumerator

+ (PIMR_EmptyEnumerator *)getInstance {
  return PIMR_EmptyEnumerator_getInstance();
}

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIMR_EmptyEnumerator_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (jboolean)hasMoreElements {
  return false;
}

- (id)nextElement {
  @throw new_JavaUtilNoSuchElementException_initWithNSString_(@"Hashtable Enumerator");
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPIMR_EmptyEnumerator;", 0x19, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getInstance);
  methods[1].selector = @selector(init);
  methods[2].selector = @selector(hasMoreElements);
  methods[3].selector = @selector(nextElement);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "instance", "LPIMR_EmptyEnumerator;", .constantValue.asLong = 0, 0xa, -1, 0, -1, -1 },
  };
  static const void *ptrTable[] = { &PIMR_EmptyEnumerator_instance };
  static const J2ObjcClassInfo _PIMR_EmptyEnumerator = { "EmptyEnumerator", "de.pidata.models.tree", ptrTable, methods, fields, 7, 0x1, 4, 1, -1, -1, -1, -1, -1 };
  return &_PIMR_EmptyEnumerator;
}

@end

PIMR_EmptyEnumerator *PIMR_EmptyEnumerator_getInstance() {
  PIMR_EmptyEnumerator_initialize();
  if (PIMR_EmptyEnumerator_instance == nil) {
    PIMR_EmptyEnumerator_instance = new_PIMR_EmptyEnumerator_init();
  }
  return PIMR_EmptyEnumerator_instance;
}

void PIMR_EmptyEnumerator_init(PIMR_EmptyEnumerator *self) {
  NSObject_init(self);
}

PIMR_EmptyEnumerator *new_PIMR_EmptyEnumerator_init() {
  J2OBJC_NEW_IMPL(PIMR_EmptyEnumerator, init)
}

PIMR_EmptyEnumerator *create_PIMR_EmptyEnumerator_init() {
  J2OBJC_CREATE_IMPL(PIMR_EmptyEnumerator, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMR_EmptyEnumerator)

J2OBJC_NAME_MAPPING(PIMR_EmptyEnumerator, "de.pidata.models.tree", "PIMR_")
