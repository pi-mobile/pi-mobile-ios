//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/ModelFactory.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ModelFactory.h"
#include "de/pidata/qnames/Namespace.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/tree/ModelFactory must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIMR_ModelFactory)

PIQ_Namespace *PIMR_ModelFactory_NAMESPACE;

@implementation PIMR_ModelFactory

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPIMR_Model;", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x401, 0, 2, -1, 3, -1, -1 },
    { NULL, "LPIMR_Model;", 0x401, 0, 4, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x401, 0, 5, -1, -1, -1, -1 },
    { NULL, "LPIQ_Namespace;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_Relation;", 0x401, 6, 5, -1, -1, -1, -1 },
    { NULL, "LPIMT_SimpleType;", 0x401, 7, 5, -1, -1, -1, -1 },
    { NULL, "LPIMT_Type;", 0x401, 8, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 11, 12, -1, -1, -1, -1 },
    { NULL, "LPIMT_Relation;", 0x401, 11, 13, -1, -1, -1, -1 },
    { NULL, "LPIMR_QNameIterator;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_QNameIterator;", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(createInstanceWithPIMT_SimpleType:withId:);
  methods[1].selector = @selector(createInstanceWithPIQ_Key:withPIMT_Type:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(createInstanceWithPIMT_ComplexType:);
  methods[3].selector = @selector(createInstanceWithPIQ_QName:);
  methods[4].selector = @selector(getTargetNamespace);
  methods[5].selector = @selector(getRootRelationWithPIQ_QName:);
  methods[6].selector = @selector(getAttributeWithPIQ_QName:);
  methods[7].selector = @selector(getTypeWithPIQ_QName:);
  methods[8].selector = @selector(getSchemaLocation);
  methods[9].selector = @selector(getVersion);
  methods[10].selector = @selector(addTypeWithPIMT_Type:);
  methods[11].selector = @selector(addRootRelationWithPIMT_Relation:);
  methods[12].selector = @selector(addRootRelationWithPIQ_QName:withPIMT_Type:withInt:withInt:withPIQ_QName:);
  methods[13].selector = @selector(typeNames);
  methods[14].selector = @selector(relationNames);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
  };
  static const void *ptrTable[] = { "createInstance", "LPIMT_SimpleType;LNSObject;", "LPIQ_Key;LPIMT_Type;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/Type;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)Lde/pidata/models/tree/Model;", "LPIMT_ComplexType;", "LPIQ_QName;", "getRootRelation", "getAttribute", "getType", "addType", "LPIMT_Type;", "addRootRelation", "LPIMT_Relation;", "LPIQ_QName;LPIMT_Type;IILPIQ_QName;", &PIMR_ModelFactory_NAMESPACE };
  static const J2ObjcClassInfo _PIMR_ModelFactory = { "ModelFactory", "de.pidata.models.tree", ptrTable, methods, fields, 7, 0x609, 15, 1, -1, -1, -1, -1, -1 };
  return &_PIMR_ModelFactory;
}

+ (void)initialize {
  if (self == [PIMR_ModelFactory class]) {
    PIMR_ModelFactory_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.models");
    J2OBJC_SET_INITIALIZED(PIMR_ModelFactory)
  }
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIMR_ModelFactory)

J2OBJC_NAME_MAPPING(PIMR_ModelFactory, "de.pidata.models.tree", "PIMR_")
