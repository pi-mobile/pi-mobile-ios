//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/ModelHelper.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTreeModelHelper")
#ifdef RESTRICT_DePidataModelsTreeModelHelper
#define INCLUDE_ALL_DePidataModelsTreeModelHelper 0
#else
#define INCLUDE_ALL_DePidataModelsTreeModelHelper 1
#endif
#undef RESTRICT_DePidataModelsTreeModelHelper

#if !defined (PIMR_ModelHelper_) && (INCLUDE_ALL_DePidataModelsTreeModelHelper || defined(INCLUDE_PIMR_ModelHelper))
#define PIMR_ModelHelper_

@class IOSObjectArray;
@class PIQ_QName;
@protocol PIMR_Filter;
@protocol PIMR_Model;
@protocol PIMR_ModelReference;

@interface PIMR_ModelHelper : NSObject

#pragma mark Public

- (instancetype)init;

+ (id<PIMR_Model>)findModelWithPIMR_ModelReference:(id<PIMR_ModelReference>)reference;

+ (id<PIMR_Model>)findNextWithPIMR_Model:(id<PIMR_Model>)from
                           withPIQ_QName:(PIQ_QName *)relationName
                             withBoolean:(jboolean)forward
                           withPIQ_QName:(PIQ_QName *)attrName
                                  withId:(id)value;

+ (jboolean)isAncestorWithPIMR_Model:(id<PIMR_Model>)checkAncestor
                      withPIMR_Model:(id<PIMR_Model>)model;

+ (IOSObjectArray *)resizeArrayWithPIMR_ModelArray:(IOSObjectArray *)oldArray
                                           withInt:(jint)newSize;

+ (IOSObjectArray *)toArrayWithPIMR_Model:(id<PIMR_Model>)parent
                            withPIQ_QName:(PIQ_QName *)relationID
                          withPIMR_Filter:(id<PIMR_Filter>)filter;

+ (void)updateFromOlderWithPIMR_Model:(id<PIMR_Model>)newModel
                       withPIMR_Model:(id<PIMR_Model>)oldModel
                          withBoolean:(jboolean)childrenOnly;

+ (void)updateOriginalWithPIMR_Model:(id<PIMR_Model>)original
                      withPIMR_Model:(id<PIMR_Model>)clone;

+ (void)updateRecursiveWithPIMR_Model:(id<PIMR_Model>)original
                       withPIMR_Model:(id<PIMR_Model>)copy_;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMR_ModelHelper)

FOUNDATION_EXPORT void PIMR_ModelHelper_init(PIMR_ModelHelper *self);

FOUNDATION_EXPORT PIMR_ModelHelper *new_PIMR_ModelHelper_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_ModelHelper *create_PIMR_ModelHelper_init(void);

FOUNDATION_EXPORT IOSObjectArray *PIMR_ModelHelper_resizeArrayWithPIMR_ModelArray_withInt_(IOSObjectArray *oldArray, jint newSize);

FOUNDATION_EXPORT jboolean PIMR_ModelHelper_isAncestorWithPIMR_Model_withPIMR_Model_(id<PIMR_Model> checkAncestor, id<PIMR_Model> model);

FOUNDATION_EXPORT void PIMR_ModelHelper_updateOriginalWithPIMR_Model_withPIMR_Model_(id<PIMR_Model> original, id<PIMR_Model> clone);

FOUNDATION_EXPORT void PIMR_ModelHelper_updateRecursiveWithPIMR_Model_withPIMR_Model_(id<PIMR_Model> original, id<PIMR_Model> copy_);

FOUNDATION_EXPORT void PIMR_ModelHelper_updateFromOlderWithPIMR_Model_withPIMR_Model_withBoolean_(id<PIMR_Model> newModel, id<PIMR_Model> oldModel, jboolean childrenOnly);

FOUNDATION_EXPORT id<PIMR_Model> PIMR_ModelHelper_findNextWithPIMR_Model_withPIQ_QName_withBoolean_withPIQ_QName_withId_(id<PIMR_Model> from, PIQ_QName *relationName, jboolean forward, PIQ_QName *attrName, id value);

FOUNDATION_EXPORT id<PIMR_Model> PIMR_ModelHelper_findModelWithPIMR_ModelReference_(id<PIMR_ModelReference> reference);

FOUNDATION_EXPORT IOSObjectArray *PIMR_ModelHelper_toArrayWithPIMR_Model_withPIQ_QName_withPIMR_Filter_(id<PIMR_Model> parent, PIQ_QName *relationID, id<PIMR_Filter> filter);

J2OBJC_TYPE_LITERAL_HEADER(PIMR_ModelHelper)

@compatibility_alias DePidataModelsTreeModelHelper PIMR_ModelHelper;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTreeModelHelper")
