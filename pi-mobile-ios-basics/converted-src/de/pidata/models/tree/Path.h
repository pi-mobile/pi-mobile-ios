//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/Path.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTreePath")
#ifdef RESTRICT_DePidataModelsTreePath
#define INCLUDE_ALL_DePidataModelsTreePath 0
#else
#define INCLUDE_ALL_DePidataModelsTreePath 1
#endif
#undef RESTRICT_DePidataModelsTreePath

#if !defined (PIMR_Path_) && (INCLUDE_ALL_DePidataModelsTreePath || defined(INCLUDE_PIMR_Path))
#define PIMR_Path_

@class IOSObjectArray;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMR_Model;

@interface PIMR_Path : NSObject

#pragma mark Public

- (instancetype)initWithPIMR_Model:(id<PIMR_Model>)model;

- (instancetype)initWithPIQ_Namespace:(PIQ_Namespace *)namespace_
                         withNSString:(NSString *)pathExpression;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)model0;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)model0
                    withPIQ_QName:(PIQ_QName *)model1;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)model0
                    withPIQ_QName:(PIQ_QName *)model1
                    withPIQ_QName:(PIQ_QName *)model2;

- (instancetype)initWithPIQ_QNameArray:(IOSObjectArray *)path;

- (jboolean)isEqual:(id)obj;

- (id<PIMR_Model>)getModelWithPIMR_Model:(id<PIMR_Model>)startModel;

- (NSUInteger)hash;

- (PIQ_QName *)modelIDAtWithInt:(jint)index;

- (jint)size;

- (NSString *)description;

- (NSString *)toStringNoNS;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMR_Path)

inline jchar PIMR_Path_get_DELIMITER(void);
#define PIMR_Path_DELIMITER '/'
J2OBJC_STATIC_FIELD_CONSTANT(PIMR_Path, DELIMITER, jchar)

FOUNDATION_EXPORT void PIMR_Path_initWithPIQ_QNameArray_(PIMR_Path *self, IOSObjectArray *path);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIQ_QNameArray_(IOSObjectArray *path) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIQ_QNameArray_(IOSObjectArray *path);

FOUNDATION_EXPORT void PIMR_Path_initWithPIQ_QName_(PIMR_Path *self, PIQ_QName *model0);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIQ_QName_(PIQ_QName *model0) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIQ_QName_(PIQ_QName *model0);

FOUNDATION_EXPORT void PIMR_Path_initWithPIQ_QName_withPIQ_QName_(PIMR_Path *self, PIQ_QName *model0, PIQ_QName *model1);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *model0, PIQ_QName *model1) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *model0, PIQ_QName *model1);

FOUNDATION_EXPORT void PIMR_Path_initWithPIQ_QName_withPIQ_QName_withPIQ_QName_(PIMR_Path *self, PIQ_QName *model0, PIQ_QName *model1, PIQ_QName *model2);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIQ_QName_withPIQ_QName_withPIQ_QName_(PIQ_QName *model0, PIQ_QName *model1, PIQ_QName *model2) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIQ_QName_withPIQ_QName_withPIQ_QName_(PIQ_QName *model0, PIQ_QName *model1, PIQ_QName *model2);

FOUNDATION_EXPORT void PIMR_Path_initWithPIQ_Namespace_withNSString_(PIMR_Path *self, PIQ_Namespace *namespace_, NSString *pathExpression);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIQ_Namespace_withNSString_(PIQ_Namespace *namespace_, NSString *pathExpression) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIQ_Namespace_withNSString_(PIQ_Namespace *namespace_, NSString *pathExpression);

FOUNDATION_EXPORT void PIMR_Path_initWithPIMR_Model_(PIMR_Path *self, id<PIMR_Model> model);

FOUNDATION_EXPORT PIMR_Path *new_PIMR_Path_initWithPIMR_Model_(id<PIMR_Model> model) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMR_Path *create_PIMR_Path_initWithPIMR_Model_(id<PIMR_Model> model);

J2OBJC_TYPE_LITERAL_HEADER(PIMR_Path)

@compatibility_alias DePidataModelsTreePath PIMR_Path;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTreePath")
