//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/tree/QNameIterator.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTreeQNameIterator")
#ifdef RESTRICT_DePidataModelsTreeQNameIterator
#define INCLUDE_ALL_DePidataModelsTreeQNameIterator 0
#else
#define INCLUDE_ALL_DePidataModelsTreeQNameIterator 1
#endif
#undef RESTRICT_DePidataModelsTreeQNameIterator

#if !defined (PIMR_QNameIterator_) && (INCLUDE_ALL_DePidataModelsTreeQNameIterator || defined(INCLUDE_PIMR_QNameIterator))
#define PIMR_QNameIterator_

#define RESTRICT_JavaUtilIterator 1
#define INCLUDE_JavaUtilIterator 1
#include "java/util/Iterator.h"

#define RESTRICT_JavaLangIterable 1
#define INCLUDE_JavaLangIterable 1
#include "java/lang/Iterable.h"

@class PIQ_QName;

@protocol PIMR_QNameIterator < JavaUtilIterator, JavaLangIterable, JavaObject >

- (jboolean)hasNext;

- (PIQ_QName *)next;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMR_QNameIterator)

J2OBJC_TYPE_LITERAL_HEADER(PIMR_QNameIterator)

#define DePidataModelsTreeQNameIterator PIMR_QNameIterator

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTreeQNameIterator")
