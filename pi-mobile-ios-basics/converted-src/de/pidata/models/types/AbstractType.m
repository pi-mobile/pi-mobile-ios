//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/AbstractType.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/ValidationException.h"
#include "de/pidata/models/types/AbstractType.h"
#include "de/pidata/models/types/Type.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/ClassNotFoundException.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/AbstractType must be compiled with ARC (-fobjc-arc)"
#endif

#pragma clang diagnostic ignored "-Wprotocol"

@interface PIMT_AbstractType () {
 @public
  PIQ_QName *typeID_;
  id<PIMT_Type> parentType_;
}

@end

J2OBJC_FIELD_SETTER(PIMT_AbstractType, typeID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIMT_AbstractType, parentType_, id<PIMT_Type>)

@implementation PIMT_AbstractType

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)valueClassName
                    withPIMT_Type:(id<PIMT_Type>)parentType {
  PIMT_AbstractType_initWithPIQ_QName_withNSString_withPIMT_Type_(self, typeID, valueClassName, parentType);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withIOSClass:(IOSClass *)valueClass
                    withPIMT_Type:(id<PIMT_Type>)parentType {
  PIMT_AbstractType_initWithPIQ_QName_withIOSClass_withPIMT_Type_(self, typeID, valueClass, parentType);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID {
  PIMT_AbstractType_initWithPIQ_QName_(self, typeID);
  return self;
}

- (void)init__WithNSString:(NSString *)valueClassName
             withPIMT_Type:(id<PIMT_Type>)parentType {
  self->parentType_ = parentType;
  if (valueClassName != nil) {
    @try {
      self->valueClass_ = IOSClass_forName_(valueClassName);
    }
    @catch (JavaLangClassNotFoundException *e) {
      PICL_Logger_warnWithNSString_(JreStrcat("$$", @"AbstractType: value class not found: ", valueClassName));
    }
  }
}

- (PIQ_QName *)name {
  return self->typeID_;
}

- (IOSClass *)getValueClass {
  return self->valueClass_;
}

- (id<PIMT_Type>)getBaseType {
  return parentType_;
}

- (jint)checkValidWithId:(id)value {
  if ((value == nil) || (JreObjectEqualsEquals(valueClass_, [nil_chk(value) java_getClass])) || ([((IOSClass *) nil_chk(valueClass_)) isAssignableFrom:[value java_getClass]])) {
    return PIMR_ValidationException_NO_ERROR;
  }
  else {
    return PIMR_ValidationException_ERROR_WRONG_CLASS;
  }
}

- (id)convertWithId:(id)value {
  jint valid = [self checkValidWithId:value];
  if ((valid == PIMR_ValidationException_NO_ERROR) || (valid == PIMR_ValidationException_NEEDS_CONVERSION)) {
    return value;
  }
  else {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@$@", @"Value='", value, @"' cannot be converted for type=", self));
  }
}

- (jboolean)isAssignableFromWithPIMT_Type:(id<PIMT_Type>)childType {
  if (JreObjectEqualsEquals([((id<PIMT_Type>) nil_chk(childType)) name], self->typeID_)) {
    return true;
  }
  id<PIMT_Type> baseType = [childType getBaseType];
  while (baseType != nil) {
    if (JreObjectEqualsEquals([baseType name], self->typeID_)) {
      return true;
    }
    baseType = [baseType getBaseType];
  }
  return false;
}

- (NSString *)getErrorMessageWithInt:(jint)errorID {
  return PIMT_AbstractType_createErrorMessageWithInt_withPIMT_Type_(errorID, self);
}

+ (NSString *)createErrorMessageWithInt:(jint)errorID
                          withPIMT_Type:(id<PIMT_Type>)type {
  return PIMT_AbstractType_createErrorMessageWithInt_withPIMT_Type_(errorID, type);
}

- (NSUInteger)hash {
  return ((jint) [((PIQ_QName *) nil_chk([self name])) hash]);
}

- (jboolean)isEqual:(id)obj {
  if (obj == nil) {
    return false;
  }
  else if ([obj isKindOfClass:[PIMT_AbstractType class]]) {
    PIMT_AbstractType *other = (PIMT_AbstractType *) obj;
    return JreObjectEqualsEquals([self name], [other name]);
  }
  else {
    return false;
  }
}

- (NSString *)description {
  if (typeID_ == nil) {
    return @"-- unnamed --";
  }
  return [typeID_ description];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIOSClass;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_Type;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 7, 6, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 12, 13, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 14, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 15, 6, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 16, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_QName:withNSString:withPIMT_Type:);
  methods[1].selector = @selector(initWithPIQ_QName:withIOSClass:withPIMT_Type:);
  methods[2].selector = @selector(initWithPIQ_QName:);
  methods[3].selector = @selector(init__WithNSString:withPIMT_Type:);
  methods[4].selector = @selector(name);
  methods[5].selector = @selector(getValueClass);
  methods[6].selector = @selector(getBaseType);
  methods[7].selector = @selector(checkValidWithId:);
  methods[8].selector = @selector(convertWithId:);
  methods[9].selector = @selector(isAssignableFromWithPIMT_Type:);
  methods[10].selector = @selector(getErrorMessageWithInt:);
  methods[11].selector = @selector(createErrorMessageWithInt:withPIMT_Type:);
  methods[12].selector = @selector(hash);
  methods[13].selector = @selector(isEqual:);
  methods[14].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "valueClass_", "LIOSClass;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "typeID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "parentType_", "LPIMT_Type;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_QName;LNSString;LPIMT_Type;", "LPIQ_QName;LIOSClass;LPIMT_Type;", "LPIQ_QName;", "init", "LNSString;LPIMT_Type;", "checkValid", "LNSObject;", "convert", "isAssignableFrom", "LPIMT_Type;", "getErrorMessage", "I", "createErrorMessage", "ILPIMT_Type;", "hashCode", "equals", "toString" };
  static const J2ObjcClassInfo _PIMT_AbstractType = { "AbstractType", "de.pidata.models.types", ptrTable, methods, fields, 7, 0x401, 15, 3, -1, -1, -1, -1, -1 };
  return &_PIMT_AbstractType;
}

@end

void PIMT_AbstractType_initWithPIQ_QName_withNSString_withPIMT_Type_(PIMT_AbstractType *self, PIQ_QName *typeID, NSString *valueClassName, id<PIMT_Type> parentType) {
  PIMT_AbstractType_initWithPIQ_QName_(self, typeID);
  [self init__WithNSString:valueClassName withPIMT_Type:parentType];
}

void PIMT_AbstractType_initWithPIQ_QName_withIOSClass_withPIMT_Type_(PIMT_AbstractType *self, PIQ_QName *typeID, IOSClass *valueClass, id<PIMT_Type> parentType) {
  PIMT_AbstractType_initWithPIQ_QName_(self, typeID);
  self->parentType_ = parentType;
  self->valueClass_ = valueClass;
}

void PIMT_AbstractType_initWithPIQ_QName_(PIMT_AbstractType *self, PIQ_QName *typeID) {
  NSObject_init(self);
  if (typeID == nil) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"TypeID must not be null!");
  }
  self->typeID_ = typeID;
}

NSString *PIMT_AbstractType_createErrorMessageWithInt_withPIMT_Type_(jint errorID, id<PIMT_Type> type) {
  PIMT_AbstractType_initialize();
  if (errorID == PIMR_ValidationException_ERROR_WRONG_CLASS) {
    return JreStrcat("$$", @"The value's class was wrong, expected ", [((IOSClass *) nil_chk([((id<PIMT_Type>) nil_chk(type)) getValueClass])) description]);
  }
  else if (errorID == PIMR_ValidationException_ERROR_NOT_NULLABLE) {
    return @"The value's class must not be null";
  }
  else if (errorID == PIMR_ValidationException_ERROR_WRONG_TYPE) {
    return JreStrcat("$@", @"The value's type was wrong, expected ", [((id<PIMT_Type>) nil_chk(type)) name]);
  }
  else if (errorID == PIMR_ValidationException_ERROR_ATTR_UNKNOWN) {
    return @"Unknown Attribute";
  }
  else if (errorID == PIMR_ValidationException_ERROR_ANY_ATTR_UNKNOWN) {
    return @"Unknown AnyAttribute";
  }
  else if (errorID == PIMR_ValidationException_ERROR_ATTR_READONLY) {
    return @"Attribute is read only";
  }
  else if (errorID == PIMR_ValidationException_ERROR_KEY_ATTR_READONLY) {
    return @"Attribute is read only because ist's part of the key";
  }
  else if (errorID == PIMR_ValidationException_ERROR_TOO_BIG) {
    return @"Value is too big";
  }
  else if (errorID == PIMR_ValidationException_ERROR_TOO_SMALL) {
    return @"Value is too small";
  }
  else if (errorID == PIMR_ValidationException_ERROR_NAN_NOT_ALLOW) {
    return @"Value NAN is not allowed";
  }
  else if (errorID == PIMR_ValidationException_ERROR_TOO_SHORT) {
    return @"Value is too short";
  }
  else if (errorID == PIMR_ValidationException_ERROR_TOO_LONG) {
    return @"Value is too long";
  }
  else {
    return JreStrcat("$I", @"Unknown error id=", errorID);
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIMT_AbstractType)

J2OBJC_NAME_MAPPING(PIMT_AbstractType, "de.pidata.models.types", "PIMT_")
