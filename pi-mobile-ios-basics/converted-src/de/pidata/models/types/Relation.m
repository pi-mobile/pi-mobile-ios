//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/Relation.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/types/Relation.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/Relation must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIMT_Relation : NSObject

@end

@implementation PIMT_Relation

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPIMT_Type;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x401, -1, -1, -1, -1, -1, -1 },
    { NULL, "LIOSClass;", 0x401, -1, -1, 0, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x401, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getChildType);
  methods[1].selector = @selector(getMaxOccurs);
  methods[2].selector = @selector(getMinOccurs);
  methods[3].selector = @selector(getRelationID);
  methods[4].selector = @selector(getCollection);
  methods[5].selector = @selector(getSubstitutionGroup);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LJavaLangClassNotFoundException;" };
  static const J2ObjcClassInfo _PIMT_Relation = { "Relation", "de.pidata.models.types", ptrTable, methods, NULL, 7, 0x609, 6, 0, -1, -1, -1, -1, -1 };
  return &_PIMT_Relation;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIMT_Relation)

J2OBJC_NAME_MAPPING(PIMT_Relation, "de.pidata.models.types", "PIMT_")
