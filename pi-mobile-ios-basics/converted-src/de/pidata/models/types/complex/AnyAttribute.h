//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/complex/AnyAttribute.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTypesComplexAnyAttribute")
#ifdef RESTRICT_DePidataModelsTypesComplexAnyAttribute
#define INCLUDE_ALL_DePidataModelsTypesComplexAnyAttribute 0
#else
#define INCLUDE_ALL_DePidataModelsTypesComplexAnyAttribute 1
#endif
#undef RESTRICT_DePidataModelsTypesComplexAnyAttribute

#if !defined (PIMO_AnyAttribute_) && (INCLUDE_ALL_DePidataModelsTypesComplexAnyAttribute || defined(INCLUDE_PIMO_AnyAttribute))
#define PIMO_AnyAttribute_

#define RESTRICT_DePidataModelsTypesAbstractType 1
#define INCLUDE_PIMT_AbstractType 1
#include "de/pidata/models/types/AbstractType.h"

#define RESTRICT_DePidataModelsTypesSimpleType 1
#define INCLUDE_PIMT_SimpleType 1
#include "de/pidata/models/types/SimpleType.h"

@class IOSClass;
@class IOSObjectArray;
@class PIQ_Namespace;
@class PIQ_NamespaceTable;
@class PIQ_QName;
@protocol PIMT_Type;
@protocol PIMT_ValueEnum;

@interface PIMO_AnyAttribute : PIMT_AbstractType < PIMT_SimpleType >

#pragma mark Public

- (instancetype)initWithPIQ_NamespaceArray:(IOSObjectArray *)namespaces;

- (jboolean)allowsNamespaceWithPIQ_Namespace:(PIQ_Namespace *)ns;

- (id)createDefaultValue;

- (id)createValueWithNSString:(NSString *)stringValue
       withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaces;

+ (id<PIMT_SimpleType>)findTypeWithPIQ_QName:(PIQ_QName *)attrName;

- (id<PIMT_SimpleType>)getContentType;

- (PIQ_Namespace *)getNamespaceWithInt:(jint)index;

- (id<PIMT_SimpleType>)getRootType;

- (id<PIMT_ValueEnum>)getValueEnum;

- (jint)namespaceCount;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withIOSClass:(IOSClass *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIMO_AnyAttribute)

inline PIQ_QName *PIMO_AnyAttribute_get_ANY_ATTRIBUTE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIMO_AnyAttribute_ANY_ATTRIBUTE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIMO_AnyAttribute, ANY_ATTRIBUTE, PIQ_QName *)

inline PIMO_AnyAttribute *PIMO_AnyAttribute_get_ANY_ATTR_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIMO_AnyAttribute *PIMO_AnyAttribute_ANY_ATTR_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIMO_AnyAttribute, ANY_ATTR_TYPE, PIMO_AnyAttribute *)

FOUNDATION_EXPORT void PIMO_AnyAttribute_initWithPIQ_NamespaceArray_(PIMO_AnyAttribute *self, IOSObjectArray *namespaces);

FOUNDATION_EXPORT PIMO_AnyAttribute *new_PIMO_AnyAttribute_initWithPIQ_NamespaceArray_(IOSObjectArray *namespaces) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMO_AnyAttribute *create_PIMO_AnyAttribute_initWithPIQ_NamespaceArray_(IOSObjectArray *namespaces);

FOUNDATION_EXPORT id<PIMT_SimpleType> PIMO_AnyAttribute_findTypeWithPIQ_QName_(PIQ_QName *attrName);

J2OBJC_TYPE_LITERAL_HEADER(PIMO_AnyAttribute)

@compatibility_alias DePidataModelsTypesComplexAnyAttribute PIMO_AnyAttribute;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTypesComplexAnyAttribute")
