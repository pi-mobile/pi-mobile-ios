//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/complex/AnyElement.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTypesComplexAnyElement")
#ifdef RESTRICT_DePidataModelsTypesComplexAnyElement
#define INCLUDE_ALL_DePidataModelsTypesComplexAnyElement 0
#else
#define INCLUDE_ALL_DePidataModelsTypesComplexAnyElement 1
#endif
#undef RESTRICT_DePidataModelsTypesComplexAnyElement

#if !defined (PIMO_AnyElement_) && (INCLUDE_ALL_DePidataModelsTypesComplexAnyElement || defined(INCLUDE_PIMO_AnyElement))
#define PIMO_AnyElement_

#define RESTRICT_DePidataModelsTypesAbstractType 1
#define INCLUDE_PIMT_AbstractType 1
#include "de/pidata/models/types/AbstractType.h"

#define RESTRICT_DePidataModelsTypesComplexType 1
#define INCLUDE_PIMT_ComplexType 1
#include "de/pidata/models/types/ComplexType.h"

@class IOSClass;
@class IOSObjectArray;
@class PIMO_AnyAttribute;
@class PIQ_QName;
@protocol PIMR_ModelReference;
@protocol PIMR_QNameIterator;
@protocol PIMT_Relation;
@protocol PIMT_SimpleType;
@protocol PIMT_Type;

@interface PIMO_AnyElement : PIMT_AbstractType < PIMT_ComplexType >

#pragma mark Public

- (instancetype)init;

- (jint)allowsChildWithPIQ_QName:(PIQ_QName *)relationID
                   withPIMT_Type:(id<PIMT_Type>)childType;

- (jint)attributeCount;

- (IOSObjectArray *)createAttributeArray;

- (id<PIMT_Type>)fetchTypeWithPIQ_QName:(PIQ_QName *)relationID;

- (PIMO_AnyAttribute *)getAnyAttribute;

- (id)getAttributeDefaultWithInt:(jint)index;

- (PIQ_QName *)getAttributeNameWithInt:(jint)index;

- (id<PIMT_SimpleType>)getAttributeTypeWithInt:(jint)index;

- (id<PIMT_Type>)getChildTypeWithPIQ_QName:(PIQ_QName *)relationID;

- (id<PIMT_SimpleType>)getContentType;

- (PIQ_QName *)getKeyAttributeWithInt:(jint)i;

- (id<PIMT_SimpleType>)getKeyAttributeTypeWithInt:(jint)index;

- (jint)getKeyIndexWithPIQ_QName:(PIQ_QName *)attributeName;

- (id<PIMR_ModelReference>)getKeyReferenceWithPIQ_QName:(PIQ_QName *)refName;

- (id<PIMT_Relation>)getRelationWithPIQ_QName:(PIQ_QName *)relationID;

- (jint)indexOfAttributeWithPIQ_QName:(PIQ_QName *)attributeID;

- (jboolean)isAbstract;

- (jboolean)isAssignableFromWithPIMT_Type:(id<PIMT_Type>)childType;

- (jboolean)isMixed;

- (jint)keyAttributeCount;

- (id<PIMR_QNameIterator>)keyRefNames;

- (jint)relationCount;

- (id<PIMR_QNameIterator>)relationNames;

- (void)setKeyAttributeWithInt:(jint)index
                 withPIQ_QName:(PIQ_QName *)attributeName;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withIOSClass:(IOSClass *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIMO_AnyElement)

inline PIQ_QName *PIMO_AnyElement_get_ANY_ELEMENT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIMO_AnyElement_ANY_ELEMENT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIMO_AnyElement, ANY_ELEMENT, PIQ_QName *)

inline PIMO_AnyElement *PIMO_AnyElement_get_ANY_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIMO_AnyElement *PIMO_AnyElement_ANY_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIMO_AnyElement, ANY_TYPE, PIMO_AnyElement *)

FOUNDATION_EXPORT void PIMO_AnyElement_init(PIMO_AnyElement *self);

FOUNDATION_EXPORT PIMO_AnyElement *new_PIMO_AnyElement_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMO_AnyElement *create_PIMO_AnyElement_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIMO_AnyElement)

@compatibility_alias DePidataModelsTypesComplexAnyElement PIMO_AnyElement;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTypesComplexAnyElement")
