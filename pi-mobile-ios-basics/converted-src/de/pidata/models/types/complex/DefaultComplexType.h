//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/complex/DefaultComplexType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTypesComplexDefaultComplexType")
#ifdef RESTRICT_DePidataModelsTypesComplexDefaultComplexType
#define INCLUDE_ALL_DePidataModelsTypesComplexDefaultComplexType 0
#else
#define INCLUDE_ALL_DePidataModelsTypesComplexDefaultComplexType 1
#endif
#undef RESTRICT_DePidataModelsTypesComplexDefaultComplexType

#if !defined (PIMO_DefaultComplexType_) && (INCLUDE_ALL_DePidataModelsTypesComplexDefaultComplexType || defined(INCLUDE_PIMO_DefaultComplexType))
#define PIMO_DefaultComplexType_

#define RESTRICT_DePidataModelsTypesAbstractType 1
#define INCLUDE_PIMT_AbstractType 1
#include "de/pidata/models/types/AbstractType.h"

#define RESTRICT_DePidataModelsTypesComplexType 1
#define INCLUDE_PIMT_ComplexType 1
#include "de/pidata/models/types/ComplexType.h"

@class IOSClass;
@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMO_AnyAttribute;
@class PIQ_QName;
@protocol PIMR_ModelReference;
@protocol PIMR_QNameIterator;
@protocol PIMT_Relation;
@protocol PIMT_SimpleType;
@protocol PIMT_Type;

@interface PIMO_DefaultComplexType : PIMT_AbstractType < PIMT_ComplexType > {
 @public
  JavaUtilHashtable *childRelations_;
}

#pragma mark Public

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)modelClassName
                          withInt:(jint)keyAttributeCount;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)modelClassName
                          withInt:(jint)additionalKeyAttributeCount
                    withPIMT_Type:(id<PIMT_Type>)parentType;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)modelClassName
                          withInt:(jint)additionalKeyAttributeCount
                    withPIMT_Type:(id<PIMT_Type>)parentType
                      withBoolean:(jboolean)abstractType
                      withBoolean:(jboolean)mixedType;

- (void)addAttributeTypeWithPIQ_QName:(PIQ_QName *)attributeName
                  withPIMT_SimpleType:(id<PIMT_SimpleType>)attributeType;

- (void)addAttributeTypeWithPIQ_QName:(PIQ_QName *)attributeName
                  withPIMT_SimpleType:(id<PIMT_SimpleType>)attributeType
                               withId:(id)attrDefault;

- (void)addKeyAttributeTypeWithInt:(jint)keyIndex
                     withPIQ_QName:(PIQ_QName *)attributeName
               withPIMT_SimpleType:(id<PIMT_SimpleType>)attributeType;

- (void)addKeyAttributeTypeWithInt:(jint)keyIndex
                     withPIQ_QName:(PIQ_QName *)attributeName
               withPIMT_SimpleType:(id<PIMT_SimpleType>)attributeType
                            withId:(id)attrDefault;

- (void)addKeyReferenceWithPIMR_ModelReference:(id<PIMR_ModelReference>)reference;

- (void)addKeyReferenceWithPIQ_QName:(PIQ_QName *)name
                       withPIQ_QName:(PIQ_QName *)refTypeName
                       withPIQ_QName:(PIQ_QName *)refAttribute;

- (void)addRelationWithPIQ_QName:(PIQ_QName *)relationID
                   withPIMT_Type:(id<PIMT_Type>)childType
                         withInt:(jint)minOccurs
                         withInt:(jint)maxOccurs;

- (void)addRelationWithPIQ_QName:(PIQ_QName *)relationID
                   withPIMT_Type:(id<PIMT_Type>)childType
                         withInt:(jint)minOccurs
                         withInt:(jint)maxOccurs
                    withIOSClass:(IOSClass *)collection;

- (void)addRelationWithPIQ_QName:(PIQ_QName *)relationID
                   withPIMT_Type:(id<PIMT_Type>)childType
                         withInt:(jint)minOccurs
                         withInt:(jint)maxOccurs
                    withIOSClass:(IOSClass *)collection
                   withPIQ_QName:(PIQ_QName *)substitutionGroup;

- (void)addRelationWithPIMT_Relation:(id<PIMT_Relation>)relation;

- (jint)allowsChildWithPIQ_QName:(PIQ_QName *)relationID
                   withPIMT_Type:(id<PIMT_Type>)childType;

- (jint)attributeCount;

- (IOSObjectArray *)createAttributeArray;

- (PIMO_AnyAttribute *)getAnyAttribute;

- (id)getAttributeDefaultWithInt:(jint)index;

- (PIQ_QName *)getAttributeNameWithInt:(jint)index;

- (id<PIMT_SimpleType>)getAttributeTypeWithInt:(jint)index;

- (id<PIMT_SimpleType>)getAttributeTypeWithPIQ_QName:(PIQ_QName *)attributeID;

- (id<PIMT_Type>)getChildTypeWithPIQ_QName:(PIQ_QName *)relationID;

- (id<PIMT_SimpleType>)getContentType;

- (PIQ_QName *)getKeyAttributeWithInt:(jint)i;

- (id<PIMT_SimpleType>)getKeyAttributeTypeWithInt:(jint)index;

- (jint)getKeyIndexWithPIQ_QName:(PIQ_QName *)attributeName;

- (id<PIMR_ModelReference>)getKeyReferenceWithPIQ_QName:(PIQ_QName *)refName;

- (id<PIMT_Relation>)getRelationWithPIQ_QName:(PIQ_QName *)relationID;

- (jint)indexOfAttributeWithPIQ_QName:(PIQ_QName *)attributeID;

- (jboolean)isAbstract;

- (jboolean)isMixed;

- (jint)keyAttributeCount;

- (id<PIMR_QNameIterator>)keyRefNames;

- (jint)relationCount;

- (id<PIMR_QNameIterator>)relationNames;

- (void)setAnyAttributeWithPIMO_AnyAttribute:(PIMO_AnyAttribute *)anyAttribute;

- (void)setKeyAttributeWithInt:(jint)index
                 withPIQ_QName:(PIQ_QName *)attributeName;

#pragma mark Protected

- (void)setContentTypeWithPIMT_SimpleType:(id<PIMT_SimpleType>)contentType;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withIOSClass:(IOSClass *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIMO_DefaultComplexType)

J2OBJC_FIELD_SETTER(PIMO_DefaultComplexType, childRelations_, JavaUtilHashtable *)

FOUNDATION_EXPORT void PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_(PIMO_DefaultComplexType *self, PIQ_QName *typeID, NSString *modelClassName, jint keyAttributeCount);

FOUNDATION_EXPORT PIMO_DefaultComplexType *new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_(PIQ_QName *typeID, NSString *modelClassName, jint keyAttributeCount) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMO_DefaultComplexType *create_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_(PIQ_QName *typeID, NSString *modelClassName, jint keyAttributeCount);

FOUNDATION_EXPORT void PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_(PIMO_DefaultComplexType *self, PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType);

FOUNDATION_EXPORT PIMO_DefaultComplexType *new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_(PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMO_DefaultComplexType *create_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_(PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType);

FOUNDATION_EXPORT void PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_withBoolean_withBoolean_(PIMO_DefaultComplexType *self, PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType, jboolean abstractType, jboolean mixedType);

FOUNDATION_EXPORT PIMO_DefaultComplexType *new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_withBoolean_withBoolean_(PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType, jboolean abstractType, jboolean mixedType) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIMO_DefaultComplexType *create_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_withBoolean_withBoolean_(PIQ_QName *typeID, NSString *modelClassName, jint additionalKeyAttributeCount, id<PIMT_Type> parentType, jboolean abstractType, jboolean mixedType);

J2OBJC_TYPE_LITERAL_HEADER(PIMO_DefaultComplexType)

@compatibility_alias DePidataModelsTypesComplexDefaultComplexType PIMO_DefaultComplexType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTypesComplexDefaultComplexType")
