//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/BinaryType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTypesSimpleBinaryType")
#ifdef RESTRICT_DePidataModelsTypesSimpleBinaryType
#define INCLUDE_ALL_DePidataModelsTypesSimpleBinaryType 0
#else
#define INCLUDE_ALL_DePidataModelsTypesSimpleBinaryType 1
#endif
#undef RESTRICT_DePidataModelsTypesSimpleBinaryType

#if !defined (PIME_BinaryType_) && (INCLUDE_ALL_DePidataModelsTypesSimpleBinaryType || defined(INCLUDE_PIME_BinaryType))
#define PIME_BinaryType_

#define RESTRICT_DePidataModelsTypesAbstractType 1
#define INCLUDE_PIMT_AbstractType 1
#include "de/pidata/models/types/AbstractType.h"

#define RESTRICT_DePidataModelsTypesSimpleType 1
#define INCLUDE_PIMT_SimpleType 1
#include "de/pidata/models/types/SimpleType.h"

@class IOSByteArray;
@class IOSClass;
@class PIQ_NamespaceTable;
@class PIQ_QName;
@protocol PIMT_Type;
@protocol PIMT_ValueEnum;

@interface PIME_BinaryType : PIMT_AbstractType < PIMT_SimpleType >

#pragma mark Public

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)className_;

- (id)createDefaultValue;

- (id)createValueWithNSString:(NSString *)stringValue
       withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaces;

+ (IOSByteArray *)decodeBase64WithByteArray:(IOSByteArray *)base64Data;

+ (IOSByteArray *)encodeBase64WithByteArray:(IOSByteArray *)binaryData
                                    withInt:(jint)length;

+ (IOSByteArray *)encodeBase64WithByteArray:(IOSByteArray *)binaryData
                                    withInt:(jint)length
                                withBoolean:(jboolean)isChunked;

+ (IOSByteArray *)encodeBase64ChunkedWithByteArray:(IOSByteArray *)binaryData
                                           withInt:(jint)length;

- (id<PIMT_SimpleType>)getContentType;

- (id<PIMT_SimpleType>)getRootType;

- (id<PIMT_ValueEnum>)getValueEnum;

+ (jboolean)isArrayByteBase64WithByteArray:(IOSByteArray *)arrayOctect;

+ (NSString *)toBase64StringWithByteArray:(IOSByteArray *)data
                                  withInt:(jint)length;

#pragma mark Package-Private

+ (IOSByteArray *)discardNonBase64WithByteArray:(IOSByteArray *)data;

+ (IOSByteArray *)discardWhitespaceWithByteArray:(IOSByteArray *)data;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withIOSClass:(IOSClass *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIME_BinaryType)

inline PIQ_QName *PIME_BinaryType_get_TYPE_BASE64BINARY(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIME_BinaryType_TYPE_BASE64BINARY;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIME_BinaryType, TYPE_BASE64BINARY, PIQ_QName *)

inline PIME_BinaryType *PIME_BinaryType_get_base64Binary(void);
inline PIME_BinaryType *PIME_BinaryType_set_base64Binary(PIME_BinaryType *value);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIME_BinaryType *PIME_BinaryType_base64Binary;
J2OBJC_STATIC_FIELD_OBJ(PIME_BinaryType, base64Binary, PIME_BinaryType *)

inline jint PIME_BinaryType_get_CHUNK_SIZE(void);
#define PIME_BinaryType_CHUNK_SIZE 76
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, CHUNK_SIZE, jint)

inline IOSByteArray *PIME_BinaryType_get_CHUNK_SEPARATOR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_CHUNK_SEPARATOR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIME_BinaryType, CHUNK_SEPARATOR, IOSByteArray *)

inline jint PIME_BinaryType_get_BASELENGTH(void);
#define PIME_BinaryType_BASELENGTH 255
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, BASELENGTH, jint)

inline jint PIME_BinaryType_get_LOOKUPLENGTH(void);
#define PIME_BinaryType_LOOKUPLENGTH 64
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, LOOKUPLENGTH, jint)

inline jint PIME_BinaryType_get_EIGHTBIT(void);
#define PIME_BinaryType_EIGHTBIT 8
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, EIGHTBIT, jint)

inline jint PIME_BinaryType_get_SIXTEENBIT(void);
#define PIME_BinaryType_SIXTEENBIT 16
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, SIXTEENBIT, jint)

inline jint PIME_BinaryType_get_TWENTYFOURBITGROUP(void);
#define PIME_BinaryType_TWENTYFOURBITGROUP 24
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, TWENTYFOURBITGROUP, jint)

inline jint PIME_BinaryType_get_FOURBYTE(void);
#define PIME_BinaryType_FOURBYTE 4
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, FOURBYTE, jint)

inline jint PIME_BinaryType_get_SIGN(void);
#define PIME_BinaryType_SIGN -128
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, SIGN, jint)

inline jbyte PIME_BinaryType_get_PAD(void);
#define PIME_BinaryType_PAD 61
J2OBJC_STATIC_FIELD_CONSTANT(PIME_BinaryType, PAD, jbyte)

FOUNDATION_EXPORT jboolean PIME_BinaryType_isArrayByteBase64WithByteArray_(IOSByteArray *arrayOctect);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_encodeBase64WithByteArray_withInt_(IOSByteArray *binaryData, jint length);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_encodeBase64ChunkedWithByteArray_withInt_(IOSByteArray *binaryData, jint length);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_encodeBase64WithByteArray_withInt_withBoolean_(IOSByteArray *binaryData, jint length, jboolean isChunked);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_decodeBase64WithByteArray_(IOSByteArray *base64Data);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_discardWhitespaceWithByteArray_(IOSByteArray *data);

FOUNDATION_EXPORT IOSByteArray *PIME_BinaryType_discardNonBase64WithByteArray_(IOSByteArray *data);

FOUNDATION_EXPORT void PIME_BinaryType_initWithPIQ_QName_withNSString_(PIME_BinaryType *self, PIQ_QName *typeID, NSString *className_);

FOUNDATION_EXPORT PIME_BinaryType *new_PIME_BinaryType_initWithPIQ_QName_withNSString_(PIQ_QName *typeID, NSString *className_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIME_BinaryType *create_PIME_BinaryType_initWithPIQ_QName_withNSString_(PIQ_QName *typeID, NSString *className_);

FOUNDATION_EXPORT NSString *PIME_BinaryType_toBase64StringWithByteArray_withInt_(IOSByteArray *data, jint length);

J2OBJC_TYPE_LITERAL_HEADER(PIME_BinaryType)

@compatibility_alias DePidataModelsTypesSimpleBinaryType PIME_BinaryType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTypesSimpleBinaryType")
