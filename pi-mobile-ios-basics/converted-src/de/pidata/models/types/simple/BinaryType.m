//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/BinaryType.java
//

#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/types/AbstractType.h"
#include "de/pidata/models/types/SimpleType.h"
#include "de/pidata/models/types/ValueEnum.h"
#include "de/pidata/models/types/simple/AbstractSimpleType.h"
#include "de/pidata/models/types/simple/BinaryBytes.h"
#include "de/pidata/models/types/simple/BinaryType.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/System.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/simple/BinaryType must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIME_BinaryType ()

+ (jboolean)isBase64WithByte:(jbyte)octect;

@end

inline IOSByteArray *PIME_BinaryType_get_base64Alphabet(void);
inline IOSByteArray *PIME_BinaryType_set_base64Alphabet(IOSByteArray *value);
static IOSByteArray *PIME_BinaryType_base64Alphabet;
J2OBJC_STATIC_FIELD_OBJ(PIME_BinaryType, base64Alphabet, IOSByteArray *)

inline IOSByteArray *PIME_BinaryType_get_lookUpBase64Alphabet(void);
inline IOSByteArray *PIME_BinaryType_set_lookUpBase64Alphabet(IOSByteArray *value);
static IOSByteArray *PIME_BinaryType_lookUpBase64Alphabet;
J2OBJC_STATIC_FIELD_OBJ(PIME_BinaryType, lookUpBase64Alphabet, IOSByteArray *)

__attribute__((unused)) static jboolean PIME_BinaryType_isBase64WithByte_(jbyte octect);

J2OBJC_INITIALIZED_DEFN(PIME_BinaryType)

PIQ_QName *PIME_BinaryType_TYPE_BASE64BINARY;
PIME_BinaryType *PIME_BinaryType_base64Binary;
IOSByteArray *PIME_BinaryType_CHUNK_SEPARATOR;

@implementation PIME_BinaryType

+ (jboolean)isBase64WithByte:(jbyte)octect {
  return PIME_BinaryType_isBase64WithByte_(octect);
}

+ (jboolean)isArrayByteBase64WithByteArray:(IOSByteArray *)arrayOctect {
  return PIME_BinaryType_isArrayByteBase64WithByteArray_(arrayOctect);
}

+ (IOSByteArray *)encodeBase64WithByteArray:(IOSByteArray *)binaryData
                                    withInt:(jint)length {
  return PIME_BinaryType_encodeBase64WithByteArray_withInt_(binaryData, length);
}

+ (IOSByteArray *)encodeBase64ChunkedWithByteArray:(IOSByteArray *)binaryData
                                           withInt:(jint)length {
  return PIME_BinaryType_encodeBase64ChunkedWithByteArray_withInt_(binaryData, length);
}

+ (IOSByteArray *)encodeBase64WithByteArray:(IOSByteArray *)binaryData
                                    withInt:(jint)length
                                withBoolean:(jboolean)isChunked {
  return PIME_BinaryType_encodeBase64WithByteArray_withInt_withBoolean_(binaryData, length, isChunked);
}

+ (IOSByteArray *)decodeBase64WithByteArray:(IOSByteArray *)base64Data {
  return PIME_BinaryType_decodeBase64WithByteArray_(base64Data);
}

+ (IOSByteArray *)discardWhitespaceWithByteArray:(IOSByteArray *)data {
  return PIME_BinaryType_discardWhitespaceWithByteArray_(data);
}

+ (IOSByteArray *)discardNonBase64WithByteArray:(IOSByteArray *)data {
  return PIME_BinaryType_discardNonBase64WithByteArray_(data);
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withNSString:(NSString *)className_ {
  PIME_BinaryType_initWithPIQ_QName_withNSString_(self, typeID, className_);
  return self;
}

- (id)createDefaultValue {
  return nil;
}

- (id)createValueWithNSString:(NSString *)stringValue
       withPIQ_NamespaceTable:(PIQ_NamespaceTable *)namespaces {
  if ((stringValue == nil) || ([((NSString *) nil_chk(stringValue)) java_length] == 0)) {
    return nil;
  }
  else {
    IOSByteArray *data = PIME_BinaryType_decodeBase64WithByteArray_([((NSString *) nil_chk(stringValue)) java_getBytes]);
    return new_PIME_BinaryBytes_initWithByteArray_(data);
  }
}

- (id<PIMT_SimpleType>)getRootType {
  return PIME_StringType_getDefString();
}

- (id<PIMT_SimpleType>)getContentType {
  return self;
}

- (id<PIMT_ValueEnum>)getValueEnum {
  return nil;
}

+ (NSString *)toBase64StringWithByteArray:(IOSByteArray *)data
                                  withInt:(jint)length {
  return PIME_BinaryType_toBase64StringWithByteArray_withInt_(data, length);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "Z", 0xa, 0, 1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 2, 3, -1, -1, -1, -1 },
    { NULL, "[B", 0x9, 4, 5, -1, -1, -1, -1 },
    { NULL, "[B", 0x9, 6, 5, -1, -1, -1, -1 },
    { NULL, "[B", 0x9, 4, 7, -1, -1, -1, -1 },
    { NULL, "[B", 0x9, 8, 3, -1, -1, -1, -1 },
    { NULL, "[B", 0x8, 9, 3, -1, -1, -1, -1 },
    { NULL, "[B", 0x8, 10, 3, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 11, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "LPIMT_SimpleType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_SimpleType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_ValueEnum;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 14, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(isBase64WithByte:);
  methods[1].selector = @selector(isArrayByteBase64WithByteArray:);
  methods[2].selector = @selector(encodeBase64WithByteArray:withInt:);
  methods[3].selector = @selector(encodeBase64ChunkedWithByteArray:withInt:);
  methods[4].selector = @selector(encodeBase64WithByteArray:withInt:withBoolean:);
  methods[5].selector = @selector(decodeBase64WithByteArray:);
  methods[6].selector = @selector(discardWhitespaceWithByteArray:);
  methods[7].selector = @selector(discardNonBase64WithByteArray:);
  methods[8].selector = @selector(initWithPIQ_QName:withNSString:);
  methods[9].selector = @selector(createDefaultValue);
  methods[10].selector = @selector(createValueWithNSString:withPIQ_NamespaceTable:);
  methods[11].selector = @selector(getRootType);
  methods[12].selector = @selector(getContentType);
  methods[13].selector = @selector(getValueEnum);
  methods[14].selector = @selector(toBase64StringWithByteArray:withInt:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "TYPE_BASE64BINARY", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "base64Binary", "LPIME_BinaryType;", .constantValue.asLong = 0, 0x9, -1, 16, -1, -1 },
    { "CHUNK_SIZE", "I", .constantValue.asInt = PIME_BinaryType_CHUNK_SIZE, 0x18, -1, -1, -1, -1 },
    { "CHUNK_SEPARATOR", "[B", .constantValue.asLong = 0, 0x18, -1, 17, -1, -1 },
    { "BASELENGTH", "I", .constantValue.asInt = PIME_BinaryType_BASELENGTH, 0x18, -1, -1, -1, -1 },
    { "LOOKUPLENGTH", "I", .constantValue.asInt = PIME_BinaryType_LOOKUPLENGTH, 0x18, -1, -1, -1, -1 },
    { "EIGHTBIT", "I", .constantValue.asInt = PIME_BinaryType_EIGHTBIT, 0x18, -1, -1, -1, -1 },
    { "SIXTEENBIT", "I", .constantValue.asInt = PIME_BinaryType_SIXTEENBIT, 0x18, -1, -1, -1, -1 },
    { "TWENTYFOURBITGROUP", "I", .constantValue.asInt = PIME_BinaryType_TWENTYFOURBITGROUP, 0x18, -1, -1, -1, -1 },
    { "FOURBYTE", "I", .constantValue.asInt = PIME_BinaryType_FOURBYTE, 0x18, -1, -1, -1, -1 },
    { "SIGN", "I", .constantValue.asInt = PIME_BinaryType_SIGN, 0x18, -1, -1, -1, -1 },
    { "PAD", "B", .constantValue.asChar = PIME_BinaryType_PAD, 0x18, -1, -1, -1, -1 },
    { "base64Alphabet", "[B", .constantValue.asLong = 0, 0xa, -1, 18, -1, -1 },
    { "lookUpBase64Alphabet", "[B", .constantValue.asLong = 0, 0xa, -1, 19, -1, -1 },
  };
  static const void *ptrTable[] = { "isBase64", "B", "isArrayByteBase64", "[B", "encodeBase64", "[BI", "encodeBase64Chunked", "[BIZ", "decodeBase64", "discardWhitespace", "discardNonBase64", "LPIQ_QName;LNSString;", "createValue", "LNSString;LPIQ_NamespaceTable;", "toBase64String", &PIME_BinaryType_TYPE_BASE64BINARY, &PIME_BinaryType_base64Binary, &PIME_BinaryType_CHUNK_SEPARATOR, &PIME_BinaryType_base64Alphabet, &PIME_BinaryType_lookUpBase64Alphabet };
  static const J2ObjcClassInfo _PIME_BinaryType = { "BinaryType", "de.pidata.models.types.simple", ptrTable, methods, fields, 7, 0x1, 15, 14, -1, -1, -1, -1, -1 };
  return &_PIME_BinaryType;
}

+ (void)initialize {
  if (self == [PIME_BinaryType class]) {
    PIME_BinaryType_TYPE_BASE64BINARY = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIME_AbstractSimpleType, NAMESPACE_SCHEMA))) getQNameWithNSString:@"base64Binary"];
    PIME_BinaryType_base64Binary = new_PIME_BinaryType_initWithPIQ_QName_withNSString_(PIME_BinaryType_TYPE_BASE64BINARY, @"de.pidata.models.types.simple.Binary");
    PIME_BinaryType_CHUNK_SEPARATOR = [@"\x0d\n" java_getBytes];
    PIME_BinaryType_base64Alphabet = [IOSByteArray newArrayWithLength:PIME_BinaryType_BASELENGTH];
    PIME_BinaryType_lookUpBase64Alphabet = [IOSByteArray newArrayWithLength:PIME_BinaryType_LOOKUPLENGTH];
    {
      for (jint i = 0; i < PIME_BinaryType_BASELENGTH; i++) {
        *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, i) = (jbyte) -1;
      }
      for (jint i = 'Z'; i >= 'A'; i--) {
        *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, i) = (jbyte) (i - 'A');
      }
      for (jint i = 'z'; i >= 'a'; i--) {
        *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, i) = (jbyte) (i - 'a' + 26);
      }
      for (jint i = '9'; i >= '0'; i--) {
        *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, i) = (jbyte) (i - '0' + 52);
      }
      *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, '+') = 62;
      *IOSByteArray_GetRef(PIME_BinaryType_base64Alphabet, '/') = 63;
      for (jint i = 0; i <= 25; i++) {
        *IOSByteArray_GetRef(PIME_BinaryType_lookUpBase64Alphabet, i) = (jbyte) ('A' + i);
      }
      for (jint i = 26, j = 0; i <= 51; i++, j++) {
        *IOSByteArray_GetRef(PIME_BinaryType_lookUpBase64Alphabet, i) = (jbyte) ('a' + j);
      }
      for (jint i = 52, j = 0; i <= 61; i++, j++) {
        *IOSByteArray_GetRef(PIME_BinaryType_lookUpBase64Alphabet, i) = (jbyte) ('0' + j);
      }
      *IOSByteArray_GetRef(PIME_BinaryType_lookUpBase64Alphabet, 62) = (jbyte) '+';
      *IOSByteArray_GetRef(PIME_BinaryType_lookUpBase64Alphabet, 63) = (jbyte) '/';
    }
    J2OBJC_SET_INITIALIZED(PIME_BinaryType)
  }
}

@end

jboolean PIME_BinaryType_isBase64WithByte_(jbyte octect) {
  PIME_BinaryType_initialize();
  if (octect == PIME_BinaryType_PAD) {
    return true;
  }
  else if (IOSByteArray_Get(nil_chk(PIME_BinaryType_base64Alphabet), octect) == -1) {
    return false;
  }
  else {
    return true;
  }
}

jboolean PIME_BinaryType_isArrayByteBase64WithByteArray_(IOSByteArray *arrayOctect) {
  PIME_BinaryType_initialize();
  arrayOctect = PIME_BinaryType_discardWhitespaceWithByteArray_(arrayOctect);
  jint length = ((IOSByteArray *) nil_chk(arrayOctect))->size_;
  if (length == 0) {
    return true;
  }
  for (jint i = 0; i < length; i++) {
    if (!PIME_BinaryType_isBase64WithByte_(IOSByteArray_Get(arrayOctect, i))) {
      return false;
    }
  }
  return true;
}

IOSByteArray *PIME_BinaryType_encodeBase64WithByteArray_withInt_(IOSByteArray *binaryData, jint length) {
  PIME_BinaryType_initialize();
  return PIME_BinaryType_encodeBase64WithByteArray_withInt_withBoolean_(binaryData, length, false);
}

IOSByteArray *PIME_BinaryType_encodeBase64ChunkedWithByteArray_withInt_(IOSByteArray *binaryData, jint length) {
  PIME_BinaryType_initialize();
  return PIME_BinaryType_encodeBase64WithByteArray_withInt_withBoolean_(binaryData, length, true);
}

IOSByteArray *PIME_BinaryType_encodeBase64WithByteArray_withInt_withBoolean_(IOSByteArray *binaryData, jint length, jboolean isChunked) {
  PIME_BinaryType_initialize();
  jint lengthDataBits = length * PIME_BinaryType_EIGHTBIT;
  jint fewerThan24bits = JreIntMod(lengthDataBits, PIME_BinaryType_TWENTYFOURBITGROUP);
  jint numberTriplets = JreIntDiv(lengthDataBits, PIME_BinaryType_TWENTYFOURBITGROUP);
  IOSByteArray *encodedData = nil;
  jint encodedDataLength = 0;
  jint nbrChunks = 0;
  if (fewerThan24bits != 0) {
    encodedDataLength = (numberTriplets + 1) * 4;
  }
  else {
    encodedDataLength = numberTriplets * 4;
  }
  if (isChunked) {
    if (((IOSByteArray *) nil_chk(PIME_BinaryType_CHUNK_SEPARATOR))->size_ == 0) {
      nbrChunks = 0;
    }
    else {
      nbrChunks = encodedDataLength + JreIntDiv((PIME_BinaryType_CHUNK_SIZE - 1), PIME_BinaryType_CHUNK_SIZE);
    }
    encodedDataLength += nbrChunks * PIME_BinaryType_CHUNK_SEPARATOR->size_;
  }
  encodedData = [IOSByteArray newArrayWithLength:encodedDataLength];
  jbyte k = 0;
  jbyte l = 0;
  jbyte b1 = 0;
  jbyte b2 = 0;
  jbyte b3 = 0;
  jint encodedIndex = 0;
  jint dataIndex = 0;
  jint i = 0;
  jint nextSeparatorIndex = PIME_BinaryType_CHUNK_SIZE;
  jint chunksSoFar = 0;
  for (i = 0; i < numberTriplets; i++) {
    dataIndex = i * 3;
    b1 = IOSByteArray_Get(nil_chk(binaryData), dataIndex);
    b2 = IOSByteArray_Get(binaryData, dataIndex + 1);
    b3 = IOSByteArray_Get(binaryData, dataIndex + 2);
    l = (jbyte) (b2 & (jint) 0x0f);
    k = (jbyte) (b1 & (jint) 0x03);
    jbyte val1 = ((b1 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b1, 2)) : (jbyte) (JreRShift32((b1), 2) ^ (jint) 0xc0);
    jbyte val2 = ((b2 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b2, 4)) : (jbyte) (JreRShift32((b2), 4) ^ (jint) 0xf0);
    jbyte val3 = ((b3 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b3, 6)) : (jbyte) (JreRShift32((b3), 6) ^ (jint) 0xfc);
    *IOSByteArray_GetRef(encodedData, encodedIndex) = IOSByteArray_Get(nil_chk(PIME_BinaryType_lookUpBase64Alphabet), val1);
    *IOSByteArray_GetRef(encodedData, encodedIndex + 1) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, val2 | (JreLShift32(k, 4)));
    *IOSByteArray_GetRef(encodedData, encodedIndex + 2) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, (JreLShift32(l, 2)) | val3);
    *IOSByteArray_GetRef(encodedData, encodedIndex + 3) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, b3 & (jint) 0x3f);
    encodedIndex += 4;
    if (isChunked) {
      if (encodedIndex == nextSeparatorIndex) {
        JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(PIME_BinaryType_CHUNK_SEPARATOR, 0, encodedData, encodedIndex, ((IOSByteArray *) nil_chk(PIME_BinaryType_CHUNK_SEPARATOR))->size_);
        chunksSoFar++;
        nextSeparatorIndex = (PIME_BinaryType_CHUNK_SIZE * (chunksSoFar + 1)) + (chunksSoFar * PIME_BinaryType_CHUNK_SEPARATOR->size_);
        encodedIndex += PIME_BinaryType_CHUNK_SEPARATOR->size_;
      }
    }
  }
  dataIndex = i * 3;
  if (fewerThan24bits == PIME_BinaryType_EIGHTBIT) {
    b1 = IOSByteArray_Get(nil_chk(binaryData), dataIndex);
    k = (jbyte) (b1 & (jint) 0x03);
    jbyte val1 = ((b1 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b1, 2)) : (jbyte) (JreRShift32((b1), 2) ^ (jint) 0xc0);
    *IOSByteArray_GetRef(encodedData, encodedIndex) = IOSByteArray_Get(nil_chk(PIME_BinaryType_lookUpBase64Alphabet), val1);
    *IOSByteArray_GetRef(encodedData, encodedIndex + 1) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, JreLShift32(k, 4));
    *IOSByteArray_GetRef(encodedData, encodedIndex + 2) = PIME_BinaryType_PAD;
    *IOSByteArray_GetRef(encodedData, encodedIndex + 3) = PIME_BinaryType_PAD;
  }
  else if (fewerThan24bits == PIME_BinaryType_SIXTEENBIT) {
    b1 = IOSByteArray_Get(nil_chk(binaryData), dataIndex);
    b2 = IOSByteArray_Get(binaryData, dataIndex + 1);
    l = (jbyte) (b2 & (jint) 0x0f);
    k = (jbyte) (b1 & (jint) 0x03);
    jbyte val1 = ((b1 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b1, 2)) : (jbyte) (JreRShift32((b1), 2) ^ (jint) 0xc0);
    jbyte val2 = ((b2 & PIME_BinaryType_SIGN) == 0) ? (jbyte) (JreRShift32(b2, 4)) : (jbyte) (JreRShift32((b2), 4) ^ (jint) 0xf0);
    *IOSByteArray_GetRef(encodedData, encodedIndex) = IOSByteArray_Get(nil_chk(PIME_BinaryType_lookUpBase64Alphabet), val1);
    *IOSByteArray_GetRef(encodedData, encodedIndex + 1) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, val2 | (JreLShift32(k, 4)));
    *IOSByteArray_GetRef(encodedData, encodedIndex + 2) = IOSByteArray_Get(PIME_BinaryType_lookUpBase64Alphabet, JreLShift32(l, 2));
    *IOSByteArray_GetRef(encodedData, encodedIndex + 3) = PIME_BinaryType_PAD;
  }
  if (isChunked) {
    if (chunksSoFar < nbrChunks) {
      JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(PIME_BinaryType_CHUNK_SEPARATOR, 0, encodedData, encodedDataLength - ((IOSByteArray *) nil_chk(PIME_BinaryType_CHUNK_SEPARATOR))->size_, PIME_BinaryType_CHUNK_SEPARATOR->size_);
    }
  }
  return encodedData;
}

IOSByteArray *PIME_BinaryType_decodeBase64WithByteArray_(IOSByteArray *base64Data) {
  PIME_BinaryType_initialize();
  base64Data = PIME_BinaryType_discardNonBase64WithByteArray_(base64Data);
  if (((IOSByteArray *) nil_chk(base64Data))->size_ == 0) {
    return [IOSByteArray newArrayWithLength:0];
  }
  jint numberQuadruple = JreIntDiv(base64Data->size_, PIME_BinaryType_FOURBYTE);
  IOSByteArray *decodedData = nil;
  jbyte b1 = 0;
  jbyte b2 = 0;
  jbyte b3 = 0;
  jbyte b4 = 0;
  jbyte marker0 = 0;
  jbyte marker1 = 0;
  jint encodedIndex = 0;
  jint dataIndex = 0;
  {
    jint lastData = base64Data->size_;
    while (IOSByteArray_Get(base64Data, lastData - 1) == PIME_BinaryType_PAD) {
      if (--lastData == 0) {
        return [IOSByteArray newArrayWithLength:0];
      }
    }
    decodedData = [IOSByteArray newArrayWithLength:lastData - numberQuadruple];
  }
  for (jint i = 0; i < numberQuadruple; i++) {
    dataIndex = i * 4;
    marker0 = IOSByteArray_Get(base64Data, dataIndex + 2);
    marker1 = IOSByteArray_Get(base64Data, dataIndex + 3);
    b1 = IOSByteArray_Get(nil_chk(PIME_BinaryType_base64Alphabet), IOSByteArray_Get(base64Data, dataIndex));
    b2 = IOSByteArray_Get(PIME_BinaryType_base64Alphabet, IOSByteArray_Get(base64Data, dataIndex + 1));
    if (marker0 != PIME_BinaryType_PAD && marker1 != PIME_BinaryType_PAD) {
      b3 = IOSByteArray_Get(PIME_BinaryType_base64Alphabet, marker0);
      b4 = IOSByteArray_Get(PIME_BinaryType_base64Alphabet, marker1);
      *IOSByteArray_GetRef(decodedData, encodedIndex) = (jbyte) ((JreLShift32(b1, 2)) | (JreRShift32(b2, 4)));
      *IOSByteArray_GetRef(decodedData, encodedIndex + 1) = (jbyte) ((JreLShift32((b2 & (jint) 0xf), 4)) | ((JreRShift32(b3, 2)) & (jint) 0xf));
      *IOSByteArray_GetRef(decodedData, encodedIndex + 2) = (jbyte) ((JreLShift32(b3, 6)) | b4);
    }
    else if (marker0 == PIME_BinaryType_PAD) {
      *IOSByteArray_GetRef(decodedData, encodedIndex) = (jbyte) ((JreLShift32(b1, 2)) | (JreRShift32(b2, 4)));
    }
    else if (marker1 == PIME_BinaryType_PAD) {
      b3 = IOSByteArray_Get(PIME_BinaryType_base64Alphabet, marker0);
      *IOSByteArray_GetRef(decodedData, encodedIndex) = (jbyte) ((JreLShift32(b1, 2)) | (JreRShift32(b2, 4)));
      *IOSByteArray_GetRef(decodedData, encodedIndex + 1) = (jbyte) ((JreLShift32((b2 & (jint) 0xf), 4)) | ((JreRShift32(b3, 2)) & (jint) 0xf));
    }
    encodedIndex += 3;
  }
  return decodedData;
}

IOSByteArray *PIME_BinaryType_discardWhitespaceWithByteArray_(IOSByteArray *data) {
  PIME_BinaryType_initialize();
  IOSByteArray *groomedData = [IOSByteArray newArrayWithLength:((IOSByteArray *) nil_chk(data))->size_];
  jint bytesCopied = 0;
  for (jint i = 0; i < data->size_; i++) {
    switch (IOSByteArray_Get(data, i)) {
      case (jbyte) ' ':
      case (jbyte) 0x000a:
      case (jbyte) 0x000d:
      case (jbyte) 0x0009:
      break;
      default:
      *IOSByteArray_GetRef(groomedData, bytesCopied++) = IOSByteArray_Get(data, i);
    }
  }
  IOSByteArray *packedData = [IOSByteArray newArrayWithLength:bytesCopied];
  JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(groomedData, 0, packedData, 0, bytesCopied);
  return packedData;
}

IOSByteArray *PIME_BinaryType_discardNonBase64WithByteArray_(IOSByteArray *data) {
  PIME_BinaryType_initialize();
  IOSByteArray *groomedData = [IOSByteArray newArrayWithLength:((IOSByteArray *) nil_chk(data))->size_];
  jint bytesCopied = 0;
  for (jint i = 0; i < data->size_; i++) {
    if (PIME_BinaryType_isBase64WithByte_(IOSByteArray_Get(data, i))) {
      *IOSByteArray_GetRef(groomedData, bytesCopied++) = IOSByteArray_Get(data, i);
    }
  }
  IOSByteArray *packedData = [IOSByteArray newArrayWithLength:bytesCopied];
  JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(groomedData, 0, packedData, 0, bytesCopied);
  return packedData;
}

void PIME_BinaryType_initWithPIQ_QName_withNSString_(PIME_BinaryType *self, PIQ_QName *typeID, NSString *className_) {
  PIMT_AbstractType_initWithPIQ_QName_withNSString_withPIMT_Type_(self, typeID, className_, PIME_StringType_getDefString());
}

PIME_BinaryType *new_PIME_BinaryType_initWithPIQ_QName_withNSString_(PIQ_QName *typeID, NSString *className_) {
  J2OBJC_NEW_IMPL(PIME_BinaryType, initWithPIQ_QName_withNSString_, typeID, className_)
}

PIME_BinaryType *create_PIME_BinaryType_initWithPIQ_QName_withNSString_(PIQ_QName *typeID, NSString *className_) {
  J2OBJC_CREATE_IMPL(PIME_BinaryType, initWithPIQ_QName_withNSString_, typeID, className_)
}

NSString *PIME_BinaryType_toBase64StringWithByteArray_withInt_(IOSByteArray *data, jint length) {
  PIME_BinaryType_initialize();
  if (data == nil) return nil;
  else return [NSString java_stringWithBytes:PIME_BinaryType_encodeBase64WithByteArray_withInt_(data, length)];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIME_BinaryType)

J2OBJC_NAME_MAPPING(PIME_BinaryType, "de.pidata.models.types.simple", "PIME_")
