//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/DateObject.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/types/simple/DateObject.h"
#include "de/pidata/models/types/simple/DateTimeType.h"
#include "de/pidata/models/types/simple/DurationObject.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/IllegalArgumentException.h"
#include "java/lang/Integer.h"
#include "java/lang/Math.h"
#include "java/lang/StringBuffer.h"
#include "java/lang/System.h"
#include "java/util/Calendar.h"
#include "java/util/Date.h"
#include "java/util/TimeZone.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/simple/DateObject must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIME_DateObject () {
 @public
  jlong date_;
  PIQ_QName *type_;
}

- (void)setTypeWithPIQ_QName:(PIQ_QName *)type;

+ (jlong)adjustDateWithLong:(jlong)date
       withJavaUtilCalendar:(JavaUtilCalendar *)cal
                   withLong:(jlong)newdate;

@end

J2OBJC_FIELD_SETTER(PIME_DateObject, type_, PIQ_QName *)

inline JavaUtilDate *PIME_DateObject_get_BASE_DATE(void);
static JavaUtilDate *PIME_DateObject_BASE_DATE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIME_DateObject, BASE_DATE, JavaUtilDate *)

inline JavaUtilCalendar *PIME_DateObject_get_calendar(void);
inline JavaUtilCalendar *PIME_DateObject_set_calendar(JavaUtilCalendar *value);
static JavaUtilCalendar *PIME_DateObject_calendar;
J2OBJC_STATIC_FIELD_OBJ(PIME_DateObject, calendar, JavaUtilCalendar *)

__attribute__((unused)) static void PIME_DateObject_setTypeWithPIQ_QName_(PIME_DateObject *self, PIQ_QName *type);

__attribute__((unused)) static jlong PIME_DateObject_adjustDateWithLong_withJavaUtilCalendar_withLong_(jlong date, JavaUtilCalendar *cal, jlong newdate);

J2OBJC_INITIALIZED_DEFN(PIME_DateObject)

@implementation PIME_DateObject

- (instancetype)initWithPIQ_QName:(PIQ_QName *)type {
  PIME_DateObject_initWithPIQ_QName_(self, type);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)type
                         withLong:(jlong)date {
  PIME_DateObject_initWithPIQ_QName_withLong_(self, type, date);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)type
                          withInt:(jint)year
                          withInt:(jint)month
                          withInt:(jint)dayOfMonth {
  PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_(self, type, year, month, dayOfMonth);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)type
                          withInt:(jint)year
                          withInt:(jint)month
                          withInt:(jint)dayOfMonth
                          withInt:(jint)hourOfDay
                          withInt:(jint)minute
                          withInt:(jint)second {
  PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_(self, type, year, month, dayOfMonth, hourOfDay, minute, second);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)type
                     withNSString:(NSString *)strValue {
  PIME_DateObject_initWithPIQ_QName_withNSString_(self, type, strValue);
  return self;
}

- (void)setTypeWithPIQ_QName:(PIQ_QName *)type {
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

- (PIQ_QName *)getType {
  return type_;
}

- (jlong)getTime {
  return self->date_;
}

+ (JavaUtilCalendar *)getCalendar {
  return PIME_DateObject_getCalendar();
}

+ (void)setCalendarWithJavaUtilCalendar:(JavaUtilCalendar *)newCalendar {
  PIME_DateObject_setCalendarWithJavaUtilCalendar_(newCalendar);
}

+ (PIME_DateObject *)addDurationWithPIME_DateObject:(PIME_DateObject *)date
                            withPIME_DurationObject:(PIME_DurationObject *)duration {
  return PIME_DateObject_addDurationWithPIME_DateObject_withPIME_DurationObject_(date, duration);
}

+ (PIME_DateObject *)subtractDurationWithPIME_DateObject:(PIME_DateObject *)date
                                 withPIME_DurationObject:(PIME_DurationObject *)duration {
  return PIME_DateObject_subtractDurationWithPIME_DateObject_withPIME_DurationObject_(date, duration);
}

+ (jlong)addYearWithLong:(jlong)date
                 withInt:(jint)years {
  return PIME_DateObject_addYearWithLong_withInt_(date, years);
}

+ (jlong)addMonthsWithLong:(jlong)date
                   withInt:(jint)months {
  return PIME_DateObject_addMonthsWithLong_withInt_(date, months);
}

+ (jlong)addDaysWithLong:(jlong)date
                 withInt:(jint)days {
  return PIME_DateObject_addDaysWithLong_withInt_(date, days);
}

+ (jlong)addHoursWithLong:(jlong)date
                  withInt:(jint)hours {
  return PIME_DateObject_addHoursWithLong_withInt_(date, hours);
}

+ (jlong)addMinutesWithLong:(jlong)date
                    withInt:(jint)minutes {
  return PIME_DateObject_addMinutesWithLong_withInt_(date, minutes);
}

+ (jlong)addSecondsWithLong:(jlong)date
                    withInt:(jint)seconds {
  return PIME_DateObject_addSecondsWithLong_withInt_(date, seconds);
}

+ (jlong)addMillisecWithLong:(jlong)date
                    withLong:(jlong)millisec {
  return PIME_DateObject_addMillisecWithLong_withLong_(date, millisec);
}

+ (jlong)subtractYearWithLong:(jlong)date
                      withInt:(jint)years {
  return PIME_DateObject_subtractYearWithLong_withInt_(date, years);
}

+ (jlong)subtractMonthsWithLong:(jlong)date
                        withInt:(jint)months {
  return PIME_DateObject_subtractMonthsWithLong_withInt_(date, months);
}

+ (jlong)subtractDaysWithLong:(jlong)date
                      withInt:(jint)days {
  return PIME_DateObject_subtractDaysWithLong_withInt_(date, days);
}

+ (jlong)subtractHoursWithLong:(jlong)date
                       withInt:(jint)hours {
  return PIME_DateObject_subtractHoursWithLong_withInt_(date, hours);
}

+ (jlong)subtractMinutesWithLong:(jlong)date
                         withInt:(jint)minutes {
  return PIME_DateObject_subtractMinutesWithLong_withInt_(date, minutes);
}

+ (jlong)subtractSecondsWithLong:(jlong)date
                         withInt:(jint)seconds {
  return PIME_DateObject_subtractSecondsWithLong_withInt_(date, seconds);
}

+ (jlong)subtractMillisecWithLong:(jlong)date
                         withLong:(jlong)millisec {
  return PIME_DateObject_subtractMillisecWithLong_withLong_(date, millisec);
}

+ (jlong)adjustDateWithLong:(jlong)date
       withJavaUtilCalendar:(JavaUtilCalendar *)cal
                   withLong:(jlong)newdate {
  return PIME_DateObject_adjustDateWithLong_withJavaUtilCalendar_withLong_(date, cal, newdate);
}

- (jboolean)isEqual:(id)obj {
  if ((obj != nil) && ([obj isKindOfClass:[PIME_DateObject class]])) {
    return [((PIME_DateObject *) nil_chk(((PIME_DateObject *) cast_chk(obj, [PIME_DateObject class])))) getTime] == self->date_;
  }
  else {
    return false;
  }
}

- (jboolean)equalsDateWithPIME_DateObject:(PIME_DateObject *)otherDate {
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:new_JavaUtilDate_initWithLong_(date_)];
  jint year = [cal getWithInt:JavaUtilCalendar_YEAR];
  jint month = [cal getWithInt:JavaUtilCalendar_MONTH];
  jint day = [cal getWithInt:JavaUtilCalendar_DAY_OF_MONTH];
  [cal setTimeWithJavaUtilDate:new_JavaUtilDate_initWithLong_([((PIME_DateObject *) nil_chk(otherDate)) getTime])];
  return (year == [cal getWithInt:JavaUtilCalendar_YEAR]) && (month == [cal getWithInt:JavaUtilCalendar_MONTH]) && (day == [cal getWithInt:JavaUtilCalendar_DAY_OF_MONTH]);
}

- (jint)compareToWithPIQ_QName:(PIQ_QName *)type
           withPIME_DateObject:(PIME_DateObject *)obj {
  if (obj == nil) {
    return 1;
  }
  jlong objTime;
  jlong myTime;
  if (JreObjectEqualsEquals(type, JreLoadStatic(PIME_DateTimeType, TYPE_DATE))) {
    objTime = [obj getDate];
    myTime = [self getDate];
  }
  else {
    objTime = [obj getTime];
    myTime = [self getTime];
  }
  if (myTime == objTime) return 0;
  else if (myTime > objTime) return 1;
  else return -1;
}

- (jlong)getDate {
  JavaUtilCalendar *calendar = PIME_DateObject_getCalendar();
  JavaUtilDate *tmpDate = new_JavaUtilDate_initWithLong_(date_);
  [((JavaUtilCalendar *) nil_chk(calendar)) setTimeWithJavaUtilDate:tmpDate];
  [calendar setWithInt:JavaUtilCalendar_HOUR withInt:0];
  [calendar setWithInt:JavaUtilCalendar_MINUTE withInt:0];
  [calendar setWithInt:JavaUtilCalendar_SECOND withInt:0];
  [calendar setWithInt:JavaUtilCalendar_MILLISECOND withInt:0];
  return [((JavaUtilDate *) nil_chk([calendar getTime])) getTime];
}

- (jboolean)betweenWithPIQ_QName:(PIQ_QName *)type
             withPIME_DateObject:(PIME_DateObject *)start
             withPIME_DateObject:(PIME_DateObject *)end {
  return (([self compareToWithPIQ_QName:type withPIME_DateObject:start] >= 0) && ([self compareToWithPIQ_QName:type withPIME_DateObject:end] <= 0));
}

- (NSString *)description {
  return PIME_DateTimeType_toDateTimeStringWithPIME_DateObject_withBoolean_(self, true);
}

- (JavaUtilCalendar *)toCalendar {
  JavaUtilCalendar *calendar = PIME_DateObject_getCalendar();
  [((JavaUtilCalendar *) nil_chk(calendar)) setTimeInMillisWithLong:date_];
  return calendar;
}

- (NSString *)toDisplayStringWithJavaUtilCalendar:(JavaUtilCalendar *)calendar
                                    withPIQ_QName:(PIQ_QName *)dateFormat
                                      withBoolean:(jboolean)shortYear {
  [((JavaUtilCalendar *) nil_chk(calendar)) setTimeWithJavaUtilDate:new_JavaUtilDate_initWithLong_([self getTime])];
  return PIME_DateObject_calendarToStringWithJavaUtilCalendar_withPIQ_QName_withBoolean_(calendar, dateFormat, shortYear);
}

+ (NSString *)calendarToStringWithJavaUtilCalendar:(JavaUtilCalendar *)calendar
                                     withPIQ_QName:(PIQ_QName *)dateFormat
                                       withBoolean:(jboolean)shortYear {
  return PIME_DateObject_calendarToStringWithJavaUtilCalendar_withPIQ_QName_withBoolean_(calendar, dateFormat, shortYear);
}

+ (void)calendarToTimeWithJavaUtilCalendar:(JavaUtilCalendar *)calendar
                  withJavaLangStringBuffer:(JavaLangStringBuffer *)sb {
  PIME_DateObject_calendarToTimeWithJavaUtilCalendar_withJavaLangStringBuffer_(calendar, sb);
}

+ (void)calendarToDateWithJavaUtilCalendar:(JavaUtilCalendar *)calendar
                  withJavaLangStringBuffer:(JavaLangStringBuffer *)sb
                               withBoolean:(jboolean)shortYear {
  PIME_DateObject_calendarToDateWithJavaUtilCalendar_withJavaLangStringBuffer_withBoolean_(calendar, sb, shortYear);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 2, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 3, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 5, 0, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "J", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCalendar;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 6, 7, -1, -1, -1, -1 },
    { NULL, "LPIME_DateObject;", 0x9, 8, 9, -1, -1, -1, -1 },
    { NULL, "LPIME_DateObject;", 0x9, 10, 9, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 11, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 14, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 15, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 16, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 17, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 18, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 19, 20, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 21, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 22, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 23, 12, 13, -1, -1, -1 },
    { NULL, "J", 0x9, 24, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 25, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 26, 12, -1, -1, -1, -1 },
    { NULL, "J", 0x9, 27, 20, -1, -1, -1, -1 },
    { NULL, "J", 0xa, 28, 29, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 30, 31, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 32, 33, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 34, 35, -1, -1, -1, -1 },
    { NULL, "J", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 36, 37, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 38, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCalendar;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 39, 40, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 41, 40, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 42, 43, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 44, 45, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_QName:);
  methods[1].selector = @selector(initWithPIQ_QName:withLong:);
  methods[2].selector = @selector(initWithPIQ_QName:withInt:withInt:withInt:);
  methods[3].selector = @selector(initWithPIQ_QName:withInt:withInt:withInt:withInt:withInt:withInt:);
  methods[4].selector = @selector(initWithPIQ_QName:withNSString:);
  methods[5].selector = @selector(setTypeWithPIQ_QName:);
  methods[6].selector = @selector(getType);
  methods[7].selector = @selector(getTime);
  methods[8].selector = @selector(getCalendar);
  methods[9].selector = @selector(setCalendarWithJavaUtilCalendar:);
  methods[10].selector = @selector(addDurationWithPIME_DateObject:withPIME_DurationObject:);
  methods[11].selector = @selector(subtractDurationWithPIME_DateObject:withPIME_DurationObject:);
  methods[12].selector = @selector(addYearWithLong:withInt:);
  methods[13].selector = @selector(addMonthsWithLong:withInt:);
  methods[14].selector = @selector(addDaysWithLong:withInt:);
  methods[15].selector = @selector(addHoursWithLong:withInt:);
  methods[16].selector = @selector(addMinutesWithLong:withInt:);
  methods[17].selector = @selector(addSecondsWithLong:withInt:);
  methods[18].selector = @selector(addMillisecWithLong:withLong:);
  methods[19].selector = @selector(subtractYearWithLong:withInt:);
  methods[20].selector = @selector(subtractMonthsWithLong:withInt:);
  methods[21].selector = @selector(subtractDaysWithLong:withInt:);
  methods[22].selector = @selector(subtractHoursWithLong:withInt:);
  methods[23].selector = @selector(subtractMinutesWithLong:withInt:);
  methods[24].selector = @selector(subtractSecondsWithLong:withInt:);
  methods[25].selector = @selector(subtractMillisecWithLong:withLong:);
  methods[26].selector = @selector(adjustDateWithLong:withJavaUtilCalendar:withLong:);
  methods[27].selector = @selector(isEqual:);
  methods[28].selector = @selector(equalsDateWithPIME_DateObject:);
  methods[29].selector = @selector(compareToWithPIQ_QName:withPIME_DateObject:);
  methods[30].selector = @selector(getDate);
  methods[31].selector = @selector(betweenWithPIQ_QName:withPIME_DateObject:withPIME_DateObject:);
  methods[32].selector = @selector(description);
  methods[33].selector = @selector(toCalendar);
  methods[34].selector = @selector(toDisplayStringWithJavaUtilCalendar:withPIQ_QName:withBoolean:);
  methods[35].selector = @selector(calendarToStringWithJavaUtilCalendar:withPIQ_QName:withBoolean:);
  methods[36].selector = @selector(calendarToTimeWithJavaUtilCalendar:withJavaLangStringBuffer:);
  methods[37].selector = @selector(calendarToDateWithJavaUtilCalendar:withJavaLangStringBuffer:withBoolean:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ONE_DAY_MILLIS", "J", .constantValue.asLong = PIME_DateObject_ONE_DAY_MILLIS, 0x19, -1, -1, -1, -1 },
    { "ONE_HOUR_MILLIS", "J", .constantValue.asLong = PIME_DateObject_ONE_HOUR_MILLIS, 0x19, -1, -1, -1, -1 },
    { "ONE_MINUTE_MILLIS", "J", .constantValue.asLong = PIME_DateObject_ONE_MINUTE_MILLIS, 0x19, -1, -1, -1, -1 },
    { "ONE_SECOND_MILLIS", "J", .constantValue.asLong = PIME_DateObject_ONE_SECOND_MILLIS, 0x19, -1, -1, -1, -1 },
    { "date_", "J", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "type_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "BASE_DATE", "LJavaUtilDate;", .constantValue.asLong = 0, 0x1a, -1, 46, -1, -1 },
    { "calendar", "LJavaUtilCalendar;", .constantValue.asLong = 0, 0xa, -1, 47, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_QName;", "LPIQ_QName;J", "LPIQ_QName;III", "LPIQ_QName;IIIIII", "LPIQ_QName;LNSString;", "setType", "setCalendar", "LJavaUtilCalendar;", "addDuration", "LPIME_DateObject;LPIME_DurationObject;", "subtractDuration", "addYear", "JI", "LJavaLangIllegalArgumentException;", "addMonths", "addDays", "addHours", "addMinutes", "addSeconds", "addMillisec", "JJ", "subtractYear", "subtractMonths", "subtractDays", "subtractHours", "subtractMinutes", "subtractSeconds", "subtractMillisec", "adjustDate", "JLJavaUtilCalendar;J", "equals", "LNSObject;", "equalsDate", "LPIME_DateObject;", "compareTo", "LPIQ_QName;LPIME_DateObject;", "between", "LPIQ_QName;LPIME_DateObject;LPIME_DateObject;", "toString", "toDisplayString", "LJavaUtilCalendar;LPIQ_QName;Z", "calendarToString", "calendarToTime", "LJavaUtilCalendar;LJavaLangStringBuffer;", "calendarToDate", "LJavaUtilCalendar;LJavaLangStringBuffer;Z", &PIME_DateObject_BASE_DATE, &PIME_DateObject_calendar };
  static const J2ObjcClassInfo _PIME_DateObject = { "DateObject", "de.pidata.models.types.simple", ptrTable, methods, fields, 7, 0x1, 38, 8, -1, -1, -1, -1, -1 };
  return &_PIME_DateObject;
}

+ (void)initialize {
  if (self == [PIME_DateObject class]) {
    PIME_DateObject_BASE_DATE = new_JavaUtilDate_initWithLong_(0);
    J2OBJC_SET_INITIALIZED(PIME_DateObject)
  }
}

@end

void PIME_DateObject_initWithPIQ_QName_(PIME_DateObject *self, PIQ_QName *type) {
  NSObject_init(self);
  self->date_ = JavaLangSystem_currentTimeMillis();
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

PIME_DateObject *new_PIME_DateObject_initWithPIQ_QName_(PIQ_QName *type) {
  J2OBJC_NEW_IMPL(PIME_DateObject, initWithPIQ_QName_, type)
}

PIME_DateObject *create_PIME_DateObject_initWithPIQ_QName_(PIQ_QName *type) {
  J2OBJC_CREATE_IMPL(PIME_DateObject, initWithPIQ_QName_, type)
}

void PIME_DateObject_initWithPIQ_QName_withLong_(PIME_DateObject *self, PIQ_QName *type, jlong date) {
  NSObject_init(self);
  self->date_ = date;
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

PIME_DateObject *new_PIME_DateObject_initWithPIQ_QName_withLong_(PIQ_QName *type, jlong date) {
  J2OBJC_NEW_IMPL(PIME_DateObject, initWithPIQ_QName_withLong_, type, date)
}

PIME_DateObject *create_PIME_DateObject_initWithPIQ_QName_withLong_(PIQ_QName *type, jlong date) {
  J2OBJC_CREATE_IMPL(PIME_DateObject, initWithPIQ_QName_withLong_, type, date)
}

void PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_(PIME_DateObject *self, PIQ_QName *type, jint year, jint month, jint dayOfMonth) {
  NSObject_init(self);
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  [((JavaUtilCalendar *) nil_chk(cal)) setWithInt:year withInt:month withInt:dayOfMonth];
  self->date_ = [cal getTimeInMillis];
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

PIME_DateObject *new_PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_(PIQ_QName *type, jint year, jint month, jint dayOfMonth) {
  J2OBJC_NEW_IMPL(PIME_DateObject, initWithPIQ_QName_withInt_withInt_withInt_, type, year, month, dayOfMonth)
}

PIME_DateObject *create_PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_(PIQ_QName *type, jint year, jint month, jint dayOfMonth) {
  J2OBJC_CREATE_IMPL(PIME_DateObject, initWithPIQ_QName_withInt_withInt_withInt_, type, year, month, dayOfMonth)
}

void PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_(PIME_DateObject *self, PIQ_QName *type, jint year, jint month, jint dayOfMonth, jint hourOfDay, jint minute, jint second) {
  NSObject_init(self);
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  [((JavaUtilCalendar *) nil_chk(cal)) setWithInt:year withInt:month withInt:dayOfMonth withInt:hourOfDay withInt:minute withInt:second];
  self->date_ = [cal getTimeInMillis];
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

PIME_DateObject *new_PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_(PIQ_QName *type, jint year, jint month, jint dayOfMonth, jint hourOfDay, jint minute, jint second) {
  J2OBJC_NEW_IMPL(PIME_DateObject, initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_, type, year, month, dayOfMonth, hourOfDay, minute, second)
}

PIME_DateObject *create_PIME_DateObject_initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_(PIQ_QName *type, jint year, jint month, jint dayOfMonth, jint hourOfDay, jint minute, jint second) {
  J2OBJC_CREATE_IMPL(PIME_DateObject, initWithPIQ_QName_withInt_withInt_withInt_withInt_withInt_withInt_, type, year, month, dayOfMonth, hourOfDay, minute, second)
}

void PIME_DateObject_initWithPIQ_QName_withNSString_(PIME_DateObject *self, PIQ_QName *type, NSString *strValue) {
  NSObject_init(self);
  NSString *fieldStr;
  jint pos;
  jint start;
  jint fieldVal;
  jint tzPos = -1;
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  jboolean hasTime = false;
  jint timeZoneHours = 0;
  jint timeZoneMin = 0;
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:PIME_DateObject_BASE_DATE];
  pos = [((NSString *) nil_chk(strValue)) java_indexOf:'-'];
  if (pos >= 0) {
    fieldStr = [strValue java_substring:0 endIndex:pos];
    fieldVal = JavaLangInteger_parseIntWithNSString_(fieldStr);
    if (fieldVal < 100) fieldVal += 2000;
    [cal setWithInt:JavaUtilCalendar_YEAR withInt:fieldVal];
    start = pos + 1;
    pos = [strValue java_indexOf:'-' fromIndex:start];
    fieldStr = [strValue java_substring:start endIndex:pos];
    [cal setWithInt:JavaUtilCalendar_MONTH withInt:JavaLangInteger_parseIntWithNSString_(fieldStr) - 1];
    start = pos + 1;
    pos = [strValue java_indexOf:' ' fromIndex:start];
    if (pos < 0) pos = [strValue java_indexOf:'T' fromIndex:start];
    if (pos < 0) fieldStr = [strValue java_substring:start endIndex:[strValue java_length]];
    else fieldStr = [strValue java_substring:start endIndex:pos];
    [cal setWithInt:JavaUtilCalendar_DAY_OF_MONTH withInt:JavaLangInteger_parseIntWithNSString_(fieldStr)];
    hasTime = (pos >= 0);
  }
  else {
    hasTime = true;
  }
  if (hasTime) {
    start = pos + 1;
    pos = [strValue java_indexOf:':' fromIndex:start];
    fieldStr = [strValue java_substring:start endIndex:pos];
    [cal setWithInt:JavaUtilCalendar_HOUR_OF_DAY withInt:JavaLangInteger_parseIntWithNSString_([((NSString *) nil_chk(fieldStr)) java_trim])];
    start = pos + 1;
    pos = [strValue java_indexOf:':' fromIndex:start];
    if (pos < 0) fieldStr = [strValue java_substring:start];
    else fieldStr = [strValue java_substring:start endIndex:pos];
    [cal setWithInt:JavaUtilCalendar_MINUTE withInt:JavaLangInteger_parseIntWithNSString_([((NSString *) nil_chk(fieldStr)) java_trim])];
    if (pos >= 0) {
      start = pos + 1;
      pos = [strValue java_indexOf:'.' fromIndex:start];
      tzPos = [strValue java_indexOf:'Z' fromIndex:start];
      if (tzPos < 0) tzPos = [strValue java_indexOf:'-' fromIndex:start];
      if (tzPos < 0) tzPos = [strValue java_indexOf:'+' fromIndex:start];
      if (pos < 0) {
        if (tzPos < 0) fieldStr = [strValue java_substring:start];
        else fieldStr = [strValue java_substring:start endIndex:tzPos];
      }
      else {
        fieldStr = [strValue java_substring:start endIndex:pos];
      }
      [cal setWithInt:JavaUtilCalendar_SECOND withInt:JavaLangInteger_parseIntWithNSString_([((NSString *) nil_chk(fieldStr)) java_trim])];
      if (pos >= 0) {
        if (tzPos < 0) fieldStr = [strValue java_substring:pos + 1];
        else fieldStr = [strValue java_substring:pos + 1 endIndex:tzPos];
        [cal setWithInt:JavaUtilCalendar_MILLISECOND withInt:JavaLangInteger_parseIntWithNSString_([((NSString *) nil_chk(fieldStr)) java_trim])];
      }
    }
  }
  if (tzPos >= 0) {
    if ([strValue charAtWithInt:tzPos] == 'Z') {
      timeZoneHours = 0;
      timeZoneMin = 0;
    }
    else {
      fieldStr = [((NSString *) nil_chk([strValue java_substring:tzPos + 1])) java_trim];
      pos = [((NSString *) nil_chk(fieldStr)) java_indexOf:':'];
      if (pos < 0) {
        timeZoneHours = JavaLangInteger_parseIntWithNSString_([fieldStr java_substring:0]);
        timeZoneMin = 0;
      }
      else {
        timeZoneHours = JavaLangInteger_parseIntWithNSString_([fieldStr java_substring:0 endIndex:pos]);
        timeZoneMin = JavaLangInteger_parseIntWithNSString_([fieldStr java_substring:pos + 1]);
      }
      if ([strValue charAtWithInt:tzPos] == '-') {
        timeZoneHours = -timeZoneHours;
        timeZoneMin = -timeZoneMin;
      }
    }
    [cal setTimeZoneWithJavaUtilTimeZone:JavaUtilTimeZone_getTimeZoneWithNSString_(@"GMT")];
  }
  self->date_ = [((JavaUtilDate *) nil_chk([cal getTime])) getTime];
  if (timeZoneHours != 0) {
    self->date_ = PIME_DateObject_addHoursWithLong_withInt_(self->date_, -timeZoneHours);
  }
  if (timeZoneMin != 0) {
    self->date_ = PIME_DateObject_addMinutesWithLong_withInt_(self->date_, -timeZoneMin);
  }
  PIME_DateObject_setTypeWithPIQ_QName_(self, type);
}

PIME_DateObject *new_PIME_DateObject_initWithPIQ_QName_withNSString_(PIQ_QName *type, NSString *strValue) {
  J2OBJC_NEW_IMPL(PIME_DateObject, initWithPIQ_QName_withNSString_, type, strValue)
}

PIME_DateObject *create_PIME_DateObject_initWithPIQ_QName_withNSString_(PIQ_QName *type, NSString *strValue) {
  J2OBJC_CREATE_IMPL(PIME_DateObject, initWithPIQ_QName_withNSString_, type, strValue)
}

void PIME_DateObject_setTypeWithPIQ_QName_(PIME_DateObject *self, PIQ_QName *type) {
  if (JreObjectEqualsEquals(type, JreLoadStatic(PIME_DateTimeType, TYPE_DATE))) {
    self->type_ = type;
    self->date_ = [self getDate];
  }
  else if ((JreObjectEqualsEquals(type, JreLoadStatic(PIME_DateTimeType, TYPE_TIME))) || (JreObjectEqualsEquals(type, JreLoadStatic(PIME_DateTimeType, TYPE_DATETIME)))) {
    self->type_ = type;
  }
  else {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@$@$@$@", @"Illegal type=", type, @", expected ", JreLoadStatic(PIME_DateTimeType, TYPE_DATE), @", ", JreLoadStatic(PIME_DateTimeType, TYPE_TIME), @" or ", JreLoadStatic(PIME_DateTimeType, TYPE_DATETIME)));
  }
}

JavaUtilCalendar *PIME_DateObject_getCalendar() {
  PIME_DateObject_initialize();
  if (PIME_DateObject_calendar == nil) {
    PIME_DateObject_calendar = JavaUtilCalendar_getInstance();
  }
  return PIME_DateObject_calendar;
}

void PIME_DateObject_setCalendarWithJavaUtilCalendar_(JavaUtilCalendar *newCalendar) {
  PIME_DateObject_initialize();
  PIME_DateObject_calendar = newCalendar;
}

PIME_DateObject *PIME_DateObject_addDurationWithPIME_DateObject_withPIME_DurationObject_(PIME_DateObject *date, PIME_DurationObject *duration) {
  PIME_DateObject_initialize();
  jlong millis = [((PIME_DateObject *) nil_chk(date)) getTime];
  millis = PIME_DateObject_addSecondsWithLong_withInt_(millis, [((PIME_DurationObject *) nil_chk(duration)) getSeconds]);
  millis = PIME_DateObject_addMinutesWithLong_withInt_(millis, [duration getMinutes]);
  millis = PIME_DateObject_addHoursWithLong_withInt_(millis, [duration getHours]);
  millis = PIME_DateObject_addDaysWithLong_withInt_(millis, [duration getDays]);
  millis = PIME_DateObject_addMonthsWithLong_withInt_(millis, [duration getMonths]);
  millis = PIME_DateObject_addYearWithLong_withInt_(millis, [duration getYears]);
  return new_PIME_DateObject_initWithPIQ_QName_withLong_([date getType], millis);
}

PIME_DateObject *PIME_DateObject_subtractDurationWithPIME_DateObject_withPIME_DurationObject_(PIME_DateObject *date, PIME_DurationObject *duration) {
  PIME_DateObject_initialize();
  jlong millis = [((PIME_DateObject *) nil_chk(date)) getTime];
  millis = PIME_DateObject_subtractSecondsWithLong_withInt_(millis, [((PIME_DurationObject *) nil_chk(duration)) getSeconds]);
  millis = PIME_DateObject_subtractMinutesWithLong_withInt_(millis, [duration getMinutes]);
  millis = PIME_DateObject_subtractHoursWithLong_withInt_(millis, [duration getHours]);
  millis = PIME_DateObject_subtractDaysWithLong_withInt_(millis, [duration getDays]);
  millis = PIME_DateObject_subtractMonthsWithLong_withInt_(millis, [duration getMonths]);
  millis = PIME_DateObject_subtractYearWithLong_withInt_(millis, [duration getYears]);
  return new_PIME_DateObject_initWithPIQ_QName_withLong_([date getType], millis);
}

jlong PIME_DateObject_addYearWithLong_withInt_(jlong date, jint years) {
  PIME_DateObject_initialize();
  if (years < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument years are wrong: years < 0");
  }
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  JavaUtilDate *Adate = new_JavaUtilDate_initWithLong_(date);
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:Adate];
  jint yearFromDate = [cal getWithInt:JavaUtilCalendar_YEAR];
  yearFromDate = yearFromDate + years;
  [cal setWithInt:JavaUtilCalendar_YEAR withInt:yearFromDate];
  return [((JavaUtilDate *) nil_chk([cal getTime])) getTime];
}

jlong PIME_DateObject_addMonthsWithLong_withInt_(jlong date, jint months) {
  PIME_DateObject_initialize();
  if (months < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument months are wrong: months < 0");
  }
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  JavaUtilDate *tmpDate = new_JavaUtilDate_initWithLong_(date);
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:tmpDate];
  jint monthFromDate = [cal getWithInt:JavaUtilCalendar_MONTH];
  jint yearFromDate = [cal getWithInt:JavaUtilCalendar_YEAR];
  jint years;
  monthFromDate = monthFromDate + months;
  if (monthFromDate > 11) {
    years = JreIntDiv((monthFromDate + 1), 12);
    yearFromDate = yearFromDate + years;
    monthFromDate = monthFromDate - years * 12;
  }
  [cal setWithInt:JavaUtilCalendar_YEAR withInt:yearFromDate];
  [cal setWithInt:JavaUtilCalendar_MONTH withInt:monthFromDate];
  return [((JavaUtilDate *) nil_chk([cal getTime])) getTime];
}

jlong PIME_DateObject_addDaysWithLong_withInt_(jlong date, jint days) {
  PIME_DateObject_initialize();
  if (days < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument days are wrong: days < 0");
  }
  if (days != 0) {
    JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
    jlong newdate = date + days * PIME_DateObject_ONE_DAY_MILLIS;
    return PIME_DateObject_adjustDateWithLong_withJavaUtilCalendar_withLong_(date, cal, newdate);
  }
  else {
    return date;
  }
}

jlong PIME_DateObject_addHoursWithLong_withInt_(jlong date, jint hours) {
  PIME_DateObject_initialize();
  return date + (hours * PIME_DateObject_ONE_HOUR_MILLIS);
}

jlong PIME_DateObject_addMinutesWithLong_withInt_(jlong date, jint minutes) {
  PIME_DateObject_initialize();
  return date + (minutes * PIME_DateObject_ONE_MINUTE_MILLIS);
}

jlong PIME_DateObject_addSecondsWithLong_withInt_(jlong date, jint seconds) {
  PIME_DateObject_initialize();
  return date + (seconds * PIME_DateObject_ONE_SECOND_MILLIS);
}

jlong PIME_DateObject_addMillisecWithLong_withLong_(jlong date, jlong millisec) {
  PIME_DateObject_initialize();
  return date + millisec;
}

jlong PIME_DateObject_subtractYearWithLong_withInt_(jlong date, jint years) {
  PIME_DateObject_initialize();
  if (years < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument years are years: days < 0");
  }
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  JavaUtilDate *Adate = new_JavaUtilDate_initWithLong_(date);
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:Adate];
  jint yearFromDate = [cal getWithInt:JavaUtilCalendar_YEAR];
  yearFromDate = yearFromDate - years;
  [cal setWithInt:JavaUtilCalendar_YEAR withInt:yearFromDate];
  return [((JavaUtilDate *) nil_chk([cal getTime])) getTime];
}

jlong PIME_DateObject_subtractMonthsWithLong_withInt_(jlong date, jint months) {
  PIME_DateObject_initialize();
  if (months < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument months are wrong: months < 0");
  }
  JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
  JavaUtilDate *Adate = new_JavaUtilDate_initWithLong_(date);
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:Adate];
  jint monthFromDate = [cal getWithInt:JavaUtilCalendar_MONTH];
  jint yearFromDate = [cal getWithInt:JavaUtilCalendar_YEAR];
  jint years;
  monthFromDate = monthFromDate - months;
  if (monthFromDate < 0) {
    years = JreIntDiv(JavaLangMath_absWithInt_(monthFromDate), 12);
    yearFromDate = yearFromDate - years;
    monthFromDate = monthFromDate + years * 12;
  }
  [cal setWithInt:JavaUtilCalendar_YEAR withInt:yearFromDate];
  [cal setWithInt:JavaUtilCalendar_MONTH withInt:monthFromDate];
  return [((JavaUtilDate *) nil_chk([cal getTime])) getTime];
}

jlong PIME_DateObject_subtractDaysWithLong_withInt_(jlong date, jint days) {
  PIME_DateObject_initialize();
  if (days < 0) {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(@"Argument days are wrong: days < 0");
  }
  if (days != 0) {
    JavaUtilCalendar *cal = PIME_DateObject_getCalendar();
    jlong newdate = date - days * PIME_DateObject_ONE_DAY_MILLIS;
    return PIME_DateObject_adjustDateWithLong_withJavaUtilCalendar_withLong_(date, cal, newdate);
  }
  else {
    return date;
  }
}

jlong PIME_DateObject_subtractHoursWithLong_withInt_(jlong date, jint hours) {
  PIME_DateObject_initialize();
  return date - (hours * PIME_DateObject_ONE_HOUR_MILLIS);
}

jlong PIME_DateObject_subtractMinutesWithLong_withInt_(jlong date, jint minutes) {
  PIME_DateObject_initialize();
  return date - (minutes * PIME_DateObject_ONE_MINUTE_MILLIS);
}

jlong PIME_DateObject_subtractSecondsWithLong_withInt_(jlong date, jint seconds) {
  PIME_DateObject_initialize();
  return date - (seconds * PIME_DateObject_ONE_SECOND_MILLIS);
}

jlong PIME_DateObject_subtractMillisecWithLong_withLong_(jlong date, jlong millisec) {
  PIME_DateObject_initialize();
  return date - millisec;
}

jlong PIME_DateObject_adjustDateWithLong_withJavaUtilCalendar_withLong_(jlong date, JavaUtilCalendar *cal, jlong newdate) {
  PIME_DateObject_initialize();
  jint offsetDate;
  jint offsetNewdate;
  [((JavaUtilCalendar *) nil_chk(cal)) setTimeWithJavaUtilDate:new_JavaUtilDate_initWithLong_(date)];
  offsetDate = [((JavaUtilTimeZone *) nil_chk([cal getTimeZone])) getOffsetWithInt:1 withInt:[cal getWithInt:JavaUtilCalendar_YEAR] withInt:[cal getWithInt:JavaUtilCalendar_MONTH] withInt:[cal getWithInt:JavaUtilCalendar_DAY_OF_MONTH] withInt:[cal getWithInt:JavaUtilCalendar_DAY_OF_WEEK] withInt:[cal getWithInt:JavaUtilCalendar_MILLISECOND]];
  [cal setTimeWithJavaUtilDate:new_JavaUtilDate_initWithLong_(newdate)];
  offsetNewdate = [((JavaUtilTimeZone *) nil_chk([cal getTimeZone])) getOffsetWithInt:1 withInt:[cal getWithInt:JavaUtilCalendar_YEAR] withInt:[cal getWithInt:JavaUtilCalendar_MONTH] withInt:[cal getWithInt:JavaUtilCalendar_DAY_OF_MONTH] withInt:[cal getWithInt:JavaUtilCalendar_DAY_OF_WEEK] withInt:[cal getWithInt:JavaUtilCalendar_MILLISECOND]];
  if (offsetDate == offsetNewdate) {
    return newdate;
  }
  else {
    return newdate + offsetDate - offsetNewdate;
  }
}

NSString *PIME_DateObject_calendarToStringWithJavaUtilCalendar_withPIQ_QName_withBoolean_(JavaUtilCalendar *calendar, PIQ_QName *dateFormat, jboolean shortYear) {
  PIME_DateObject_initialize();
  JavaLangStringBuffer *sb = new_JavaLangStringBuffer_init();
  if (JreObjectEqualsEquals(dateFormat, JreLoadStatic(PIME_DateTimeType, TYPE_DATE))) {
    PIME_DateObject_calendarToDateWithJavaUtilCalendar_withJavaLangStringBuffer_withBoolean_(calendar, sb, shortYear);
  }
  else if (JreObjectEqualsEquals(dateFormat, JreLoadStatic(PIME_DateTimeType, TYPE_TIME))) {
    PIME_DateObject_calendarToTimeWithJavaUtilCalendar_withJavaLangStringBuffer_(calendar, sb);
  }
  else if (JreObjectEqualsEquals(dateFormat, JreLoadStatic(PIME_DateTimeType, TYPE_DATETIME))) {
    PIME_DateObject_calendarToDateWithJavaUtilCalendar_withJavaLangStringBuffer_withBoolean_(calendar, sb, shortYear);
    (void) [sb appendWithChar:' '];
    PIME_DateObject_calendarToTimeWithJavaUtilCalendar_withJavaLangStringBuffer_(calendar, sb);
  }
  else {
    @throw new_JavaLangIllegalArgumentException_initWithNSString_(JreStrcat("$@", @"Invalid date format: ", dateFormat));
  }
  return [sb description];
}

void PIME_DateObject_calendarToTimeWithJavaUtilCalendar_withJavaLangStringBuffer_(JavaUtilCalendar *calendar, JavaLangStringBuffer *sb) {
  PIME_DateObject_initialize();
  jshort hour = (jshort) [((JavaUtilCalendar *) nil_chk(calendar)) getWithInt:JavaUtilCalendar_HOUR_OF_DAY];
  if (hour > 9) {
    (void) [((JavaLangStringBuffer *) nil_chk(sb)) appendWithNSString:JavaLangInteger_toStringWithInt_(hour)];
    (void) [sb appendWithChar:':'];
  }
  else {
    (void) [((JavaLangStringBuffer *) nil_chk(sb)) appendWithChar:'0'];
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(hour)];
    (void) [sb appendWithChar:':'];
  }
  jshort minute = (jshort) ([calendar getWithInt:JavaUtilCalendar_MINUTE]);
  if (minute > 9) {
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(minute)];
  }
  else {
    (void) [sb appendWithChar:'0'];
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(minute)];
  }
  jshort second = (jshort) ([calendar getWithInt:JavaUtilCalendar_SECOND]);
  if (second > 0) {
    (void) [sb appendWithChar:':'];
    if (second > 9) {
      (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(second)];
    }
    else {
      (void) [sb appendWithChar:'0'];
      (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(second)];
    }
  }
  jshort milli = (jshort) ([calendar getWithInt:JavaUtilCalendar_MILLISECOND]);
  if (milli > 0) {
    (void) [sb appendWithChar:'.'];
    if (milli > 99) {
      (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(milli)];
    }
    else if (milli > 9) {
      (void) [sb appendWithChar:'0'];
      (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(milli)];
    }
    else {
      (void) [sb appendWithNSString:@"00"];
      (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(milli)];
    }
  }
}

void PIME_DateObject_calendarToDateWithJavaUtilCalendar_withJavaLangStringBuffer_withBoolean_(JavaUtilCalendar *calendar, JavaLangStringBuffer *sb, jboolean shortYear) {
  PIME_DateObject_initialize();
  jshort day = (jshort) [((JavaUtilCalendar *) nil_chk(calendar)) getWithInt:JavaUtilCalendar_DAY_OF_MONTH];
  if (day > 9) {
    (void) [((JavaLangStringBuffer *) nil_chk(sb)) appendWithNSString:JavaLangInteger_toStringWithInt_(day)];
    (void) [sb appendWithChar:'.'];
  }
  else {
    (void) [((JavaLangStringBuffer *) nil_chk(sb)) appendWithChar:'0'];
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(day)];
    (void) [sb appendWithChar:'.'];
  }
  jshort month = (jshort) ([calendar getWithInt:JavaUtilCalendar_MONTH] + 1);
  if (month > 9) {
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(month)];
    (void) [sb appendWithChar:'.'];
  }
  else {
    (void) [sb appendWithChar:'0'];
    (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(month)];
    (void) [sb appendWithChar:'.'];
  }
  jshort year = (jshort) [calendar getWithInt:JavaUtilCalendar_YEAR];
  if (shortYear) {
    if (year >= 2000) year -= 2000;
    if (year <= 9) (void) [sb appendWithChar:'0'];
  }
  (void) [sb appendWithNSString:JavaLangInteger_toStringWithInt_(year)];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIME_DateObject)

J2OBJC_NAME_MAPPING(PIME_DateObject, "de.pidata.models.types.simple", "PIME_")
