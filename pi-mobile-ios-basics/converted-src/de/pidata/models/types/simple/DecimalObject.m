//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/DecimalObject.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/types/simple/DecimalObject.h"
#include "java/lang/ArithmeticException.h"
#include "java/lang/Deprecated.h"
#include "java/lang/Double.h"
#include "java/lang/Exception.h"
#include "java/lang/Float.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/lang/Math.h"
#include "java/lang/NumberFormatException.h"
#include "java/lang/RuntimeException.h"
#include "java/lang/StringBuffer.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/annotation/Annotation.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/simple/DecimalObject must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIME_DecimalObject () {
 @public
  jlong value_;
  jint scale__;
  jint totalDigits_;
  jint exponent_;
}

- (PIME_DecimalObject *)scalingDownAccuracyWithPIME_DecimalObject:(PIME_DecimalObject *)obj;

- (void)cutZeros;

- (jint)tenthRootWithLong:(jlong)val;

@end

inline jint PIME_DecimalObject_get_MAXDIGITSFORMULTIPLY(void);
#define PIME_DecimalObject_MAXDIGITSFORMULTIPLY 18
J2OBJC_STATIC_FIELD_CONSTANT(PIME_DecimalObject, MAXDIGITSFORMULTIPLY, jint)

__attribute__((unused)) static PIME_DecimalObject *PIME_DecimalObject_scalingDownAccuracyWithPIME_DecimalObject_(PIME_DecimalObject *self, PIME_DecimalObject *obj);

__attribute__((unused)) static void PIME_DecimalObject_cutZeros(PIME_DecimalObject *self);

__attribute__((unused)) static jint PIME_DecimalObject_tenthRootWithLong_(PIME_DecimalObject *self, jlong val);

__attribute__((unused)) static IOSObjectArray *PIME_DecimalObject__Annotations$0(void);

J2OBJC_INITIALIZED_DEFN(PIME_DecimalObject)

PIME_DecimalObject *PIME_DecimalObject_ZERO;
PIME_DecimalObject *PIME_DecimalObject_ONE;

@implementation PIME_DecimalObject

+ (jlong)powerOfTenWithInt:(jint)exp {
  return PIME_DecimalObject_powerOfTenWithInt_(exp);
}

- (instancetype)initWithDouble:(jdouble)value
                       withInt:(jint)scale_ {
  PIME_DecimalObject_initWithDouble_withInt_(self, value, scale_);
  return self;
}

- (instancetype)initWithLong:(jlong)value
                     withInt:(jint)scale_ {
  PIME_DecimalObject_initWithLong_withInt_(self, value, scale_);
  return self;
}

- (instancetype)initWithNSString:(NSString *)strValue {
  PIME_DecimalObject_initWithNSString_(self, strValue);
  return self;
}

- (instancetype)initWithNSString:(NSString *)strValue
                        withChar:(jchar)decimalSeparator
                        withChar:(jchar)thousandSeparator {
  PIME_DecimalObject_initWithNSString_withChar_withChar_(self, strValue, decimalSeparator, thousandSeparator);
  return self;
}

- (jint)getScale {
  return scale__;
}

- (jlong)getValue {
  return value_;
}

- (PIME_DecimalObject *)addWithPIME_DecimalObject:(PIME_DecimalObject *)obj {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (obj == nil) return self;
  jlong addresult;
  jint this_scale = 0;
  jint obj_scale = 0;
  if ([obj getScale] < self->scale__) {
    obj_scale = self->scale__ - [obj getScale];
  }
  else if ([obj getScale] > self->scale__) {
    this_scale = [obj getScale] - self->scale__;
  }
  addresult = [self scaleToWithInt:this_scale] + [obj scaleToWithInt:obj_scale];
  return new_PIME_DecimalObject_initWithLong_withInt_(addresult, self->scale__ + this_scale);
}

- (PIME_DecimalObject *)subtractWithPIME_DecimalObject:(PIME_DecimalObject *)obj {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (obj == nil) return self;
  jlong subtractresult;
  jint this_scale = 0;
  jint obj_scale = 0;
  if ([obj getScale] < self->scale__) {
    obj_scale = self->scale__ - [obj getScale];
  }
  else if ([obj getScale] > self->scale__) {
    this_scale = [obj getScale] - self->scale__;
  }
  subtractresult = [self scaleToWithInt:this_scale] - [obj scaleToWithInt:obj_scale];
  return new_PIME_DecimalObject_initWithLong_withInt_(subtractresult, self->scale__ + this_scale);
}

- (PIME_DecimalObject *)multiplyWithPIME_DecimalObject:(PIME_DecimalObject *)obj {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (obj == nil) return PIME_DecimalObject_ZERO;
  jlong multiplyresult;
  jint new_scale;
  if ((self->totalDigits_ + obj->totalDigits_) > PIME_DecimalObject_MAXDIGITSFORMULTIPLY) {
    obj = PIME_DecimalObject_scalingDownAccuracyWithPIME_DecimalObject_(self, obj);
  }
  multiplyresult = self->value_ * ((PIME_DecimalObject *) nil_chk(obj))->value_;
  new_scale = self->scale__ + obj->scale__;
  if (new_scale < 0) {
    @throw new_JavaLangArithmeticException_initWithNSString_(@"multiply: out of range");
  }
  return new_PIME_DecimalObject_initWithLong_withInt_(multiplyresult, new_scale);
}

- (PIME_DecimalObject *)scalingDownAccuracyWithPIME_DecimalObject:(PIME_DecimalObject *)obj {
  return PIME_DecimalObject_scalingDownAccuracyWithPIME_DecimalObject_(self, obj);
}

- (PIME_DecimalObject *)optimizeScale {
  PIME_DecimalObject *result = new_PIME_DecimalObject_initWithLong_withInt_([self getValue], [self getScale]);
  PIME_DecimalObject_cutZeros(result);
  return result;
}

- (void)cutZeros {
  PIME_DecimalObject_cutZeros(self);
}

- (PIME_DecimalObject *)divideWithPIME_DecimalObject:(PIME_DecimalObject *)obj
                                             withInt:(jint)scale_ {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (obj == nil) obj = PIME_DecimalObject_ZERO;
  if (scale_ < 0) {
    @throw new_JavaLangArithmeticException_initWithNSString_(@"Negative scale");
  }
  jlong divideresult;
  jint this_scale = 0;
  jint obj_scale = 0;
  jint lastDigit = 0;
  if ([((PIME_DecimalObject *) nil_chk(obj)) getScale] < self->scale__) {
    obj_scale = self->scale__ - [obj getScale];
  }
  else if ([obj getScale] > self->scale__) {
    this_scale = [obj getScale] - self->scale__;
  }
  divideresult = JreLongDiv([self scaleToWithInt:this_scale + scale_ + 1], [obj scaleToWithInt:obj_scale]);
  lastDigit = (jint) (divideresult - (JreLongDiv(divideresult, 10)) * 10);
  if (lastDigit > 4) {
    divideresult = divideresult + 10;
  }
  return new_PIME_DecimalObject_initWithLong_withInt_(JreLongDiv(divideresult, PIME_DecimalObject_powerOfTenWithInt_(1)), scale_);
}

- (PIME_DecimalObject *)roundWithInt:(jint)fracionDigits {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (scale__ <= fracionDigits) {
    return self;
  }
  else {
    jlong factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__ - fracionDigits);
    jlong newValue = JreLongDiv(self->value_, factor);
    return new_PIME_DecimalObject_initWithLong_withInt_(newValue, fracionDigits);
  }
}

- (jlong)longLongValue {
  if (self->scale__ < self->exponent_) {
    return 0;
  }
  else {
    jlong factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__ - self->exponent_);
    jlong result = (JreLongDiv(self->value_, factor));
    return result;
  }
}

- (jint)intValue {
  return (jint) [self longLongValue];
}

- (jlong)getFraction {
  jlong factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__);
  return JavaLangMath_absWithLong_(JreLongMod(self->value_, factor));
}

- (jint)getFractionDigits {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  jlong fraction = [self getFraction];
  jint count = self->scale__;
  while ((count > 0) && (JreLongMod(fraction, 10)) == 0) {
    fraction = JreLongDiv(fraction, 10);
    count--;
  }
  return count;
}

- (jint)getTotalDigits {
  return totalDigits_;
}

- (jint)getExponent {
  return exponent_;
}

- (jlong)scaleToWithInt:(jint)ascale {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (ascale < 0) {
    @throw new_JavaLangNumberFormatException_initWithNSString_(@"Negative scale");
  }
  jlong result = self->value_ * PIME_DecimalObject_powerOfTenWithInt_(ascale);
  if ((self->value_ > 0) && (result < 0)) result = JavaLangLong_MAX_VALUE;
  else if ((self->value_ < 0) && (result > 0)) result = JavaLangLong_MIN_VALUE;
  return result;
}

- (jint)compareToWithPIME_DecimalObject:(PIME_DecimalObject *)obj {
  if (obj == nil) obj = PIME_DecimalObject_ZERO;
  if (self->value_ == 0) {
    if ([((PIME_DecimalObject *) nil_chk(obj)) getValue] == 0) {
      return 0;
    }
    else if ([obj getValue] < 0) {
      return 1;
    }
    else if ([obj getValue] > 0) {
      return -1;
    }
  }
  if (self->value_ > 0 && [((PIME_DecimalObject *) nil_chk(obj)) getValue] <= 0) {
    return 1;
  }
  if (self->value_ < 0 && [((PIME_DecimalObject *) nil_chk(obj)) getValue] >= 0) {
    return -1;
  }
  jint this_int_magnitude = self->totalDigits_ - self->scale__ + self->exponent_;
  if (self->exponent_ < 0) {
    this_int_magnitude -= 1;
  }
  jint obj_int_magnitude = [((PIME_DecimalObject *) nil_chk(obj)) getTotalDigits] - [obj getScale] + [obj getExponent];
  if ([obj getExponent] < 0) {
    obj_int_magnitude -= 1;
  }
  if (this_int_magnitude > obj_int_magnitude) {
    if (self->value_ > 0) {
      return 1;
    }
    else {
      return -1;
    }
  }
  else if (this_int_magnitude < obj_int_magnitude) {
    if (self->value_ > 0) {
      return -1;
    }
    else {
      return 1;
    }
  }
  if (self->exponent_ != 0 || [obj getExponent] != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  jint result = 0;
  jint this_scale = 0;
  jint obj_scale = 0;
  if ([obj getScale] < self->scale__) {
    obj_scale = self->scale__ - [obj getScale];
  }
  else if ([obj getScale] > self->scale__) {
    this_scale = [obj getScale] - self->scale__;
  }
  jlong myValue = [self scaleToAdjustedWithInt:this_scale];
  jlong otherValue = [obj scaleToAdjustedWithInt:obj_scale];
  if (myValue == otherValue) {
    result = 0;
  }
  else if (myValue > otherValue) {
    result = 1;
  }
  else if (myValue < otherValue) {
    result = -1;
  }
  return result;
}

- (jlong)scaleToAdjustedWithInt:(jint)aScale {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (aScale < 0) {
    @throw new_JavaLangNumberFormatException_initWithNSString_(@"Negative scale");
  }
  jlong result;
  if ((aScale != 0) && (self->value_ != 0)) {
    if (self->value_ == JavaLangLong_MAX_VALUE) {
      result = JavaLangLong_MAX_VALUE;
    }
    else if (self->value_ == JavaLangLong_MIN_VALUE) {
      result = JavaLangLong_MIN_VALUE;
    }
    else {
      jlong scaleFactor = PIME_DecimalObject_powerOfTenWithInt_(aScale);
      if ((self->value_ > 0) && (scaleFactor > JreLongDiv(JavaLangLong_MAX_VALUE, self->value_))) {
        result = JavaLangLong_MAX_VALUE;
      }
      else if ((self->value_ < 0) && (scaleFactor < JreLongDiv(JavaLangLong_MIN_VALUE, self->value_))) {
        result = JavaLangLong_MIN_VALUE;
      }
      else {
        result = [self scaleToWithInt:aScale];
      }
    }
  }
  else {
    result = self->value_;
  }
  return result;
}

- (jboolean)isEqual:(id)o {
  if ([o isKindOfClass:[PIME_DecimalObject class]]) {
    return [self compareToWithPIME_DecimalObject:(PIME_DecimalObject *) o] == 0;
  }
  else {
    return false;
  }
}

- (NSString *)getIntegerPartString {
  if (exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  jlong value;
  jlong factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__);
  value = JreLongDiv(self->value_, factor);
  return JavaLangLong_toStringWithLong_(value);
}

- (NSString *)getFractionPartStringWithInt:(jint)fractionDigits {
  if (fractionDigits <= 0) {
    @throw new_JavaLangNumberFormatException_initWithNSString_(JreStrcat("$I", @"fractionDigits must not be negative: ", fractionDigits));
  }
  JavaLangStringBuffer *buffer = new_JavaLangStringBuffer_init();
  (void) [buffer appendWithLong:[self getFraction]];
  while ([buffer java_length] < self->scale__) {
    (void) [buffer insertWithInt:0 withNSString:@"0"];
  }
  jint fractionLenth = [buffer java_length];
  if (fractionLenth < fractionDigits) {
    for (jint i = 0; i < (fractionDigits - fractionLenth); i++) {
      (void) [buffer appendWithNSString:@"0"];
    }
  }
  else if (fractionLenth > fractionDigits) {
    jchar lastDigit = [buffer charAtWithInt:fractionDigits - 1];
    jchar nextDigit = [buffer charAtWithInt:fractionDigits];
    if (nextDigit >= '5') lastDigit += 1;
    [buffer setCharAtWithInt:fractionDigits - 1 withChar:lastDigit];
    [buffer setLengthWithInt:fractionDigits];
  }
  return [buffer description];
}

- (NSString *)description {
  return [self toStringWithChar:PIME_DecimalObject_DECIMAL_SEPARATOR withInt:self->scale__];
}

- (NSString *)toStringWithChar:(jchar)decimalSeparator
                       withInt:(jint)fractionDigits {
  if (fractionDigits < 0) {
    @throw new_JavaLangNumberFormatException_initWithNSString_(JreStrcat("$I", @"fractionDigits must not be negative: ", fractionDigits));
  }
  JavaLangStringBuilder *result = new_JavaLangStringBuilder_init();
  jlong value;
  jboolean isNegative = (self->value_ < 0);
  jlong factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__);
  value = JreLongDiv(self->value_, factor);
  if (fractionDigits == 0) {
    (void) [result appendWithNSString:JavaLangLong_toStringWithLong_(value)];
  }
  else {
    NSString *fractionStr = [self getFractionPartStringWithInt:fractionDigits];
    if (isNegative && value >= 0) {
      (void) [result appendWithNSString:JreStrcat("C$C$", PIME_DecimalObject_SIGNUM_SIGN, JavaLangLong_toStringWithLong_(value), decimalSeparator, fractionStr)];
    }
    else {
      (void) [result appendWithNSString:JreStrcat("$C$", JavaLangLong_toStringWithLong_(value), decimalSeparator, fractionStr)];
    }
  }
  if (self->exponent_ != 0) {
    (void) [((JavaLangStringBuilder *) nil_chk([result appendWithNSString:@"e"])) appendWithNSString:JavaLangInteger_toStringWithInt_(self->exponent_)];
  }
  return [result description];
}

- (jint)tenthRootWithLong:(jlong)val {
  return PIME_DecimalObject_tenthRootWithLong_(self, val);
}

- (jdouble)doubleValue {
  return JavaLangDouble_parseDoubleWithNSString_([self description]);
}

- (jfloat)floatValue {
  return JavaLangFloat_parseFloatWithNSString_([self description]);
}

- (NSUInteger)hash {
  return (NSUInteger)self;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "J", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 2, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 3, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 4, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 5, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "J", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, 9, 7, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x2, 10, 7, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "LPIME_DecimalObject;", 0x1, 13, 1, -1, -1, -1, -1 },
    { NULL, "J", 0x1, 14, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "J", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "J", 0x1, 15, 1, -1, -1, 16, -1 },
    { NULL, "I", 0x1, 17, 7, -1, -1, -1, -1 },
    { NULL, "J", 0x1, 18, 1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 19, 20, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 21, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 22, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 22, 23, -1, -1, -1, -1 },
    { NULL, "I", 0x2, 24, 25, -1, -1, -1, -1 },
    { NULL, "D", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "F", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(powerOfTenWithInt:);
  methods[1].selector = @selector(initWithDouble:withInt:);
  methods[2].selector = @selector(initWithLong:withInt:);
  methods[3].selector = @selector(initWithNSString:);
  methods[4].selector = @selector(initWithNSString:withChar:withChar:);
  methods[5].selector = @selector(getScale);
  methods[6].selector = @selector(getValue);
  methods[7].selector = @selector(addWithPIME_DecimalObject:);
  methods[8].selector = @selector(subtractWithPIME_DecimalObject:);
  methods[9].selector = @selector(multiplyWithPIME_DecimalObject:);
  methods[10].selector = @selector(scalingDownAccuracyWithPIME_DecimalObject:);
  methods[11].selector = @selector(optimizeScale);
  methods[12].selector = @selector(cutZeros);
  methods[13].selector = @selector(divideWithPIME_DecimalObject:withInt:);
  methods[14].selector = @selector(roundWithInt:);
  methods[15].selector = @selector(longLongValue);
  methods[16].selector = @selector(intValue);
  methods[17].selector = @selector(getFraction);
  methods[18].selector = @selector(getFractionDigits);
  methods[19].selector = @selector(getTotalDigits);
  methods[20].selector = @selector(getExponent);
  methods[21].selector = @selector(scaleToWithInt:);
  methods[22].selector = @selector(compareToWithPIME_DecimalObject:);
  methods[23].selector = @selector(scaleToAdjustedWithInt:);
  methods[24].selector = @selector(isEqual:);
  methods[25].selector = @selector(getIntegerPartString);
  methods[26].selector = @selector(getFractionPartStringWithInt:);
  methods[27].selector = @selector(description);
  methods[28].selector = @selector(toStringWithChar:withInt:);
  methods[29].selector = @selector(tenthRootWithLong:);
  methods[30].selector = @selector(doubleValue);
  methods[31].selector = @selector(floatValue);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ZERO", "LPIME_DecimalObject;", .constantValue.asLong = 0, 0x9, -1, 26, -1, -1 },
    { "ONE", "LPIME_DecimalObject;", .constantValue.asLong = 0, 0x9, -1, 27, -1, -1 },
    { "DECIMAL_SEPARATOR", "C", .constantValue.asUnichar = PIME_DecimalObject_DECIMAL_SEPARATOR, 0x19, -1, -1, -1, -1 },
    { "THOUSAND_SEPARATOR", "C", .constantValue.asUnichar = PIME_DecimalObject_THOUSAND_SEPARATOR, 0x19, -1, -1, -1, -1 },
    { "SIGNUM_SIGN", "C", .constantValue.asUnichar = PIME_DecimalObject_SIGNUM_SIGN, 0x19, -1, -1, -1, -1 },
    { "MAXDIGITSFORMULTIPLY", "I", .constantValue.asInt = PIME_DecimalObject_MAXDIGITSFORMULTIPLY, 0x1a, -1, -1, -1, -1 },
    { "value_", "J", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "scale__", "I", .constantValue.asLong = 0, 0x2, 28, -1, -1, -1 },
    { "totalDigits_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "exponent_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "powerOfTen", "I", "DI", "JI", "LNSString;", "LNSString;CC", "add", "LPIME_DecimalObject;", "subtract", "multiply", "scalingDownAccuracy", "divide", "LPIME_DecimalObject;I", "round", "longValue", "scaleTo", (void *)&PIME_DecimalObject__Annotations$0, "compareTo", "scaleToAdjusted", "equals", "LNSObject;", "getFractionPartString", "toString", "CI", "tenthRoot", "J", &PIME_DecimalObject_ZERO, &PIME_DecimalObject_ONE, "scale" };
  static const J2ObjcClassInfo _PIME_DecimalObject = { "DecimalObject", "de.pidata.models.types.simple", ptrTable, methods, fields, 7, 0x1, 32, 10, -1, -1, -1, -1, -1 };
  return &_PIME_DecimalObject;
}

+ (void)initialize {
  if (self == [PIME_DecimalObject class]) {
    PIME_DecimalObject_ZERO = new_PIME_DecimalObject_initWithLong_withInt_(0, 0);
    PIME_DecimalObject_ONE = new_PIME_DecimalObject_initWithLong_withInt_(1, 0);
    J2OBJC_SET_INITIALIZED(PIME_DecimalObject)
  }
}

@end

jlong PIME_DecimalObject_powerOfTenWithInt_(jint exp) {
  PIME_DecimalObject_initialize();
  if (exp >= 19) {
    @throw new_JavaLangNumberFormatException_initWithNSString_(@"Too big exp");
  }
  jlong result = 1;
  if (exp == 0) {
    result = 1;
  }
  else {
    for (jint i = 1; i <= exp; ++i) {
      result = result * 10;
    }
  }
  return result;
}

void PIME_DecimalObject_initWithDouble_withInt_(PIME_DecimalObject *self, jdouble value, jint scale_) {
  NSNumber_init(self);
  if (scale_ < 0) @throw new_JavaLangNumberFormatException_initWithNSString_(@"Negative scale");
  self->scale__ = scale_;
  self->value_ = JreFpToLong((value * PIME_DecimalObject_powerOfTenWithInt_(scale_)));
  self->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, self->value_);
}

PIME_DecimalObject *new_PIME_DecimalObject_initWithDouble_withInt_(jdouble value, jint scale_) {
  J2OBJC_NEW_IMPL(PIME_DecimalObject, initWithDouble_withInt_, value, scale_)
}

PIME_DecimalObject *create_PIME_DecimalObject_initWithDouble_withInt_(jdouble value, jint scale_) {
  J2OBJC_CREATE_IMPL(PIME_DecimalObject, initWithDouble_withInt_, value, scale_)
}

void PIME_DecimalObject_initWithLong_withInt_(PIME_DecimalObject *self, jlong value, jint scale_) {
  NSNumber_init(self);
  if (scale_ < 0) @throw new_JavaLangNumberFormatException_initWithNSString_(@"Negative scale");
  self->scale__ = scale_;
  self->value_ = value;
  self->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, value);
}

PIME_DecimalObject *new_PIME_DecimalObject_initWithLong_withInt_(jlong value, jint scale_) {
  J2OBJC_NEW_IMPL(PIME_DecimalObject, initWithLong_withInt_, value, scale_)
}

PIME_DecimalObject *create_PIME_DecimalObject_initWithLong_withInt_(jlong value, jint scale_) {
  J2OBJC_CREATE_IMPL(PIME_DecimalObject, initWithLong_withInt_, value, scale_)
}

void PIME_DecimalObject_initWithNSString_(PIME_DecimalObject *self, NSString *strValue) {
  PIME_DecimalObject_initWithNSString_withChar_withChar_(self, strValue, PIME_DecimalObject_DECIMAL_SEPARATOR, PIME_DecimalObject_THOUSAND_SEPARATOR);
}

PIME_DecimalObject *new_PIME_DecimalObject_initWithNSString_(NSString *strValue) {
  J2OBJC_NEW_IMPL(PIME_DecimalObject, initWithNSString_, strValue)
}

PIME_DecimalObject *create_PIME_DecimalObject_initWithNSString_(NSString *strValue) {
  J2OBJC_CREATE_IMPL(PIME_DecimalObject, initWithNSString_, strValue)
}

void PIME_DecimalObject_initWithNSString_withChar_withChar_(PIME_DecimalObject *self, NSString *strValue, jchar decimalSeparator, jchar thousandSeparator) {
  NSNumber_init(self);
  jlong value;
  jlong fraction;
  jlong factor;
  jint pos;
  NSString *temp;
  @try {
    NSString *digitsStr;
    IOSObjectArray *eParts = [((NSString *) nil_chk(strValue)) java_split:@"[eE]"];
    if (((IOSObjectArray *) nil_chk(eParts))->size_ == 2) {
      digitsStr = IOSObjectArray_Get(eParts, 0);
      NSString *expoStr = IOSObjectArray_Get(eParts, 1);
      self->exponent_ = JavaLangInteger_parseIntWithNSString_(expoStr);
    }
    else {
      digitsStr = strValue;
    }
    pos = [((NSString *) nil_chk(digitsStr)) java_indexOf:PIME_DecimalObject_SIGNUM_SIGN];
    jboolean isNegative = false;
    if (pos >= 0) {
      isNegative = true;
      digitsStr = [digitsStr java_substring:pos + 1];
    }
    pos = [((NSString *) nil_chk(digitsStr)) java_indexOf:decimalSeparator];
    if (pos < 0) {
      self->scale__ = 0;
      self->value_ = JavaLangLong_parseLongWithNSString_(digitsStr);
    }
    else {
      temp = [digitsStr java_substring:0 endIndex:pos];
      if ([((NSString *) nil_chk(temp)) java_length] > 0) {
        value = JavaLangLong_parseLongWithNSString_(temp);
      }
      else {
        value = 0;
      }
      temp = [digitsStr java_substring:pos + 1];
      if ([((NSString *) nil_chk(temp)) java_length] > 0) {
        fraction = JavaLangLong_parseLongWithNSString_(temp);
        self->scale__ = [temp java_length];
      }
      else {
        fraction = 0;
        self->scale__ = 0;
      }
      factor = PIME_DecimalObject_powerOfTenWithInt_(self->scale__);
      if ((value != 0) && (factor > JreLongDiv((JavaLangLong_MAX_VALUE - fraction), value))) {
        self->value_ = JavaLangLong_MAX_VALUE;
      }
      else {
        self->value_ = (value * factor) + fraction;
      }
    }
    if (isNegative) {
      self->value_ = -self->value_;
    }
    self->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, self->value_);
  }
  @catch (JavaLangException *ex) {
    JavaLangStringBuilder *valueMsg = new_JavaLangStringBuilder_init();
    (void) [valueMsg appendWithNSString:strValue];
    if (self->exponent_ == 38) {
      (void) [valueMsg appendWithNSString:@" (Float.MAX_VALUE?)"];
    }
    else if (self->exponent_ == -11) {
      (void) [valueMsg appendWithNSString:@" (Float.NaN?)"];
    }
    @throw new_JavaLangNumberFormatException_initWithNSString_(JreStrcat("$@", @"Cannot parse value: ", valueMsg));
  }
}

PIME_DecimalObject *new_PIME_DecimalObject_initWithNSString_withChar_withChar_(NSString *strValue, jchar decimalSeparator, jchar thousandSeparator) {
  J2OBJC_NEW_IMPL(PIME_DecimalObject, initWithNSString_withChar_withChar_, strValue, decimalSeparator, thousandSeparator)
}

PIME_DecimalObject *create_PIME_DecimalObject_initWithNSString_withChar_withChar_(NSString *strValue, jchar decimalSeparator, jchar thousandSeparator) {
  J2OBJC_CREATE_IMPL(PIME_DecimalObject, initWithNSString_withChar_withChar_, strValue, decimalSeparator, thousandSeparator)
}

PIME_DecimalObject *PIME_DecimalObject_scalingDownAccuracyWithPIME_DecimalObject_(PIME_DecimalObject *self, PIME_DecimalObject *obj) {
  if (self->exponent_ != 0) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO - exponent not yet implemented");
  }
  if (obj == nil) obj = PIME_DecimalObject_ZERO;
  PIME_DecimalObject_cutZeros(nil_chk(obj));
  PIME_DecimalObject_cutZeros(self);
  jint digits = self->totalDigits_ + obj->totalDigits_;
  jint tooManyDigits;
  if (digits > PIME_DecimalObject_MAXDIGITSFORMULTIPLY) {
    tooManyDigits = digits - PIME_DecimalObject_MAXDIGITSFORMULTIPLY;
    jint weightThis = JreIntDiv((self->totalDigits_ * 1000), (digits));
    jint tooManyDigitsThis = JreIntDiv(tooManyDigits * weightThis, (1000));
    jint tooManyDigitsObj = tooManyDigits - tooManyDigitsThis;
    self->value_ = JreLongDiv(self->value_, PIME_DecimalObject_powerOfTenWithInt_(tooManyDigitsThis));
    self->scale__ = self->scale__ - tooManyDigitsThis;
    self->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, self->value_);
    obj->value_ = JreLongDiv(obj->value_, PIME_DecimalObject_powerOfTenWithInt_(tooManyDigitsObj));
    obj->scale__ = obj->scale__ - tooManyDigitsObj;
    obj->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, obj->value_);
  }
  return obj;
}

void PIME_DecimalObject_cutZeros(PIME_DecimalObject *self) {
  jint i = 1;
  jlong expTen = 10;
  jint borderTotalDigits = self->totalDigits_;
  jint newScale = self->scale__ - 1;
  jlong val = JreLongMod(self->value_, expTen);
  while (val == 0 && newScale >= 0 && i < borderTotalDigits) {
    newScale--;
    i++;
    expTen = expTen * 10;
    val = JreLongMod(self->value_, expTen);
  }
  self->scale__ = newScale + 1;
  self->value_ = JreLongDiv(self->value_, (JreLongDiv(expTen, 10)));
  self->totalDigits_ = PIME_DecimalObject_tenthRootWithLong_(self, self->value_);
}

jint PIME_DecimalObject_tenthRootWithLong_(PIME_DecimalObject *self, jlong val) {
  jint i = 0;
  while (val != 0) {
    val = JreLongDiv(val, 10);
    i++;
  }
  return i;
}

IOSObjectArray *PIME_DecimalObject__Annotations$0() {
  return [IOSObjectArray newArrayWithObjects:(id[]){ create_JavaLangDeprecated() } count:1 type:JavaLangAnnotationAnnotation_class_()];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIME_DecimalObject)

J2OBJC_NAME_MAPPING(PIME_DecimalObject, "de.pidata.models.types.simple", "PIME_")
