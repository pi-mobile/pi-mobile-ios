//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/DefaultValueEnum.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/types/simple/DefaultValueEnum.h"
#include "de/pidata/qnames/QName.h"

#if !__has_feature(objc_arc)
#error "de/pidata/models/types/simple/DefaultValueEnum must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIME_DefaultValueEnum () {
 @public
  NSString *path_;
  PIQ_QName *relation_;
  PIQ_QName *displayAttribute_;
}

@end

J2OBJC_FIELD_SETTER(PIME_DefaultValueEnum, path_, NSString *)
J2OBJC_FIELD_SETTER(PIME_DefaultValueEnum, relation_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIME_DefaultValueEnum, displayAttribute_, PIQ_QName *)

@implementation PIME_DefaultValueEnum

- (instancetype)initWithNSString:(NSString *)path
                   withPIQ_QName:(PIQ_QName *)relation
                   withPIQ_QName:(PIQ_QName *)displayAttribute {
  PIME_DefaultValueEnum_initWithNSString_withPIQ_QName_withPIQ_QName_(self, path, relation, displayAttribute);
  return self;
}

- (NSString *)getPath {
  return path_;
}

- (PIQ_QName *)getRelation {
  return relation_;
}

- (PIQ_QName *)getDisplayAttribute {
  return displayAttribute_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithNSString:withPIQ_QName:withPIQ_QName:);
  methods[1].selector = @selector(getPath);
  methods[2].selector = @selector(getRelation);
  methods[3].selector = @selector(getDisplayAttribute);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "path_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "relation_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "displayAttribute_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LNSString;LPIQ_QName;LPIQ_QName;" };
  static const J2ObjcClassInfo _PIME_DefaultValueEnum = { "DefaultValueEnum", "de.pidata.models.types.simple", ptrTable, methods, fields, 7, 0x1, 4, 3, -1, -1, -1, -1, -1 };
  return &_PIME_DefaultValueEnum;
}

@end

void PIME_DefaultValueEnum_initWithNSString_withPIQ_QName_withPIQ_QName_(PIME_DefaultValueEnum *self, NSString *path, PIQ_QName *relation, PIQ_QName *displayAttribute) {
  NSObject_init(self);
  self->path_ = path;
  self->relation_ = relation;
  self->displayAttribute_ = displayAttribute;
}

PIME_DefaultValueEnum *new_PIME_DefaultValueEnum_initWithNSString_withPIQ_QName_withPIQ_QName_(NSString *path, PIQ_QName *relation, PIQ_QName *displayAttribute) {
  J2OBJC_NEW_IMPL(PIME_DefaultValueEnum, initWithNSString_withPIQ_QName_withPIQ_QName_, path, relation, displayAttribute)
}

PIME_DefaultValueEnum *create_PIME_DefaultValueEnum_initWithNSString_withPIQ_QName_withPIQ_QName_(NSString *path, PIQ_QName *relation, PIQ_QName *displayAttribute) {
  J2OBJC_CREATE_IMPL(PIME_DefaultValueEnum, initWithNSString_withPIQ_QName_withPIQ_QName_, path, relation, displayAttribute)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIME_DefaultValueEnum)

J2OBJC_NAME_MAPPING(PIME_DefaultValueEnum, "de.pidata.models.types.simple", "PIME_")
