//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-base/src/de/pidata/models/types/simple/NumberType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsTypesSimpleNumberType")
#ifdef RESTRICT_DePidataModelsTypesSimpleNumberType
#define INCLUDE_ALL_DePidataModelsTypesSimpleNumberType 0
#else
#define INCLUDE_ALL_DePidataModelsTypesSimpleNumberType 1
#endif
#undef RESTRICT_DePidataModelsTypesSimpleNumberType

#if !defined (PIME_NumberType_) && (INCLUDE_ALL_DePidataModelsTypesSimpleNumberType || defined(INCLUDE_PIME_NumberType))
#define PIME_NumberType_

#define RESTRICT_DePidataModelsTypesSimpleAbstractSimpleType 1
#define INCLUDE_PIME_AbstractSimpleType 1
#include "de/pidata/models/types/simple/AbstractSimpleType.h"

@class IOSClass;
@class PIQ_QName;
@protocol PIMT_Type;

@interface PIME_NumberType : PIME_AbstractSimpleType

#pragma mark Public

- (instancetype)initWithPIQ_QName:(PIQ_QName *)typeID
                     withIOSClass:(IOSClass *)valueClass
                    withPIMT_Type:(id<PIMT_Type>)parentType;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_QName:(PIQ_QName *)arg0
                     withNSString:(NSString *)arg1
                    withPIMT_Type:(id<PIMT_Type>)arg2 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIME_NumberType)

FOUNDATION_EXPORT void PIME_NumberType_initWithPIQ_QName_withIOSClass_withPIMT_Type_(PIME_NumberType *self, PIQ_QName *typeID, IOSClass *valueClass, id<PIMT_Type> parentType);

J2OBJC_TYPE_LITERAL_HEADER(PIME_NumberType)

@compatibility_alias DePidataModelsTypesSimpleNumberType PIME_NumberType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataModelsTypesSimpleNumberType")
