//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/models/pi-models-xml/src/de/pidata/models/xml/binder/Space.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataModelsXmlBinderSpace")
#ifdef RESTRICT_DePidataModelsXmlBinderSpace
#define INCLUDE_ALL_DePidataModelsXmlBinderSpace 0
#else
#define INCLUDE_ALL_DePidataModelsXmlBinderSpace 1
#endif
#undef RESTRICT_DePidataModelsXmlBinderSpace

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (PIMX_Space_) && (INCLUDE_ALL_DePidataModelsXmlBinderSpace || defined(INCLUDE_PIMX_Space))
#define PIMX_Space_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;
@class PIQ_QName;

typedef NS_ENUM(NSUInteger, PIMX_Space_Enum) {
  PIMX_Space_Enum__default = 0,
  PIMX_Space_Enum_preserve = 1,
};

@interface PIMX_Space : JavaLangEnum

#pragma mark Public

+ (PIMX_Space *)fromQNameWithPIQ_QName:(PIQ_QName *)qNameValue;

+ (PIMX_Space *)fromStringWithNSString:(NSString *)stringValue;

- (PIQ_QName *)getValue;

- (NSString *)description;

+ (PIMX_Space *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (PIMX_Space_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(PIMX_Space)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT PIMX_Space *PIMX_Space_values_[];

inline PIMX_Space *PIMX_Space_get__default(void);
J2OBJC_ENUM_CONSTANT(PIMX_Space, _default)

inline PIMX_Space *PIMX_Space_get_preserve(void);
J2OBJC_ENUM_CONSTANT(PIMX_Space, preserve)

FOUNDATION_EXPORT PIMX_Space *PIMX_Space_fromQNameWithPIQ_QName_(PIQ_QName *qNameValue);

FOUNDATION_EXPORT PIMX_Space *PIMX_Space_fromStringWithNSString_(NSString *stringValue);

FOUNDATION_EXPORT IOSObjectArray *PIMX_Space_values(void);

FOUNDATION_EXPORT PIMX_Space *PIMX_Space_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT PIMX_Space *PIMX_Space_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(PIMX_Space)

@compatibility_alias DePidataModelsXmlBinderSpace PIMX_Space;

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_DePidataModelsXmlBinderSpace")
