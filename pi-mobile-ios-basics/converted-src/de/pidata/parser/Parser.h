//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-parser/src/de/pidata/parser/Parser.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataParserParser")
#ifdef RESTRICT_DePidataParserParser
#define INCLUDE_ALL_DePidataParserParser 0
#else
#define INCLUDE_ALL_DePidataParserParser 1
#endif
#undef RESTRICT_DePidataParserParser

#if !defined (PICP_Parser_) && (INCLUDE_ALL_DePidataParserParser || defined(INCLUDE_PICP_Parser))
#define PICP_Parser_

@class JavaIoInputStream;
@class JavaIoInputStreamReader;
@class JavaLangStringBuilder;
@protocol JavaUtilList;

@interface PICP_Parser : NSObject {
 @public
  JavaLangStringBuilder *inputBuffer_;
  jint inputPos_;
  JavaIoInputStream *dataStream_;
  JavaIoInputStreamReader *reader_;
}

#pragma mark Public

- (instancetype)init;

- (jint)currentChar;

- (void)doParse;

- (id<JavaUtilList>)getParseRanges;

- (void)initBufferWithJavaIoInputStream:(JavaIoInputStream *)dataStream
                           withNSString:(NSString *)encoding OBJC_METHOD_FAMILY_NONE;

- (jboolean)isDumpOnError;

- (jint)nextChar;

- (void)setDumpOnErrorWithBoolean:(jboolean)dumpOnError;

#pragma mark Protected

- (void)addParseRangeWithId:(id)parsedObject;

- (void)addParseRangeFromStartWithId:(id)parsedObject
                             withInt:(jint)numChars;

- (void)back;

- (void)dumpRemainingMsg;

- (void)endOfInputErrorWithNSString:(NSString *)message;

- (jint)getCol;

- (jint)getRow;

- (void)parseErrorWithNSString:(NSString *)message;

- (void)parseFixedStringWithNSString:(NSString *)fixedString
                         withBoolean:(jboolean)ignoreCase;

- (NSString *)parseName;

- (NSString *)parseQuotedStringWithChar:(jchar)quoteChar;

- (jint)parseUntilWithJavaLangStringBuilder:(JavaLangStringBuilder *)resultBuf
                               withNSString:(NSString *)stopChars
                                withBoolean:(jboolean)moveCurrentEndPos;

- (void)setCurrentEndPos;

- (void)setCurrentStartOnEndPos;

- (void)setCurrentStartPos;

- (jint)skipBlank;

- (void)skipUntilWithChar:(jchar)stopChar;

- (jint)skipWhitespaceWithBoolean:(jboolean)moveStartPos;

@end

J2OBJC_EMPTY_STATIC_INIT(PICP_Parser)

J2OBJC_FIELD_SETTER(PICP_Parser, inputBuffer_, JavaLangStringBuilder *)
J2OBJC_FIELD_SETTER(PICP_Parser, dataStream_, JavaIoInputStream *)
J2OBJC_FIELD_SETTER(PICP_Parser, reader_, JavaIoInputStreamReader *)

FOUNDATION_EXPORT void PICP_Parser_init(PICP_Parser *self);

J2OBJC_TYPE_LITERAL_HEADER(PICP_Parser)

@compatibility_alias DePidataParserParser PICP_Parser;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataParserParser")
