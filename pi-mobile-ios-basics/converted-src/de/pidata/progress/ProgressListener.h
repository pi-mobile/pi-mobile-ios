//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/progress/ProgressListener.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataProgressProgressListener")
#ifdef RESTRICT_DePidataProgressProgressListener
#define INCLUDE_ALL_DePidataProgressProgressListener 0
#else
#define INCLUDE_ALL_DePidataProgressProgressListener 1
#endif
#undef RESTRICT_DePidataProgressProgressListener

#if !defined (PICO_ProgressListener_) && (INCLUDE_ALL_DePidataProgressProgressListener || defined(INCLUDE_PICO_ProgressListener))
#define PICO_ProgressListener_

@protocol PICO_ProgressListener < JavaObject >

- (void)setMaxValueWithDouble:(jdouble)maxValue;

- (void)updateProgressWithNSString:(NSString *)newMessage
                        withDouble:(jdouble)progress;

- (void)updateProgressWithDouble:(jdouble)progress;

- (void)showErrorWithNSString:(NSString *)errorMessage;

- (void)hideError;

- (void)showInfoWithNSString:(NSString *)infoMessage;

- (void)showWarningWithNSString:(NSString *)warningMessage;

- (void)resetColor;

@end

J2OBJC_EMPTY_STATIC_INIT(PICO_ProgressListener)

J2OBJC_TYPE_LITERAL_HEADER(PICO_ProgressListener)

#define DePidataProgressProgressListener PICO_ProgressListener

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataProgressProgressListener")
