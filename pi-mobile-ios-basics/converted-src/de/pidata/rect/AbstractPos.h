//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-rect/src/de/pidata/rect/AbstractPos.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRectAbstractPos")
#ifdef RESTRICT_DePidataRectAbstractPos
#define INCLUDE_ALL_DePidataRectAbstractPos 0
#else
#define INCLUDE_ALL_DePidataRectAbstractPos 1
#endif
#undef RESTRICT_DePidataRectAbstractPos

#if !defined (PICR_AbstractPos_) && (INCLUDE_ALL_DePidataRectAbstractPos || defined(INCLUDE_PICR_AbstractPos))
#define PICR_AbstractPos_

#define RESTRICT_DePidataRectPos 1
#define INCLUDE_PICR_Pos 1
#include "de/pidata/rect/Pos.h"

@class PICR_PosSizeEventSender;

@interface PICR_AbstractPos : NSObject < PICR_Pos > {
 @public
  PICR_PosSizeEventSender *eventSender_;
}

#pragma mark Public

- (instancetype)init;

- (PICR_PosSizeEventSender *)getEventSender;

- (NSString *)description;

#pragma mark Protected

- (void)firePosChangedWithDouble:(jdouble)oldX
                      withDouble:(jdouble)oldY;

@end

J2OBJC_EMPTY_STATIC_INIT(PICR_AbstractPos)

J2OBJC_FIELD_SETTER(PICR_AbstractPos, eventSender_, PICR_PosSizeEventSender *)

FOUNDATION_EXPORT void PICR_AbstractPos_init(PICR_AbstractPos *self);

J2OBJC_TYPE_LITERAL_HEADER(PICR_AbstractPos)

@compatibility_alias DePidataRectAbstractPos PICR_AbstractPos;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRectAbstractPos")
