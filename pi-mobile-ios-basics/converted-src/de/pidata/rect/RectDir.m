//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-rect/src/de/pidata/rect/RectDir.java
//

#include "J2ObjC_source.h"
#include "de/pidata/rect/RectDir.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rect/RectDir must be compiled with ARC (-fobjc-arc)"
#endif

@interface PICR_RectDir : NSObject

@end

@implementation PICR_RectDir

+ (const J2ObjcClassInfo *)__metadata {
  static const J2ObjcClassInfo _PICR_RectDir = { "RectDir", "de.pidata.rect", NULL, NULL, NULL, 7, 0x609, 0, 0, -1, -1, -1, -1, -1 };
  return &_PICR_RectDir;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PICR_RectDir)

J2OBJC_NAME_MAPPING(PICR_RectDir, "de.pidata.rect", "PICR_")
