//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-rect/src/de/pidata/rect/RelativeRect.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRectRelativeRect")
#ifdef RESTRICT_DePidataRectRelativeRect
#define INCLUDE_ALL_DePidataRectRelativeRect 0
#else
#define INCLUDE_ALL_DePidataRectRelativeRect 1
#endif
#undef RESTRICT_DePidataRectRelativeRect

#if !defined (PICR_RelativeRect_) && (INCLUDE_ALL_DePidataRectRelativeRect || defined(INCLUDE_PICR_RelativeRect))
#define PICR_RelativeRect_

#define RESTRICT_DePidataRectRelativePos 1
#define INCLUDE_PICR_RelativePos 1
#include "de/pidata/rect/RelativePos.h"

#define RESTRICT_DePidataRectRectDir 1
#define INCLUDE_PICR_RectDir 1
#include "de/pidata/rect/RectDir.h"

@protocol PICR_Pos;

@interface PICR_RelativeRect : PICR_RelativePos < PICR_RectDir >

#pragma mark Public

- (instancetype)initWithPICR_Pos:(id<PICR_Pos>)basePos
                      withDouble:(jdouble)deltaX
                      withDouble:(jdouble)deltaY
                      withDouble:(jdouble)width
                      withDouble:(jdouble)height
                      withDouble:(jdouble)deltaAngle;

- (jdouble)getBottom;

- (jdouble)getHeight;

- (jdouble)getRight;

- (jdouble)getWidth;

- (void)setHeightWithDouble:(jdouble)height;

- (void)setSizeWithDouble:(jdouble)width
               withDouble:(jdouble)height;

- (void)setWidthWithDouble:(jdouble)width;

#pragma mark Protected

- (void)fireSizeChangedWithDouble:(jdouble)oldWidth
                       withDouble:(jdouble)oldHeight;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPICR_Pos:(id<PICR_Pos>)arg0
                      withDouble:(jdouble)arg1
                      withDouble:(jdouble)arg2 NS_UNAVAILABLE;

- (instancetype)initWithPICR_Pos:(id<PICR_Pos>)arg0
                      withDouble:(jdouble)arg1
                      withDouble:(jdouble)arg2
                      withDouble:(jdouble)arg3 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PICR_RelativeRect)

FOUNDATION_EXPORT void PICR_RelativeRect_initWithPICR_Pos_withDouble_withDouble_withDouble_withDouble_withDouble_(PICR_RelativeRect *self, id<PICR_Pos> basePos, jdouble deltaX, jdouble deltaY, jdouble width, jdouble height, jdouble deltaAngle);

FOUNDATION_EXPORT PICR_RelativeRect *new_PICR_RelativeRect_initWithPICR_Pos_withDouble_withDouble_withDouble_withDouble_withDouble_(id<PICR_Pos> basePos, jdouble deltaX, jdouble deltaY, jdouble width, jdouble height, jdouble deltaAngle) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PICR_RelativeRect *create_PICR_RelativeRect_initWithPICR_Pos_withDouble_withDouble_withDouble_withDouble_withDouble_(id<PICR_Pos> basePos, jdouble deltaX, jdouble deltaY, jdouble width, jdouble height, jdouble deltaAngle);

J2OBJC_TYPE_LITERAL_HEADER(PICR_RelativeRect)

@compatibility_alias DePidataRectRelativeRect PICR_RelativeRect;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRectRelativeRect")
