//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-rect/src/de/pidata/rect/SimplePosDir.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRectSimplePosDir")
#ifdef RESTRICT_DePidataRectSimplePosDir
#define INCLUDE_ALL_DePidataRectSimplePosDir 0
#else
#define INCLUDE_ALL_DePidataRectSimplePosDir 1
#endif
#undef RESTRICT_DePidataRectSimplePosDir

#if !defined (PICR_SimplePosDir_) && (INCLUDE_ALL_DePidataRectSimplePosDir || defined(INCLUDE_PICR_SimplePosDir))
#define PICR_SimplePosDir_

#define RESTRICT_DePidataRectSimplePos 1
#define INCLUDE_PICR_SimplePos 1
#include "de/pidata/rect/SimplePos.h"

#define RESTRICT_DePidataRectPosDir 1
#define INCLUDE_PICR_PosDir 1
#include "de/pidata/rect/PosDir.h"

@class PICR_Rotation;
@protocol PICR_Rect;

@interface PICR_SimplePosDir : PICR_SimplePos < PICR_PosDir >

#pragma mark Public

- (instancetype)initWithDouble:(jdouble)x
                    withDouble:(jdouble)y
             withPICR_Rotation:(PICR_Rotation *)rotation;

- (PICR_Rotation *)getRotation;

#pragma mark Protected

- (void)fireRotationChangedWithPICR_Rotation:(PICR_Rotation *)oldRotation;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithDouble:(jdouble)arg0
                    withDouble:(jdouble)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPICR_Rect:(id<PICR_Rect>)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PICR_SimplePosDir)

FOUNDATION_EXPORT void PICR_SimplePosDir_initWithDouble_withDouble_withPICR_Rotation_(PICR_SimplePosDir *self, jdouble x, jdouble y, PICR_Rotation *rotation);

FOUNDATION_EXPORT PICR_SimplePosDir *new_PICR_SimplePosDir_initWithDouble_withDouble_withPICR_Rotation_(jdouble x, jdouble y, PICR_Rotation *rotation) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PICR_SimplePosDir *create_PICR_SimplePosDir_initWithDouble_withDouble_withPICR_Rotation_(jdouble x, jdouble y, PICR_Rotation *rotation);

J2OBJC_TYPE_LITERAL_HEADER(PICR_SimplePosDir)

@compatibility_alias DePidataRectSimplePosDir PICR_SimplePosDir;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRectSimplePosDir")
