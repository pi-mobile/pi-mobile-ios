//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-basics/src/de/pidata/service/base/ServiceException.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/service/base/ServiceException.h"
#include "java/lang/Exception.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/Throwable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/service/base/ServiceException must be compiled with ARC (-fobjc-arc)"
#endif

@interface PISB_ServiceException () {
 @public
  JavaLangThrowable *cause_ServiceException_;
  NSString *code_;
}

@end

J2OBJC_FIELD_SETTER(PISB_ServiceException, cause_ServiceException_, JavaLangThrowable *)
J2OBJC_FIELD_SETTER(PISB_ServiceException, code_, NSString *)

NSString *PISB_ServiceException_SERVICE_NOT_FOUND = @"SERVICE_NOT_FOUND";
NSString *PISB_ServiceException_UNKNOWN_OPERATION = @"UNKNOWN_OPERATION";
NSString *PISB_ServiceException_SERVICE_FAILED = @"SERVICE_FAILED";
NSString *PISB_ServiceException_INVALID_INPUT = @"INVALID_INPUT";
NSString *PISB_ServiceException_INVALID_OUTPUT = @"INVALID_OUTPUT";
NSString *PISB_ServiceException_SERVICE_CONFIG_ERROR = @"SERVICE_CONFIG_ERROR";
NSString *PISB_ServiceException_SERVICE_COMM_ERROR = @"SERVICE_COMM_ERROR";

@implementation PISB_ServiceException

- (instancetype)initWithNSString:(NSString *)code
                    withNSString:(NSString *)message {
  PISB_ServiceException_initWithNSString_withNSString_(self, code, message);
  return self;
}

- (instancetype)initWithNSString:(NSString *)code
                    withNSString:(NSString *)message
           withJavaLangThrowable:(JavaLangThrowable *)ex {
  PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(self, code, message, ex);
  return self;
}

- (NSString *)getMessage {
  return JreStrcat("$$$$", @"code=", code_, @", ", [super getMessage]);
}

- (NSString *)getCode {
  return code_;
}

- (JavaLangThrowable *)getCause {
  return cause_ServiceException_;
}

- (void)printStackTrace {
  [super printStackTrace];
  if (cause_ServiceException_ != nil) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"caused by:", cause_ServiceException_);
  }
}

- (NSString *)description {
  JavaLangStringBuilder *builder = new_JavaLangStringBuilder_initWithNSString_([[self java_getClass] getName]);
  (void) [((JavaLangStringBuilder *) nil_chk([builder appendWithNSString:@", "])) appendWithNSString:[self getMessage]];
  if (cause_ServiceException_ != nil) {
    (void) [((JavaLangStringBuilder *) nil_chk([builder appendWithNSString:@", caused by "])) appendWithId:cause_ServiceException_];
  }
  return [builder description];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaLangThrowable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithNSString:withNSString:);
  methods[1].selector = @selector(initWithNSString:withNSString:withJavaLangThrowable:);
  methods[2].selector = @selector(getMessage);
  methods[3].selector = @selector(getCode);
  methods[4].selector = @selector(getCause);
  methods[5].selector = @selector(printStackTrace);
  methods[6].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "SERVICE_NOT_FOUND", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "UNKNOWN_OPERATION", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
    { "SERVICE_FAILED", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 5, -1, -1 },
    { "INVALID_INPUT", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 6, -1, -1 },
    { "INVALID_OUTPUT", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 7, -1, -1 },
    { "SERVICE_CONFIG_ERROR", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 8, -1, -1 },
    { "SERVICE_COMM_ERROR", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
    { "cause_ServiceException_", "LJavaLangThrowable;", .constantValue.asLong = 0, 0x2, 10, -1, -1, -1 },
    { "code_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LNSString;LNSString;", "LNSString;LNSString;LJavaLangThrowable;", "toString", &PISB_ServiceException_SERVICE_NOT_FOUND, &PISB_ServiceException_UNKNOWN_OPERATION, &PISB_ServiceException_SERVICE_FAILED, &PISB_ServiceException_INVALID_INPUT, &PISB_ServiceException_INVALID_OUTPUT, &PISB_ServiceException_SERVICE_CONFIG_ERROR, &PISB_ServiceException_SERVICE_COMM_ERROR, "cause" };
  static const J2ObjcClassInfo _PISB_ServiceException = { "ServiceException", "de.pidata.service.base", ptrTable, methods, fields, 7, 0x1, 7, 9, -1, -1, -1, -1, -1 };
  return &_PISB_ServiceException;
}

@end

void PISB_ServiceException_initWithNSString_withNSString_(PISB_ServiceException *self, NSString *code, NSString *message) {
  JavaLangException_initWithNSString_(self, message);
  self->code_ = code;
}

PISB_ServiceException *new_PISB_ServiceException_initWithNSString_withNSString_(NSString *code, NSString *message) {
  J2OBJC_NEW_IMPL(PISB_ServiceException, initWithNSString_withNSString_, code, message)
}

PISB_ServiceException *create_PISB_ServiceException_initWithNSString_withNSString_(NSString *code, NSString *message) {
  J2OBJC_CREATE_IMPL(PISB_ServiceException, initWithNSString_withNSString_, code, message)
}

void PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(PISB_ServiceException *self, NSString *code, NSString *message, JavaLangThrowable *ex) {
  JavaLangException_initWithNSString_(self, message);
  self->code_ = code;
  self->cause_ServiceException_ = ex;
}

PISB_ServiceException *new_PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(NSString *code, NSString *message, JavaLangThrowable *ex) {
  J2OBJC_NEW_IMPL(PISB_ServiceException, initWithNSString_withNSString_withJavaLangThrowable_, code, message, ex)
}

PISB_ServiceException *create_PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(NSString *code, NSString *message, JavaLangThrowable *ex) {
  J2OBJC_CREATE_IMPL(PISB_ServiceException, initWithNSString_withNSString_withJavaLangThrowable_, code, message, ex)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PISB_ServiceException)

J2OBJC_NAME_MAPPING(PISB_ServiceException, "de.pidata.service.base", "PISB_")
