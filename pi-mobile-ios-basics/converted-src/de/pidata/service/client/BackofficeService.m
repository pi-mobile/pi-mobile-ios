//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-client/src/de/pidata/service/client/BackofficeService.java
//

#include "J2ObjC_source.h"
#include "de/pidata/models/service/Operation.h"
#include "de/pidata/models/service/PortType.h"
#include "de/pidata/models/tree/Context.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/service/base/ServiceException.h"
#include "de/pidata/service/client/BackofficeService.h"
#include "de/pidata/service/log/LogEntry.h"
#include "de/pidata/service/log/LogManager.h"
#include "de/pidata/system/base/BackgroundSynchronizer.h"
#include "de/pidata/wsdl/WsdlService.h"
#include "java/lang/Exception.h"

#if !__has_feature(objc_arc)
#error "de/pidata/service/client/BackofficeService must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PISC_BackofficeService

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PISC_BackofficeService_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (id<PIMR_Model>)invokeWithPIMR_Context:(PIMR_Context *)caller
                      withPIMS_Operation:(id<PIMS_Operation>)operation
                          withPIMR_Model:(id<PIMR_Model>)input {
  @try {
    PISL_LogEntry *log = PISL_LogManager_createLogEntryWithPIQ_QName_withPIQ_QName_withPIMR_Model_([((id<PIMS_PortType>) nil_chk([self portType])) getName], [((id<PIMS_Operation>) nil_chk(operation)) getName], input);
    [((PISL_LogManager *) nil_chk(PISL_LogManager_getInstanceWithNSString_(nil))) addLogWithPISL_LogEntry:log];
  }
  @catch (JavaLangException *ex) {
    @throw new_PISB_ServiceException_initWithNSString_withNSString_withJavaLangThrowable_(PISB_ServiceException_SERVICE_FAILED, JreStrcat("$@", @"Service failed: opID=", [((id<PIMS_Operation>) nil_chk(operation)) getName]), ex);
  }
  [((PISYSB_BackgroundSynchronizer *) nil_chk(PISYSB_BackgroundSynchronizer_getInstance())) synchronize];
  return nil;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x4, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(invokeWithPIMR_Context:withPIMS_Operation:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "invoke", "LPIMR_Context;LPIMS_Operation;LPIMR_Model;", "LPISB_ServiceException;" };
  static const J2ObjcClassInfo _PISC_BackofficeService = { "BackofficeService", "de.pidata.service.client", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_PISC_BackofficeService;
}

@end

void PISC_BackofficeService_init(PISC_BackofficeService *self) {
  DePidataWsdlWsdlService_init(self);
}

PISC_BackofficeService *new_PISC_BackofficeService_init() {
  J2OBJC_NEW_IMPL(PISC_BackofficeService, init)
}

PISC_BackofficeService *create_PISC_BackofficeService_init() {
  J2OBJC_CREATE_IMPL(PISC_BackofficeService, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PISC_BackofficeService)

J2OBJC_NAME_MAPPING(PISC_BackofficeService, "de.pidata.service.client", "PISC_")
