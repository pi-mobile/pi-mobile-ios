//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-log/src/de/pidata/service/log/LogManager.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Level.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelFactory.h"
#include "de/pidata/models/tree/ModelFactoryTable.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/Type.h"
#include "de/pidata/models/types/simple/BooleanType.h"
#include "de/pidata/models/xml/binder/XmlReader.h"
#include "de/pidata/models/xml/binder/XmlWriter.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/NamespaceTable.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/service/log/BOStateType.h"
#include "de/pidata/service/log/LogEntry.h"
#include "de/pidata/service/log/LogFactory.h"
#include "de/pidata/service/log/LogManager.h"
#include "de/pidata/service/log/LogState.h"
#include "de/pidata/service/log/MessageType.h"
#include "de/pidata/stream/StreamHelper.h"
#include "de/pidata/system/base/Storage.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/io/IOException.h"
#include "java/io/InputStream.h"
#include "java/io/OutputStream.h"
#include "java/lang/Boolean.h"
#include "java/lang/Exception.h"
#include "java/lang/Integer.h"
#include "java/lang/RuntimeException.h"
#include "java/lang/StringBuilder.h"
#include "java/util/Collection.h"
#include "java/util/HashMap.h"
#include "java/util/Vector.h"

#if !__has_feature(objc_arc)
#error "de/pidata/service/log/LogManager must be compiled with ARC (-fobjc-arc)"
#endif

@interface PISL_LogManager () {
 @public
  PIQ_QName *targetID_;
  PISL_LogState *logStore_;
  PISYSB_Storage *storage_;
  PIMX_XmlWriter *xmlWriter_;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)targetID;

- (void)removeLogsWithInt:(jint)keepFromIndex
                  withInt:(jint)keepToIndex;

- (jint)extractSeqNumberWithNSString:(NSString *)filename;

- (IOSObjectArray *)getPendingLogNames;

- (NSString *)createResourceNameWithInt:(jint)seq;

- (void)setSeqSentWithInt:(jint)number;

- (void)checkLogStore;

- (void)flushLogIndex;

- (void)init__ OBJC_METHOD_FAMILY_NONE;

@end

J2OBJC_FIELD_SETTER(PISL_LogManager, targetID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PISL_LogManager, logStore_, PISL_LogState *)
J2OBJC_FIELD_SETTER(PISL_LogManager, storage_, PISYSB_Storage *)
J2OBJC_FIELD_SETTER(PISL_LogManager, xmlWriter_, PIMX_XmlWriter *)

inline jboolean PISL_LogManager_get_DEBUG(void);
#define PISL_LogManager_DEBUG false
J2OBJC_STATIC_FIELD_CONSTANT(PISL_LogManager, DEBUG, jboolean)

inline JavaUtilHashMap *PISL_LogManager_get_instances(void);
inline JavaUtilHashMap *PISL_LogManager_set_instances(JavaUtilHashMap *value);
static JavaUtilHashMap *PISL_LogManager_instances;
J2OBJC_STATIC_FIELD_OBJ(PISL_LogManager, instances, JavaUtilHashMap *)

inline PISL_LogManager *PISL_LogManager_get_defaultInstance(void);
inline PISL_LogManager *PISL_LogManager_set_defaultInstance(PISL_LogManager *value);
static PISL_LogManager *PISL_LogManager_defaultInstance;
J2OBJC_STATIC_FIELD_OBJ(PISL_LogManager, defaultInstance, PISL_LogManager *)

__attribute__((unused)) static void PISL_LogManager_initWithPIQ_QName_(PISL_LogManager *self, PIQ_QName *targetID);

__attribute__((unused)) static PISL_LogManager *new_PISL_LogManager_initWithPIQ_QName_(PIQ_QName *targetID) NS_RETURNS_RETAINED;

__attribute__((unused)) static PISL_LogManager *create_PISL_LogManager_initWithPIQ_QName_(PIQ_QName *targetID);

__attribute__((unused)) static void PISL_LogManager_removeLogsWithInt_withInt_(PISL_LogManager *self, jint keepFromIndex, jint keepToIndex);

__attribute__((unused)) static jint PISL_LogManager_extractSeqNumberWithNSString_(PISL_LogManager *self, NSString *filename);

__attribute__((unused)) static IOSObjectArray *PISL_LogManager_getPendingLogNames(PISL_LogManager *self);

__attribute__((unused)) static NSString *PISL_LogManager_createResourceNameWithInt_(PISL_LogManager *self, jint seq);

__attribute__((unused)) static void PISL_LogManager_setSeqSentWithInt_(PISL_LogManager *self, jint number);

__attribute__((unused)) static void PISL_LogManager_checkLogStore(PISL_LogManager *self);

__attribute__((unused)) static void PISL_LogManager_flushLogIndex(PISL_LogManager *self);

__attribute__((unused)) static void PISL_LogManager_init__(PISL_LogManager *self);

J2OBJC_INITIALIZED_DEFN(PISL_LogManager)

NSString *PISL_LogManager_LOG_DIR = @"log";
NSString *PISL_LogManager_LOG_STORE_RESOURCE = @"index.xml";
NSString *PISL_LogManager_LOGFILE_PREFIX = @"S_LOG_";
NSString *PISL_LogManager_BUFFERFILE_PREFIX = @"BUFFER_";
NSString *PISL_LogManager_LOGFILE_EXTENSION = @".xml";
PIQ_QName *PISL_LogManager_KEY_PROCESSED_SEQ;
PIQ_QName *PISL_LogManager_KEY_LAST_SYNC_TIME;
NSString *PISL_LogManager_OUT_OF_SYNC = @"OUT_OF_SYNC";
NSString *PISL_LogManager_SERVICE_LOG_MANAGER = @"LogManager";
PIQ_QName *PISL_LogManager_OP_SYNC;

@implementation PISL_LogManager

+ (PISL_LogManager *)getInstanceWithNSString:(NSString *)targetID {
  return PISL_LogManager_getInstanceWithNSString_(targetID);
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)targetID {
  PISL_LogManager_initWithPIQ_QName_(self, targetID);
  return self;
}

+ (JavaUtilVector *)getInstalledManagers {
  return PISL_LogManager_getInstalledManagers();
}

+ (PISL_LogEntry *)createLogEntryWithPIQ_QName:(PIQ_QName *)portTypeName
                                 withPIQ_QName:(PIQ_QName *)operationName
                                withPIMR_Model:(id<PIMR_Model>)input {
  return PISL_LogManager_createLogEntryWithPIQ_QName_withPIQ_QName_withPIMR_Model_(portTypeName, operationName, input);
}

+ (PISL_LogEntry *)createLogEntryWithPIQ_QName:(PIQ_QName *)portTypeName
                                 withPIQ_QName:(PIQ_QName *)operationName
                                withPIMR_Model:(id<PIMR_Model>)input
                                 withPIQ_QName:(PIQ_QName *)messageName {
  return PISL_LogManager_createLogEntryWithPIQ_QName_withPIQ_QName_withPIMR_Model_withPIQ_QName_(portTypeName, operationName, input, messageName);
}

- (NSString *)getTargetID {
  if (targetID_ == nil) return nil;
  else return [targetID_ getName];
}

- (PIQ_QName *)getTargetQName {
  return targetID_;
}

- (jint)getSeqReceived {
  return [((JavaLangInteger *) nil_chk([((PISL_LogState *) nil_chk(logStore_)) getReceived])) intValue];
}

- (jint)getSeqSent {
  return [((JavaLangInteger *) nil_chk([((PISL_LogState *) nil_chk(logStore_)) getSent])) intValue];
}

- (jint)getSeqNext {
  return [((JavaLangInteger *) nil_chk([((PISL_LogState *) nil_chk(logStore_)) getNext])) intValue];
}

- (NSString *)getLastSyncTime {
  return [((PISL_LogState *) nil_chk(logStore_)) getLastSyncTime];
}

- (void)setLastSyncTimeWithNSString:(NSString *)lastSyncTime {
  [((PISL_LogState *) nil_chk(logStore_)) setLastSyncTimeWithNSString:lastSyncTime];
  PISL_LogManager_flushLogIndex(self);
}

- (NSString *)getNewSyncTime {
  return [((PISL_LogState *) nil_chk(logStore_)) getNewSyncTime];
}

- (void)setNewSyncTimeWithNSString:(NSString *)syncTime {
  [((PISL_LogState *) nil_chk(logStore_)) setNewSyncTimeWithNSString:syncTime];
  PISL_LogManager_flushLogIndex(self);
}

- (jboolean)isSynchronized {
  return [((JavaLangBoolean *) nil_chk([((PISL_LogState *) nil_chk(logStore_)) getSync])) booleanValue];
}

- (void)outOfSync {
  @synchronized(self) {
    [self removePendingLogs];
    [self resetAll];
  }
}

- (void)setInSync {
  @synchronized(self) {
    if (![self isSynchronized]) {
      [((PISL_LogState *) nil_chk(logStore_)) setSyncWithJavaLangBoolean:JreLoadStatic(PIME_BooleanType, TRUE)];
      PISL_LogManager_flushLogIndex(self);
    }
  }
}

- (PISL_LogState *)createLogStateWithBoolean:(jboolean)includeRepeat {
  @synchronized(self) {
    PISL_LogState *state = (PISL_LogState *) cast_chk([((PISL_LogState *) nil_chk(logStore_)) cloneWithPIQ_Key:[logStore_ key] withBoolean:true withBoolean:false], [PISL_LogState class]);
    if (targetID_ != nil) {
      [((PISL_LogState *) nil_chk(state)) setLastSyncTimeWithNSString:[state getNewSyncTime]];
      [state setNewSyncTimeWithNSString:nil];
    }
    if (!includeRepeat) {
      [((PISL_LogState *) nil_chk(state)) setRepeatNameWithNSString:nil];
      [state setRepeatIndexWithJavaLangInteger:nil];
    }
    return JreRetainedLocalValue(state);
  }
}

- (void)addLogWithPISL_LogEntry:(PISL_LogEntry *)serviceLog {
  @synchronized(self) {
    JavaLangInteger *seqInt = [((PISL_LogState *) nil_chk(logStore_)) getNext];
    NSString *resourceName = PISL_LogManager_createResourceNameWithInt_(self, [((JavaLangInteger *) nil_chk(seqInt)) intValue]);
    [((PISL_LogEntry *) nil_chk(serviceLog)) setSEQWithJavaLangInteger:seqInt];
    [((PIMX_XmlWriter *) nil_chk(xmlWriter_)) writeWithNSString:resourceName withPIMR_Model:serviceLog withPIQ_QName:[((id<PIMT_Type>) nil_chk([serviceLog type])) name]];
    [((PISL_LogState *) nil_chk(logStore_)) setNextWithJavaLangInteger:new_JavaLangInteger_initWithInt_([seqInt intValue] + 1)];
    PISL_LogManager_flushLogIndex(self);
    if (PICL_Logger_getLogLevel() == JreLoadEnum(PICL_Level, DEBUG)) {
      JavaLangStringBuilder *buf = [((PIMX_XmlWriter *) nil_chk(xmlWriter_)) writeXMLWithPIMR_Model:serviceLog withPIQ_QName:[((id<PIMT_Type>) nil_chk([serviceLog type])) name]];
      PICL_Logger_debugWithNSString_(JreStrcat("$@$@", @"Wrote log for targetID=\"", self->targetID_, @"\":\n", buf));
    }
  }
}

- (jboolean)hasPendingLogs {
  @synchronized(self) {
    return (((IOSObjectArray *) nil_chk(PISL_LogManager_getPendingLogNames(self)))->size_ > 0);
  }
}

- (JavaUtilVector *)getPendingLogs {
  @synchronized(self) {
    JavaIoInputStream *in = nil;
    JavaUtilVector *logs = new_JavaUtilVector_init();
    PIMX_XmlReader *reader = new_PIMX_XmlReader_init();
    IOSObjectArray *resources = PISL_LogManager_getPendingLogNames(self);
    for (jint i = 0; i < ((IOSObjectArray *) nil_chk(resources))->size_; i++) {
      @try {
        in = [((PISYSB_Storage *) nil_chk(storage_)) readWithNSString:IOSObjectArray_Get(resources, i)];
        [logs addElementWithId:[reader loadDataWithJavaIoInputStream:in withPICO_ProgressListener:nil]];
      }
      @finally {
        PICM_StreamHelper_closeWithJavaIoInputStream_(in);
      }
    }
    return JreRetainedLocalValue(logs);
  }
}

- (void)confirmProcessingWithInt:(jint)seqNumber {
  @synchronized(self) {
    [((PISL_LogState *) nil_chk(logStore_)) setReceivedWithJavaLangInteger:new_JavaLangInteger_initWithInt_(seqNumber)];
    PISL_LogManager_flushLogIndex(self);
  }
}

- (void)confirmReceivalWithInt:(jint)number {
  @synchronized(self) {
    NSString *filename;
    @try {
      PISL_LogManager_setSeqSentWithInt_(self, number);
    }
    @catch (JavaIoIOException *e) {
      PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not flush log index"), e);
      return;
    }
    PISL_LogManager_removeLogsWithInt_withInt_(self, [self getSeqSent] + 1, [self getSeqNext] - 1);
  }
}

- (void)removeLogsWithInt:(jint)keepFromIndex
                  withInt:(jint)keepToIndex {
  PISL_LogManager_removeLogsWithInt_withInt_(self, keepFromIndex, keepToIndex);
}

- (jint)extractSeqNumberWithNSString:(NSString *)filename {
  return PISL_LogManager_extractSeqNumberWithNSString_(self, filename);
}

- (IOSObjectArray *)getPendingLogNames {
  return PISL_LogManager_getPendingLogNames(self);
}

- (NSString *)createResourceNameWithInt:(jint)seq {
  return PISL_LogManager_createResourceNameWithInt_(self, seq);
}

- (void)setSeqSentWithInt:(jint)number {
  PISL_LogManager_setSeqSentWithInt_(self, number);
}

- (void)checkLogStore {
  PISL_LogManager_checkLogStore(self);
}

- (void)flushLogIndex {
  PISL_LogManager_flushLogIndex(self);
}

- (void)init__ {
  PISL_LogManager_init__(self);
}

- (void)resetAll {
  @synchronized(self) {
    PICL_Logger_warnWithNSString_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" resetting all status variables."));
    id<PIMR_ModelFactory> factory = [((PIMR_ModelFactoryTable *) nil_chk(PIMR_ModelFactoryTable_getInstance())) getFactoryWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE)];
    logStore_ = (PISL_LogState *) cast_chk([((id<PIMR_ModelFactory>) nil_chk(factory)) createInstanceWithPIQ_Key:[((id<PIMT_ComplexType>) nil_chk(JreLoadStatic(PISL_LogState, TYPE))) name] withPIMT_Type:JreLoadStatic(PISL_LogState, TYPE) withNSObjectArray:nil withJavaUtilHashtable:nil withPIMR_ChildList:nil], [PISL_LogState class]);
    [((PIQ_NamespaceTable *) nil_chk([((PISL_LogState *) nil_chk(logStore_)) namespaceTable])) addNamespaceWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE) withNSString:@"log"];
    [((PISL_LogState *) nil_chk(logStore_)) setSyncWithJavaLangBoolean:JreLoadStatic(PIME_BooleanType, FALSE)];
    [((PISL_LogState *) nil_chk(logStore_)) setSentWithJavaLangInteger:new_JavaLangInteger_initWithInt_(-1)];
    [((PISL_LogState *) nil_chk(logStore_)) setReceivedWithJavaLangInteger:new_JavaLangInteger_initWithInt_(-1)];
    [((PISL_LogState *) nil_chk(logStore_)) setNextWithJavaLangInteger:new_JavaLangInteger_initWithInt_(0)];
    [((PISL_LogState *) nil_chk(logStore_)) setLastSyncTimeWithNSString:nil];
    [((PISL_LogState *) nil_chk(logStore_)) setNewSyncTimeWithNSString:nil];
    [((PISL_LogState *) nil_chk(logStore_)) setRepeatNameWithNSString:nil];
    [((PISL_LogState *) nil_chk(logStore_)) setRepeatIndexWithJavaLangInteger:nil];
    @try {
      PISL_LogManager_flushLogIndex(self);
    }
    @catch (JavaIoIOException *e) {
      PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not flush log index"), e);
    }
  }
}

- (void)removePendingLogs {
  @synchronized(self) {
    [((PISL_LogState *) nil_chk(logStore_)) setNextWithJavaLangInteger:new_JavaLangInteger_initWithInt_([self getSeqSent] + 1)];
    [((PISL_LogState *) nil_chk(logStore_)) setLastSyncTimeWithNSString:nil];
    @try {
      PISL_LogManager_flushLogIndex(self);
      PISL_LogManager_removeLogsWithInt_withInt_(self, [self getSeqSent] + 1, [self getSeqNext] - 1);
    }
    @catch (JavaIoIOException *e) {
      PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not flush log index"), e);
    }
  }
}

- (void)removeTarget {
  @synchronized(self) {
    PICL_Logger_infoWithNSString_(JreStrcat("$@", @"Removing LogManager and all logs for target ID=", self->targetID_));
    [self removePendingLogs];
    (void) [((JavaUtilHashMap *) nil_chk(PISL_LogManager_instances)) removeWithId:self->targetID_];
    [((PISYSB_Storage *) nil_chk(storage_)) delete__WithNSString:PISL_LogManager_LOG_STORE_RESOURCE];
    PISYSB_SystemManager *sysMan = PISYSB_SystemManager_getInstance();
    if (targetID_ == nil) {
      [((PISYSB_Storage *) nil_chk([((PISYSB_SystemManager *) nil_chk(sysMan)) getStorageWithNSString:nil])) delete__WithNSString:[((PISYSB_Storage *) nil_chk(storage_)) getName]];
    }
    else {
      [((PISYSB_Storage *) nil_chk([((PISYSB_SystemManager *) nil_chk(sysMan)) getStorageWithNSString:[targetID_ getName]])) delete__WithNSString:PISL_LogManager_LOG_DIR];
      [((PISYSB_Storage *) nil_chk([sysMan getStorageWithNSString:nil])) delete__WithNSString:[((PIQ_QName *) nil_chk(targetID_)) getName]];
    }
  }
}

- (PISL_BOStateType *)getBOStateWithPIQ_QName:(PIQ_QName *)boName {
  return [((PISL_LogState *) nil_chk(logStore_)) getBOStateWithPIQ_QName:boName];
}

- (void)setBOStateWithPIQ_QName:(PIQ_QName *)boName
           withPISL_BOStateType:(PISL_BOStateType *)boState {
  @synchronized(self) {
    if (boState == nil) {
      PISL_BOStateType *oldBOState = [self getBOStateWithPIQ_QName:boName];
      if (oldBOState != nil) [((PISL_LogState *) nil_chk(logStore_)) removeBOStateWithPISL_BOStateType:oldBOState];
    }
    else {
      [((PISL_LogState *) nil_chk(logStore_)) replaceWithPIQ_QName:JreLoadStatic(PISL_LogState, ID_BOSTATE) withId:boState];
    }
    @try {
      PISL_LogManager_flushLogIndex(self);
    }
    @catch (JavaIoIOException *e) {
      PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not flush log index"), e);
    }
  }
}

- (void)setRepeatWithNSString:(NSString *)repeatName
                      withInt:(jint)repeatIndex {
  @synchronized(self) {
    [((PISL_LogState *) nil_chk(logStore_)) setRepeatNameWithNSString:repeatName];
    [((PISL_LogState *) nil_chk(logStore_)) setRepeatIndexWithJavaLangInteger:new_JavaLangInteger_initWithInt_(repeatIndex)];
    @try {
      PISL_LogManager_flushLogIndex(self);
    }
    @catch (JavaIoIOException *e) {
      PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not flush log index"), e);
    }
  }
}

- (JavaIoInputStream *)readBufferWithNSString:(NSString *)bufferName {
  @synchronized(self) {
    bufferName = JreStrcat("$$", PISL_LogManager_BUFFERFILE_PREFIX, bufferName);
    if ([((PISYSB_Storage *) nil_chk(storage_)) existsWithNSString:bufferName]) {
      return JreRetainedLocalValue([((PISYSB_Storage *) nil_chk(storage_)) readWithNSString:bufferName]);
    }
    else {
      return JreRetainedLocalValue(nil);
    }
  }
}

- (JavaIoOutputStream *)writeBufferWithNSString:(NSString *)bufferName {
  @synchronized(self) {
    bufferName = JreStrcat("$$", PISL_LogManager_BUFFERFILE_PREFIX, bufferName);
    return JreRetainedLocalValue([((PISYSB_Storage *) nil_chk(storage_)) writeWithNSString:bufferName withBoolean:true withBoolean:false]);
  }
}

- (void)removeBufferWithNSString:(NSString *)bufferName {
  bufferName = JreStrcat("$$", PISL_LogManager_BUFFERFILE_PREFIX, bufferName);
  [((PISYSB_Storage *) nil_chk(storage_)) delete__WithNSString:bufferName];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LPISL_LogManager;", 0x29, 0, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x2, -1, 2, -1, -1, -1, -1 },
    { NULL, "LJavaUtilVector;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPISL_LogEntry;", 0x9, 3, 4, -1, -1, -1, -1 },
    { NULL, "LPISL_LogEntry;", 0x9, 3, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 1, 7, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 1, 7, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, 7, -1, -1, -1 },
    { NULL, "LPISL_LogState;", 0x21, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 11, 12, 7, -1, -1, -1 },
    { NULL, "Z", 0x21, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilVector;", 0x21, -1, -1, 7, -1, -1, -1 },
    { NULL, "V", 0x21, 13, 14, 7, -1, -1, -1 },
    { NULL, "V", 0x21, 15, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 16, 17, -1, -1, -1, -1 },
    { NULL, "I", 0x2, 18, 1, -1, -1, -1, -1 },
    { NULL, "[LNSString;", 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x2, 19, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 20, 14, 7, -1, -1, -1 },
    { NULL, "V", 0x2, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x22, -1, -1, 7, -1, -1, -1 },
    { NULL, "V", 0x2, 21, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, -1, -1, 7, -1, -1, -1 },
    { NULL, "LPISL_BOStateType;", 0x1, 22, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 23, 24, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 25, 26, -1, -1, -1, -1 },
    { NULL, "LJavaIoInputStream;", 0x21, 27, 1, 7, -1, -1, -1 },
    { NULL, "LJavaIoOutputStream;", 0x21, 28, 1, 7, -1, -1, -1 },
    { NULL, "V", 0x1, 29, 1, 7, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getInstanceWithNSString:);
  methods[1].selector = @selector(initWithPIQ_QName:);
  methods[2].selector = @selector(getInstalledManagers);
  methods[3].selector = @selector(createLogEntryWithPIQ_QName:withPIQ_QName:withPIMR_Model:);
  methods[4].selector = @selector(createLogEntryWithPIQ_QName:withPIQ_QName:withPIMR_Model:withPIQ_QName:);
  methods[5].selector = @selector(getTargetID);
  methods[6].selector = @selector(getTargetQName);
  methods[7].selector = @selector(getSeqReceived);
  methods[8].selector = @selector(getSeqSent);
  methods[9].selector = @selector(getSeqNext);
  methods[10].selector = @selector(getLastSyncTime);
  methods[11].selector = @selector(setLastSyncTimeWithNSString:);
  methods[12].selector = @selector(getNewSyncTime);
  methods[13].selector = @selector(setNewSyncTimeWithNSString:);
  methods[14].selector = @selector(isSynchronized);
  methods[15].selector = @selector(outOfSync);
  methods[16].selector = @selector(setInSync);
  methods[17].selector = @selector(createLogStateWithBoolean:);
  methods[18].selector = @selector(addLogWithPISL_LogEntry:);
  methods[19].selector = @selector(hasPendingLogs);
  methods[20].selector = @selector(getPendingLogs);
  methods[21].selector = @selector(confirmProcessingWithInt:);
  methods[22].selector = @selector(confirmReceivalWithInt:);
  methods[23].selector = @selector(removeLogsWithInt:withInt:);
  methods[24].selector = @selector(extractSeqNumberWithNSString:);
  methods[25].selector = @selector(getPendingLogNames);
  methods[26].selector = @selector(createResourceNameWithInt:);
  methods[27].selector = @selector(setSeqSentWithInt:);
  methods[28].selector = @selector(checkLogStore);
  methods[29].selector = @selector(flushLogIndex);
  methods[30].selector = @selector(init__);
  methods[31].selector = @selector(resetAll);
  methods[32].selector = @selector(removePendingLogs);
  methods[33].selector = @selector(removeTarget);
  methods[34].selector = @selector(getBOStateWithPIQ_QName:);
  methods[35].selector = @selector(setBOStateWithPIQ_QName:withPISL_BOStateType:);
  methods[36].selector = @selector(setRepeatWithNSString:withInt:);
  methods[37].selector = @selector(readBufferWithNSString:);
  methods[38].selector = @selector(writeBufferWithNSString:);
  methods[39].selector = @selector(removeBufferWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "DEBUG", "Z", .constantValue.asBOOL = PISL_LogManager_DEBUG, 0x1a, -1, -1, -1, -1 },
    { "instances", "LJavaUtilHashMap;", .constantValue.asLong = 0, 0xa, -1, 30, 31, -1 },
    { "defaultInstance", "LPISL_LogManager;", .constantValue.asLong = 0, 0xa, -1, 32, -1, -1 },
    { "targetID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "LOG_DIR", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 33, -1, -1 },
    { "LOG_STORE_RESOURCE", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "LOGFILE_PREFIX", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 35, -1, -1 },
    { "BUFFERFILE_PREFIX", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "LOGFILE_EXTENSION", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "KEY_PROCESSED_SEQ", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "KEY_LAST_SYNC_TIME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 39, -1, -1 },
    { "OUT_OF_SYNC", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 40, -1, -1 },
    { "logStore_", "LPISL_LogState;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "storage_", "LPISYSB_Storage;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "xmlWriter_", "LPIMX_XmlWriter;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "SERVICE_LOG_MANAGER", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 41, -1, -1 },
    { "OP_SYNC", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 42, -1, -1 },
  };
  static const void *ptrTable[] = { "getInstance", "LNSString;", "LPIQ_QName;", "createLogEntry", "LPIQ_QName;LPIQ_QName;LPIMR_Model;", "LPIQ_QName;LPIQ_QName;LPIMR_Model;LPIQ_QName;", "setLastSyncTime", "LJavaIoIOException;", "setNewSyncTime", "createLogState", "Z", "addLog", "LPISL_LogEntry;", "confirmProcessing", "I", "confirmReceival", "removeLogs", "II", "extractSeqNumber", "createResourceName", "setSeqSent", "init", "getBOState", "setBOState", "LPIQ_QName;LPISL_BOStateType;", "setRepeat", "LNSString;I", "readBuffer", "writeBuffer", "removeBuffer", &PISL_LogManager_instances, "Ljava/util/HashMap<Lde/pidata/qnames/QName;Lde/pidata/service/log/LogManager;>;", &PISL_LogManager_defaultInstance, &PISL_LogManager_LOG_DIR, &PISL_LogManager_LOG_STORE_RESOURCE, &PISL_LogManager_LOGFILE_PREFIX, &PISL_LogManager_BUFFERFILE_PREFIX, &PISL_LogManager_LOGFILE_EXTENSION, &PISL_LogManager_KEY_PROCESSED_SEQ, &PISL_LogManager_KEY_LAST_SYNC_TIME, &PISL_LogManager_OUT_OF_SYNC, &PISL_LogManager_SERVICE_LOG_MANAGER, &PISL_LogManager_OP_SYNC };
  static const J2ObjcClassInfo _PISL_LogManager = { "LogManager", "de.pidata.service.log", ptrTable, methods, fields, 7, 0x1, 40, 17, -1, -1, -1, -1, -1 };
  return &_PISL_LogManager;
}

+ (void)initialize {
  if (self == [PISL_LogManager class]) {
    PISL_LogManager_instances = new_JavaUtilHashMap_init();
    PISL_LogManager_KEY_PROCESSED_SEQ = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PISL_LogFactory, NAMESPACE))) getQNameWithNSString:@"processedSeq"];
    PISL_LogManager_KEY_LAST_SYNC_TIME = [JreLoadStatic(PISL_LogFactory, NAMESPACE) getQNameWithNSString:@"lastSyncTime"];
    PISL_LogManager_OP_SYNC = [JreLoadStatic(PISL_LogFactory, NAMESPACE) getQNameWithNSString:@"sync"];
    J2OBJC_SET_INITIALIZED(PISL_LogManager)
  }
}

@end

PISL_LogManager *PISL_LogManager_getInstanceWithNSString_(NSString *targetID) {
  PISL_LogManager_initialize();
  @synchronized(PISL_LogManager_class_()) {
    if (targetID == nil) {
      if (PISL_LogManager_defaultInstance == nil) {
        PISL_LogManager_defaultInstance = new_PISL_LogManager_initWithPIQ_QName_(nil);
      }
      return JreRetainedLocalValue(PISL_LogManager_defaultInstance);
    }
    else {
      jint pos = [targetID java_lastIndexOf:':'];
      if (pos >= 0) {
        targetID = [targetID java_substring:pos + 1];
      }
      PIQ_QName *qTargetID = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PISL_LogFactory, NAMESPACE))) getQNameWithNSString:targetID];
      PISL_LogManager *result = [((JavaUtilHashMap *) nil_chk(PISL_LogManager_instances)) getWithId:qTargetID];
      if (result == nil) {
        result = new_PISL_LogManager_initWithPIQ_QName_(qTargetID);
        (void) [((JavaUtilHashMap *) nil_chk(PISL_LogManager_instances)) putWithId:qTargetID withId:result];
      }
      return JreRetainedLocalValue(result);
    }
  }
}

void PISL_LogManager_initWithPIQ_QName_(PISL_LogManager *self, PIQ_QName *targetID) {
  NSObject_init(self);
  self->targetID_ = targetID;
  PISL_LogManager_init__(self);
}

PISL_LogManager *new_PISL_LogManager_initWithPIQ_QName_(PIQ_QName *targetID) {
  J2OBJC_NEW_IMPL(PISL_LogManager, initWithPIQ_QName_, targetID)
}

PISL_LogManager *create_PISL_LogManager_initWithPIQ_QName_(PIQ_QName *targetID) {
  J2OBJC_CREATE_IMPL(PISL_LogManager, initWithPIQ_QName_, targetID)
}

JavaUtilVector *PISL_LogManager_getInstalledManagers() {
  PISL_LogManager_initialize();
  JavaUtilVector *managers = new_JavaUtilVector_init();
  for (PISL_LogManager * __strong logMgr in nil_chk([((JavaUtilHashMap *) nil_chk(PISL_LogManager_instances)) values])) {
    [managers addElementWithId:logMgr];
  }
  return managers;
}

PISL_LogEntry *PISL_LogManager_createLogEntryWithPIQ_QName_withPIQ_QName_withPIMR_Model_(PIQ_QName *portTypeName, PIQ_QName *operationName, id<PIMR_Model> input) {
  PISL_LogManager_initialize();
  id<PIMR_ModelFactory> logFactory = [((PIMR_ModelFactoryTable *) nil_chk(PIMR_ModelFactoryTable_getInstance())) getFactoryWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE)];
  PISL_LogEntry *logEntry = (PISL_LogEntry *) cast_chk([((id<PIMR_ModelFactory>) nil_chk(logFactory)) createInstanceWithPIMT_ComplexType:JreLoadStatic(PISL_LogEntry, TYPE)], [PISL_LogEntry class]);
  [((PIQ_NamespaceTable *) nil_chk([((PISL_LogEntry *) nil_chk(logEntry)) namespaceTable])) addNamespaceWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE) withNSString:@"log"];
  [logEntry setServiceWithPIQ_QName:portTypeName];
  [logEntry setOperationWithPIQ_QName:operationName];
  PISL_MessageType *inputMsg = (PISL_MessageType *) cast_chk([logFactory createInstanceWithPIMT_ComplexType:JreLoadStatic(PISL_MessageType, TYPE)], [PISL_MessageType class]);
  [((PISL_MessageType *) nil_chk(inputMsg)) addWithPIQ_QName:[((id<PIMR_Model>) nil_chk(input)) getParentRelationID] withPIMR_Model:input];
  [logEntry setInputWithPISL_MessageType:inputMsg];
  return logEntry;
}

PISL_LogEntry *PISL_LogManager_createLogEntryWithPIQ_QName_withPIQ_QName_withPIMR_Model_withPIQ_QName_(PIQ_QName *portTypeName, PIQ_QName *operationName, id<PIMR_Model> input, PIQ_QName *messageName) {
  PISL_LogManager_initialize();
  id<PIMR_ModelFactory> logFactory = [((PIMR_ModelFactoryTable *) nil_chk(PIMR_ModelFactoryTable_getInstance())) getFactoryWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE)];
  PISL_LogEntry *logEntry = (PISL_LogEntry *) cast_chk([((id<PIMR_ModelFactory>) nil_chk(logFactory)) createInstanceWithPIMT_ComplexType:JreLoadStatic(PISL_LogEntry, TYPE)], [PISL_LogEntry class]);
  [((PIQ_NamespaceTable *) nil_chk([((PISL_LogEntry *) nil_chk(logEntry)) namespaceTable])) addNamespaceWithPIQ_Namespace:JreLoadStatic(PISL_LogFactory, NAMESPACE) withNSString:@"log"];
  [logEntry setServiceWithPIQ_QName:portTypeName];
  [logEntry setOperationWithPIQ_QName:operationName];
  PISL_MessageType *inputMsg = (PISL_MessageType *) cast_chk([logFactory createInstanceWithPIMT_ComplexType:JreLoadStatic(PISL_MessageType, TYPE)], [PISL_MessageType class]);
  [((PISL_MessageType *) nil_chk(inputMsg)) addWithPIQ_QName:messageName withPIMR_Model:input];
  [logEntry setInputWithPISL_MessageType:inputMsg];
  return logEntry;
}

void PISL_LogManager_removeLogsWithInt_withInt_(PISL_LogManager *self, jint keepFromIndex, jint keepToIndex) {
  NSString *filename;
  jint number;
  IOSObjectArray *names;
  @try {
    names = [((PISYSB_Storage *) nil_chk(self->storage_)) list];
  }
  @catch (JavaIoIOException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not read log directory"), e);
    return;
  }
  for (jint i = 0; i < ((IOSObjectArray *) nil_chk(names))->size_; i++) {
    filename = IOSObjectArray_Get(names, i);
    number = PISL_LogManager_extractSeqNumberWithNSString_(self, filename);
    if (number >= 0) {
      if ((number < keepFromIndex) || (number > keepToIndex)) {
        @try {
          [((PISYSB_Storage *) nil_chk(self->storage_)) delete__WithNSString:filename];
        }
        @catch (JavaIoIOException *e) {
          PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$$", @"LogManager ID=", self->targetID_, @" could not delete log file: ", filename), e);
        }
      }
    }
  }
}

jint PISL_LogManager_extractSeqNumberWithNSString_(PISL_LogManager *self, NSString *filename) {
  if ([((NSString *) nil_chk(filename)) java_hasPrefix:PISL_LogManager_LOGFILE_PREFIX]) {
    return JavaLangInteger_parseIntWithNSString_([filename java_substring:[((NSString *) nil_chk(PISL_LogManager_LOGFILE_PREFIX)) java_length] endIndex:[filename java_length] - [((NSString *) nil_chk(PISL_LogManager_LOGFILE_EXTENSION)) java_length]]);
  }
  else {
    return -1;
  }
}

IOSObjectArray *PISL_LogManager_getPendingLogNames(PISL_LogManager *self) {
  jint next = [self getSeqNext];
  jint sent = [self getSeqSent];
  IOSObjectArray *resources = [IOSObjectArray newArrayWithLength:next - (sent + 1) type:NSString_class_()];
  jint number;
  NSString *filename;
  IOSObjectArray *names;
  @try {
    names = [((PISYSB_Storage *) nil_chk(self->storage_)) list];
  }
  @catch (JavaIoIOException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not read log directory"), e);
    return nil;
  }
  if (names != nil) {
    for (jint i = 0; i < names->size_; i++) {
      filename = IOSObjectArray_Get(names, i);
      number = PISL_LogManager_extractSeqNumberWithNSString_(self, filename);
      if (number >= 0) {
        if ((number > [self getSeqSent]) && (number < [self getSeqNext])) {
          (void) IOSObjectArray_Set(resources, number - (sent + 1), filename);
        }
      }
    }
  }
  return resources;
}

NSString *PISL_LogManager_createResourceNameWithInt_(PISL_LogManager *self, jint seq) {
  return JreStrcat("$I$", PISL_LogManager_LOGFILE_PREFIX, seq, PISL_LogManager_LOGFILE_EXTENSION);
}

void PISL_LogManager_setSeqSentWithInt_(PISL_LogManager *self, jint number) {
  if (number > [self getSeqSent]) {
    [((PISL_LogState *) nil_chk(self->logStore_)) setSentWithJavaLangInteger:new_JavaLangInteger_initWithInt_(number)];
    if ([self getSeqNext] <= number) {
      [((PISL_LogState *) nil_chk(self->logStore_)) setNextWithJavaLangInteger:new_JavaLangInteger_initWithInt_(number + 1)];
    }
    PISL_LogManager_flushLogIndex(self);
  }
}

void PISL_LogManager_checkLogStore(PISL_LogManager *self) {
  jint sent = [self getSeqSent];
  jint num;
  IOSObjectArray *pending = PISL_LogManager_getPendingLogNames(self);
  for (jint i = 0; i < ((IOSObjectArray *) nil_chk(pending))->size_; i++) {
    if ((IOSObjectArray_Get(pending, i) == nil) || ([((NSString *) nil_chk(IOSObjectArray_Get(pending, i))) java_length] == 0)) {
      num = sent + 1 + i;
      @throw new_JavaLangRuntimeException_initWithNSString_(JreStrcat("$I", @"LogStore is inkonsistent: missing seqNr=", num));
    }
  }
}

void PISL_LogManager_flushLogIndex(PISL_LogManager *self) {
  @synchronized(self) {
    PISL_LogManager_checkLogStore(self);
    [((PIMX_XmlWriter *) nil_chk(self->xmlWriter_)) writeWithNSString:PISL_LogManager_LOG_STORE_RESOURCE withPIMR_Model:self->logStore_ withPIQ_QName:[((id<PIMT_Type>) nil_chk([((PISL_LogState *) nil_chk(self->logStore_)) type])) name]];
  }
}

void PISL_LogManager_init__(PISL_LogManager *self) {
  PIMX_XmlReader *reader;
  JavaIoInputStream *in = nil;
  NSString *path;
  @try {
    if (self->targetID_ == nil) path = PISL_LogManager_LOG_DIR;
    else path = JreStrcat("$C$", [self->targetID_ getName], '/', PISL_LogManager_LOG_DIR);
    self->storage_ = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getStorageWithNSString:path];
    self->xmlWriter_ = new_PIMX_XmlWriter_initWithPISYSB_Storage_(self->storage_);
    reader = new_PIMX_XmlReader_init();
    in = [((PISYSB_Storage *) nil_chk(self->storage_)) readWithNSString:PISL_LogManager_LOG_STORE_RESOURCE];
    self->logStore_ = (PISL_LogState *) cast_chk([reader loadDataWithJavaIoInputStream:in withPICO_ProgressListener:nil], [PISL_LogState class]);
  }
  @catch (JavaLangException *e) {
    PICL_Logger_warnWithNSString_(JreStrcat("$@$", @"LogManager ID=", self->targetID_, @" could not read log index, using initial values."));
    [self resetAll];
  }
  @finally {
    PICM_StreamHelper_closeWithJavaIoInputStream_(in);
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PISL_LogManager)

J2OBJC_NAME_MAPPING(PISL_LogManager, "de.pidata.service.log", "PISL_")
