//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-log/src/de/pidata/service/log/MessageType.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataServiceLogMessageType")
#ifdef RESTRICT_DePidataServiceLogMessageType
#define INCLUDE_ALL_DePidataServiceLogMessageType 0
#else
#define INCLUDE_ALL_DePidataServiceLogMessageType 1
#endif
#undef RESTRICT_DePidataServiceLogMessageType

#if !defined (PISL_MessageType_) && (INCLUDE_ALL_DePidataServiceLogMessageType || defined(INCLUDE_PISL_MessageType))
#define PISL_MessageType_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PISL_MessageType : PIMR_SequenceModel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)id_;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)modelID
                withNSObjectArray:(IOSObjectArray *)attributeNames
            withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
               withPIMR_ChildList:(PIMR_ChildList *)childNames;

#pragma mark Protected

- (instancetype)initWithPIQ_QName:(PIQ_QName *)modelID
             withPIMT_ComplexType:(id<PIMT_ComplexType>)type
                withNSObjectArray:(IOSObjectArray *)attributeNames
            withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
               withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
          withJavaUtilHashtable:(JavaUtilHashtable *)arg3
             withPIMR_ChildList:(PIMR_ChildList *)arg4 NS_UNAVAILABLE;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PISL_MessageType)

inline PIQ_Namespace *PISL_MessageType_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PISL_MessageType_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PISL_MessageType, NAMESPACE, PIQ_Namespace *)

inline id<PIMT_ComplexType> PISL_MessageType_get_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id<PIMT_ComplexType> PISL_MessageType_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PISL_MessageType, TYPE, id<PIMT_ComplexType>)

FOUNDATION_EXPORT void PISL_MessageType_init(PISL_MessageType *self);

FOUNDATION_EXPORT PISL_MessageType *new_PISL_MessageType_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PISL_MessageType *create_PISL_MessageType_init(void);

FOUNDATION_EXPORT void PISL_MessageType_initWithPIQ_QName_(PISL_MessageType *self, PIQ_QName *id_);

FOUNDATION_EXPORT PISL_MessageType *new_PISL_MessageType_initWithPIQ_QName_(PIQ_QName *id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PISL_MessageType *create_PISL_MessageType_initWithPIQ_QName_(PIQ_QName *id_);

FOUNDATION_EXPORT void PISL_MessageType_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PISL_MessageType *self, PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PISL_MessageType *new_PISL_MessageType_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PISL_MessageType *create_PISL_MessageType_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PISL_MessageType_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PISL_MessageType *self, PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PISL_MessageType *new_PISL_MessageType_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PISL_MessageType *create_PISL_MessageType_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PISL_MessageType)

@compatibility_alias DePidataServiceLogMessageType PISL_MessageType;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataServiceLogMessageType")
