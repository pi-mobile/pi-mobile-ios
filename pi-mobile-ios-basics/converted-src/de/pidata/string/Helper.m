//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/string/Helper.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/string/Helper.h"
#include "java/io/PrintStream.h"
#include "java/lang/Boolean.h"
#include "java/lang/Exception.h"
#include "java/lang/Integer.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/util/AbstractMap.h"
#include "java/util/ArrayList.h"
#include "java/util/Collection.h"
#include "java/util/Collections.h"
#include "java/util/HashMap.h"
#include "java/util/Iterator.h"
#include "java/util/List.h"
#include "java/util/Map.h"
#include "java/util/Properties.h"
#include "java/util/Random.h"
#include "java/util/Set.h"
#include "java/util/SortedMap.h"
#include "java/util/TreeMap.h"
#include "java/util/function/BiConsumer.h"
#include "java/util/function/IntPredicate.h"
#include "java/util/function/ObjIntConsumer.h"
#include "java/util/function/Supplier.h"
#include "java/util/regex/Matcher.h"
#include "java/util/regex/Pattern.h"
#include "java/util/stream/IntStream.h"

#if !__has_feature(objc_arc)
#error "de/pidata/string/Helper must be compiled with ARC (-fobjc-arc)"
#endif

#pragma clang diagnostic ignored "-Wprotocol"

@interface PICS_Helper_$Lambda$1 : NSObject < JavaUtilFunctionIntPredicate >

- (jboolean)testWithInt:(jint)i;

@end

J2OBJC_STATIC_INIT(PICS_Helper_$Lambda$1)

inline PICS_Helper_$Lambda$1 *PICS_Helper_$Lambda$1_get_instance(void);
static PICS_Helper_$Lambda$1 *PICS_Helper_$Lambda$1_instance;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICS_Helper_$Lambda$1, instance, PICS_Helper_$Lambda$1 *)

__attribute__((unused)) static void PICS_Helper_$Lambda$1_init(PICS_Helper_$Lambda$1 *self);

__attribute__((unused)) static PICS_Helper_$Lambda$1 *new_PICS_Helper_$Lambda$1_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static PICS_Helper_$Lambda$1 *create_PICS_Helper_$Lambda$1_init(void);

@interface PICS_Helper_$Lambda$2 : NSObject < JavaUtilFunctionSupplier >

- (id)get;

@end

J2OBJC_STATIC_INIT(PICS_Helper_$Lambda$2)

inline PICS_Helper_$Lambda$2 *PICS_Helper_$Lambda$2_get_instance(void);
static PICS_Helper_$Lambda$2 *PICS_Helper_$Lambda$2_instance;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICS_Helper_$Lambda$2, instance, PICS_Helper_$Lambda$2 *)

__attribute__((unused)) static void PICS_Helper_$Lambda$2_init(PICS_Helper_$Lambda$2 *self);

__attribute__((unused)) static PICS_Helper_$Lambda$2 *new_PICS_Helper_$Lambda$2_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static PICS_Helper_$Lambda$2 *create_PICS_Helper_$Lambda$2_init(void);

@interface PICS_Helper_$Lambda$3 : NSObject < JavaUtilFunctionObjIntConsumer >

- (void)acceptWithId:(JavaLangStringBuilder *)a
             withInt:(jint)b;

@end

J2OBJC_STATIC_INIT(PICS_Helper_$Lambda$3)

inline PICS_Helper_$Lambda$3 *PICS_Helper_$Lambda$3_get_instance(void);
static PICS_Helper_$Lambda$3 *PICS_Helper_$Lambda$3_instance;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICS_Helper_$Lambda$3, instance, PICS_Helper_$Lambda$3 *)

__attribute__((unused)) static void PICS_Helper_$Lambda$3_init(PICS_Helper_$Lambda$3 *self);

__attribute__((unused)) static PICS_Helper_$Lambda$3 *new_PICS_Helper_$Lambda$3_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static PICS_Helper_$Lambda$3 *create_PICS_Helper_$Lambda$3_init(void);

@interface PICS_Helper_$Lambda$4 : NSObject < JavaUtilFunctionBiConsumer >

- (void)acceptWithId:(JavaLangStringBuilder *)a
              withId:(JavaLangStringBuilder *)b;

@end

J2OBJC_STATIC_INIT(PICS_Helper_$Lambda$4)

inline PICS_Helper_$Lambda$4 *PICS_Helper_$Lambda$4_get_instance(void);
static PICS_Helper_$Lambda$4 *PICS_Helper_$Lambda$4_instance;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PICS_Helper_$Lambda$4, instance, PICS_Helper_$Lambda$4 *)

__attribute__((unused)) static void PICS_Helper_$Lambda$4_init(PICS_Helper_$Lambda$4 *self);

__attribute__((unused)) static PICS_Helper_$Lambda$4 *new_PICS_Helper_$Lambda$4_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static PICS_Helper_$Lambda$4 *create_PICS_Helper_$Lambda$4_init(void);

@implementation PICS_Helper

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PICS_Helper_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (NSString *)getNonbreakingStringWithNSString:(NSString *)string {
  return PICS_Helper_getNonbreakingStringWithNSString_(string);
}

+ (id<JavaUtilIterator>)sortIteratorWithJavaUtilIterator:(id<JavaUtilIterator>)iter {
  return PICS_Helper_sortIteratorWithJavaUtilIterator_(iter);
}

+ (NSString *)removeWhitespaceWithNSString:(NSString *)text {
  return PICS_Helper_removeWhitespaceWithNSString_(text);
}

+ (jboolean)equalsIgnoreWhitespaceWithNSString:(NSString *)str1
                                  withNSString:(NSString *)str2 {
  return PICS_Helper_equalsIgnoreWhitespaceWithNSString_withNSString_(str1, str2);
}

+ (NSString *)sanitizeFilenameWithNSString:(NSString *)origFilename
                               withBoolean:(jboolean)keepExtensionSeparator {
  return PICS_Helper_sanitizeFilenameWithNSString_withBoolean_(origFilename, keepExtensionSeparator);
}

+ (NSString *)convertToValidNameWithNSString:(NSString *)name {
  return PICS_Helper_convertToValidNameWithNSString_(name);
}

+ (jboolean)matchesTextWithNSString:(NSString *)currentText
                       withNSString:(NSString *)expectedText {
  return PICS_Helper_matchesTextWithNSString_withNSString_(currentText, expectedText);
}

+ (jboolean)isNotNullAndNotEmptyWithId:(id)obj {
  return PICS_Helper_isNotNullAndNotEmptyWithId_(obj);
}

+ (jboolean)isNullOrEmptyWithId:(id)obj {
  return PICS_Helper_isNullOrEmptyWithId_(obj);
}

+ (jboolean)equalsWithId:(id)obj1
                  withId:(id)obj2 {
  return PICS_Helper_equalsWithId_withId_(obj1, obj2);
}

+ (jboolean)getBooleanWithId:(id)boolObject {
  return PICS_Helper_getBooleanWithId_(boolObject);
}

+ (JavaUtilAbstractMap_SimpleEntry *)splitKeyValueWithNSString:(NSString *)keyValueString
                                                  withNSString:(NSString *)separator {
  return PICS_Helper_splitKeyValueWithNSString_withNSString_(keyValueString, separator);
}

+ (id<JavaUtilList>)getIntegerArrayFromStringWithNSString:(NSString *)string
                                             withNSString:(NSString *)separator {
  return PICS_Helper_getIntegerArrayFromStringWithNSString_withNSString_(string, separator);
}

+ (NSString *)fixLineEndsWindowsWithNSString:(NSString *)text {
  return PICS_Helper_fixLineEndsWindowsWithNSString_(text);
}

+ (id<JavaUtilSortedMap>)getSortedListWithJavaUtilMap:(id<JavaUtilMap>)theDictionary {
  return PICS_Helper_getSortedListWithJavaUtilMap_(theDictionary);
}

+ (id<JavaUtilSortedMap>)getSortedStringListWithJavaUtilMap:(id<JavaUtilMap>)theDictionary {
  return PICS_Helper_getSortedStringListWithJavaUtilMap_(theDictionary);
}

+ (NSString *)getRandomizedStringWithInt:(jint)targetStringLength {
  return PICS_Helper_getRandomizedStringWithInt_(targetStringLength);
}

+ (void)infoLogOnDebugWithNSString:(NSString *)message
                       withBoolean:(jboolean)debug {
  PICS_Helper_infoLogOnDebugWithNSString_withBoolean_(message, debug);
}

+ (void)debugLogOnDebugWithNSString:(NSString *)message
                        withBoolean:(jboolean)debug {
  PICS_Helper_debugLogOnDebugWithNSString_withBoolean_(message, debug);
}

+ (NSString *)removePrefixWithNSString:(NSString *)stringWithPrefix {
  return PICS_Helper_removePrefixWithNSString_(stringWithPrefix);
}

+ (NSString *)trimIfPossibleWithNSString:(NSString *)stringToTrim {
  return PICS_Helper_trimIfPossibleWithNSString_(stringToTrim);
}

+ (NSString *)replaceParamsWithNSString:(NSString *)str
                           withNSString:(NSString *)paramStart
                           withNSString:(NSString *)paramEnd
                 withJavaUtilProperties:(JavaUtilProperties *)paramProps {
  return PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_(str, paramStart, paramEnd, paramProps);
}

+ (void)addLogEntriesFromListWithJavaUtilArrayList:(JavaUtilArrayList *)logList
                                       withBoolean:(jboolean)loggingEnabled {
  PICS_Helper_addLogEntriesFromListWithJavaUtilArrayList_withBoolean_(logList, loggingEnabled);
}

+ (NSString *)getValueOrDefaultStringIfNullWithNSString:(NSString *)value {
  return PICS_Helper_getValueOrDefaultStringIfNullWithNSString_(value);
}

+ (NSString *)getValueOrDefaultStringIfNullWithNSString:(NSString *)value
                                           withNSString:(NSString *)defaultIfNull {
  return PICS_Helper_getValueOrDefaultStringIfNullWithNSString_withNSString_(value, defaultIfNull);
}

+ (void)logUsefulExceptionReasonsWithJavaLangException:(JavaLangException *)e {
  PICS_Helper_logUsefulExceptionReasonsWithJavaLangException_(e);
}

+ (NSString *)removeShapeAttributeFromATagsWithNSString:(NSString *)input {
  return PICS_Helper_removeShapeAttributeFromATagsWithNSString_(input);
}

+ (NSString *)removeAttributeFromAllTagsWithNSString:(NSString *)input
                                        withNSString:(NSString *)tagName
                                        withNSString:(NSString *)attributeName {
  return PICS_Helper_removeAttributeFromAllTagsWithNSString_withNSString_withNSString_(input, tagName, attributeName);
}

+ (NSString *)getMessageByBooleanValueWithBoolean:(jboolean)b
                                     withNSString:(NSString *)messageIfTrue
                                     withNSString:(NSString *)messageIfFalse {
  return PICS_Helper_getMessageByBooleanValueWithBoolean_withNSString_withNSString_(b, messageIfTrue, messageIfFalse);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilIterator;", 0x9, 2, 3, -1, 4, -1, -1 },
    { NULL, "LNSString;", 0x9, 5, 1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 6, 7, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 8, 9, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 10, 1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 11, 7, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 12, 13, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 14, 13, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 15, 16, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 17, 13, -1, -1, -1, -1 },
    { NULL, "LJavaUtilAbstractMap_SimpleEntry;", 0x9, 18, 7, -1, 19, -1, -1 },
    { NULL, "LJavaUtilList;", 0x9, 20, 7, -1, 21, -1, -1 },
    { NULL, "LNSString;", 0x9, 22, 1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilSortedMap;", 0x9, 23, 24, -1, 25, -1, -1 },
    { NULL, "LJavaUtilSortedMap;", 0x9, 26, 24, -1, 27, -1, -1 },
    { NULL, "LNSString;", 0x9, 28, 29, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 30, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 31, 9, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 32, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 33, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 34, 35, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 36, 37, -1, 38, -1, -1 },
    { NULL, "LNSString;", 0x9, 39, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 39, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 40, 41, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 42, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 43, 44, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 45, 46, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getNonbreakingStringWithNSString:);
  methods[2].selector = @selector(sortIteratorWithJavaUtilIterator:);
  methods[3].selector = @selector(removeWhitespaceWithNSString:);
  methods[4].selector = @selector(equalsIgnoreWhitespaceWithNSString:withNSString:);
  methods[5].selector = @selector(sanitizeFilenameWithNSString:withBoolean:);
  methods[6].selector = @selector(convertToValidNameWithNSString:);
  methods[7].selector = @selector(matchesTextWithNSString:withNSString:);
  methods[8].selector = @selector(isNotNullAndNotEmptyWithId:);
  methods[9].selector = @selector(isNullOrEmptyWithId:);
  methods[10].selector = @selector(equalsWithId:withId:);
  methods[11].selector = @selector(getBooleanWithId:);
  methods[12].selector = @selector(splitKeyValueWithNSString:withNSString:);
  methods[13].selector = @selector(getIntegerArrayFromStringWithNSString:withNSString:);
  methods[14].selector = @selector(fixLineEndsWindowsWithNSString:);
  methods[15].selector = @selector(getSortedListWithJavaUtilMap:);
  methods[16].selector = @selector(getSortedStringListWithJavaUtilMap:);
  methods[17].selector = @selector(getRandomizedStringWithInt:);
  methods[18].selector = @selector(infoLogOnDebugWithNSString:withBoolean:);
  methods[19].selector = @selector(debugLogOnDebugWithNSString:withBoolean:);
  methods[20].selector = @selector(removePrefixWithNSString:);
  methods[21].selector = @selector(trimIfPossibleWithNSString:);
  methods[22].selector = @selector(replaceParamsWithNSString:withNSString:withNSString:withJavaUtilProperties:);
  methods[23].selector = @selector(addLogEntriesFromListWithJavaUtilArrayList:withBoolean:);
  methods[24].selector = @selector(getValueOrDefaultStringIfNullWithNSString:);
  methods[25].selector = @selector(getValueOrDefaultStringIfNullWithNSString:withNSString:);
  methods[26].selector = @selector(logUsefulExceptionReasonsWithJavaLangException:);
  methods[27].selector = @selector(removeShapeAttributeFromATagsWithNSString:);
  methods[28].selector = @selector(removeAttributeFromAllTagsWithNSString:withNSString:withNSString:);
  methods[29].selector = @selector(getMessageByBooleanValueWithBoolean:withNSString:withNSString:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "getNonbreakingString", "LNSString;", "sortIterator", "LJavaUtilIterator;", "(Ljava/util/Iterator<Ljava/lang/Object;>;)Ljava/util/Iterator<Ljava/lang/Object;>;", "removeWhitespace", "equalsIgnoreWhitespace", "LNSString;LNSString;", "sanitizeFilename", "LNSString;Z", "convertToValidName", "matchesText", "isNotNullAndNotEmpty", "LNSObject;", "isNullOrEmpty", "equals", "LNSObject;LNSObject;", "getBoolean", "splitKeyValue", "(Ljava/lang/String;Ljava/lang/String;)Ljava/util/AbstractMap$SimpleEntry<Ljava/lang/String;Ljava/lang/String;>;", "getIntegerArrayFromString", "(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Ljava/lang/Integer;>;", "fixLineEndsWindows", "getSortedList", "LJavaUtilMap;", "<T:Ljava/lang/Object;>(Ljava/util/Map<Ljava/lang/Integer;TT;>;)Ljava/util/SortedMap<Ljava/lang/Integer;TT;>;", "getSortedStringList", "<T:Ljava/lang/Object;>(Ljava/util/Map<Ljava/lang/String;TT;>;)Ljava/util/SortedMap<Ljava/lang/String;TT;>;", "getRandomizedString", "I", "infoLogOnDebug", "debugLogOnDebug", "removePrefix", "trimIfPossible", "replaceParams", "LNSString;LNSString;LNSString;LJavaUtilProperties;", "addLogEntriesFromList", "LJavaUtilArrayList;Z", "(Ljava/util/ArrayList<Ljava/lang/String;>;Z)V", "getValueOrDefaultStringIfNull", "logUsefulExceptionReasons", "LJavaLangException;", "removeShapeAttributeFromATags", "removeAttributeFromAllTags", "LNSString;LNSString;LNSString;", "getMessageByBooleanValue", "ZLNSString;LNSString;" };
  static const J2ObjcClassInfo _PICS_Helper = { "Helper", "de.pidata.string", ptrTable, methods, NULL, 7, 0x1, 30, 0, -1, -1, -1, -1, -1 };
  return &_PICS_Helper;
}

@end

void PICS_Helper_init(PICS_Helper *self) {
  NSObject_init(self);
}

PICS_Helper *new_PICS_Helper_init() {
  J2OBJC_NEW_IMPL(PICS_Helper, init)
}

PICS_Helper *create_PICS_Helper_init() {
  J2OBJC_CREATE_IMPL(PICS_Helper, init)
}

NSString *PICS_Helper_getNonbreakingStringWithNSString_(NSString *string) {
  PICS_Helper_initialize();
  if (string == nil) return string;
  else return [string java_replaceAll:@"\\n" withReplacement:@""];
}

id<JavaUtilIterator> PICS_Helper_sortIteratorWithJavaUtilIterator_(id<JavaUtilIterator> iter) {
  PICS_Helper_initialize();
  id<JavaUtilList> keys = new_JavaUtilArrayList_init();
  while ([((id<JavaUtilIterator>) nil_chk(iter)) hasNext]) {
    [keys addWithId:[iter next]];
  }
  JavaUtilCollections_sortWithJavaUtilList_(keys);
  iter = [keys iterator];
  return iter;
}

NSString *PICS_Helper_removeWhitespaceWithNSString_(NSString *text) {
  PICS_Helper_initialize();
  if (text == nil) return @"";
  text = [text java_trim];
  JavaLangStringBuilder *builder = new_JavaLangStringBuilder_init();
  jboolean lastWasSpace = false;
  for (jint i = 0; i < [((NSString *) nil_chk(text)) java_length]; i++) {
    jchar ch = [text charAtWithInt:i];
    if ((ch <= ' ') || (ch == 160)) {
      if (!lastWasSpace) {
        (void) [builder appendWithChar:' '];
        lastWasSpace = true;
      }
    }
    else if (ch == 8201) {
    }
    else {
      (void) [builder appendWithChar:ch];
      lastWasSpace = false;
    }
  }
  return [builder description];
}

jboolean PICS_Helper_equalsIgnoreWhitespaceWithNSString_withNSString_(NSString *str1, NSString *str2) {
  PICS_Helper_initialize();
  NSString *tmp1 = [((NSString *) nil_chk(str1)) java_replaceAll:@"\\s" withReplacement:@""];
  NSString *tmp2 = [((NSString *) nil_chk(str2)) java_replaceAll:@"\\s" withReplacement:@""];
  return [((NSString *) nil_chk(tmp1)) isEqual:tmp2];
}

NSString *PICS_Helper_sanitizeFilenameWithNSString_withBoolean_(NSString *origFilename, jboolean keepExtensionSeparator) {
  PICS_Helper_initialize();
  if (!PICS_Helper_isNullOrEmptyWithId_(origFilename)) {
    NSString *regEx;
    if (keepExtensionSeparator) {
      regEx = @"[^a-zA-Z0-9\\.\\-]";
    }
    else {
      regEx = @"[^a-zA-Z0-9\\-]";
    }
    NSString *sanitizedFilename = [((NSString *) nil_chk(origFilename)) java_replaceAll:regEx withReplacement:@"_"];
    return sanitizedFilename;
  }
  return origFilename;
}

NSString *PICS_Helper_convertToValidNameWithNSString_(NSString *name) {
  PICS_Helper_initialize();
  if (!PICS_Helper_isNullOrEmptyWithId_(name)) {
    NSString *regEx = @"[^a-zA-Z0-9_]";
    NSString *sanitizedName = [((NSString *) nil_chk(name)) java_replaceAll:regEx withReplacement:@"_"];
    regEx = @"^[0-9]";
    if ([((NSString *) nil_chk(sanitizedName)) java_matches:regEx]) {
      sanitizedName = JreStrcat("C$", '_', sanitizedName);
    }
    return sanitizedName;
  }
  return name;
}

jboolean PICS_Helper_matchesTextWithNSString_withNSString_(NSString *currentText, NSString *expectedText) {
  PICS_Helper_initialize();
  if (currentText == nil || expectedText == nil) {
    return false;
  }
  if ([currentText isEqual:expectedText]) {
    return true;
  }
  if ([currentText java_hasSuffix:@"..."]) {
    NSString *testCurrent = [currentText java_substring:0 endIndex:[currentText java_length] - [@"..." java_length]];
    if ([expectedText java_hasSuffix:@"*"]) {
      NSString *testExpected = [expectedText java_substring:0 endIndex:[expectedText java_length] - [@"*" java_length]];
      return [((NSString *) nil_chk(testCurrent)) java_hasPrefix:testExpected];
    }
    else {
      return [expectedText java_hasPrefix:testCurrent];
    }
  }
  else {
    if ([expectedText java_hasSuffix:@"*"]) {
      NSString *testExpected = [expectedText java_substring:0 endIndex:[expectedText java_length] - [@"*" java_length]];
      return [currentText java_hasPrefix:testExpected];
    }
  }
  return false;
}

jboolean PICS_Helper_isNotNullAndNotEmptyWithId_(id obj) {
  PICS_Helper_initialize();
  return !PICS_Helper_isNullOrEmptyWithId_(obj);
}

jboolean PICS_Helper_isNullOrEmptyWithId_(id obj) {
  PICS_Helper_initialize();
  if (obj == nil) {
    return true;
  }
  else if ([obj isKindOfClass:[NSString class]]) {
    return [((NSString *) obj) java_isEmpty];
  }
  else if ([obj isKindOfClass:[JavaLangStringBuilder class]]) {
    return [((JavaLangStringBuilder *) obj) java_length] == 0;
  }
  else if ([JavaUtilCollection_class_() isInstance:obj]) {
    return [((id<JavaUtilCollection>) cast_check(obj, JavaUtilCollection_class_())) isEmpty];
  }
  else if ([obj isKindOfClass:[JavaUtilHashMap class]]) {
    return [((JavaUtilHashMap *) obj) isEmpty];
  }
  else {
    PICL_Logger_warnWithNSString_(JreStrcat("$$$@", @"isNullOrEmpty unknown for [", [[obj java_getClass] getName], @"] ", obj));
  }
  return false;
}

jboolean PICS_Helper_equalsWithId_withId_(id obj1, id obj2) {
  PICS_Helper_initialize();
  if (obj1 == nil) {
    return obj2 == nil;
  }
  else {
    return ([obj1 isEqual:obj2]);
  }
}

jboolean PICS_Helper_getBooleanWithId_(id boolObject) {
  PICS_Helper_initialize();
  if (boolObject == nil) return false;
  else if ([boolObject isKindOfClass:[JavaLangBoolean class]]) return [((JavaLangBoolean *) boolObject) booleanValue];
  else return false;
}

JavaUtilAbstractMap_SimpleEntry *PICS_Helper_splitKeyValueWithNSString_withNSString_(NSString *keyValueString, NSString *separator) {
  PICS_Helper_initialize();
  NSString *tmp = [((NSString *) nil_chk(keyValueString)) java_replaceAll:@"\\s" withReplacement:@""];
  IOSObjectArray *keyValue = [((NSString *) nil_chk(tmp)) java_split:separator];
  if (((IOSObjectArray *) nil_chk(keyValue))->size_ == 2) {
    return new_JavaUtilAbstractMap_SimpleEntry_initWithId_withId_(IOSObjectArray_Get(keyValue, 0), IOSObjectArray_Get(keyValue, 1));
  }
  return nil;
}

id<JavaUtilList> PICS_Helper_getIntegerArrayFromStringWithNSString_withNSString_(NSString *string, NSString *separator) {
  PICS_Helper_initialize();
  id<JavaUtilList> numList = new_JavaUtilArrayList_init();
  if (!PICS_Helper_isNullOrEmptyWithId_(string)) {
    IOSObjectArray *stringArray = [((NSString *) nil_chk(string)) java_split:separator];
    {
      IOSObjectArray *a__ = stringArray;
      NSString * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
      NSString * const *e__ = b__ + a__->size_;
      while (b__ < e__) {
        NSString *numString = *b__++;
        jint num = JavaLangInteger_parseIntWithNSString_(numString);
        [numList addWithId:JavaLangInteger_valueOfWithInt_(num)];
      }
    }
  }
  return numList;
}

NSString *PICS_Helper_fixLineEndsWindowsWithNSString_(NSString *text) {
  PICS_Helper_initialize();
  JavaLangStringBuilder *builder = new_JavaLangStringBuilder_initWithNSString_(text);
  for (jint i = [builder java_length] - 1; i >= 0; i--) {
    jchar ch = [builder charAtWithInt:i];
    if (ch == 0x000a) {
      if ((i == 0) || ([builder charAtWithInt:i - 1] != 0x000d)) {
        (void) [builder insertWithInt:i withChar:0x000d];
      }
    }
  }
  return [builder description];
}

id<JavaUtilSortedMap> PICS_Helper_getSortedListWithJavaUtilMap_(id<JavaUtilMap> theDictionary) {
  PICS_Helper_initialize();
  id<JavaUtilSortedMap> sortList = new_JavaUtilTreeMap_init();
  for (JavaLangInteger * __strong dictKey in nil_chk([((id<JavaUtilMap>) nil_chk(theDictionary)) keySet])) {
    (void) [sortList putWithId:dictKey withId:[theDictionary getWithId:dictKey]];
  }
  return sortList;
}

id<JavaUtilSortedMap> PICS_Helper_getSortedStringListWithJavaUtilMap_(id<JavaUtilMap> theDictionary) {
  PICS_Helper_initialize();
  id<JavaUtilSortedMap> sortList = new_JavaUtilTreeMap_init();
  for (NSString * __strong dictKey in nil_chk([((id<JavaUtilMap>) nil_chk(theDictionary)) keySet])) {
    (void) [sortList putWithId:dictKey withId:[theDictionary getWithId:dictKey]];
  }
  return sortList;
}

NSString *PICS_Helper_getRandomizedStringWithInt_(jint targetStringLength) {
  PICS_Helper_initialize();
  jint leftLimit = 48;
  jint rightLimit = 122;
  JavaUtilRandom *random = new_JavaUtilRandom_init();
  NSString *generatedString = [((JavaLangStringBuilder *) nil_chk([((id<JavaUtilStreamIntStream>) nil_chk([((id<JavaUtilStreamIntStream>) nil_chk([((id<JavaUtilStreamIntStream>) nil_chk([random intsWithInt:leftLimit withInt:rightLimit + 1])) filterWithJavaUtilFunctionIntPredicate:JreLoadStatic(PICS_Helper_$Lambda$1, instance)])) limitWithLong:targetStringLength])) collectWithJavaUtilFunctionSupplier:JreLoadStatic(PICS_Helper_$Lambda$2, instance) withJavaUtilFunctionObjIntConsumer:JreLoadStatic(PICS_Helper_$Lambda$3, instance) withJavaUtilFunctionBiConsumer:JreLoadStatic(PICS_Helper_$Lambda$4, instance)])) description];
  [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) printlnWithNSString:generatedString];
  return generatedString;
}

void PICS_Helper_infoLogOnDebugWithNSString_withBoolean_(NSString *message, jboolean debug) {
  PICS_Helper_initialize();
  if (debug) {
    PICL_Logger_infoWithNSString_(message);
  }
}

void PICS_Helper_debugLogOnDebugWithNSString_withBoolean_(NSString *message, jboolean debug) {
  PICS_Helper_initialize();
  if (debug) {
    PICL_Logger_debugWithNSString_(message);
  }
}

NSString *PICS_Helper_removePrefixWithNSString_(NSString *stringWithPrefix) {
  PICS_Helper_initialize();
  if ([((NSString *) nil_chk(stringWithPrefix)) java_contains:@"::"]) {
    stringWithPrefix = [stringWithPrefix java_substring:[stringWithPrefix java_indexOfString:@"::"] + 2];
  }
  return stringWithPrefix;
}

NSString *PICS_Helper_trimIfPossibleWithNSString_(NSString *stringToTrim) {
  PICS_Helper_initialize();
  if (stringToTrim != nil) {
    stringToTrim = [stringToTrim java_trim];
  }
  return stringToTrim;
}

NSString *PICS_Helper_replaceParamsWithNSString_withNSString_withNSString_withJavaUtilProperties_(NSString *str, NSString *paramStart, NSString *paramEnd, JavaUtilProperties *paramProps) {
  PICS_Helper_initialize();
  if (paramProps == nil) {
    return str;
  }
  if (str == nil) {
    return nil;
  }
  jint pos = [str java_indexOfString:paramStart];
  if (pos < 0) {
    return str;
  }
  jint start = 0;
  JavaLangStringBuilder *result = new_JavaLangStringBuilder_init();
  while (pos >= 0) {
    (void) [result appendWithNSString:[str java_substring:start endIndex:pos]];
    jint pos2 = [str java_indexOfString:paramEnd fromIndex:pos + 1];
    if (pos2 > 0) {
      NSString *paramName = [str java_substring:pos + [((NSString *) nil_chk(paramStart)) java_length] endIndex:pos2];
      id value = [paramProps getWithId:paramName];
      (void) [result appendWithId:value];
      start = pos2 + [((NSString *) nil_chk(paramEnd)) java_length];
    }
    else {
      start = pos + [((NSString *) nil_chk(paramStart)) java_length];
    }
    pos = [str java_indexOfString:paramStart fromIndex:start];
  }
  (void) [result appendWithNSString:[str java_substring:start]];
  return [result description];
}

void PICS_Helper_addLogEntriesFromListWithJavaUtilArrayList_withBoolean_(JavaUtilArrayList *logList, jboolean loggingEnabled) {
  PICS_Helper_initialize();
  if (loggingEnabled) {
    for (NSString * __strong s in nil_chk(logList)) {
      PICL_Logger_debugWithNSString_(s);
    }
  }
}

NSString *PICS_Helper_getValueOrDefaultStringIfNullWithNSString_(NSString *value) {
  PICS_Helper_initialize();
  return PICS_Helper_getValueOrDefaultStringIfNullWithNSString_withNSString_(value, @"");
}

NSString *PICS_Helper_getValueOrDefaultStringIfNullWithNSString_withNSString_(NSString *value, NSString *defaultIfNull) {
  PICS_Helper_initialize();
  if (PICS_Helper_isNullOrEmptyWithId_(value)) {
    if (PICS_Helper_isNotNullAndNotEmptyWithId_(defaultIfNull)) {
      return defaultIfNull;
    }
    else {
      return @"";
    }
  }
  else {
    return value;
  }
}

void PICS_Helper_logUsefulExceptionReasonsWithJavaLangException_(JavaLangException *e) {
  PICS_Helper_initialize();
  NSString *usefulMsg = @"";
  NSString *errorMsg = [((JavaLangException *) nil_chk(e)) getMessage];
  if ([((NSString *) nil_chk(errorMsg)) java_contains:@"Unauthorized"]) {
    usefulMsg = JreStrcat("$$", usefulMsg, @"got statuscode 401 - unauthorized");
  }
  else {
    usefulMsg = [e getMessage];
  }
  if (PICS_Helper_isNotNullAndNotEmptyWithId_(usefulMsg)) {
    PICL_Logger_errorWithNSString_(usefulMsg);
  }
}

NSString *PICS_Helper_removeShapeAttributeFromATagsWithNSString_(NSString *input) {
  PICS_Helper_initialize();
  return PICS_Helper_removeAttributeFromAllTagsWithNSString_withNSString_withNSString_(input, @"a", @"shape");
}

NSString *PICS_Helper_removeAttributeFromAllTagsWithNSString_withNSString_withNSString_(NSString *input, NSString *tagName, NSString *attributeName) {
  PICS_Helper_initialize();
  if (PICS_Helper_isNullOrEmptyWithId_(input)) {
    return input;
  }
  if (PICS_Helper_isNullOrEmptyWithId_(tagName)) {
    PICL_Logger_warnWithNSString_(@"removeAttributeFromAllTags tagName is null or empty - returning input as is");
    return input;
  }
  if (PICS_Helper_isNullOrEmptyWithId_(attributeName)) {
    PICL_Logger_warnWithNSString_(@"removeAttributesFromAllTags attributeName is null or empty - returning input as is");
    return input;
  }
  NSString *regex = JreStrcat("$$$$$", @"(<", tagName, @"\\b[^>]*?)\\s+", attributeName, @"=\"[^\"]*\"(.*?>)");
  JavaUtilRegexPattern *pattern = JavaUtilRegexPattern_compileWithNSString_(regex);
  JavaUtilRegexMatcher *matcher = [((JavaUtilRegexPattern *) nil_chk(pattern)) matcherWithJavaLangCharSequence:input];
  return [((JavaUtilRegexMatcher *) nil_chk(matcher)) replaceAllWithNSString:@"$1$2"];
}

NSString *PICS_Helper_getMessageByBooleanValueWithBoolean_withNSString_withNSString_(jboolean b, NSString *messageIfTrue, NSString *messageIfFalse) {
  PICS_Helper_initialize();
  if (b) {
    return messageIfTrue;
  }
  else {
    return messageIfFalse;
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PICS_Helper)

J2OBJC_NAME_MAPPING(PICS_Helper, "de.pidata.string", "PICS_")

J2OBJC_INITIALIZED_DEFN(PICS_Helper_$Lambda$1)

@implementation PICS_Helper_$Lambda$1

- (jboolean)testWithInt:(jint)i {
  return (i <= 57 || i >= 65) && (i <= 90 || i >= 97);
}

- (id<JavaUtilFunctionIntPredicate>)and__WithJavaUtilFunctionIntPredicate:(id<JavaUtilFunctionIntPredicate>)arg0 {
  return JavaUtilFunctionIntPredicate_and__WithJavaUtilFunctionIntPredicate_(self, arg0);
}

- (id<JavaUtilFunctionIntPredicate>)negate {
  return JavaUtilFunctionIntPredicate_negate(self);
}

- (id<JavaUtilFunctionIntPredicate>)or__WithJavaUtilFunctionIntPredicate:(id<JavaUtilFunctionIntPredicate>)arg0 {
  return JavaUtilFunctionIntPredicate_or__WithJavaUtilFunctionIntPredicate_(self, arg0);
}

+ (void)initialize {
  if (self == [PICS_Helper_$Lambda$1 class]) {
    PICS_Helper_$Lambda$1_instance = new_PICS_Helper_$Lambda$1_init();
    J2OBJC_SET_INITIALIZED(PICS_Helper_$Lambda$1)
  }
}

@end

void PICS_Helper_$Lambda$1_init(PICS_Helper_$Lambda$1 *self) {
  NSObject_init(self);
}

PICS_Helper_$Lambda$1 *new_PICS_Helper_$Lambda$1_init() {
  J2OBJC_NEW_IMPL(PICS_Helper_$Lambda$1, init)
}

PICS_Helper_$Lambda$1 *create_PICS_Helper_$Lambda$1_init() {
  J2OBJC_CREATE_IMPL(PICS_Helper_$Lambda$1, init)
}

J2OBJC_INITIALIZED_DEFN(PICS_Helper_$Lambda$2)

@implementation PICS_Helper_$Lambda$2

- (id)get {
  return new_JavaLangStringBuilder_init();
}

+ (void)initialize {
  if (self == [PICS_Helper_$Lambda$2 class]) {
    PICS_Helper_$Lambda$2_instance = new_PICS_Helper_$Lambda$2_init();
    J2OBJC_SET_INITIALIZED(PICS_Helper_$Lambda$2)
  }
}

@end

void PICS_Helper_$Lambda$2_init(PICS_Helper_$Lambda$2 *self) {
  NSObject_init(self);
}

PICS_Helper_$Lambda$2 *new_PICS_Helper_$Lambda$2_init() {
  J2OBJC_NEW_IMPL(PICS_Helper_$Lambda$2, init)
}

PICS_Helper_$Lambda$2 *create_PICS_Helper_$Lambda$2_init() {
  J2OBJC_CREATE_IMPL(PICS_Helper_$Lambda$2, init)
}

J2OBJC_INITIALIZED_DEFN(PICS_Helper_$Lambda$3)

@implementation PICS_Helper_$Lambda$3

- (void)acceptWithId:(JavaLangStringBuilder *)a
             withInt:(jint)b {
  (void) [((JavaLangStringBuilder *) nil_chk(a)) appendCodePointWithInt:b];
}

+ (void)initialize {
  if (self == [PICS_Helper_$Lambda$3 class]) {
    PICS_Helper_$Lambda$3_instance = new_PICS_Helper_$Lambda$3_init();
    J2OBJC_SET_INITIALIZED(PICS_Helper_$Lambda$3)
  }
}

@end

void PICS_Helper_$Lambda$3_init(PICS_Helper_$Lambda$3 *self) {
  NSObject_init(self);
}

PICS_Helper_$Lambda$3 *new_PICS_Helper_$Lambda$3_init() {
  J2OBJC_NEW_IMPL(PICS_Helper_$Lambda$3, init)
}

PICS_Helper_$Lambda$3 *create_PICS_Helper_$Lambda$3_init() {
  J2OBJC_CREATE_IMPL(PICS_Helper_$Lambda$3, init)
}

J2OBJC_INITIALIZED_DEFN(PICS_Helper_$Lambda$4)

@implementation PICS_Helper_$Lambda$4

- (void)acceptWithId:(JavaLangStringBuilder *)a
              withId:(JavaLangStringBuilder *)b {
  (void) [((JavaLangStringBuilder *) nil_chk(a)) appendWithJavaLangCharSequence:b];
}

- (id<JavaUtilFunctionBiConsumer>)andThenWithJavaUtilFunctionBiConsumer:(id<JavaUtilFunctionBiConsumer>)arg0 {
  return JavaUtilFunctionBiConsumer_andThenWithJavaUtilFunctionBiConsumer_(self, arg0);
}

+ (void)initialize {
  if (self == [PICS_Helper_$Lambda$4 class]) {
    PICS_Helper_$Lambda$4_instance = new_PICS_Helper_$Lambda$4_init();
    J2OBJC_SET_INITIALIZED(PICS_Helper_$Lambda$4)
  }
}

@end

void PICS_Helper_$Lambda$4_init(PICS_Helper_$Lambda$4 *self) {
  NSObject_init(self);
}

PICS_Helper_$Lambda$4 *new_PICS_Helper_$Lambda$4_init() {
  J2OBJC_NEW_IMPL(PICS_Helper_$Lambda$4, init)
}

PICS_Helper_$Lambda$4 *create_PICS_Helper_$Lambda$4_init() {
  J2OBJC_CREATE_IMPL(PICS_Helper_$Lambda$4, init)
}
