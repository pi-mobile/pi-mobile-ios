//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/system/pi-system-base/src/de/pidata/system/base/WifiNetwork.java
//

#include "J2ObjC_source.h"
#include "de/pidata/system/base/WifiNetwork.h"

#if !__has_feature(objc_arc)
#error "de/pidata/system/base/WifiNetwork must be compiled with ARC (-fobjc-arc)"
#endif

@interface PISYSB_WifiNetwork () {
 @public
  jboolean inUse_;
  NSString *SSID_;
  NSString *mode_;
  jint channel_;
  jint rate_;
  jint signalStrength_;
  NSString *security_;
}

@end

J2OBJC_FIELD_SETTER(PISYSB_WifiNetwork, SSID_, NSString *)
J2OBJC_FIELD_SETTER(PISYSB_WifiNetwork, mode_, NSString *)
J2OBJC_FIELD_SETTER(PISYSB_WifiNetwork, security_, NSString *)

@implementation PISYSB_WifiNetwork

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PISYSB_WifiNetwork_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "inUse_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "SSID_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "mode_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "channel_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "rate_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "signalStrength_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "security_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const J2ObjcClassInfo _PISYSB_WifiNetwork = { "WifiNetwork", "de.pidata.system.base", NULL, methods, fields, 7, 0x1, 1, 7, -1, -1, -1, -1, -1 };
  return &_PISYSB_WifiNetwork;
}

@end

void PISYSB_WifiNetwork_init(PISYSB_WifiNetwork *self) {
  NSObject_init(self);
}

PISYSB_WifiNetwork *new_PISYSB_WifiNetwork_init() {
  J2OBJC_NEW_IMPL(PISYSB_WifiNetwork, init)
}

PISYSB_WifiNetwork *create_PISYSB_WifiNetwork_init() {
  J2OBJC_CREATE_IMPL(PISYSB_WifiNetwork, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PISYSB_WifiNetwork)

J2OBJC_NAME_MAPPING(PISYSB_WifiNetwork, "de.pidata.system.base", "PISYSB_")
