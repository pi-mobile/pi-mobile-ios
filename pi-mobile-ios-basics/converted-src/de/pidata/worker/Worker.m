//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/worker/Worker.java
//

#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/worker/Worker.h"
#include "de/pidata/worker/WorkerJob.h"
#include "de/pidata/worker/WorkerListener.h"
#include "de/pidata/worker/WorkerThread.h"
#include "java/lang/Exception.h"
#include "java/lang/InterruptedException.h"
#include "java/lang/Thread.h"
#include "java/util/LinkedList.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/worker/Worker must be compiled with ARC (-fobjc-arc)"
#endif

@interface PICW_Worker () {
 @public
  id<JavaUtilList> jobList_;
  jboolean running_;
  jboolean working_;
  id<JavaUtilList> listeners_;
}

@end

J2OBJC_FIELD_SETTER(PICW_Worker, jobList_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(PICW_Worker, listeners_, id<JavaUtilList>)

inline jboolean PICW_Worker_get_DEBUG(void);
#define PICW_Worker_DEBUG false
J2OBJC_STATIC_FIELD_CONSTANT(PICW_Worker, DEBUG, jboolean)

@implementation PICW_Worker

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PICW_Worker_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)addJobWithPICW_WorkerJob:(PICW_WorkerJob *)workerJob {
  @synchronized(self) {
    [((id<JavaUtilList>) nil_chk(self->jobList_)) addWithId:workerJob];
  }
}

- (void)close {
  working_ = false;
}

- (jboolean)isRunning {
  return running_;
}

- (void)starting {
}

- (void)stopping {
}

- (void)jobFinishedWithPICW_WorkerJob:(PICW_WorkerJob *)job
                          withBoolean:(jboolean)success {
}

- (void)jobErrorWithPICW_WorkerJob:(PICW_WorkerJob *)job
             withJavaLangException:(JavaLangException *)ex {
  PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$$", @"Exception running job name=", [((PICW_WorkerJob *) nil_chk(job)) getName]), ex);
}

- (void)run {
  running_ = true;
  working_ = true;
  PICW_WorkerThread *workerThread = (PICW_WorkerThread *) cast_chk(JavaLangThread_currentThread(), [PICW_WorkerThread class]);
  @try {
    [self starting];
    while (working_) {
      PICW_WorkerJob *job = nil;
      @synchronized(self) {
        if ([((id<JavaUtilList>) nil_chk(jobList_)) size] > 0) {
          job = JreRetainedLocalValue([((id<JavaUtilList>) nil_chk(jobList_)) removeWithInt:0]);
        }
      }
      if (job == nil) {
        [self idle];
        for (id<PICW_WorkerListener> __strong listener in nil_chk(listeners_)) {
          @try {
            [((id<PICW_WorkerListener>) nil_chk(listener)) workerStateIdleWithPICW_Worker:self];
          }
          @catch (JavaLangException *e) {
            [e printStackTrace];
            PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Exception while firing worker event: Idle", e);
          }
        }
      }
      else {
        jboolean success = false;
        for (id<PICW_WorkerListener> __strong listener in nil_chk(listeners_)) {
          @try {
            [((id<PICW_WorkerListener>) nil_chk(listener)) workerStateStartingWithPICW_Worker:self withPICW_WorkerJob:job];
          }
          @catch (JavaLangException *e) {
            [e printStackTrace];
            PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Exception while firing worker event: starting", e);
          }
        }
        [((PICW_WorkerThread *) nil_chk(workerThread)) setJobWithPICW_WorkerJob:job];
        @try {
          [job execute];
          success = true;
        }
        @catch (JavaLangException *ex) {
          [self jobErrorWithPICW_WorkerJob:job withJavaLangException:ex];
        }
        [self jobFinishedWithPICW_WorkerJob:job withBoolean:success];
        [workerThread setJobWithPICW_WorkerJob:nil];
        for (id<PICW_WorkerListener> __strong listener in nil_chk(listeners_)) {
          @try {
            [((id<PICW_WorkerListener>) nil_chk(listener)) workerStateFinishedWithPICW_Worker:self withPICW_WorkerJob:job];
          }
          @catch (JavaLangException *e) {
            [e printStackTrace];
            PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Exception while firing worker event: finished", e);
          }
        }
      }
    }
    [self stopping];
  }
  @finally {
    running_ = false;
  }
}

- (void)idle {
  @try {
    JavaLangThread_sleepWithLong_(100);
  }
  @catch (JavaLangInterruptedException *e) {
  }
}

- (jboolean)addJobIfIdleWithPICW_WorkerJob:(PICW_WorkerJob *)workerJob {
  @synchronized(self) {
    if ([((id<JavaUtilList>) nil_chk(jobList_)) size] == 0) {
      [self addJobWithPICW_WorkerJob:workerJob];
      return true;
    }
    else if ([((id<JavaUtilList>) nil_chk(jobList_)) size] == 1 && [((NSString *) nil_chk([((PICW_WorkerJob *) nil_chk([((id<JavaUtilList>) nil_chk(jobList_)) getWithInt:0])) getName])) isEqual:[((PICW_WorkerJob *) nil_chk(workerJob)) getName]]) {
      return true;
    }
    else {
      return false;
    }
  }
}

- (void)addWorkerListenerWithPICW_WorkerListener:(id<PICW_WorkerListener>)workerListener {
  if (self->listeners_ == nil) {
    self->listeners_ = new_JavaUtilLinkedList_init();
  }
  [self->listeners_ addWithId:workerListener];
}

- (void)removeWorkerListenerWithPICW_WorkerListener:(id<PICW_WorkerListener>)workerListener {
  if (self->listeners_ != nil) {
    [self->listeners_ removeWithId:workerListener];
  }
}

- (jint)jobCount {
  @synchronized(self) {
    return [((id<JavaUtilList>) nil_chk(jobList_)) size];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 4, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x21, 6, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 8, -1, -1, -1, -1 },
    { NULL, "I", 0x21, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(addJobWithPICW_WorkerJob:);
  methods[2].selector = @selector(close);
  methods[3].selector = @selector(isRunning);
  methods[4].selector = @selector(starting);
  methods[5].selector = @selector(stopping);
  methods[6].selector = @selector(jobFinishedWithPICW_WorkerJob:withBoolean:);
  methods[7].selector = @selector(jobErrorWithPICW_WorkerJob:withJavaLangException:);
  methods[8].selector = @selector(run);
  methods[9].selector = @selector(idle);
  methods[10].selector = @selector(addJobIfIdleWithPICW_WorkerJob:);
  methods[11].selector = @selector(addWorkerListenerWithPICW_WorkerListener:);
  methods[12].selector = @selector(removeWorkerListenerWithPICW_WorkerListener:);
  methods[13].selector = @selector(jobCount);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "DEBUG", "Z", .constantValue.asBOOL = PICW_Worker_DEBUG, 0x1a, -1, -1, -1, -1 },
    { "jobList_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 10, -1 },
    { "running_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "working_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "listeners_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 11, -1 },
  };
  static const void *ptrTable[] = { "addJob", "LPICW_WorkerJob;", "jobFinished", "LPICW_WorkerJob;Z", "jobError", "LPICW_WorkerJob;LJavaLangException;", "addJobIfIdle", "addWorkerListener", "LPICW_WorkerListener;", "removeWorkerListener", "Ljava/util/List<Lde/pidata/worker/WorkerJob;>;", "Ljava/util/List<Lde/pidata/worker/WorkerListener;>;" };
  static const J2ObjcClassInfo _PICW_Worker = { "Worker", "de.pidata.worker", ptrTable, methods, fields, 7, 0x1, 14, 5, -1, -1, -1, -1, -1 };
  return &_PICW_Worker;
}

@end

void PICW_Worker_init(PICW_Worker *self) {
  NSObject_init(self);
  self->jobList_ = new_JavaUtilLinkedList_init();
  self->running_ = false;
  self->working_ = false;
  self->listeners_ = new_JavaUtilLinkedList_init();
}

PICW_Worker *new_PICW_Worker_init() {
  J2OBJC_NEW_IMPL(PICW_Worker, init)
}

PICW_Worker *create_PICW_Worker_init() {
  J2OBJC_CREATE_IMPL(PICW_Worker, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PICW_Worker)

J2OBJC_NAME_MAPPING(PICW_Worker, "de.pidata.worker", "PICW_")
