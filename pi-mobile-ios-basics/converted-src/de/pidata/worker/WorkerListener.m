//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/core/pi-core-basics/src/de/pidata/worker/WorkerListener.java
//

#include "J2ObjC_source.h"
#include "de/pidata/worker/WorkerListener.h"

#if !__has_feature(objc_arc)
#error "de/pidata/worker/WorkerListener must be compiled with ARC (-fobjc-arc)"
#endif

@interface PICW_WorkerListener : NSObject

@end

@implementation PICW_WorkerListener

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 4, 3, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(workerStateIdleWithPICW_Worker:);
  methods[1].selector = @selector(workerStateStartingWithPICW_Worker:withPICW_WorkerJob:);
  methods[2].selector = @selector(workerStateFinishedWithPICW_Worker:withPICW_WorkerJob:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "workerStateIdle", "LPICW_Worker;", "workerStateStarting", "LPICW_Worker;LPICW_WorkerJob;", "workerStateFinished" };
  static const J2ObjcClassInfo _PICW_WorkerListener = { "WorkerListener", "de.pidata.worker", ptrTable, methods, NULL, 7, 0x609, 3, 0, -1, -1, -1, -1, -1 };
  return &_PICW_WorkerListener;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PICW_WorkerListener)

J2OBJC_NAME_MAPPING(PICW_WorkerListener, "de.pidata.worker", "PICW_")
