//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-wsdl/src/de/pidata/wsdl/TBindingOperation.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/models/types/simple/QNameType.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/wsdl/TBindingOperation.h"
#include "de/pidata/wsdl/TBindingOperationFault.h"
#include "de/pidata/wsdl/TBindingOperationMessage.h"
#include "de/pidata/wsdl/TExtensibleDocumented.h"
#include "java/lang/Integer.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/wsdl/TBindingOperation must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(DePidataWsdlTBindingOperation)

PIQ_Namespace *DePidataWsdlTBindingOperation_NAMESPACE;
PIQ_QName *DePidataWsdlTBindingOperation_ID_FAULT;
PIQ_QName *DePidataWsdlTBindingOperation_ID_NAME;
PIQ_QName *DePidataWsdlTBindingOperation_ID_OUTPUT;
PIQ_QName *DePidataWsdlTBindingOperation_ID_INPUT;
id<PIMT_ComplexType> DePidataWsdlTBindingOperation_TYPE;

@implementation DePidataWsdlTBindingOperation

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataWsdlTBindingOperation_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_QName:(PIQ_QName *)id_ {
  DePidataWsdlTBindingOperation_initWithPIQ_QName_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)modelID
                withNSObjectArray:(IOSObjectArray *)attributeNames
            withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
               withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataWsdlTBindingOperation_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)modelID
             withPIMT_ComplexType:(id<PIMT_ComplexType>)type
                withNSObjectArray:(IOSObjectArray *)attributeNames
            withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
               withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataWsdlTBindingOperation_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getName {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:DePidataWsdlTBindingOperation_ID_NAME], [PIQ_QName class]);
}

- (void)setNameWithPIQ_QName:(PIQ_QName *)name {
  [self setWithPIQ_QName:DePidataWsdlTBindingOperation_ID_NAME withId:name];
}

- (DePidataWsdlTBindingOperationMessage *)getInput {
  return (DePidataWsdlTBindingOperationMessage *) cast_chk([self getWithPIQ_QName:DePidataWsdlTBindingOperation_ID_INPUT withPIQ_Key:nil], [DePidataWsdlTBindingOperationMessage class]);
}

- (void)setInputWithDePidataWsdlTBindingOperationMessage:(DePidataWsdlTBindingOperationMessage *)input {
  [self replaceWithPIQ_QName:DePidataWsdlTBindingOperation_ID_INPUT withId:input];
}

- (DePidataWsdlTBindingOperationMessage *)getOutput {
  return (DePidataWsdlTBindingOperationMessage *) cast_chk([self getWithPIQ_QName:DePidataWsdlTBindingOperation_ID_OUTPUT withPIQ_Key:nil], [DePidataWsdlTBindingOperationMessage class]);
}

- (void)setOutputWithDePidataWsdlTBindingOperationMessage:(DePidataWsdlTBindingOperationMessage *)output {
  [self replaceWithPIQ_QName:DePidataWsdlTBindingOperation_ID_OUTPUT withId:output];
}

- (DePidataWsdlTBindingOperationFault *)getFaultWithPIQ_QName:(PIQ_QName *)faultID {
  return (DePidataWsdlTBindingOperationFault *) cast_chk([self getWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT withPIQ_Key:faultID], [DePidataWsdlTBindingOperationFault class]);
}

- (void)addFaultWithDePidataWsdlTBindingOperationFault:(DePidataWsdlTBindingOperationFault *)fault {
  [self addWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT withPIMR_Model:fault];
}

- (void)removeFaultWithDePidataWsdlTBindingOperationFault:(DePidataWsdlTBindingOperationFault *)fault {
  [self removeWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT withPIMR_Model:fault];
}

- (id<PIMR_ModelIterator>)faultIter {
  return [self iteratorWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT withPIMR_Filter:nil];
}

- (jint)faultCount {
  return [self childCountWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 0, -1, -1, -1, -1 },
    { NULL, "LDePidataWsdlTBindingOperationMessage;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LDePidataWsdlTBindingOperationMessage;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 5, -1, -1, -1, -1 },
    { NULL, "LDePidataWsdlTBindingOperationFault;", 0x1, 7, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 9, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_QName:);
  methods[2].selector = @selector(initWithPIQ_QName:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(initWithPIQ_QName:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[4].selector = @selector(getName);
  methods[5].selector = @selector(setNameWithPIQ_QName:);
  methods[6].selector = @selector(getInput);
  methods[7].selector = @selector(setInputWithDePidataWsdlTBindingOperationMessage:);
  methods[8].selector = @selector(getOutput);
  methods[9].selector = @selector(setOutputWithDePidataWsdlTBindingOperationMessage:);
  methods[10].selector = @selector(getFaultWithPIQ_QName:);
  methods[11].selector = @selector(addFaultWithDePidataWsdlTBindingOperationFault:);
  methods[12].selector = @selector(removeFaultWithDePidataWsdlTBindingOperationFault:);
  methods[13].selector = @selector(faultIter);
  methods[14].selector = @selector(faultCount);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
    { "ID_FAULT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_NAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "ID_OUTPUT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
    { "ID_INPUT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 16, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_QName;", "LPIQ_QName;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_QName;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "setName", "setInput", "LDePidataWsdlTBindingOperationMessage;", "setOutput", "getFault", "addFault", "LDePidataWsdlTBindingOperationFault;", "removeFault", &DePidataWsdlTBindingOperation_NAMESPACE, &DePidataWsdlTBindingOperation_ID_FAULT, &DePidataWsdlTBindingOperation_ID_NAME, &DePidataWsdlTBindingOperation_ID_OUTPUT, &DePidataWsdlTBindingOperation_ID_INPUT, &DePidataWsdlTBindingOperation_TYPE };
  static const J2ObjcClassInfo _DePidataWsdlTBindingOperation = { "TBindingOperation", "de.pidata.wsdl", ptrTable, methods, fields, 7, 0x1, 15, 6, -1, -1, -1, -1, -1 };
  return &_DePidataWsdlTBindingOperation;
}

+ (void)initialize {
  if (self == [DePidataWsdlTBindingOperation class]) {
    DePidataWsdlTBindingOperation_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://schemas.xmlsoap.org/wsdl/");
    DePidataWsdlTBindingOperation_ID_FAULT = [((PIQ_Namespace *) nil_chk(DePidataWsdlTBindingOperation_NAMESPACE)) getQNameWithNSString:@"fault"];
    DePidataWsdlTBindingOperation_ID_NAME = [DePidataWsdlTBindingOperation_NAMESPACE getQNameWithNSString:@"name"];
    DePidataWsdlTBindingOperation_ID_OUTPUT = [DePidataWsdlTBindingOperation_NAMESPACE getQNameWithNSString:@"output"];
    DePidataWsdlTBindingOperation_ID_INPUT = [DePidataWsdlTBindingOperation_NAMESPACE getQNameWithNSString:@"input"];
    {
      PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_([DePidataWsdlTBindingOperation_NAMESPACE getQNameWithNSString:@"tBindingOperation"], [DePidataWsdlTBindingOperation_class_() getName], 1, JreLoadStatic(DePidataWsdlTExtensibleDocumented, TYPE));
      DePidataWsdlTBindingOperation_TYPE = type;
      [type addKeyAttributeTypeWithInt:0 withPIQ_QName:DePidataWsdlTBindingOperation_ID_NAME withPIMT_SimpleType:PIME_QNameType_getNCName()];
      [type addRelationWithPIQ_QName:DePidataWsdlTBindingOperation_ID_INPUT withPIMT_Type:JreLoadStatic(DePidataWsdlTBindingOperationMessage, TYPE) withInt:0 withInt:1];
      [type addRelationWithPIQ_QName:DePidataWsdlTBindingOperation_ID_OUTPUT withPIMT_Type:JreLoadStatic(DePidataWsdlTBindingOperationMessage, TYPE) withInt:0 withInt:1];
      [type addRelationWithPIQ_QName:DePidataWsdlTBindingOperation_ID_FAULT withPIMT_Type:JreLoadStatic(DePidataWsdlTBindingOperationFault, TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
    }
    J2OBJC_SET_INITIALIZED(DePidataWsdlTBindingOperation)
  }
}

@end

void DePidataWsdlTBindingOperation_init(DePidataWsdlTBindingOperation *self) {
  DePidataWsdlTBindingOperation_initWithPIQ_QName_(self, nil);
}

DePidataWsdlTBindingOperation *new_DePidataWsdlTBindingOperation_init() {
  J2OBJC_NEW_IMPL(DePidataWsdlTBindingOperation, init)
}

DePidataWsdlTBindingOperation *create_DePidataWsdlTBindingOperation_init() {
  J2OBJC_CREATE_IMPL(DePidataWsdlTBindingOperation, init)
}

void DePidataWsdlTBindingOperation_initWithPIQ_QName_(DePidataWsdlTBindingOperation *self, PIQ_QName *id_) {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, DePidataWsdlTBindingOperation_TYPE, nil, nil, nil);
}

DePidataWsdlTBindingOperation *new_DePidataWsdlTBindingOperation_initWithPIQ_QName_(PIQ_QName *id_) {
  J2OBJC_NEW_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_, id_)
}

DePidataWsdlTBindingOperation *create_DePidataWsdlTBindingOperation_initWithPIQ_QName_(PIQ_QName *id_) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_, id_)
}

void DePidataWsdlTBindingOperation_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataWsdlTBindingOperation *self, PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, DePidataWsdlTBindingOperation_TYPE, attributeNames, anyAttribs, childNames);
}

DePidataWsdlTBindingOperation *new_DePidataWsdlTBindingOperation_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, attributeNames, anyAttribs, childNames)
}

DePidataWsdlTBindingOperation *create_DePidataWsdlTBindingOperation_initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, attributeNames, anyAttribs, childNames)
}

void DePidataWsdlTBindingOperation_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataWsdlTBindingOperation *self, PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, type, attributeNames, anyAttribs, childNames);
}

DePidataWsdlTBindingOperation *new_DePidataWsdlTBindingOperation_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, type, attributeNames, anyAttribs, childNames)
}

DePidataWsdlTBindingOperation *create_DePidataWsdlTBindingOperation_initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIQ_QName *modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTBindingOperation, initWithPIQ_QName_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataWsdlTBindingOperation)
