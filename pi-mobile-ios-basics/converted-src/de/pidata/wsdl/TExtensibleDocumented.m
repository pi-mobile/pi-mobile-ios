//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../../pi-mobile-core/service/pi-service-wsdl/src/de/pidata/wsdl/TExtensibleDocumented.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/AnyElement.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/wsdl/TDocumented.h"
#include "de/pidata/wsdl/TExtensibleDocumented.h"
#include "java/lang/Integer.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/wsdl/TExtensibleDocumented must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(DePidataWsdlTExtensibleDocumented)

PIQ_Namespace *DePidataWsdlTExtensibleDocumented_NAMESPACE;
id<PIMT_ComplexType> DePidataWsdlTExtensibleDocumented_TYPE;

@implementation DePidataWsdlTExtensibleDocumented

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataWsdlTExtensibleDocumented_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)modelID
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)modelID
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, type, attributeNames, anyAttribs, childNames);
  return self;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, -1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:);
  methods[2].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", &DePidataWsdlTExtensibleDocumented_NAMESPACE, &DePidataWsdlTExtensibleDocumented_TYPE };
  static const J2ObjcClassInfo _DePidataWsdlTExtensibleDocumented = { "TExtensibleDocumented", "de.pidata.wsdl", ptrTable, methods, fields, 7, 0x1, 4, 2, -1, -1, -1, -1, -1 };
  return &_DePidataWsdlTExtensibleDocumented;
}

+ (void)initialize {
  if (self == [DePidataWsdlTExtensibleDocumented class]) {
    DePidataWsdlTExtensibleDocumented_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://schemas.xmlsoap.org/wsdl/");
    {
      PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_([((PIQ_Namespace *) nil_chk(DePidataWsdlTExtensibleDocumented_NAMESPACE)) getQNameWithNSString:@"tExtensibleDocumented"], [DePidataWsdlTExtensibleDocumented_class_() getName], 0, JreLoadStatic(DePidataWsdlTDocumented, TYPE));
      DePidataWsdlTExtensibleDocumented_TYPE = type;
      [type addRelationWithPIQ_QName:JreLoadStatic(PIMO_AnyElement, ANY_ELEMENT) withPIMT_Type:JreLoadStatic(PIMO_AnyElement, ANY_TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
    }
    J2OBJC_SET_INITIALIZED(DePidataWsdlTExtensibleDocumented)
  }
}

@end

void DePidataWsdlTExtensibleDocumented_init(DePidataWsdlTExtensibleDocumented *self) {
  DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_(self, nil);
}

DePidataWsdlTExtensibleDocumented *new_DePidataWsdlTExtensibleDocumented_init() {
  J2OBJC_NEW_IMPL(DePidataWsdlTExtensibleDocumented, init)
}

DePidataWsdlTExtensibleDocumented *create_DePidataWsdlTExtensibleDocumented_init() {
  J2OBJC_CREATE_IMPL(DePidataWsdlTExtensibleDocumented, init)
}

void DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_(DePidataWsdlTExtensibleDocumented *self, id<PIQ_Key> id_) {
  DePidataWsdlTDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, DePidataWsdlTExtensibleDocumented_TYPE, nil, nil, nil);
}

DePidataWsdlTExtensibleDocumented *new_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_, id_)
}

DePidataWsdlTExtensibleDocumented *create_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_, id_)
}

void DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataWsdlTExtensibleDocumented *self, id<PIQ_Key> modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  DePidataWsdlTDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, DePidataWsdlTExtensibleDocumented_TYPE, attributeNames, anyAttribs, childNames);
}

DePidataWsdlTExtensibleDocumented *new_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, attributeNames, anyAttribs, childNames)
}

DePidataWsdlTExtensibleDocumented *create_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> modelID, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, attributeNames, anyAttribs, childNames)
}

void DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataWsdlTExtensibleDocumented *self, id<PIQ_Key> modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  DePidataWsdlTDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, modelID, type, attributeNames, anyAttribs, childNames);
}

DePidataWsdlTExtensibleDocumented *new_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, type, attributeNames, anyAttribs, childNames)
}

DePidataWsdlTExtensibleDocumented *create_DePidataWsdlTExtensibleDocumented_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> modelID, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataWsdlTExtensibleDocumented, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, modelID, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataWsdlTExtensibleDocumented)
