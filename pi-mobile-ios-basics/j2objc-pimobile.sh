export J2OBJC="$1/j2objc"
export J2OBJC_OPTS='--prefixes prefixes.properties -encoding UTF-8 -d converted-src -use-arc'

# ../../pi-mobile-core/core/pi-core-basics
$J2OBJC $J2OBJC_OPTS $(find  ../../pi-mobile-core/core/pi-core-basics/src -name *.java)
# ../../pi-mobile-core/core/pi-core-rect
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/core/pi-core-rect/src -name *.java)
# ../../pi-mobile-core/core/pi-core-parser
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/core/pi-core-parser/src -name *.java)
# ../../pi-mobile-core/core/pi-core-files
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/core/pi-core-files/src -name *.java)
# ../../pi-mobile-core/core/pi-core-QNames
$J2OBJC $J2OBJC_OPTS $(find  ../../pi-mobile-core/core/pi-core-QNames/src -name *.java)
# ../../pi-mobile-core/connect/pi-connect-basics
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/connect/pi-connect-basics -name *.java)
# ../../pi-mobile-core/connect/pi-connect-socket
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src $(find  ../../pi-mobile-core/connect/pi-connect-socket -name *.java)
# ../../pi-mobile-core/connect/pi-connect-ip
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src $(find  ../../pi-mobile-core/connect/pi-connect-ip -name *.java)
# ../../pi-mobile-core/connect/pi-connect-udp
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src $(find  ../../pi-mobile-core/connect/pi-connect-udp/src -name *.java)
# ../../pi-mobile-core/service/pi-service-basics
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src $(find  ../../pi-mobile-core/service/pi-service-basics/src -name *.java)
# ../../pi-mobile-core/models/pi-models-base
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/system/pi-system-base/src $(find  ../../pi-mobile-core/models/pi-models-base/src -name *.java)
# ../../pi-mobile-core/system/pi-system-base
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/core/pi-core-files/src:../../core/models/src-xml $(find  ../../pi-mobile-core/system/pi-system-base/src -name *.java)
# ../../pi-mobile-core/models/pi-models-binding
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src $(find  ../../pi-mobile-core/models/pi-models-binding/src -name *.java)
# ../../pi-mobile-core/models/pi-models-service
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/models/pi-models-xml/src $(find  ../../pi-mobile-core/models/pi-models-service/src -name *.java)
# ../../pi-mobile-core/models/pi-models-xml
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src $(find  ../../pi-mobile-core/models/pi-models-xml/src -name *.java)
# ../../pi-mobile-core/gui/pi-gui-basics
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-rect/src:../../pi-mobile-core/core/pi-core-parser/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/models/pi-models-binding/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/models/pi-models-xml/src $(find  ../../pi-mobile-core/gui/pi-gui-basics/src -name *.java)
# src -name
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-files/src:../../pi-mobile-core/core/pi-core-rect/src:../../pi-mobile-core/core/pi-core-parser/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/models/pi-models-binding/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/gui/pi-gui-basics/src:src-temp $(find  src -name *.java)
# ../../pi-mobile-core/service/pi-service-client
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/service/pi-service-wsdl/src:../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/service/pi-service-log/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/comm/pi-comm-basics/src:../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/service/pi-service-basics/src $(find  ../../pi-mobile-core/service/pi-service-client/src -name *.java)
# ../../pi-mobile-core/service/pi-service-log
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/models/pi-models-xml/src $(find  ../../pi-mobile-core/service/pi-service-log/src -name *.java)
# ../../pi-mobile-core/service/pi-service-remote
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/comm/pi-comm-basics/src:../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src $(find  ../../pi-mobile-core/service/pi-service-remote/src -name *.java)
# ../../pi-mobile-core/service/pi-service-wsdl
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/models/pi-models-service/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/core/pi-core-basics/src:../../pi-mobile-core/models/pi-models-base/src $(find  ../../pi-mobile-core/service/pi-service-wsdl/src -name *.java)
# ../../pi-mobile-core/comm/pi-comm-basics
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/comm/pi-comm-basics/src -name *.java)
# ../../pi-mobile-core/comm/pi-comm-soap
$J2OBJC $J2OBJC_OPTS -sourcepath ../../pi-mobile-core/comm/pi-comm-basics/src:../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/comm/pi-comm-soap/src -name *.java)
# ../../pi-mobile-core/comm/pi-comm-client
$J2OBJC $J2OBJC_OPTS -sourcepath  ../../pi-mobile-core/comm/pi-comm-basics/src:../../pi-mobile-core/models/pi-models-xml/src:../../pi-mobile-core/service/pi-service-basics/src:../../pi-mobile-core/connect/pi-connect-basics/src:../../pi-mobile-core/comm/pi-comm-soap/src:../../pi-mobile-core/models/pi-models-base/src:../../pi-mobile-core/system/pi-system-base/src:../../pi-mobile-core/core/pi-core-QNames/src:../../pi-mobile-core/core/pi-core-basics/src $(find  ../../pi-mobile-core/comm/pi-comm-client/src -name *.java)


