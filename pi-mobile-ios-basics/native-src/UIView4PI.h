//
//  UIView4PI.h
//  stopstart
//
//  Created by pi-data on 05.04.18.
//  Copyright © 2018 Mohit Deshpande. All rights reserved.
//

#ifndef UIView4PI_h
#define UIView4PI_h

@import UIKit;

@protocol PIGU_UIFactory;

@interface UIView (UIViewPI)

@property(nonatomic)IBInspectable NSString *stringTag;

@end

#endif /* UIView4PI_h */
