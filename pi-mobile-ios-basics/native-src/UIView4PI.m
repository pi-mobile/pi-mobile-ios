//
//  StringTagAddition.m
//  stopstart
//
//  Created by pi-data on 15.05.18.
//  Copyright © 2018 Mohit Deshpande. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "UIView4PI.h"
#import <objc/runtime.h>

static const void *tagKey = &tagKey;

@implementation UIView (StringTag)

- (void)setStringTag:(NSString *)stringTag
{
    objc_setAssociatedObject(self, tagKey, stringTag, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (id)stringTag
{
    return objc_getAssociatedObject(self, tagKey);
}

@end
