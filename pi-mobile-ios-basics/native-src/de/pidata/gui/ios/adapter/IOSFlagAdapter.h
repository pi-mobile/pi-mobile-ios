//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/gui/ios/adapter/IOSFlagAdapter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSFlagAdapter")
#ifdef RESTRICT_DePidataGuiIosAdapterIOSFlagAdapter
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSFlagAdapter 0
#else
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSFlagAdapter 1
#endif
#undef RESTRICT_DePidataGuiIosAdapterIOSFlagAdapter

#if !defined (PIIA_IOSFlagAdapter_) && (INCLUDE_ALL_DePidataGuiIosAdapterIOSFlagAdapter || defined(INCLUDE_PIIA_IOSFlagAdapter))
#define PIIA_IOSFlagAdapter_

#define RESTRICT_DePidataGuiIosAdapterIOSValueAdapter 1
#define INCLUDE_PIIA_IOSValueAdapter 1
#include "de/pidata/gui/ios/adapter/IOSValueAdapter.h"

#define RESTRICT_DePidataGuiUiBaseUIFlagAdapter 1
#define INCLUDE_PIGU_UIFlagAdapter 1
#include "de/pidata/gui/ui/base/UIFlagAdapter.h"

@class PIGV_FlagViewPI;
@protocol PIGU_UIContainer;
@protocol PIGV_ViewPI;

@interface PIIA_IOSFlagAdapter : PIIA_IOSValueAdapter < PIGU_UIFlagAdapter >

#pragma mark Public

- (instancetype)initWithPIGV_FlagViewPI:(PIGV_FlagViewPI *)flagViewPI
                                 withId:(id)iosFlagView
                   withPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer;

- (id)getLabelValue;

- (id)getValue;

- (void)setLabelValueWithId:(id)value;

- (void)setListenFlagChangesWithBoolean:(jboolean)listenFlagChanges;

- (void)setValueWithId:(id)value;

#pragma mark Protected

- (id)getIosView;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGV_ViewPI:(id<PIGV_ViewPI>)arg0
               withPIGU_UIContainer:(id<PIGU_UIContainer>)arg1 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIIA_IOSFlagAdapter)

FOUNDATION_EXPORT void PIIA_IOSFlagAdapter_initWithPIGV_FlagViewPI_withId_withPIGU_UIContainer_(PIIA_IOSFlagAdapter *self, PIGV_FlagViewPI *flagViewPI, id iosFlagView, id<PIGU_UIContainer> uiContainer);

FOUNDATION_EXPORT PIIA_IOSFlagAdapter *new_PIIA_IOSFlagAdapter_initWithPIGV_FlagViewPI_withId_withPIGU_UIContainer_(PIGV_FlagViewPI *flagViewPI, id iosFlagView, id<PIGU_UIContainer> uiContainer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIIA_IOSFlagAdapter *create_PIIA_IOSFlagAdapter_initWithPIGV_FlagViewPI_withId_withPIGU_UIContainer_(PIGV_FlagViewPI *flagViewPI, id iosFlagView, id<PIGU_UIContainer> uiContainer);

J2OBJC_TYPE_LITERAL_HEADER(PIIA_IOSFlagAdapter)

@compatibility_alias DePidataGuiIosAdapterIOSFlagAdapter PIIA_IOSFlagAdapter;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSFlagAdapter")
