//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/gui/ios/adapter/IOSPaintAdapter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSPaintAdapter")
#ifdef RESTRICT_DePidataGuiIosAdapterIOSPaintAdapter
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSPaintAdapter 0
#else
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSPaintAdapter 1
#endif
#undef RESTRICT_DePidataGuiIosAdapterIOSPaintAdapter

#if !defined (PIIA_IOSPaintAdapter_) && (INCLUDE_ALL_DePidataGuiIosAdapterIOSPaintAdapter || defined(INCLUDE_PIIA_IOSPaintAdapter))
#define PIIA_IOSPaintAdapter_

#define RESTRICT_DePidataGuiIosAdapterIOSUIAdapter 1
#define INCLUDE_PIIA_IOSUIAdapter 1
#include "de/pidata/gui/ios/adapter/IOSUIAdapter.h"

#define RESTRICT_DePidataGuiUiBaseUIPaintAdapter 1
#define INCLUDE_PIGU_UIPaintAdapter 1
#include "de/pidata/gui/ui/base/UIPaintAdapter.h"

@class IOSObjectArray;
@class PIGV_PaintViewPI;
@protocol PIGF_Figure;
@protocol PIGF_ShapePI;
@protocol PIGU_UIContainer;
@protocol PIGV_ViewPI;

@interface PIIA_IOSPaintAdapter : PIIA_IOSUIAdapter < PIGU_UIPaintAdapter >

#pragma mark Public

- (instancetype)initWithPIGV_PaintViewPI:(PIGV_PaintViewPI *)paintViewPI
                                  withId:(UIView *)iosView
                    withPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer;

- (void)detach;

- (void)drawWithId:(CGRect)rect
            withId:(CGContextRef)cgContextRef;

- (void)figureAddedWithPIGF_Figure:(id<PIGF_Figure>)figure;

- (void)figureModifiedWithPIGF_Figure:(id<PIGF_Figure>)figure
                     withPIGF_ShapePI:(id<PIGF_ShapePI>)shapePI;

- (void)figureRemovedWithPIGF_Figure:(id<PIGF_Figure>)figure;

- (id)getValue;

- (id<PIGV_ViewPI>)getViewPI;

- (void)setSelectionHandlesWithPIGF_FigureHandleArray:(IOSObjectArray *)selectionHandles;

- (void)setValueWithId:(id)value;

- (void)shapeRemovedWithPIGF_Figure:(id<PIGF_Figure>)figure
                   withPIGF_ShapePI:(id<PIGF_ShapePI>)shapePI;

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer;

#pragma mark Protected

- (id)getIosView;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGV_ViewPI:(id<PIGV_ViewPI>)arg0
               withPIGU_UIContainer:(id<PIGU_UIContainer>)arg1 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIIA_IOSPaintAdapter)

FOUNDATION_EXPORT void PIIA_IOSPaintAdapter_initWithPIGV_PaintViewPI_withId_withPIGU_UIContainer_(PIIA_IOSPaintAdapter *self, PIGV_PaintViewPI *paintViewPI, id iosView, id<PIGU_UIContainer> uiContainer);

FOUNDATION_EXPORT PIIA_IOSPaintAdapter *new_PIIA_IOSPaintAdapter_initWithPIGV_PaintViewPI_withId_withPIGU_UIContainer_(PIGV_PaintViewPI *paintViewPI, id iosView, id<PIGU_UIContainer> uiContainer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIIA_IOSPaintAdapter *create_PIIA_IOSPaintAdapter_initWithPIGV_PaintViewPI_withId_withPIGU_UIContainer_(PIGV_PaintViewPI *paintViewPI, id iosView, id<PIGU_UIContainer> uiContainer);

J2OBJC_TYPE_LITERAL_HEADER(PIIA_IOSPaintAdapter)

@compatibility_alias DePidataGuiIosAdapterIOSPaintAdapter PIIA_IOSPaintAdapter;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSPaintAdapter")
