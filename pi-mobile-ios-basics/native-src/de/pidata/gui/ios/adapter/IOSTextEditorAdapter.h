//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/gui/ios/adapter/IOSTextEditorAdapter.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSTextEditorAdapter")
#ifdef RESTRICT_DePidataGuiIosAdapterIOSTextEditorAdapter
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSTextEditorAdapter 0
#else
#define INCLUDE_ALL_DePidataGuiIosAdapterIOSTextEditorAdapter 1
#endif
#undef RESTRICT_DePidataGuiIosAdapterIOSTextEditorAdapter

#if !defined (PIIA_IOSTextEditorAdapter_) && (INCLUDE_ALL_DePidataGuiIosAdapterIOSTextEditorAdapter || defined(INCLUDE_PIIA_IOSTextEditorAdapter))
#define PIIA_IOSTextEditorAdapter_

#define RESTRICT_DePidataGuiIosAdapterIOSValueAdapter 1
#define INCLUDE_PIIA_IOSValueAdapter 1
#include "de/pidata/gui/ios/adapter/IOSValueAdapter.h"

#define RESTRICT_DePidataGuiUiBaseUITextEditorAdapter 1
#define INCLUDE_PIGU_UITextEditorAdapter 1
#include "de/pidata/gui/ui/base/UITextEditorAdapter.h"

@class PIGV_TextViewPI;
@protocol PIGB_ComponentColor;
@protocol PIGU_UIContainer;
@protocol PIGV_ViewPI;

@interface PIIA_IOSTextEditorAdapter : PIIA_IOSValueAdapter < PIGU_UITextEditorAdapter >

#pragma mark Public

- (instancetype)initWithPIGV_TextViewPI:(PIGV_TextViewPI *)textViewPI
                                 withId:(UITextField *)iosTextView
                   withPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer;

- (id)getValue;

- (void)selectWithInt:(jint)fromPos
              withInt:(jint)toPos;

- (void)selectAll;

- (void)setCursorPosWithShort:(jshort)posX
                    withShort:(jshort)posY;

- (void)setHideInputWithBoolean:(jboolean)hideInput;

- (void)setListenTextChangesWithBoolean:(jboolean)listenTextChanges;

- (void)setTextColorWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color;

- (void)setValueWithId:(id)value;

#pragma mark Protected

- (id)getIosView;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGV_ViewPI:(id<PIGV_ViewPI>)arg0
               withPIGU_UIContainer:(id<PIGU_UIContainer>)arg1 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIIA_IOSTextEditorAdapter)

FOUNDATION_EXPORT void PIIA_IOSTextEditorAdapter_initWithPIGV_TextViewPI_withId_withPIGU_UIContainer_(PIIA_IOSTextEditorAdapter *self, PIGV_TextViewPI *textViewPI, UITextField * iosTextView, id<PIGU_UIContainer> uiContainer);

FOUNDATION_EXPORT PIIA_IOSTextEditorAdapter *new_PIIA_IOSTextEditorAdapter_initWithPIGV_TextViewPI_withId_withPIGU_UIContainer_(PIGV_TextViewPI *textViewPI, UITextField * iosTextView, id<PIGU_UIContainer> uiContainer) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIIA_IOSTextEditorAdapter *create_PIIA_IOSTextEditorAdapter_initWithPIGV_TextViewPI_withId_withPIGU_UIContainer_(PIGV_TextViewPI *textViewPI, UITextField * iosTextView, id<PIGU_UIContainer> uiContainer);

J2OBJC_TYPE_LITERAL_HEADER(PIIA_IOSTextEditorAdapter)

@compatibility_alias DePidataGuiIosAdapterIOSTextEditorAdapter PIIA_IOSTextEditorAdapter;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataGuiIosAdapterIOSTextEditorAdapter")
