//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/gui/ios/adapter/IOSUIAdapter.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/ios/adapter/IOSUIAdapter.h"
#include "de/pidata/gui/ios/platform/IOSColor.h"
#include "de/pidata/gui/ui/base/UIContainer.h"
#include "de/pidata/gui/view/base/ViewPI.h"
#include "de/pidata/qnames/QName.h"
#include "java/lang/Boolean.h"
#include "java/lang/RuntimeException.h"

#pragma clang diagnostic ignored "-Wprotocol"

@interface PIIA_IOSUIAdapter () {
 @public
  id<PIGV_ViewPI> viewPI_;
  PIIP_IOSColor *defaultColor_;
}

- (void)setSelectedWithBoolean:(jboolean)selected;

@end

J2OBJC_FIELD_SETTER(PIIA_IOSUIAdapter, viewPI_, id<PIGV_ViewPI>)
J2OBJC_FIELD_SETTER(PIIA_IOSUIAdapter, defaultColor_, PIIP_IOSColor *)

__attribute__((unused)) static void PIIA_IOSUIAdapter_setSelectedWithBoolean_(PIIA_IOSUIAdapter *self, jboolean selected);

@implementation PIIA_IOSUIAdapter

- (instancetype)initWithPIGV_ViewPI:(id<PIGV_ViewPI>)viewPI
               withPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer {
  PIIA_IOSUIAdapter_initWithPIGV_ViewPI_withPIGU_UIContainer_(self, viewPI, uiContainer);
  return self;
}

- (UIView *)getIosView {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (id<PIGV_ViewPI>)getViewPI {
  return viewPI_;
}

- (id<PIGU_UIContainer>)getUIContainer {
  return self->uiContainer_;
}

- (void)setVisibleWithBoolean:(jboolean)visible {
  if ([NSThread isMainThread]) {
    [self doSetVisible:visible];
  }
  else {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
      [self doSetVisible:visible];
    }];
  }
}

- (void)doSetVisible:(jboolean)visible {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
      if (visible) {
        iosView.hidden = false;
      }
      else {
        iosView.hidden = true;
      }
  }
}

- (jboolean)isVisible {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
    return ![iosView isHidden];
  }
  else {
    return false;
  }
}

- (jboolean)isFocused {
  id iosView = [self getIosView];
  if (iosView != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
  else {
    return false;
  }
}



- (void)setEnabledWithBoolean:(jboolean)enabled {
  UIView * iosView = [self getIosView];

  if (iosView != nil) {

    if ([NSThread isMainThread]) {
      [self doSetEnabled:enabled iosView:iosView];
    }
    else {
      [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        [self doSetEnabled:enabled iosView:iosView];
      }];
    }



  }
}
- (void)doSetEnabled:(jboolean)enabled iosView:(UIView *)iosView {
  if([iosView isKindOfClass:[UIControl class]] ) {
    UIControl *uiControl = (UIControl*) iosView;
    if (enabled) {
      uiControl.enabled = true;
    }
    else {
      uiControl.enabled = false;
    }
  }
  else {
    if (enabled) {
      iosView.userInteractionEnabled = true;
    }
    else {
      iosView.userInteractionEnabled = false;
    }
  }
}

- (jboolean)isEnabled {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
    return iosView.userInteractionEnabled;
  }
  else {
    return false;
  }
}

- (void)setSelectedWithBoolean:(jboolean)selected {
  PIIA_IOSUIAdapter_setSelectedWithBoolean_(self, selected);
}

- (void)detach {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
    // do nothing
  }
}

- (void)repaint {
  UIView *iosView = [self getIosView];
  if (iosView != nil) {
    if ([NSThread isMainThread]) {
      [self doRepaint:iosView];
    }
    else {
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self doRepaint:iosView];
      }];
    }
  }
}

- (void)doRepaint:(UIView *)iosView {
  [iosView layoutIfNeeded];
  [iosView setNeedsLayout];
  [iosView setNeedsDisplay];
}

- (jboolean)processCommandWithPIQ_QName:(PIQ_QName *)cmd
                               withChar:(jchar)inputChar
                                withInt:(jint)index {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
  else {
    return false;
  }
}

- (void)performOnClick {
  UIView * iosView = [self getIosView];
  if (iosView != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
}

- (void)requestFocus {
  UIView * view = [self getIosView];
  if (view != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
}

- (void)setStateWithShort:(jshort)posX
                withShort:(jshort)posY
      withJavaLangBoolean:(JavaLangBoolean *)focus
      withJavaLangBoolean:(JavaLangBoolean *)selected
      withJavaLangBoolean:(JavaLangBoolean *)enabled
      withJavaLangBoolean:(JavaLangBoolean *)visible {
  if (enabled != nil) {
    [self setEnabledWithBoolean:[enabled booleanValue]];
  }
  if (visible != nil) {
    [self setVisibleWithBoolean:[visible booleanValue]];
  }
  if (focus != nil) {
    if ([focus booleanValue]) {
      [self requestFocus];
    }
  }
  if (selected != nil) {
    PIIA_IOSUIAdapter_setSelectedWithBoolean_(self, [selected booleanValue]);
  }
}

- (void)setBackgroundWithPIGB_ComponentColor:(id<PIGB_ComponentColor>)color {
  UIView* iosView = [self getIosView];
  if (iosView != nil) {

    if ([NSThread isMainThread]) {
      [self doSetBackground:color iosView:iosView];
    }
    else {
      [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self doSetBackground:color iosView:iosView];
      }];
    }
  }
}

- (void)doSetBackground:(id <PIGB_ComponentColor>)color iosView:(UIView *)iosView {
  if (defaultColor_ == nil) {
    UIColor *col = iosView.backgroundColor;
    if (col != nil) {
      defaultColor_ = new_PIIP_IOSColor_initWithUIColor_( col );
    }
    else {
      defaultColor_ = new_PIIP_IOSColor_initWithInt_withInt_withInt_withDouble_( 0, 0, 0, 0.0 );
    }
  }
  UIColor *fillColor = (UIColor *) [color getColor];
  [iosView setBackgroundColor:fillColor];
}

- (void)resetBackground {
  [self setBackgroundWithPIGB_ComponentColor:self->defaultColor_];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x404, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGV_ViewPI;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIGU_UIContainer;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 2, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 4, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGV_ViewPI:withPIGU_UIContainer:);
  methods[1].selector = @selector(getIosView);
  methods[2].selector = @selector(getViewPI);
  methods[3].selector = @selector(getUIContainer);
  methods[4].selector = @selector(setVisibleWithBoolean:);
  methods[5].selector = @selector(isVisible);
  methods[6].selector = @selector(setEnabledWithBoolean:);
  methods[7].selector = @selector(isEnabled);
  methods[8].selector = @selector(setSelectedWithBoolean:);
  methods[9].selector = @selector(detach);
  methods[10].selector = @selector(repaint);
  methods[11].selector = @selector(processCommandWithPIQ_QName:withChar:withInt:);
  methods[12].selector = @selector(performOnClick);
  methods[13].selector = @selector(requestFocus);
  methods[14].selector = @selector(setStateWithShort:withShort:withJavaLangBoolean:withJavaLangBoolean:withJavaLangBoolean:withJavaLangBoolean:);
  methods[15].selector = @selector(setBackgroundWithPIGB_ComponentColor:);
  methods[16].selector = @selector(resetBackground);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "viewPI_", "LPIGV_ViewPI;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "uiContainer_", "LPIGU_UIContainer;", .constantValue.asLong = 0, 0x4, -1, -1, -1, -1 },
    { "defaultColor_", "LPIIP_IOSColor;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIGV_ViewPI;LPIGU_UIContainer;", "setVisible", "Z", "setEnabled", "setSelected", "processCommand", "LPIQ_QName;CI", "setState", "SSLJavaLangBoolean;LJavaLangBoolean;LJavaLangBoolean;LJavaLangBoolean;", "setBackground", "LPIGB_ComponentColor;" };
  static const J2ObjcClassInfo _PIIA_IOSUIAdapter = { "IOSUIAdapter", "de.pidata.gui.ios.adapter", ptrTable, methods, fields, 7, 0x401, 17, 3, -1, -1, -1, -1, -1 };
  return &_PIIA_IOSUIAdapter;
}

@end

void PIIA_IOSUIAdapter_initWithPIGV_ViewPI_withPIGU_UIContainer_(PIIA_IOSUIAdapter *self, id<PIGV_ViewPI> viewPI, id<PIGU_UIContainer> uiContainer) {
  NSObject_init(self);
  self->defaultColor_ = nil;
  self->viewPI_ = viewPI;
  self->uiContainer_ = uiContainer;
}

void PIIA_IOSUIAdapter_setSelectedWithBoolean_(PIIA_IOSUIAdapter *self, jboolean selected) {
  id iosView = [self getIosView];
  if (iosView != nil) {
    @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIIA_IOSUIAdapter)

J2OBJC_NAME_MAPPING(PIIA_IOSUIAdapter, "de.pidata.gui.ios.adapter", "PIIA_")
