//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/gui/ios/adapter/IOSValueAdapter.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/ios/adapter/IOSUIAdapter.h"
#include "de/pidata/gui/ios/adapter/IOSValueAdapter.h"
#include "de/pidata/gui/ui/base/UIContainer.h"
#include "de/pidata/gui/view/base/ViewPI.h"

#pragma clang diagnostic ignored "-Wprotocol"

@implementation PIIA_IOSValueAdapter

- (instancetype)initWithPIGV_ViewPI:(id<PIGV_ViewPI>)viewPI
               withPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer {
  PIIA_IOSValueAdapter_initWithPIGV_ViewPI_withPIGU_UIContainer_(self, viewPI, uiContainer);
  return self;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGV_ViewPI:withPIGU_UIContainer:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LPIGV_ViewPI;LPIGU_UIContainer;" };
  static const J2ObjcClassInfo _PIIA_IOSValueAdapter = { "IOSValueAdapter", "de.pidata.gui.ios.adapter", ptrTable, methods, NULL, 7, 0x401, 1, 0, -1, -1, -1, -1, -1 };
  return &_PIIA_IOSValueAdapter;
}

@end

void PIIA_IOSValueAdapter_initWithPIGV_ViewPI_withPIGU_UIContainer_(PIIA_IOSValueAdapter *self, id<PIGV_ViewPI> viewPI, id<PIGU_UIContainer> uiContainer) {
  PIIA_IOSUIAdapter_initWithPIGV_ViewPI_withPIGU_UIContainer_(self, viewPI, uiContainer);
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIIA_IOSValueAdapter)

J2OBJC_NAME_MAPPING(PIIA_IOSValueAdapter, "de.pidata.gui.ios.adapter", "PIIA_")
