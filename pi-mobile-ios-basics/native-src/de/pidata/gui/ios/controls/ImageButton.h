//
//  ImageButton.h
//  stopstart
//
//  Created by cga on 17.06.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ImageButton : UIButton

@end

NS_ASSUME_NONNULL_END
