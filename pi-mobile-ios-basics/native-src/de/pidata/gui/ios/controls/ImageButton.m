//
//  ImageButton.m
//  stopstart
//
//  Created by cga on 17.06.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

#import "ImageButton.h"

@implementation ImageButton


- (void) awakeFromNib {
    [super awakeFromNib];
    [self setAspectRatioForImage];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    [self setAspectRatioForImage];
}

- (void) setAspectRatioForImage {
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}


@end
