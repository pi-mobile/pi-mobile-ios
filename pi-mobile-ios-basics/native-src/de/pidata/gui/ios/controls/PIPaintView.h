//
//  PIPaintView.h
//  stopstart
//
//  Created by Admin on 21.01.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "de/pidata/gui/ios/adapter/IOSPaintAdapter.h"

@interface PIPaintView : UIView {
@public
    DePidataGuiIosAdapterIOSPaintAdapter* iosPaintAdapter;
}

- (void)setIosPaintAdapter:(DePidataGuiIosAdapterIOSPaintAdapter *)adapter;
@end
