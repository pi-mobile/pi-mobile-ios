//
//  PIPaintView.m
//  stopstart
//
//  Created by Admin on 21.01.19.
//  Copyright © 2019 PI-Data AG. All rights reserved.
//

#import "PIPaintView.h"

@implementation PIPaintView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
  if (self->iosPaintAdapter != NULL) {
    CGContextRef context = UIGraphicsGetCurrentContext();
    [iosPaintAdapter drawWithId:rect withId:context];
  }
}

- (void)setIosPaintAdapter:(DePidataGuiIosAdapterIOSPaintAdapter *)adapter {
  self->iosPaintAdapter = adapter;
}
@end
