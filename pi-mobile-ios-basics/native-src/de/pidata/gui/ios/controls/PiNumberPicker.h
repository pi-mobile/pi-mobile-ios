//
//  PiNumberPicker.h
//  PI-Rail-XCode
//
//  Created by cga on 08.10.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// IB_DESIGNABLE
@interface PiNumberPicker : UIView

@property (nonatomic, assign) IBInspectable CGFloat minValue;
@property (nonatomic, assign) IBInspectable CGFloat maxValue;
@property (nonatomic, assign) IBInspectable CGFloat stepSize;

- (UIButton*)getDecreaseButton;
- (UIButton*)getIncreaseButton;
- (UITextField*)getNumberTextField;

- (CGFloat)getMinValue;
- (CGFloat)getMaxValue;
- (CGFloat)getStepSize;

@end

NS_ASSUME_NONNULL_END
