//
//  PiNumberPicker.m
//  PI-Rail-XCode
//
//  Created by cga on 08.10.21.
//  Copyright © 2021 PI-Data AG. All rights reserved.
//

#import "PiNumberPicker.h"
#include "UIView4PI.h"
#include "de/pidata/gui/ios/UIFactoryIOS.h"

@interface PiNumberPicker () {
    UIButton* decreaseBtn;
    UIButton* increaseBtn;
    UITextField* numberText;
}

@end

@implementation PiNumberPicker {
    UIView * numberPickerView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadNib];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self loadNib];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    numberPickerView.frame = self.bounds;
}


- (void)loadNib {
    // Set defaults here. The inspectable properties will be set automatically when the nib has been loaded.
    _minValue = 0;
    _maxValue = 100;
    _stepSize = 1;

    numberPickerView = [[UINib nibWithNibName:@"PiNumberPicker" bundle:[NSBundle mainBundle]] instantiateWithOwner:self options:nil][0];
    [self addSubview: numberPickerView];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    decreaseBtn = (UIButton *) [PIIO_UIFactoryIOS_instance findIOSViewWithId:self withNSString:@"number_picker_decrease"];
    increaseBtn = (UIButton *) [PIIO_UIFactoryIOS_instance findIOSViewWithId:self withNSString:@"number_picker_increase"];
    numberText = (UITextField *) [PIIO_UIFactoryIOS_instance findIOSViewWithId:self withNSString:@"number_picker_text"];
    if (numberText != nil) {
        numberText.keyboardType = UIKeyboardTypeNumberPad;
    }
}

- (UIButton*)getDecreaseButton {
    return decreaseBtn;
}

- (UIButton*)getIncreaseButton {
    return increaseBtn;
}

- (UITextField*)getNumberTextField {
    return numberText;
}

- (CGFloat)getMinValue {
    return _minValue;
}
- (CGFloat)getMaxValue {
    return _maxValue;
}

- (CGFloat)getStepSize {
    return _stepSize;
}

@end
