//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: src-temp/de/pidata/system/ios/IOSConnectionController.java
//

#import <UIKit/UIKit.h>
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/Context.h"
#include "de/pidata/system/base/SystemManager.h"
#include "de/pidata/system/ios/IOSConnectionController.h"
#include "java/lang/RuntimeException.h"

@implementation PIIY_IOSConnectionController {
  NSString *_clientID;
  jboolean _connected;
}

- (instancetype)initWithPIMR_Context:(PIMR_Context *)context {
  PIIY_IOSConnectionController_initWithPIMR_Context_(self, context);
  return self;
}

- (NSString *)connect {
  if (!_connected) {
    UIDevice *device = [UIDevice currentDevice];
    _clientID = [[device identifierForVendor] UUIDString];
    _connected= true;
    [PISYSB_SystemManager_getInstance() setClientIDWithNSString:_clientID];
  }
  return _clientID;
}

- (void)disconnect {
  // Nothing to disconnect
}

- (jboolean)isConnected {
  return _connected;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, 1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIMR_Context:);
  methods[1].selector = @selector(connect);
  methods[2].selector = @selector(disconnect);
  methods[3].selector = @selector(isConnected);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "LPIMR_Context;", "LJavaIoIOException;" };
  static const J2ObjcClassInfo _PIIY_IOSConnectionController = { "IOSConnectionController", "de.pidata.system.ios", ptrTable, methods, NULL, 7, 0x1, 4, 0, -1, -1, -1, -1, -1 };
  return &_PIIY_IOSConnectionController;
}

@end

void PIIY_IOSConnectionController_initWithPIMR_Context_(PIIY_IOSConnectionController *self, PIMR_Context *context) {
  NSObject_init(self);
}

PIIY_IOSConnectionController *new_PIIY_IOSConnectionController_initWithPIMR_Context_(PIMR_Context *context) {
  J2OBJC_NEW_IMPL(PIIY_IOSConnectionController, initWithPIMR_Context_, context)
}

PIIY_IOSConnectionController *create_PIIY_IOSConnectionController_initWithPIMR_Context_(PIMR_Context *context) {
  J2OBJC_CREATE_IMPL(PIIY_IOSConnectionController, initWithPIMR_Context_, context)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIIY_IOSConnectionController)

J2OBJC_NAME_MAPPING(PIIY_IOSConnectionController, "de.pidata.system.ios", "PIIY_")
