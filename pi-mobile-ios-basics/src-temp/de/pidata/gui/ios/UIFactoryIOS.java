/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios;

import de.pidata.gui.component.base.*;
import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.controller.base.ValueController;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ios.adapter.*;
import de.pidata.gui.ios.controller.IOSDialog;
import de.pidata.gui.ios.controller.UIViewControllerPI;
import de.pidata.gui.ios.platform.IOSColor;
import de.pidata.gui.renderer.StringRenderer;
import de.pidata.gui.ui.base.*;
import de.pidata.gui.view.base.*;
import de.pidata.gui.view.figure.ShapePI;
import de.pidata.log.Logger;
import de.pidata.models.binding.Binding;
import de.pidata.models.tree.ModelSource;
import de.pidata.models.binding.SimpleModelSource;
import de.pidata.models.tree.Model;
import de.pidata.models.types.simple.Binary;
import de.pidata.qnames.NamespaceTable;
import de.pidata.qnames.QName;
import de.pidata.string.Helper;
import java.util.Properties;

public class UIFactoryIOS implements de.pidata.gui.ui.base.UIFactory {

  protected static UIFactoryIOS instance;

  public UIFactoryIOS() {
    instance = this;
  }

  public static UIFactoryIOS getInstance() {
    return instance;
  }

  public static QName tag2QName( String tagStr, NamespaceTable nsTable ) {
    QName valueID;
    if (tagStr == null) {
      valueID = null;
    }
    else {
      if (tagStr.indexOf( ':' ) < 0) {
        valueID = ControllerBuilder.NAMESPACE.getQName( tagStr );
      }
      else {
        valueID = QName.getInstance( tagStr, nsTable );
      }
    }
    return valueID;
  }

  public Object findIOSView( Object containerView, String tag ) {
    return null;
  }

  public Object createFragment( ModuleGroup moduleGroup, IOSFragmentAdapter fragmentAdapter ) {
    return null;
  }

  public void updateView( Object view, Model model, IOSListAdapter listAdapter ) {
    if (view instanceof Object) {
      Object group = (Object) view;
      for (int i = 0; i < group.hashCode(); i++) {
        updateView( group.hashCode(), model, listAdapter );
      }
    }
    else {
      Object value = null;
      ColumnInfo columnInfo = null;
      boolean hasBinding = true;
      String viewID = view.toString(); // view.getId()
      IOSCellAdapter cellAdapter = listAdapter.findCellAdapter( viewID );
      ListViewPI viewPI = listAdapter.getListViewPI();
      if (cellAdapter == null) {
        if (viewPI instanceof TableViewPI) {
          // For a table it shall be possible to have static views (e.g. labels) in the row. So if we don't have a
          // cell adapter we do not modify the view, especially do not try to fetch a value.
          return;
        }
        else {
          if (viewPI == null) {
            value = null;
          }
          else {
            QName valueID = tag2QName( viewID, listAdapter.getViewPI().getController().getControllerGroup().getDialogController().getNamespaceTable() );
            value = viewPI.getCellValue( model, valueID );
          }
          value = StringRenderer.getDefault().render( value );
        }
      }
      else {
        columnInfo = cellAdapter.getColumnInfo();
        IOSColor backgroundColor = null;
        QName rowColorID = viewPI.getRowColorID();
        if (rowColorID != null) {
          QName colorColumnID = viewPI.getColorColumnID();
          // if color column is defined, set color only for that column
          // else set color for whole row
          if (colorColumnID == null || columnInfo.getColName().equals( colorColumnID )) {
            Object rowColor = model.get( rowColorID );
            if (!Helper.isNullOrEmpty( rowColor )) {
              if (rowColor instanceof QName) {
                backgroundColor = (IOSColor) Platform.getInstance().getColor( (QName) rowColor );
              }
              else {
                backgroundColor = (IOSColor) Platform.getInstance().getColor( rowColor.toString() );
              }
            }
            //view.setBackgroundColor( backgroundColor.getColorInt() );
          }
        }

        Controller renderCtrl = columnInfo.getRenderCtrl();
        if ((renderCtrl != null) && (renderCtrl instanceof ValueController)) {
          ValueController valueController = (ValueController) renderCtrl;
          Binding renderBinding = valueController.getValueBinding();
          if (renderBinding != null) {
            ModelSource modelSource = new SimpleModelSource( null, model );
            renderBinding.setModelSource( modelSource, renderBinding.getModelPath() );
            value = valueController.getValueBinding().fetchModelValue();
            if (view instanceof Object) { // TextView
              value = valueController.render( value );
            }
          }
          else {
            hasBinding = false;
          }
        }
        else {
          value = null;
        }
      }

      if (view instanceof Object) { // ImageButton
        if(columnInfo != null) {
          if (columnInfo.getValueID() == null) {
            QName iconValueID = columnInfo.getIconValueID();
            if(iconValueID != null){
              value = columnInfo.getIconValue( model );
            }
          }
        }
        updateImageButton( (Object) view, value ); // ImageButton
        listAdapter.updateButtonPos( view, model );
      }
      else if (view instanceof Object) { // ImageView
        if(columnInfo != null) {
          if (columnInfo.getValueID() == null) {
            QName iconValueID = columnInfo.getIconValueID();
            if(iconValueID != null){
              value = columnInfo.getIconValue( model );
            }
          }
        }
        updateImageView( (Object) view, value ); // ImageView
      }
      else if (view instanceof Object) { // Checkbox
        updateCheckBox( (Object) view, (Boolean) value ); //Checkbox
      }
      else if (view instanceof Object) { // Label
        if (hasBinding) {
          //a button is also a text view, but if the button has no binding, its lable will be overriden by ""
          updateLabel( (Object) view, value ); // TextView
        }
      }
      else if (view instanceof Object) { // TextView
        if (hasBinding) {
          //a button is also a text view, but if the button has no binding, its lable will be overriden by ""
          updateTextView( (Object) view, value ); // TextView
        }
      }
      else if (view instanceof Object) { // Button
        if (hasBinding) {
          //a button is also a text view, but if the button has no binding, its lable will be overriden by ""
          updateButton( (Object) view, value ); // TextView
        }
        listAdapter.updateButtonPos( view, model );
      }
      else {
        throw new IllegalArgumentException( "Unsupported view class="+view.getClass() );
      }
      if ((view instanceof Object) || (view instanceof Object)) { // ImageButton || Button
        int visibility = 1; // VISIBLE
        if(columnInfo != null &&
            ( columnInfo.getValueID() != null
                || columnInfo.getIconValueID() != null
                || columnInfo.getLabelValueID() != null)){
          if (value == null) {
            visibility = 2; //INVISIBLE
          }
        }
        view.hashCode(); // setVisibility( visibility );
      }
    }
  }

  private void updateCheckBox( Object view, Boolean value ) { // CheckBox
    boolean boolValue;
    if (value == null) {
      boolValue = false;
    }
    else {
      boolValue = value.booleanValue();
    }
    view.hashCode(); // setChecked( false );
  }

  public static void updateImageButton( Object view, Object value ) { // ImageButton
    if (value == null) {
      view.hashCode(); // setImageResource( 0 );
    }
    else {
      if ((value instanceof QName) && ((QName) value).getName().startsWith( "@" )) {
        String resId = getRessourceID((QName)value);
        view.hashCode(); // setImageResource( resId );
      }
      else if (value instanceof ComponentBitmap) {
        ComponentBitmap componentBitmap = (ComponentBitmap) value;
        Object bitmap = (Object) componentBitmap.getImage(); // Bitmap
        view.hashCode(); // setImageBitmap( bitmap );
      }
      else if (value instanceof Binary) {
        byte[] bytes = ((Binary) value).getBytes();
        Object bitmap = null; // Bitmap bitmap = BitmapFactory.decodeByteArray( bytes, 0, bytes.length );
        view.hashCode(); // setImageBitmap( bitmap );
      }
      else {
        String path = value.toString();
        if (path.length() == 0) {
          view.hashCode(); //setImageResource( 0 );
        }
        else {
          QName bitmapID;
          if(value instanceof QName){
            bitmapID = (QName) value;
          }
          else {
            bitmapID = GuiBuilder.NAMESPACE.getQName( value.toString() );
          }
          ComponentBitmap componentBitmap = Platform.getInstance().getBitmap( bitmapID );
          if (componentBitmap == null) {
            Logger.info( "Bitmap not found: " + value.toString() );
          }
          else {
            Object bitmap = (Object) componentBitmap.getImage(); // Bitmap
            if (bitmap == null) {
              Logger.info( "Cannot get Bitmap from ComponentBitmap: " + value.toString() );
            }
            else {
              view.hashCode(); // .setImageBitmap( bitmap );
            }
          }
        }
      }
    }
  }

  public static void updateImageView( Object view, Object value ) { // ImageView
    if (value == null) {
      view.hashCode(); // setImageResource( 0 );
    }
    else {
      if ((value instanceof QName) && ((QName) value).getName().startsWith( "@" )) {
        String resId = getRessourceID((QName)value);
        view.hashCode(); // setImageResource( resId );
      }
      else if (value instanceof ComponentBitmap) {
        ComponentBitmap componentBitmap = (ComponentBitmap) value;
        Object bitmap = (Object) componentBitmap.getImage(); // Bitmap
        view.hashCode(); // setImageBitmap( bitmap );
      }
      else if (value instanceof Binary) {
        byte[] bytes = ((Binary) value).getBytes();
        Object bitmap = null; // Bitmap bitmap = BitmapFactory.decodeByteArray( bytes, 0, bytes.length );
        view.hashCode(); // setImageBitmap( bitmap );
      }
      else {
        String path = value.toString();
        if (path.length() == 0) {
          view.hashCode(); //setImageResource( 0 );
        }
        else {
          QName bitmapID;
          if(value instanceof QName){
            bitmapID = (QName) value;
          }
          else {
            bitmapID = GuiBuilder.NAMESPACE.getQName( value.toString() );
          }
          ComponentBitmap componentBitmap = Platform.getInstance().getBitmap( bitmapID );
          if (componentBitmap == null) {
            Logger.info( "Bitmap not found: " + value.toString() );
          }
          else {
            Object bitmap = (Object) componentBitmap.getImage(); // Bitmap
            if (bitmap == null) {
              Logger.info( "Cannot get Bitmap from ComponentBitmap: " + value.toString() );
            }
            else {
              view.hashCode(); // .setImageBitmap( bitmap );
            }
          }
        }
      }
    }
  }

  public static String getRessourceID( QName value ) {
    String fieldName = value.getName();
    if (fieldName.startsWith( "@" )) {
      return fieldName.substring( fieldName.indexOf("/")+1 );
    }
    // Compatibility to deprecated method using a QName(String) without @class/ e.g.: @drawable/
    else {
      return value.getName();
    }
  }

  private void updateLabel( Object view, Object value ) {
    String str;
    if (value == null) {
      str = null;
    }
    else {
      str = value.toString();
    }
    view.hashCode();
  }

  private void updateTextView( Object view, Object value ) {
    String str;
    if (value == null) {
      str = null;
    }
    else {
      str = value.toString();
    }
    view.hashCode();
  }

  private void updateButton( Object view, Object value ) {
    String str;
    if (value == null) {
      str = null;
    }
    else {
      str = value.toString();
    }
    view.hashCode();
  }

  @Override
  public void updateLanguage( UIContainer uiContainer, Properties textProps, Properties glossaryProps ) {
    for (String textID : textProps.stringPropertyNames()) {
      Object containerView = ((IOSUIContainer) uiContainer).getUIView();
      Object iosView = findIOSView( containerView, GuiBuilder.NAMESPACE.getQName( textID ).getName() );
      if (iosView != null) {
         if (iosView instanceof Table) {
          String[] colTextList = textProps.getProperty( textID ).split( "\\|" );

          for (int i = 0; i < colTextList.length - 1; i++) {
            String colText = colTextList[i];
            String text = Helper.replaceParams( colText, "{", "}", glossaryProps );
            Object columnHeaderView = findIOSView( containerView, GuiBuilder.NAMESPACE.getQName( textID + "_h" + i).getName() );
            if (columnHeaderView instanceof Object) {

            }
          }
        }
        else if (iosView instanceof Label) {
          String text = Helper.replaceParams( textProps.getProperty( textID ), "{", "}", glossaryProps );
        }
        else {
          Logger.warn( "Unsupported element for updateLanguage: " + iosView.getClass().getName() );
        }
      }
    }
  }

  @Override
  public UIListAdapter createListAdapter( UIContainer uiContainer, ListViewPI listViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, listViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ listViewPI.getComponentID() );
    }
    if (iosView instanceof Object) {
      return new IOSSegmentedControlAdapter( listViewPI, iosView, uiContainer );
    }
    else if (iosView instanceof Object) {
      return new IOSTabBarAdapter( listViewPI, iosView, uiContainer );
    }
    else {
      return new IOSPickerAdapter( listViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIButtonAdapter createButtonAdapter( UIContainer uiContainer, ButtonViewPI buttonViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, buttonViewPI.getComponentID().getName() );
    if (iosView == null) {
      if (uiContainer instanceof IOSDialog) {
        return ((IOSDialog) uiContainer).addMenuEntry( buttonViewPI );
      }
      throw new IllegalArgumentException( "Could not find view ID="+ buttonViewPI.getComponentID() );
    }
    else {
      return new IOSButtonAdapter( buttonViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UITextAdapter createTextAdapter( UIContainer uiContainer, TextViewPI textViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, textViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ textViewPI.getComponentID() );
    }
    else if (iosView instanceof Object) {
      return new IOSTextViewAdapter( textViewPI, iosView, uiContainer );
    }
    else {
      return new IOSTextAdapter( textViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIDateAdapter createDateAdapter( UIContainer uiContainer, DateViewPI dateViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UITextEditorAdapter createTextEditorAdapter( UIContainer uiContainer, TextEditorViewPI textEditorViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, textEditorViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ textEditorViewPI.getComponentID() );
    }
    else {
      return new IOSTextEditorAdapter( textEditorViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIWebViewAdapter createWebViewAdapter( UIContainer uiContainer, WebViewPI webViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIHTMLEditorAdapter createHTMLEditorAdapter( UIContainer uiContainer, HTMLEditorViewPI htmlEditorViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UINodeAdapter createNodeAdapter( UIContainer uiContainer, NodeViewPI nodeViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, nodeViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ nodeViewPI.getComponentID() );
    }
    else {
      return new IOSNodeAdapter( nodeViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UITableAdapter createTableAdapter( UIContainer uiContainer, TableViewPI tableViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, tableViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ tableViewPI.getComponentID() );
    }
    else {
      return new IOSTableAdapter( tableViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIImageAdapter createImageAdapter( UIContainer uiContainer, ImageViewPI imageViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, imageViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ imageViewPI.getComponentID() );
    }
    else {
      return new IOSImageAdapter( imageViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIFlagAdapter createFlagAdapter( UIContainer uiContainer, FlagViewPI flagViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, flagViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ flagViewPI.getComponentID() );
    }
    else {
      return new IOSFlagAdapter( flagViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIPaintAdapter createPaintAdapter( UIContainer uiContainer, PaintViewPI paintViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, paintViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ paintViewPI );
    }
    else {
      return new IOSPaintAdapter( paintViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UIProgressBarAdapter createProgressAdapter( UIContainer uiContainer, ProgressBarViewPI progressBarViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, progressBarViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ progressBarViewPI.getComponentID() );
    }
    if (iosView instanceof Object) {
      return new IOSSliderAdapter( progressBarViewPI, iosView, uiContainer );
    }
    else if (iosView instanceof Object) {
      return new IOSProgressBarAdapter( progressBarViewPI, iosView, uiContainer );
    }
    else {
      throw new IllegalArgumentException( "Not supported ProgressAdapter" );
    }
  }

  @Override
  public UITabGroupAdapter createTabGroupAdapter( UIContainer uiContainer, TabGroupViewPI tabGroupViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UITreeAdapter createTreeAdapter( UIContainer uiContainer, TreeViewPI treeViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIShapeAdapter createShapeAdapter( UIPaintAdapter uiPaintAdapter, ShapePI shapePI ) {
    return new IOSShapeAdapter( shapePI );
  }

  @Override
  public UICodeEditorAdapter createCodeEditorAdapter( UIContainer uiContainer, CodeEditorViewPI codeEditorViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIFragmentAdapter createFragmentAdapter( UIContainer uiContainer, ModuleViewPI moduleViewPI ) {
    Object containerView = ((IOSUIContainer) uiContainer).getUIView();
    Object iosView = findIOSView( containerView, moduleViewPI.getComponentID().getName() );
    if (iosView == null) {
      throw new IllegalArgumentException( "Could not find view ID="+ moduleViewPI.getComponentID() );
    }
    else {
      return new IOSFragmentAdapter( moduleViewPI, iosView, uiContainer );
    }
  }

  @Override
  public UITabPaneAdapter createTabPaneAdapter( UIContainer uiContainer, TabPaneViewPI tabPaneViewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UITreeTableAdapter createTreeTableAdapter( UIContainer uiContainer, TreeTableViewPI tableViewPI ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public UIAdapter createCustomAdapter( UIContainer uiContainer, ViewPI viewPI ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public UIViewControllerPI getViewController(String nibname) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
