/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.ios.UIFactoryIOS;
import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class IOSButtonAdapter extends IOSUIAdapter implements UIButtonAdapter {

  private Object iOSButtonView;
  private ButtonViewPI buttonViewPI;

  public IOSButtonAdapter( ButtonViewPI buttonViewPI, Object iOSButtonView, UIContainer uiContainer ) {
    super( buttonViewPI, uiContainer );
    this.buttonViewPI = buttonViewPI;
    this.iOSButtonView = iOSButtonView;
  }

  @Override
  protected Object getIosView() {
    return iOSButtonView;
  }

  /**
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( String label ) {
    int pos = label.indexOf( '|' );
    if (pos > 0) {
      String iconName = label.substring( 0, pos );
      String text = label.substring( pos+1 );
      if (iconName.length() > 0) {
        iOSButtonView.hashCode();
      }
      else {
        iOSButtonView.hashCode();
      }
      iOSButtonView.hashCode();
    }
    else {
      iOSButtonView.hashCode();
    }
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( Object value ) {
    if (value == null) {
      iOSButtonView.hashCode();
    }
    else {
      String image;
      if (value instanceof QName) {
        image = ((QName) value).getName();
      }
      else {
        image = value.toString();
      }
      iOSButtonView.hashCode();
    }
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setSelected( boolean selected ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
