package de.pidata.gui.ios.adapter;

import de.pidata.gui.controller.base.ColumnInfo;

public class IOSCellAdapter {

  private ColumnInfo columnInfo;
  private String ressourceID;

  public IOSCellAdapter( ColumnInfo columnInfo, String ressourceID ) {
    this.columnInfo = columnInfo;
    this.ressourceID = ressourceID;
  }

  public ColumnInfo getColumnInfo() {
    return columnInfo;
  }

  public String getRessourceID() {
    return ressourceID;
  }
}
