/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFlagAdapter;
import de.pidata.gui.view.base.FlagViewPI;
import de.pidata.models.tree.Model;

public class IOSFlagAdapter extends IOSValueAdapter implements UIFlagAdapter {

  private Object iosFlagView;
  private FlagViewPI flagViewPI;

  public IOSFlagAdapter( FlagViewPI flagViewPI, Object iosFlagView, UIContainer uiContainer ) {
    super( flagViewPI, uiContainer );
    this.iosFlagView = iosFlagView;
    this.flagViewPI = flagViewPI;
  }

  @Override
  protected Object getIosView() {
    return iosFlagView;
  }


  public void setListenFlagChanges( boolean listenFlagChanges ) {
    // do nothing
  }

  @Override
  public Object getValue() {
    return null;
  }

  @Override
  public void setValue( Object value ) {

  }

  @Override
  public void setLabelValue( Object value ) {
    iosFlagView.toString(); // set label
  }

  @Override
  public Object getLabelValue() {
    return iosFlagView.toString(); // return label
  }
}
