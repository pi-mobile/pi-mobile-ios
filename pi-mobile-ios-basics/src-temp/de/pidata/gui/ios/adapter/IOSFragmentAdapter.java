/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.ModuleGroup;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIFragmentAdapter;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.models.tree.Model;

public class IOSFragmentAdapter extends IOSUIAdapter implements UIFragmentAdapter, IOSUIContainer {

  private ModuleViewPI moduleViewPI;
  private ModuleGroup activeModule;
  private Object iosFragmentView;
  private Object iosFragmentCtrl;

  public IOSFragmentAdapter( ModuleViewPI moduleViewPI, Object iosFragmentView, UIContainer uiContainer ) {
    super( moduleViewPI, uiContainer );
    this.moduleViewPI = moduleViewPI;
    this.iosFragmentView = iosFragmentView;
  }

  @Override
  protected Object getIosView() {
    return iosFragmentView;
  }

  public ModuleViewPI getModuleViewPI() {
    return moduleViewPI;
  }

  public Object getIosFragmentCtrl() {
    return iosFragmentCtrl;
  }

  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    if (activeModule == null) {
      return null;
    }
    else {
      return activeModule.getModel();
    }
  }

  @Override
  public Object getUIView() {
    return null;
  }

  /**
   * Called by ModuleViewPI whenever the module has been replaced
   *
   * @param moduleGroup the new module
   */
  @Override
  public void moduleChanged( ModuleGroup moduleGroup ) {
    this.activeModule = moduleGroup;
  }
}
