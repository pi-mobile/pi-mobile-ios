/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIImageAdapter;
import de.pidata.gui.view.base.ImageViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public class IOSImageAdapter extends IOSValueAdapter implements UIImageAdapter {

  private ImageViewPI imageViewPI;
  private Object iosImgageView;

  public IOSImageAdapter( ImageViewPI imageViewPI, Object iosImgageView, UIContainer uiContainer ) {
    super( imageViewPI, uiContainer );
    this.imageViewPI = imageViewPI;
    this.iosImgageView = iosImgageView;
  }

  @Override
  protected Object getIosView() {
    return iosImgageView;
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setImageResource( QName imageResourceID ) {
    String fieldName = imageResourceID.getName();
    if(fieldName.startsWith( "@" )) {
      fieldName =  fieldName.substring( fieldName.indexOf("/")+1 );
    }
  }

  @Override
  public void setImage( ComponentBitmap bitmap ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean isZoomEnabled() {
    return false;
  }

  @Override
  public void setZoomFactor( double zoomFactor ) {
    // TODO
  }

  @Override
  public double getZoomFactor() {
    return 1.0; //TODO
  }
}
