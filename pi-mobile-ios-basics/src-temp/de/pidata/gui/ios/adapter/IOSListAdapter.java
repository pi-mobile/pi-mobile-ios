/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.controller.base.SelectionController;
import de.pidata.gui.controller.base.TableController;
import de.pidata.gui.ios.UIFactoryIOS;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class IOSListAdapter extends IOSUIAdapter implements UIListAdapter {

  protected ListViewPI listViewPI;
  protected Map<Object,Model> buttonPosMap = new HashMap<Object,Model>();

  protected Object selectedRowView = null;
  protected List<Model> displayRows = new ArrayList<Model>();
  private IOSCellAdapter[] cellAdapter;

  public IOSListAdapter( ListViewPI viewPI, UIContainer uiContainer ) {
    super( viewPI, uiContainer );
    this.listViewPI = viewPI;
    attachColumns();
  }

  public ListViewPI getListViewPI() {
    return listViewPI;
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  protected abstract void reloadData();

  /**
   * Returns the count of display rows
   *
   * @return the count of display rows
   */
  @Override
  public int getDisplayRowCount() {
    synchronized (displayRows) {
      return displayRows.size();
    }
  }

  /**
   * Returns the data displayed in list at position. In case of a sorted list
   * the position may be different from data model's index
   *
   * @param position the position in displayed list
   * @return the model displayed at position
   */
  @Override
  public Model getDisplayRow( int position ) {
    synchronized (displayRows) {
      return displayRows.get( position );
    }
  }

  public int indexOfRow( Model rowModel ) {
    synchronized (displayRows) {
      for (int i = 0; i < displayRows.size(); i++) {
        if (displayRows.get( i ) == rowModel) {
          return i;
        }
      }
      return -1;
    }
  }

  /**
   * Remove all rows from displayed list
   */
  @Override
  public void removeAllDisplayRows() {
    synchronized (displayRows) {
      displayRows.clear();
    }
  }

  /**
   * Insert newRow into displayed list before beforeRow.
   *
   * @param newRow    the row to be inserted
   * @param beforeRow the row before which newRow is inserted
   */
  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    synchronized (displayRows) {
      int index;
      if (beforeRow == null) {
        index = -1;
      }
      else {
        index = indexOfRow( beforeRow );
      }
      if (index < 0) {
        displayRows.add( newRow );
        index = displayRows.size() - 1;
      }
      else {
        displayRows.add( index, newRow );
      }
    }
  }

  /**
   * Remove row from display list
   *
   * @param removedRow the row to remove
   */
  @Override
  public synchronized void removeRow( Model removedRow ) {
    synchronized (displayRows) {
      // IOS implementation of ArrayList crashes if removed row does not exist in list
      int index = indexOfRow( removedRow );
      if (index >= 0) {
        displayRows.remove( index );
      }
    }
  }

  /**
   * Update changedRow in display list
   *
   * @param changedRow the row to be updated in display list
   */
  @Override
  public void updateRow( Model changedRow ) {
    synchronized (displayRows) {
      int index = indexOfRow( changedRow );
      if (index > 0) {
        displayRows.set( index, changedRow );
      }
    }
  }

  private void attachColumns() {
    SelectionController selectionController = getListViewPI().getListCtrl();
    if (selectionController instanceof TableController) {
      TableController tableController = (TableController) selectionController;
      if (cellAdapter == null) {
        cellAdapter = new IOSCellAdapter[tableController.columnCount()];
      }
      for (short i = 0; i < cellAdapter.length; i++) {
        ColumnInfo columnInfo = tableController.getColumn( i );
        QName colCompName = columnInfo.getRenderCtrl().getView().getComponentID();
        String ressourceID = colCompName.getName();
        cellAdapter[i] = new IOSCellAdapter( columnInfo, ressourceID );
      }
    }
  }

  public IOSCellAdapter findCellAdapter( String ressourceID ) {
    if (cellAdapter != null) {
      for (short i = 0; i < cellAdapter.length; i++) {
        if (ressourceID.equals( cellAdapter[i].getRessourceID() )) {
          return cellAdapter[i];
        }
      }
    }
    return null;
  }

  public void updateButtonPos( Object button, Model rowMdel ) {
    buttonPosMap.put( button, rowMdel );
  }

  public void onButtonPressed( Object button ) {
    Model rowModel = buttonPosMap.get( button );
    if (rowModel != null) {
      listViewPI.onSelectionChanged( rowModel, true );
    }
  }
}
