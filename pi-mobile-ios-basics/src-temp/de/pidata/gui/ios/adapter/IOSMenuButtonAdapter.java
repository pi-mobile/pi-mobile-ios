package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIButtonAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;

public class IOSMenuButtonAdapter extends IOSUIAdapter implements UIButtonAdapter {

  ButtonViewPI buttonViewPI;

  public IOSMenuButtonAdapter( ButtonViewPI buttonViewPI, UIContainer uiContainer ) {
    super( buttonViewPI, uiContainer );
    this.buttonViewPI = buttonViewPI;
  }

  public void onClick( Model dataContext ) {
    buttonViewPI.onClick( null, dataContext );
  }

  /**
   * ---DEPRECATED---
   * setLabel will be called if binding exists using valueID,
   * use iconValueID or labelValueID instead!
   * <p>
   * Set this button's label
   *
   * @param label the label text
   */
  @Override
  public void setLabel( String label ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set this button's text Property
   *
   * @param text the text
   */
  @Override
  public void setText( Object text ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Set graphic on Button
   *
   * @param value
   */
  @Override
  public void setGraphic( Object value ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns this button's label
   *
   * @return this button's label
   */
  @Override
  public String getText() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setSelected( boolean selected ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  protected Object getIosView() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
