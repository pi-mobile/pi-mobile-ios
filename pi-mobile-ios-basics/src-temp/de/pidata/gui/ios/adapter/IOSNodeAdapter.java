/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UINodeAdapter;
import de.pidata.gui.view.base.NodeViewPI;
import de.pidata.models.tree.Model;

public class IOSNodeAdapter extends IOSUIAdapter implements UINodeAdapter {

  private Object view;
  private NodeViewPI nodeViewPI;

  public IOSNodeAdapter( NodeViewPI nodeViewPI, Object view, UIContainer uiContainer ) {
    super( nodeViewPI, uiContainer );
    this.view = view;
    this.nodeViewPI = nodeViewPI;
  }

  @Override
  protected Object getIosView() {
    return this.view;
  }
}
