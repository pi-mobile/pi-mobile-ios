/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.ui.base.UIPaintAdapter;
import de.pidata.gui.view.base.PaintViewPI;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.gui.view.figure.*;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.rect.Rect;

import java.util.ArrayList;
import java.util.List;

public class IOSPaintAdapter extends IOSUIAdapter implements UIPaintAdapter {

  private static final boolean DEBUG = false;

  private Object iosView;
  private PaintViewPI paintViewPI;
  private List<IOSShapeAdapter> handleShapeList = new ArrayList<IOSShapeAdapter>();
  private boolean dirty = false;

  public IOSPaintAdapter( PaintViewPI paintViewPI, Object iosView, UIContainer uiContainer ) {
    super( paintViewPI, uiContainer );
    this.paintViewPI = paintViewPI;
    this.iosView = iosView;
    //this.iosView.addOnLayoutChangeListener( this );
    Rect bounds = paintViewPI.getBounds();
    bounds.setWidth( 0 );//iosView.getWidth() );
    bounds.setHeight( 0 );//iosView.getHeight() );
    for (int i = 0; i < paintViewPI.figureCount(); i++) {
      figureAdded( paintViewPI.getFigure( i ) );
    }
  }

  @Override
  protected Object getIosView() {
    return iosView;
  }

  private void addShapes( Figure figure ) {
    UIFactory uiFactory = Platform.getInstance().getUiFactory();
    for (int k = 0; k < figure.shapeCount(); k++) {
      ShapePI shapePI = figure.getShape( k );
      IOSShapeAdapter shapeAdapter = (IOSShapeAdapter) uiFactory.createShapeAdapter( this, shapePI );
      shapePI.attachUI( shapeAdapter );
    }
  }

  private void removeShape( ShapePI shapePI) {
    IOSShapeAdapter shapeAdapter = (IOSShapeAdapter) shapePI.getUIAdapter();
    if (shapeAdapter != null) {
      shapePI.detachUI();
    }
  }

  private void removeShapes( Figure figure ) {
    for (int k = 0; k < figure.shapeCount(); k++) {
      ShapePI shapePI = figure.getShape( k );
      removeShape( shapePI );
    }
  }

  @Override
  public void detach() {
    iosView = null;
    paintViewPI = null;
  }

  @Override
  public ViewPI getViewPI() {
    return paintViewPI;
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public void draw( Object rect, Object cgContextRef ) {
    dirty = false;
    try {
      Rect bounds = paintViewPI.getBounds();
      bounds.setHeight( 100 );
      bounds.setWidth( 100 );
      Logger.info( "Figure count = " +  paintViewPI.figureCount());
      for (int i = 0; i < paintViewPI.figureCount(); i++) {
        Figure figure = paintViewPI.getFigure( i );
        Logger.info( "-- Shape count = " +  figure.shapeCount());
        for (int k = 0; k < figure.shapeCount(); k++) {
          ShapePI shapePI = figure.getShape( k );
          IOSShapeAdapter iosShapeAdapter = (IOSShapeAdapter) shapePI.getUIAdapter();
          iosShapeAdapter.drawShape( cgContextRef );
        }
      }
    }
    catch (Exception ex) {
      Logger.error( "Error in IOSPaintAdapter.draw", ex );
    }
  }

  @Override
  public void setSelectionHandles( FigureHandle[] selectionHandles ) {
    for (IOSShapeAdapter handleShapeAdapter : handleShapeList) {
      //iosView.remove( cgContextRef );
    }
    handleShapeList.clear();
    if (selectionHandles != null) {
      for (int i = 0; i < selectionHandles.length; i++) {
        IOSShapeAdapter handleShapeAdapter = (IOSShapeAdapter) Platform.getInstance().getUiFactory().createShapeAdapter( this, selectionHandles[i] );
        handleShapeList.add( handleShapeAdapter );
      }
    }
  }

  /**
   * Notifies that shape for given figure has been modified.
   * If shapePI is null all shapes of figure are modified.
   *
   * @param figure  the figure having modified shape(s)
   * @param shapePI the shape or null for all shapes
   */
  @Override
  public void figureModified( Figure figure, ShapePI shapePI ) {
    if (shapePI == null) {
      for (int i = 0; i < figure.shapeCount(); i++) {
        ShapePI childShapePI = figure.getShape( i );
        IOSShapeAdapter iosShapeAdapter = (IOSShapeAdapter) childShapePI.getUIAdapter();
        iosShapeAdapter.updateShape();
      }
    }
    else {
      IOSShapeAdapter iosShapeAdapter = (IOSShapeAdapter) shapePI.getUIAdapter();
      if (iosShapeAdapter != null) {
        iosShapeAdapter.updateShape();
      }
    }

    //--- Always update visible handles
    for (IOSShapeAdapter handleShapeAdapter : handleShapeList) {
      handleShapeAdapter.updateShape();
    }
    if (!dirty) {
      dirty = true;
      //Logger.info( "PaintAdapter.repaint, id="+getViewPI().getComponentID().getName());
      repaint();
    }
  }

  @Override
  public void figureAdded( Figure figure ) {
    addShapes( figure );
  }

  @Override
  public void figureRemoved( Figure figure ) {
    removeShapes( figure );
  }

  @Override
  public void shapeRemoved( Figure figure, ShapePI shapePI ) {
    removeShape( shapePI );
  }

  private boolean containsPos( Rect bounds, double x, double y ) {
    return ((bounds.getX() < x) && (x < bounds.getRight())
        && (bounds.getY() < y) && (y < bounds.getBottom()));
  }

  private ShapePI findShapeAt( double x, double y ) {
    for (IOSShapeAdapter handleShapeAdapter : handleShapeList) {
      ShapePI shapePI = handleShapeAdapter.getShapePI();
      if (containsPos (shapePI.getBounds(), x, y)) {
        return shapePI;
      }
    }
    for (int i = paintViewPI.figureCount()-1; i >= 0; i--) {
      Figure figure = paintViewPI.getFigure( i );
      for (int k = figure.shapeCount()-1; k >= 0; k--) {
        ShapePI shapePI = figure.getShape( k );
        if (containsPos (shapePI.getBounds(), x, y)) {
          return shapePI;
        }
      }
    }
    return null;
  }

  private void handleSingleTap() {
    double x = 0;
    double y = 0;
    ShapePI shapePI = findShapeAt( x, y );
    if (shapePI != null) {
      double posX = x - shapePI.getBounds().getX();
      double posY = y - shapePI.getBounds().getY();
      shapePI.getFigure().onMousePressed( 1, posX, posY, shapePI );
    }
  }
}
