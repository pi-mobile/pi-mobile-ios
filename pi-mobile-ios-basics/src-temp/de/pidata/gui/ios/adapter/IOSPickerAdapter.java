/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.models.tree.Model;

public class IOSPickerAdapter extends IOSListAdapter {

  private Object iOSView;

  public IOSPickerAdapter( ListViewPI listViewPI, Object iosView, UIContainer uiContainer ) {
    super( listViewPI, uiContainer );
    this.iOSView = iosView;
  }

  @Override
  protected Object getIosView() {
    return iOSView;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if (iOSView != null) {
      iOSView = null;
    }
    this.listViewPI = null;
  }

  @Override
  protected void reloadData() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   * @param selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

}
