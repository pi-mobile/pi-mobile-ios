/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIListAdapter;
import de.pidata.gui.view.base.ListViewPI;
import de.pidata.models.tree.Model;

import java.util.ArrayList;
import java.util.List;

public class IOSSegmentedControlAdapter extends IOSListAdapter {

  private Object uiSegmentedControl;
  private ListViewPI listViewPI;

  public IOSSegmentedControlAdapter( ListViewPI listViewPI, Object uiSegmentedControl, UIContainer uiContainer ) {
    super( listViewPI, uiContainer );
    this.listViewPI = listViewPI;
    this.uiSegmentedControl = uiSegmentedControl;
  }

  @Override
  protected Object getIosView() {
    return uiSegmentedControl;
  }


  @Override
  protected void reloadData() {
   for (int i = 0; i<displayRows.size(); i++) {

   }
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   * @param selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Insert newRow into displayed list before beforeRow.
   *
   * @param newRow    the row to be inserted
   * @param beforeRow the row before which newRow is inserted
   */
  @Override
  public void insertRow( Model newRow, Model beforeRow ) {
    if (newRow != null) {
      super.insertRow( newRow, beforeRow );
    }
  }
}
