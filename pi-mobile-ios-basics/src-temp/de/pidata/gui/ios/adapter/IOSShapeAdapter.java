/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.component.base.Platform;
import de.pidata.gui.ios.platform.IOSBitmap;
import de.pidata.gui.ios.platform.IOSColor;
import de.pidata.gui.ui.base.UIShapeAdapter;
import de.pidata.gui.view.figure.*;
import de.pidata.qnames.QName;
import de.pidata.rect.*;

import java.util.List;

public class IOSShapeAdapter implements UIShapeAdapter {

  private ShapePI shapePI;
  private Object cgLayerRef;

  public IOSShapeAdapter( ShapePI shapePI ) {
    this.shapePI = shapePI;
  }

  @Override
  public ShapePI getShapePI() {
    return shapePI;
  }

  private String getCycleMethod( GradientMode gradientMode ) {
    switch (gradientMode) {
      case CLAMP: return "CLAMP";
      case MIRROR: return "MIRROR";
      case REPEAT: return "REPEAT";
    }
    return null;
  }

  private void applyShapeStyle( ShapeStyle shapeStyle ) {
    if (shapeStyle instanceof LinearGradientStyle) {
      LinearGradientStyle gradientStyle = (LinearGradientStyle) shapeStyle;
      Pos start = gradientStyle.getStartPos();
      Pos end = gradientStyle.getEndPos();
      String tileMode = getCycleMethod( gradientStyle.getGradientMode() );
      List<ComponentColor> colorList = gradientStyle.getColorList();
      List<Double> offsetList = gradientStyle.getOffsetList();
      Object[] colors = new Object[colorList.size()];
      float[] positions = new float[colorList.size()];
      for (int i = 0; i < colorList.size(); i++) {
        positions[i] = offsetList.get( i ).floatValue();
        colors[i] = ((IOSColor) colorList.get( i )).getColor();
      }
      //LinearGradient gradient = new LinearGradient( (float) start.getX(), (float) start.getY(), (float) end.getX(), (float) end.getY(), colors, positions, tileMode );
      //cgContextRef.getPaint().setShader( gradient );
    }
    else if (shapeStyle instanceof CircleGradientStyle) {
      CircleGradientStyle circleGradientStyle = (CircleGradientStyle) shapeStyle;
      Pos center = circleGradientStyle.getCenterPos();
      String tileMode = getCycleMethod( circleGradientStyle.getGradientMode() );
      List<ComponentColor> colorList = circleGradientStyle.getColorList();
      List<Double> offsetList = circleGradientStyle.getOffsetList();
      Object[] colors = new Object[colorList.size()];
      float[] positions = new float[colorList.size()];
      for (int i = 0; i < colorList.size(); i++) {
        positions[i] = offsetList.get( i ).floatValue();
        colors[i] = ((IOSColor) colorList.get( i )).getColor();
      }
      //RadialGradient gradient = new RadialGradient( (float) center.getX(), (float) center.getY(), (float) circleGradientStyle.getRadius(), colors, positions, tileMode );
      //cgContextRef.getPaint().setShader( gradient );
    }
    else {
      IOSColor iosColor = ((IOSColor) shapeStyle.getFillColor());
      Object fillColor = null;
      if (iosColor != null) {
        fillColor = iosColor.getColor();
      }
      iosColor = ((IOSColor) shapeStyle.getFillColor());
      Object strokeColor = null;
      if (iosColor != null) {
        strokeColor = iosColor.getColor();
      }
      //cgContextRef.getPaint().setColor( color );
    }
  }

  private void createShape() {
    ShapeType shapeType = shapePI.getShapeType();
    ShapeStyle shapeStyle = shapePI.getShapeStyle();
    Rect bounds = shapePI.getBounds();
    double width = bounds.getRight();
    double height = bounds.getBottom();
    double strokeWidth = shapeStyle.getStrokeWidth() / 2;

    double x = bounds.getX();
    double y = bounds.getY();
    cgLayerRef = null; //cgLayerRef = new ShapeDrawableBorder( rectangle, shapeStyle );
    Object cgLayerContext = null;
    applyShapeStyle( shapeStyle );
    switch (shapeType) {
      case rect: {
        //RectShape rectangle = new RectShape();
        //cgLayerRef.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        if (bounds instanceof RectDir) {
          Rotation rotation = ((RectDir) bounds).getRotation();
          if (rotation != null) {
            if (rotation.getAngle() != 0.0) {
              throw new RuntimeException( "TODO shape rotation" );
            }
          }
        }
        break;
      }
      case line: {
        Pos startPos = shapePI.getPos( 0 );
        Pos endPos = shapePI.getPos( 1 );
        cgLayerRef = null; //cgLayerRef = new LineShapeDrawable( startPos, endPos, shapeStyle );
        break;
      }
      case ellipse: {
        //OvalShape ellipse = new OvalShape();
        cgLayerRef = null; //cgLayerRef = new ShapeDrawable( ellipse );
        //cgLayerRef.setBounds( (int) bounds.getX(), (int) bounds.getY(), (int) bounds.getRight(), (int) bounds.getBottom() );
        break;
      }
      case text: {
        //TextShape text = new TextShape( shapePI.getText() );
        cgLayerRef = null; //cgLayerRef = new ShapeDrawable( text );
        //cgLayerRef.getPaint().setTextSize( (float) shapeStyle.getFontSize() );
        String fontName = shapeStyle.getFontName();
        //cgLayerRef.getPaint().setTypeface( Typeface.create( shapeStyle.getFontName(), Typeface.NORMAL ) );
        break;
      }
      case arc: {
        double startAngle = ((ArcShapePI) shapePI).getStartAngle();
        double sweepAngle = ((ArcShapePI) shapePI).getSweepAngle();
        boolean useCenter = ((ArcShapePI) shapePI).getUseCenter();
        cgLayerRef = null; //shapeDrawable = new ArcShapeDrawable( startAngle, sweepAngle, useCenter, shapeStyle );
        break;
      }
      case bitmap: {
        QName bitmapID = ((BitmapPI) shapePI).getBitmapID();
        BitmapAdjust bitmapAdjust = ((BitmapPI) shapePI).getBitmapAdjust();
        if (shapePI instanceof AnimatedBitmapPI) {
          AnimationType animationType = ((AnimatedBitmapPI) shapePI).getAnimationType();
          //AnimatedBitmapDrawable animatedBitmap = new AnimatedBitmapDrawable( bitmapID, bitmapAdjust, animationType, shapeStyle );
          //shapeDrawable = animatedBitmap;
        }
        else {
          IOSBitmap iosBitmap = (IOSBitmap) Platform.getInstance().getBitmap( bitmapID );
          if (iosBitmap != null) {
            Object bitmap = iosBitmap.getImage();
          }
          cgLayerRef = null; // BitmapDrawable bitmap = new BitmapDrawable( bitmapID, bitmapAdjust, shapeStyle );
        }
        break;
      }
      default: {
        throw new IllegalArgumentException( "Unsupported shape type="+shapeType );
      }
    }
    if (bounds instanceof RectDir) {
      Rotation rotation = ((RectDir) bounds).getRotation();
      if (rotation != null) {
        if (rotation.getAngle() != 0.0) {
          throw new RuntimeException( "TODO shape rotation" );
        }
      }
    }
  }

  public synchronized void updateShape() {
    if (cgLayerRef != null) {
      cgLayerRef = null; // Release Layer - will be created on next draw.
    }
  }

  @Override
  public void addTransformation( Transformation transformation ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void transformationChanged( Transformation transformation ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public synchronized void drawShape( Object cgContextRef ) {
    if (cgLayerRef == null) {
      createShape();
    }
  }
}
