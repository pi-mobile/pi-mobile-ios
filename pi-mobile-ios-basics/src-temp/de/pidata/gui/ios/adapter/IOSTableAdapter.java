/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.controller.base.ColumnInfo;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITableAdapter;
import de.pidata.gui.view.base.TableViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

import java.util.HashMap;
import java.util.List;

public class IOSTableAdapter extends IOSListAdapter implements UITableAdapter {

  private Object iOSView;

  public IOSTableAdapter( TableViewPI tableViewPI, Object iosView, UIContainer uiContainer ) {
    super( tableViewPI, uiContainer );
    this.iOSView = iosView;
  }

  @Override
  protected Object getIosView() {
    return iOSView;
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    if (iOSView != null) {
      iOSView = null;
    }
    this.listViewPI = null;
  }

  /**
   * Update changedRow in display list
   *
   * @param changedRow the row to be updated in display list
   */
  @Override
  public void updateRow( Model changedRow ) {
    synchronized (displayRows) {
      int index = indexOfRow( changedRow );
    }
  }

  @Override
  protected void reloadData() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by ListViewPI in order to select row at index
   *
   * @param displayRow the row to be selected
   * @param selected
   */
  @Override
  public void setSelected( Model displayRow, boolean selected ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by TableViewPI to start cell editing form application code.
   * This call should do the same like a mouse click on an editable cell.
   *
   * @param rowModel   row model to edit
   * @param columnInfo column to edit
   */
  @Override
  public void editCell( Model rowModel, ColumnInfo columnInfo ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public HashMap<QName, Object> getFilteredValues() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void unselectValues( HashMap<QName, Object> unselectedValuesMap ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void finishedUpdateAllRows() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public List<Model> getVisibleRowModels() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void columnChanged( ColumnInfo columnInfo ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetFilter() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void resetFilter( ColumnInfo columnInfo ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public void onButtonPressed( Object button ) {
    Model rowModel = buttonPosMap.get( button );
    if (rowModel != null) {
      String tag = button.toString();
      IOSCellAdapter cellAdapter = findCellAdapter( tag );
      if (cellAdapter == null) {
        listViewPI.onSelectionChanged( rowModel, true );
      }
      else {
        ((TableViewPI) listViewPI).onSelectedCell( rowModel, cellAdapter.getColumnInfo() );
      }
    }
  }
}
