/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITextAdapter;
import de.pidata.gui.ui.base.UITextEditorAdapter;
import de.pidata.gui.view.base.TextViewPI;

public class IOSTextEditorAdapter extends IOSValueAdapter implements UITextEditorAdapter {

  private Object iosTextView;
  private TextViewPI textViewPI;

  public IOSTextEditorAdapter( TextViewPI textViewPI, Object iosTextView, UIContainer uiContainer ) {
    super( textViewPI, uiContainer );
    this.textViewPI = textViewPI;
    this.iosTextView = iosTextView;
  }

  @Override
  protected Object getIosView() {
    return iosTextView;
  }

  @Override
  public Object getValue() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setValue( Object value ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setHideInput( boolean hideInput ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setCursorPos( short posX, short posY ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setListenTextChanges( boolean listenTextChanges ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void select( int fromPos, int toPos ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void selectAll() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setTextColor( ComponentColor color ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
