package de.pidata.gui.ios.adapter;

import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UITreeAdapter;
import de.pidata.gui.view.base.TreeNodePI;
import de.pidata.gui.view.base.TreeViewPI;

import java.util.HashMap;
import java.util.Map;

public class IOSTreeAdapter extends IOSUIAdapter implements UITreeAdapter {

  private Object iOSView;
  private TreeViewPI treeViewPI;

  private Map<TreeNodePI, Object> itemMap = new HashMap<>();

  public IOSTreeAdapter( TreeViewPI treeViewPI,  Object iosView, UIContainer uiContainer ) {
    super( treeViewPI, uiContainer );
    this.iOSView = iosView;
    this.treeViewPI = treeViewPI;
    TreeNodePI rootNode = treeViewPI.getRootNode();

    if (rootNode != null) {
      setRootNode( rootNode );
    }
  }

  @Override
  protected Object getIosView() {
    return iOSView;
  }

  @Override
  public void setRootNode( TreeNodePI rootNode ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void updateNode( TreeNodePI treeNode ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void updateChildList( TreeNodePI treeNodePI ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public TreeNodePI getSelectedNode() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setSelectedNode( TreeNodePI selectedNode ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void editNode( TreeNodePI treeNode ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setExpanded( TreeNodePI treeNodePI, boolean expand ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
