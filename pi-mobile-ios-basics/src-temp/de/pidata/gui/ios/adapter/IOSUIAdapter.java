/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.adapter;

import de.pidata.gui.component.base.ComponentColor;
import de.pidata.gui.ios.platform.IOSColor;
import de.pidata.gui.ui.base.UIAdapter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.view.base.ViewPI;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;

public abstract class IOSUIAdapter implements UIAdapter {

  private ViewPI viewPI;
  protected UIContainer uiContainer;
  private IOSColor defaultColor = null;

  public IOSUIAdapter( ViewPI viewPI, UIContainer uiContainer ) {
    this.viewPI = viewPI;
    this.uiContainer = uiContainer;
  }

  protected abstract Object getIosView();

  @Override
  public ViewPI getViewPI() {
    return viewPI;
  }

  @Override
  public UIContainer getUIContainer() {
    return this.uiContainer;
  }

  @Override
  public void setVisible( boolean visible ) {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  @Override
  public boolean isVisible() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
    else {
      return false;
    }
  }

  @Override
  public boolean isFocused() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
    else {
      return false;
    }
  }

  @Override
  public void setEnabled( boolean enabled ) {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  @Override
  public boolean isEnabled() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
    else {
      return false;
    }
  }

  private void setSelected( boolean selected ) {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  /**
   * Called to detach this UIAdapter from it's ui component
   */
  @Override
  public void detach() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  @Override
  public void repaint() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  @Override
  public boolean processCommand( QName cmd, char inputChar, int index ) {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
    else {
      return false;
    }
  }

  /**
   * send onClick request from Controller -> ViewPI -> UIAdapter -> View
   */
  @Override
  public void performOnClick() {
    Object iosView = getIosView();
    if (iosView != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  public void requestFocus() {
    Object view = getIosView();
    if (view != null) {
      //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
      throw new RuntimeException( "TODO" );
    }
  }

  /**
   * Toggles focus, selected, enabled and visible state of this component at position (posX, posY) or for
   * entry identified by key. It depends on the type of component what is meant by posX, posY and key.
   * Many components will just ignore posX and posY values or key.
   * The default implementaion calls the componentListener.
   *
   * @param posX     the x position of the source event
   * @param posY     the y position of the source event
   * @param focus    set focus on/off
   * @param selected set selected if true, deselected if false, do not change if null
   * @param enabled  set enabled if true, disabled if false, do not change if null
   * @param visible  set visible if true, invisible if false, do not change if null
   */
  @Override
  public void setState( short posX, short posY, Boolean focus, Boolean selected, Boolean enabled, Boolean visible ) {
    if (enabled != null) {
      setEnabled( enabled.booleanValue() );
    }
    if (visible != null) {
      setVisible( visible.booleanValue() );
    }
    if (focus != null) {
      if (focus.booleanValue()) {
        requestFocus();
      }
    }
    if (selected != null) {
      setSelected( selected.booleanValue() );
    }
  }

  /**
   * Set this UIAdapter's background color
   *
   * @param color new background color
   */
  @Override
  public void setBackground( ComponentColor color ) {
    IOSColor iosColor = (IOSColor) color;
    Object iosView = getIosView();
    if (defaultColor == null) {
      this.defaultColor = new IOSColor( 0 ); // iosView.backgroundColor
    }
    // iosView.backgroundColor = ...
  }

  /**
   * Resets this UIAdapters background color by its initial background color
   */
  @Override
  public void resetBackground() {
    setBackground( defaultColor );
  }
}
