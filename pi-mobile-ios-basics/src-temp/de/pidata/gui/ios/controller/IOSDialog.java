/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.controller;

import de.pidata.gui.component.base.Platform;
import de.pidata.gui.controller.base.*;
import de.pidata.gui.controller.file.FileChooserParameter;
import de.pidata.gui.event.Dialog;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.guidef.DialogDef;
import de.pidata.gui.guidef.DialogType;
import de.pidata.gui.ios.UIFactoryIOS;
import de.pidata.gui.ios.adapter.IOSMenuButtonAdapter;
import de.pidata.gui.ios.adapter.IOSUIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ButtonViewPI;
import de.pidata.gui.view.base.ModuleViewPI;
import de.pidata.gui.view.base.ViewAnimation;
import de.pidata.log.Logger;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.rights.RightsRequireListener;
import de.pidata.service.base.ParameterList;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IOSDialog implements Dialog, IOSUIContainer {

  private DialogController dialogController;
  private UIViewControllerPI iosUIViewController;
  private List<RightsRequireListener> rightsRequireListeners = new ArrayList<>();
  private HashMap<String,IOSMenuButtonAdapter> menuViews;

  public void init( UIViewControllerPI iosUIViewController ) {
    this.iosUIViewController = iosUIViewController;
    this.iosUIViewController.setIosDialog( this );
  }

  @Override
  public String getTitle() {
    return iosUIViewController.toString();
  }

  @Override
  public Object getUIView() {
    return null;
  }

  /**
   * Sets the controller for this dialog.
   *
   * @param dialogController this dialog's controller
   */
  @Override
  public void setController( DialogController dialogController ) {
    this.dialogController = dialogController;
  }

  /**
   * Returns this dialog's controller
   *
   * @return this dialog's controller
   */
  @Override
  public DialogController getController() {
    return dialogController;
  }

  @Override
  public void show( Dialog parent, boolean fullScreen ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  public void updateLanguage() {
    Platform.getInstance().updateLanguage( this, dialogController.getName(), null );
    ((AbstractDialogController) dialogController).updateChildrenLanguage();
  }

  @Override
  public void toFront() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void toBack() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Show/hide busy cursor or busy animation.
   *
   * @param busy if true show busy, otherwise hide
   */
  @Override
  public void showBusy( boolean busy ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by DialogController.close() before anything other is
   * done. By returning false this Dialog can interrupt closing if
   * the Platform supports to abort close requests (e.g. Android does
   * not support that). Java FX will try to close children and
   * abort closing if any child dialog need user feedback.
   *
   * @param ok true if closing is OK operation
   * @return false to abort closing
   */
  @Override
  public boolean closing( boolean ok ) {
    return true;
  }

  @Override
  public void close( boolean ok, ParameterList resultList ) {
    // dismiss UIViewController

    DialogController parentDlgCtrl = getController().getParentDialogController();
    if (parentDlgCtrl != null) {
      parentDlgCtrl.childDialogClosed( ok, resultList );
    }
  }

  @Override
  public void speak( String text ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setAlwaysOnTop( boolean onTop ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean getAlwaysOnTop() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinWidth( int width ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMaxWidth( int width ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMinHeight( int height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void setMaxHeight( int height ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Called by DialogController after title has been changed.
   *
   * @param title the new dialog title
   */
  @Override
  public void onTitleChanged( String title ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Adds a menu entry for given buttonViewPI.
   * This must be called before dialog is showing.
   *
   * @param buttonViewPI the buton view to be added as menu entry
   */
  public IOSMenuButtonAdapter addMenuEntry( ButtonViewPI buttonViewPI ) {
    if (menuViews == null) {
      menuViews = new HashMap<>();
    }
    String id = buttonViewPI.getComponentID().getName();
    IOSMenuButtonAdapter buttonAdapter = new IOSMenuButtonAdapter( buttonViewPI, this );
    menuViews.put( id, buttonAdapter );
    return buttonAdapter;
  }

  public void menuSelected( String id ) {
    if (menuViews != null) {
      IOSMenuButtonAdapter buttonAdapter = menuViews.get( id );
      if (buttonAdapter != null) {
        buttonAdapter.onClick( getDataContext() );
      }
    }
  }

  /**
   * Opens child dialog with given definition {@link DialogType} and title
   *
   * @param dialogType    user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in {@link GuiOperation#dialogClosed(DialogController, boolean, ParameterList)}
   */
  @Override
  public void openChildDialog( DialogType dialogType, String title, ParameterList parameterList ) {
    try {
      UIViewControllerPI childViewCtrl = ((UIFactoryIOS)getUIFactory()).getViewController( dialogType.getID().getName() );
      ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
      IOSDialog iosDialog = createDialog( dialogType.getID(), dialogType, childViewCtrl);
      DialogController dlgCtrl = builder.createDialogController( dialogType.getID(), getController().getContext(), iosDialog, getController() );
      dlgCtrl.setParameterList( parameterList );
      dlgCtrl.activate( iosDialog );
    }
    catch (Exception e) {
      Logger.error( "Error opening child dialog id="+dialogType.getID(), e );
      throw new IllegalArgumentException( "Error opening child dialog" );
    }
  }

  /**
   * Opend child dialog with given dialogID and title
   *
   * @param dialogDef     user interface definition for the dialog to be opened
   * @param title         title for the dialog, if null title from dialogDef is used
   * @param parameterList dialog parameter list
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void openChildDialog( DialogDef dialogDef, String title, ParameterList parameterList ) {
    try {
      UIViewControllerPI childViewCtrl = ((UIFactoryIOS)getUIFactory()).getViewController( dialogDef.getID().getName() );
      ControllerBuilder builder = Platform.getInstance().getControllerBuilder();
      IOSDialog iosDialog = createDialog( dialogDef.getID(), dialogDef, childViewCtrl );
      DialogController dlgCtrl = builder.createDialogController( dialogDef.getID(), getController().getContext(), iosDialog, getController() );
      dlgCtrl.setParameterList( parameterList );
      dlgCtrl.activate( iosDialog );
    }
    catch (Exception e) {
      Logger.error( "Error opening child dialog id="+dialogDef.getID(), e );
      throw new IllegalArgumentException( "Error opening child dialog" );
    }
  }


  /**
   * Opens popup with given moduleDef and title
   * @param popupCtrl
   * @param moduleGroup   module group to be shown in a Popup
   * @param title         title for the dialog, if null title from dialogDef is used
   */
  @Override
  public void showPopup( PopupController popupCtrl, ModuleGroup moduleGroup, String title ) {
    try {
      ModuleViewPI moduleViewPI = (ModuleViewPI) popupCtrl.getView();
      UIViewControllerPI childViewCtrl = ((UIFactoryIOS)getUIFactory()).getViewController( moduleGroup.getName().getName() );
      //PiFragment androidPopup = new PiFragment();
      //androidPopup.init( moduleGroup, moduleViewPI, getController().getModel() );
      //moduleGroup.activate( androidPopup, getController().getModel() );
      //androidPopup.show( getActivity().getFragmentManager(), title );
      // Note: moduleViewPI is attached to its UIFragmentAdapter by PiFragement.onActivityCreated()
    }
    catch (Exception ex) {
      Logger.error( "Error showing popup", ex );
    }
  }

  /**
   * Hides current popup. If popup is not visible nothing happens.
   */
  @Override
  public void closePopup( PopupController popupController ) {
    popupController.deactivate( true );
    popupController.setModuleGroup( null, ViewAnimation.NONE );
    ModuleViewPI moduleViewPI = (ModuleViewPI) popupController.getView();
    moduleViewPI.detachUIComponent();
  }

  /**
   * Show a message dialog
   *
   * @param title   the dialog title
   * @param message the messsage, use CR (\n) for line breaks
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void showMessage( String title, String message ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Show a question dialog dialog
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param btnYesLabel    the label for yes/ok button or null to hide button
   * @param btnNoLabel     the label for no button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void showQuestion( String title, String message, String btnYesLabel, String btnNoLabel, String btnCancelLabel ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Show a dialog with a text input field
   *
   * @param title          the dialog title
   * @param message        the messsage, use CR (\n) for line breaks
   * @param defaultValue   a default value for the input field
   * @param btnOKLabel     the label for ok button or null to hide button
   * @param btnCancelLabel the label for cancel button or null to hide button
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void showInput( String title, String message, String defaultValue, String btnOKLabel, String btnCancelLabel ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows the platform specific file chooser dialog.
   *
   * @param title             dialog title
   * @param fileChooserParams parameters
   * @return handle to the opened dialog for use in GuiOperation.dialogClosed()
   */
  @Override
  public void showFileChooser( String title, FileChooserParameter fileChooserParams ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Shows the platform specific image chooser dialog.
   *
   * @param imageChooserType type of the image chooser
   * @param fileName         name of file that has to be used, if image will be created (by using camera)
   * @return dialog Handle
   */
  @Override
  public void showImageChooser( ImageChooserType imageChooserType, String fileName ) {
    File fotoFile = new File( fileName );
    fotoFile.getParentFile().mkdirs();

    switch (imageChooserType) {
      case CAMERA_ONLY: {
        // TODO
        break;
      }
      case GALLERY_ONLY: {
        // TODO
        break;
      }
      case CAMERA_AND_GALLERY: {
        // TODO
        break;
      }
    }
  }

  /**
   * Show a toast message, i.e. a message which appears only for a short moment
   * and ten disappears.
   *
   * @param message the message to show
   */
  @Override
  public void showToast( String message ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Register a key combination command as shortcut and setup the handling for it.
   *
   * @param command    the key combination to register
   * @param controller the controller to fire on
   */
  @Override
  public void registerShortcut( QName command, ActionController controller ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void requireRights() {
    for (RightsRequireListener listener : rightsRequireListeners) {
      listener.rightsRequired( true );
    }
  }

  @Override
  public void addRightsRequireListener( RightsRequireListener rightsRequireListener ) {
    rightsRequireListeners.add( rightsRequireListener );
  }

  /**
   * Set UI Style
   *
   * @param style
   */
  @Override
  public void setStyle( String style ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns the UIFactory to use when creating an UIAdapter for a
   * UI component within this container.
   */
  @Override
  public UIFactory getUIFactory() {
    return Platform.getInstance().getUiFactory();
  }

  /**
   * Returns the data context for this UIContainer
   *
   * @return
   */
  @Override
  public Model getDataContext() {
    return dialogController.getModel();
  }

  public static IOSDialog createDialog( QName dialogID, DialogDef dialogDef, UIViewControllerPI iosUIViewController ) {
    //--- Create iOS dialog
    IOSDialog dialog;
    String dlgClassName = dialogDef.getDialogClass();
    if ((dlgClassName != null) && (dlgClassName.length() > 0)) {
      try {
        dialog = (IOSDialog) Class.forName( dlgClassName ).newInstance();
      }
      catch (Exception e) {
        Logger.error( "Error creating dialog class", e );
        throw new IllegalArgumentException( "Cannot create dialog class for dialogID="+dialogID+", className="+dlgClassName );
      }
    }
    else {
      dialog = new IOSDialog();
    }
    dialog.init( iosUIViewController );
    return dialog;
  }

  public static IOSDialog createDialog( QName dialogID, DialogType dialogType, UIViewControllerPI iosUIViewController ) {
    //--- Create iOS dialog
    IOSDialog dialog;
    String dlgClassName = dialogType.getDialogClass();
    if ((dlgClassName != null) && (dlgClassName.length() > 0)) {
      try {
        dialog = (IOSDialog) Class.forName( dlgClassName ).newInstance();
      }
      catch (Exception e) {
        Logger.error( "Error creating dialog class", e );
        throw new IllegalArgumentException( "Cannot create dialog class for dialogID="+dialogID+", className="+dlgClassName );
      }
    }
    else {
      dialog = new IOSDialog();
    }
    dialog.init( iosUIViewController );
    return dialog;
  }

}
