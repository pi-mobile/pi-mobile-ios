package de.pidata.gui.ios.platform;

import de.pidata.gui.component.base.ComponentBitmap;
import de.pidata.gui.component.base.ComponentGraphics;
import de.pidata.qnames.QName;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOSBitmap implements ComponentBitmap {

  Object bitmap;

  public IOSBitmap( Object bitmap ) {
    this.bitmap = bitmap;
  }

  public ComponentGraphics getGraphics() {
    //TODO
    throw new RuntimeException( "TODO ");
  }

  public int getWidth() {
    return 0; //bitmap.getWidth()
  }

  public int getHeight() {
    return 0; //bitmap.getHeight()
  }

  public Object getImage() {
    return bitmap;
  }

  /**
   * Saves this bitmap to the given OutputStream
   *
   * @param outStream the strem to which image will be written
   * @param imageType the image type, see constants (TIFF, JPEG, ...)
   */
  public void save( OutputStream outStream, QName imageType ) throws IOException {
    if (imageType == PNG) {
      //bitmap.compress( Bitmap.CompressFormat.PNG, 90, outStream );
    }
    else if (imageType == GIF) {
      //TODO
      throw new RuntimeException( "TODO ");
    }
    else if (imageType == JPEG) {
      //bitmap.compress( Bitmap.CompressFormat.JPEG, 90, outStream );
    }
    else if (imageType == TIFF) {
      //TODO
      throw new RuntimeException( "TODO ");
    }
  }

  @Override
  public int size() {
    return 0; // bitmap.getByteCount();
  }

  @Override
  public byte[] getBytes() {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void readBytes( InputStream byteStream ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  @Override
  public void writeBytes( OutputStream out ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
