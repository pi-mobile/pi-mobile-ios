/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.platform;

import de.pidata.gui.component.base.ComponentColor;

public class IOSColor implements ComponentColor {

  private Object color;

  public IOSColor( int colorInt ) {
    color = null;    //TODO
  }

  public IOSColor( int r, int g, int b, double alpha ) {
    color = null;    //TODO
  }

  public IOSColor( String colorString ) {
    color = null;    //TODO
  }

  @Override
  public Object getColor() {
    return color;
  }

  @Override
  public void setColor( Object color ) {
    this.color = color;
  }
}
