/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.platform;

import de.pidata.gui.component.base.MediaInterface;
import de.pidata.qnames.QName;
import de.pidata.system.base.Storage;

import java.io.IOException;

public class IOSMedia implements MediaInterface {

  private QName startSoundID;
  private QName stopSoundID;
  private boolean recording = false;
  private int maxRecordingTime = 30000; // 30s

  @Override
  public void playSound( QName soundID ) {
    // TODO
  }

  @Override
  public void initRecording( QName startSoundID, QName stopSoundID, int maxRecordingTime ) {
    this.startSoundID = startSoundID;
    this.stopSoundID = stopSoundID;
    this.maxRecordingTime = maxRecordingTime;
  }

  @Override
  public boolean isRecording() {
    return recording;
  }

  @Override
  public boolean startRecording( Storage recordingStorage, String fileName ) {
    //TODO
    recording = true;
    return recording;
  }

  @Override
  public void stopRecording() {
    //TODO
    recording = false;
  }
}
