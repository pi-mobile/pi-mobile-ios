/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.platform;

import de.pidata.connect.base.ConnectionController;
import de.pidata.connect.bluetooth.SPPConnection;
import de.pidata.connect.stream.MessageSplitter;
import de.pidata.gui.component.base.*;
import de.pidata.gui.event.InputManager;
import de.pidata.gui.guidef.ControllerBuilder;
import de.pidata.gui.ios.IOSScreen;
import de.pidata.gui.ios.UIFactoryIOS;
import de.pidata.gui.ios.controller.IOSControllerBuilder;
import de.pidata.gui.layout.Layouter;
import de.pidata.gui.ui.base.UIContainer;
import de.pidata.gui.ui.base.UIFactory;
import de.pidata.gui.view.base.ViewFactory;
import de.pidata.log.Logger;
import de.pidata.models.config.Configurator;
import de.pidata.models.config.DeviceTable;
import de.pidata.models.tree.Context;
import de.pidata.models.tree.EventListener;
import de.pidata.models.tree.EventSender;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.stream.StreamHelper;
import de.pidata.system.base.BackgroundSynchronizer;
import de.pidata.system.base.Storage;
import de.pidata.system.base.SystemManager;
import de.pidata.system.ios.IOSBundleStorage;
import de.pidata.system.ios.IOSConnectionController;

import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Locale;

public class IOSPlatform extends Platform implements EventListener {

  private ComponentFactory compFactory;
  private IOSControllerBuilder controllerBuilder;
  private ViewFactory viewFactory;
  private IOSScreen screen = new IOSScreen();
  private Hashtable colors = new Hashtable();
  private boolean exiting = false;
  private Storage guiStorage;
  private IOSMedia iosMedia;
  private NfcTool nfcTool = null;

  public IOSPlatform(Context context) throws Exception {
    String[] args = new String[]{"Application"};
    this.init(context, args);
  }

  protected void init(Context context, String[] args) throws Exception {
    super.init(context, args);
    this.colors.put(ComponentColor.BLACK, new IOSColor(-16777216));
    this.colors.put(ComponentColor.GRAY, new IOSColor(-7829368));
    this.colors.put(ComponentColor.WHITE, new IOSColor(-1));
    this.colors.put(ComponentColor.BLUE, new IOSColor(-16776961));
    this.colors.put(ComponentColor.LIGHTGRAY, new IOSColor(-3355444));
    this.colors.put(ComponentColor.RED, new IOSColor(-65536));
    this.colors.put(ComponentColor.ORANGE, new IOSColor(255, 165, 0, 1.0));
    this.colors.put(ComponentColor.CYAN, new IOSColor(-16711681));
    this.colors.put(ComponentColor.MAGENTA, new IOSColor(-65281));
    this.colors.put(ComponentColor.GREEN, new IOSColor(-16711936));
    this.colors.put(ComponentColor.PINK, new IOSColor(255, 192, 203, 1.0));
    this.colors.put(ComponentColor.YELLOW, new IOSColor(-256));
    this.colors.put(ComponentColor.DARKGRAY, new IOSColor(-12303292));
    this.colors.put(ComponentColor.DARKGREEN, new IOSColor(0, 178, 0, 1.0));


    Locale langLocale = Locale.forLanguageTag( "DE" );
    Locale.setDefault( langLocale );

  }

  public String getPlatformName() {
    return "AndroidPlatform";
  }

  protected void initComm(Context context) throws Exception {
    SystemManager sysMan = SystemManager.getInstance();
    String connCtrl = sysMan.getProperty("comm.connCtrl", (String)null);
    if (connCtrl != null && connCtrl.length() != 0) {
      if ("DEFAULT".equals(connCtrl)) {
        sysMan.setConnectionController(new IOSConnectionController(context));
      }
      else {
        sysMan.setConnectionController((ConnectionController)Class.forName(connCtrl).newInstance());
      }
    }
    else {
      sysMan.setConnectionController((ConnectionController)null);
    }

    BackgroundSynchronizer synchronizer = BackgroundSynchronizer.getInstance();
    if (synchronizer != null) {
      Model statusModel = synchronizer.getStatusModel();
      statusModel.addListener(this);
    }

  }

  public ControllerBuilder getControllerBuilder() {
    if (this.controllerBuilder == null) {
      this.controllerBuilder = new IOSControllerBuilder();
    }

    return this.controllerBuilder;
  }

  protected void openInitialDialog(Context context, QName opID) {
  }

  public String getStartupFile(String programName) {
    throw new RuntimeException("TODO");
  }

  public Storage getStartupFileStorage() {
    throw new RuntimeException("TODO");
  }

  @Override
  public Storage getGuiStorage() {
    if (guiStorage == null) {
      guiStorage = new IOSBundleStorage( "gui" );
    }
    return guiStorage;
  }

  public ComponentBitmap createBitmap( int width, int height) {
    // TODO
    throw new RuntimeException( "TODO" );
  }

  /**
   * Loads a bitmap from the given file system path.
   *
   * @param path the path of a image file (gif or jpeg)
   * @return the bitmap loaded
   */
  @Override
  public ComponentBitmap loadBitmapFile( String path ) {
    Storage storage = SystemManager.getInstance().getStorage((String)null);
    if (storage.exists(path)) {
      InputStream imageStream = null;

      ComponentBitmap var4;
      try {
        imageStream = storage.read(path);
        var4 = this.loadBitmap(imageStream);
      } catch (IOException var8) {
        Logger.error("Could not load image, path=" + path, var8);
        return null;
      } finally {
        StreamHelper.close(imageStream);
      }

      return var4;
    } else {
      return null;
    }
  }

  public ComponentBitmap loadBitmap(InputStream imageStream) {
    Object bitmap = null;
    return new IOSBitmap( bitmap );
  }

  @Override
  public ComponentBitmap loadBitmapResource( QName bitmapID ) {
    String path = bitmapID.getName();
    if (path.startsWith( "@" )) {
      path = path.substring( 1 );
    }
    Object uiImage = null;
    return new IOSBitmap( uiImage );
  }

  @Override
  public ComponentBitmap loadBitmapAsset( QName bitmapID ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  public ComponentBitmap loadBitmapThumbnail(Storage imageStorage, String imageFileName, int width, int height) {
    InputStream imageStream = null;

    ComponentBitmap componentBitmap;
    try {
      imageStream = imageStorage.read(imageFileName);
      componentBitmap = this.loadBitmap(imageStream);
    }
    catch (IOException ex) {
      Logger.error("Could not load image, imageFileName=" + imageFileName, ex);
      return null;
    }
    finally {
      StreamHelper.close(imageStream);
    }

    return componentBitmap;
  }

  /**
   * Add new color to platform's color table. If a color with same name already exists it is replaced
   *
   * @param name  color's name
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  @Override
  public ComponentColor setColor( QName name, int red, int green, int blue, double alpha ) {
    IOSColor iosColor = new IOSColor( red, green, blue, alpha );
    colors.put( name, iosColor );
    return iosColor;
  }

  public ComponentColor getColor(int red, int green, int blue) {
    return new IOSColor( red, green, blue, 1.0 );
  }

  /**
   * Returns the platform specific color specified by the given values.
   *
   * @param red   red (0 .. 255)
   * @param green green (0 .. 255)
   * @param blue  blue (0 .. 255)
   * @param alpha alpha value / opacity (0.0 .. 1.0)
   * @return
   */
  @Override
  public ComponentColor getColor( int red, int green, int blue, double alpha ) {
    return new IOSColor( red, green, blue, alpha );
  }

  public ComponentColor getColor( QName colorID ) {
    ComponentColor color = (ComponentColor)this.colors.get(colorID);
    if (color == null) {
      color = new IOSColor( colorID.getName() );
      this.colors.put( colorID, color );
    }
    return color;
  }

  public ComponentColor getColor(String colorString) {
    return new IOSColor( colorString );
  }

  public void setColor(QName colorID, String value) {
    IOSColor iosColor = (IOSColor) getColor( value );
    colors.put( colorID, iosColor );
  }

  public void setFont(QName fontID, String name, QName style, int size) {
    throw new RuntimeException("TODO");
  }

  public ComponentFont createFont( String name, QName style, int size) {
    throw new RuntimeException("TODO");
  }

  public ComponentFont getFont(QName fontID) {
    throw new RuntimeException("TODO");
  }

  public Screen getScreen() {
    while(true) {
      if (this.screen == null) {
        try {
          Thread.sleep(1000L);
          continue;
        } catch (InterruptedException var2) {
          var2.printStackTrace();
        }
      }

      return this.screen;
    }
  }

  public ComponentFactory getComponentFactory() {
    return this.compFactory;
  }

  protected GuiBuilder createGuiBuilder() throws Exception {
    String builderClassName = SystemManager.getInstance().getProperty("gui.builder", (String)null);
    return builderClassName != null && builderClassName.length() > 0 ? (GuiBuilder)Class.forName(builderClassName).newInstance() : null;
  }

  public ViewFactory getViewFactory() {
    if (this.viewFactory == null) {
      this.viewFactory = new ViewFactory();
    }

    return this.viewFactory;
  }

  public UIFactory getUiFactory() {
    UIFactory uiFactory = UIFactoryIOS.getInstance();
    if (uiFactory == null) {
      uiFactory = new UIFactoryIOS();
    }
    return uiFactory;
  }

  public InputManager getInputManager() {
    throw new RuntimeException("TODO");
  }

  public void initWindow(Object dialog) {
    throw new RuntimeException("TODO");
  }

  public void toggleKeyboard( boolean hasFocus ) {
    // TODO
  }

  public boolean isSingleWindow() {
    throw new RuntimeException("TODO");
  }

  public boolean useDoubleBuffering() {
    throw new RuntimeException("TODO");
  }

  public boolean hasTableFirstRowForEmptySelection() {
    return false;
  }

  /**
   * Adds paired Bluetooth devices to deviceTable
   *
   * @param deviceTable table for the result
   */
  @Override
  public void getPairedBluetoothDevices( DeviceTable deviceTable ) {
    // To be implemented for IOS
  }

  public Dialog createDialog( Layouter layouterX, Layouter layouterY, short x, short y, short width, short height) {
    throw new RuntimeException("TODO");
  }

  public QName getNavigationKeys() {
    throw new RuntimeException("TODO");
  }

  public boolean isExiting() {
    return this.exiting;
  }

  public void exit(Context context) {
    this.exiting = true;
    System.exit( 0 );
  }

  public boolean isCharWidthUsable() {
    throw new RuntimeException("TODO");
  }

  public void setConfigurator2(Configurator configurator) throws Exception {
    this.setConfigurator(configurator);
  }

  public PlatformScheduler createScheduler() {
    return new IOSScheduler();
  }

  public SPPConnection createBluetoothSPPConnection( String serverAddress, String uuidString, MessageSplitter messageSplitter) {
    throw new IllegalArgumentException( "iOS does not support bluetooth SPP" );
  }

  @Override
  public MediaInterface getMediaInterface() {
    if (iosMedia == null) {
      iosMedia = new IOSMedia();
    }
    return iosMedia;
  }

  public void eventOccured( EventSender eventSender, int eventID, Object source, QName modelID, Object oldValue, Object newValue) {
    if (eventID == 0) {
      BackgroundSynchronizer synchronizer = BackgroundSynchronizer.getInstance();
      if (synchronizer != null && modelID == synchronizer.getStateAttrID() && source == synchronizer.getStatusModel()) {
        if (synchronizer.isStateActive((QName)newValue)) {
          // TODO
          throw new RuntimeException( "TODO" );
        }
      }
    }

  }

  @Override
  public void runOnUiThread( Runnable runnable ) {
    IOSScheduler.runOnUiThread( runnable );
  }

  /**
   * Update static text in uiContainer by properties from language file for dialogID.
   * Befor writing to UI entries enclosed in "{GLOSSARYNAME}" are replaced by matching
   * entry from language file "glossary".
   *
   * @param uiContainer the uiContainer to be updated
   * @param dialogID    the dialogID uses as property filename
   * @param language    the language, e.g. "de". If null system default is used.
   */
  @Override
  public void updateLanguage( UIContainer uiContainer, QName dialogID, String language ) {
    super.updateLanguage( uiContainer, dialogID, language );
  }

  @Override
  public boolean openBrowser( String url ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public NfcTool getNfcTool() {
    if (this.nfcTool == null) {
      String nfcToolClass = SystemManager.getInstance().getProperty( "NfcTool", null );
      if (nfcToolClass != null) {
        try {
          this.nfcTool = (NfcTool) Class.forName( nfcToolClass ).newInstance();
        }
        catch (Exception ex) {
          Logger.error( "Error creating NfcTool", ex );
        }
      }
    }
    return this.nfcTool;
  }
}
