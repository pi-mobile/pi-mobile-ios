/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.gui.ios.platform;

import de.pidata.gui.component.base.PlatformScheduler;
import de.pidata.worker.RunnableJob;
import de.pidata.worker.Worker;
import de.pidata.worker.WorkerThread;

public class IOSScheduler implements PlatformScheduler {

  private static Worker worker;

  private static synchronized Worker getWorker() {
    if (worker == null) {
      worker = new Worker();
      new WorkerThread( worker, "IOSScheduler" ).start();
    }
    return worker;
  }

  @Override
  public void post( Runnable task, boolean doLogging ) {
    getWorker().addJob( new RunnableJob( task, 0, doLogging ) );
  }

  @Override
  public void postDelayed( Runnable task, long afterMillis, boolean doLogging ) {
    getWorker().addJob( new RunnableJob( task, afterMillis, doLogging ) );
  }

  public static void runOnUiThread( Runnable task ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }
}
