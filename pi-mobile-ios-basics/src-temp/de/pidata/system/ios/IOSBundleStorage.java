/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.ios;

import de.pidata.system.base.Storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOSBundleStorage extends de.pidata.system.base.Storage {

  public IOSBundleStorage( String name ) {
    super( name );
  }

  /**
   * Returns the fully path identifying fileName.
   *
   * @param fileName the name source name within this storage
   * @return the path for fileName
   */
  @Override
  public String getPath( String fileName ) {
    return fileName;
  }

  /**
   * Returns an input stream to source within this storage
   *
   * @param source the name source name within this storage
   * @return an input stream to source within this storage
   * @throws IOException if source does not exist or cannot be accessed
   */
  @Override
  public InputStream read( String source ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns true if source exists.
   *
   * @param source the source name within this storage or null for this storage itself
   * @return true if source exists
   */
  @Override
  public boolean exists( String source ) {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns size of source in bytes.
   *
   * @param source the source name within this storage
   * @return size of source in bytes
   */
  @Override
  public long size( String source ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns true if source is a directory
   *
   * @param source the source name within this storage
   * @return true if source is a directory
   */
  @Override
  public boolean isDirectory( String source ) throws IOException {
    return false;
  }

  /**
   * Returns the sub directory for childName
   *
   * @param childName the sub directory's name
   * @return the sub directory storage
   * @throws IOException if childName is not a directory or any other io error
   */
  @Override
  public Storage getSubDirectory( String childName ) throws IOException {
    throw new IllegalArgumentException( "iOS Bundle has no dub dirs" );
  }

  /**
   * Creates a sub directory with given name
   *
   * @param childName
   * @return
   * @throws IOException if this sotrage does not allow sub directories,
   *                     childName is not valid for a sub directory
   *                     or any other IO error
   */
  @Override
  public Storage createSubDirectory( String childName ) throws IOException {
    throw new IllegalArgumentException( "iOS Bundle has no dub dirs" );
  }

  /**
   * Returns last modification timestamp of source in millis.
   *
   * @param source the source name within this storage
   * @return last modification timestamp of source in millis
   */
  @Override
  public long lastModified( String source ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Returns an output stream to destination within this storage
   *
   * @param destination the name source name within this storage
   * @param overwrite   true: destination is created or overwritten if exists
   *                    false: IOException is thrown if destination exists
   * @param append
   * @return an input stream to source within this storage
   * @throws IOException if destination cannot be written
   */
  @Override
  public OutputStream write( String destination, boolean overwrite, boolean append ) throws IOException {
    throw new IllegalArgumentException( "iOS Bundle is read only" );
  }

  /**
   * Returns an array representing the directory of this storage (e.g. file names)
   *
   * @return a unordered array of Strings, if sourcePath is not a directoy or empty an array of size 0 is returned
   */
  @Override
  public String[] list() throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }

  /**
   * Deletes resource within this storage. If resource does not exist nothing happens.
   * If resource is a directory its content is deleted recursively.
   *
   * @param resource the name of the resxource to be deleted
   * @throws IOException if resource could not be deleted
   */
  @Override
  public void delete( String resource ) throws IOException {
    throw new IllegalArgumentException( "iOS Bundle is read only" );
  }

  /**
   * Renames filename to newName
   *
   * @param filename name of the file to be renamed
   * @param newName  new name for the file
   * @throws IOException if filename could not be renamed
   */
  @Override
  public void rename( String filename, String newName ) throws IOException {
    throw new IllegalArgumentException( "iOS Bundle is read only" );
  }

  /**
   * Moves each file from this storage to another.
   *
   * @param fileName    file to be moved
   * @param destStorage name of the destination storage - may be the same like this
   * @param newName     new name or null to use fileName
   */
  @Override
  public void move( String fileName, Storage destStorage, String newName ) throws IOException {
    //TODO: not yet implemented - To change body of implemented methods use File | Settings | File Templates.
    throw new RuntimeException( "TODO" );
  }
}
