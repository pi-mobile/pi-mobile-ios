/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.ios;

import de.pidata.log.FileOwner;
import de.pidata.system.base.WifiManager;
import de.pidata.log.Logger;
import de.pidata.settings.PropertiesSettings;
import de.pidata.system.base.Storage;
import de.pidata.system.filebased.FilebasedSystem;

import java.util.*;

public class IOSSystem extends FilebasedSystem {

  private static final boolean DEBUG = false;
  private Properties applicationProps;
  private WifiManager wifiManager;

  public IOSSystem( String basePath, String programmVersion, String programmName ) {
    super( basePath );
    this.programVersion = programmVersion;
    this.programName = programmName;

    setSettings( new PropertiesSettings( new Properties() ) );
    initLogging();
  }

  /**
   * Returns a new Calendar instance for the current TimeZone, specified in application.properties, e.g:
   * timezone=Germany/Berlin
   * @return the Calender instance
   */
  public Calendar getCalendar() {
    String timeZoneString = getProperty( "timezone", "Europe/Berlin" );
    String localeLanguage = getProperty("localeLanguage", "de");
    String localeCountry = getProperty("localeCountry", "DE");

    TimeZone timeZone = TimeZone.getTimeZone( timeZoneString );
    Locale locale = new Locale( localeLanguage, localeCountry );
    Calendar calendar = Calendar.getInstance( timeZone, locale );

    if (LOG_CALENDARINFO) {
      Logger.info( "----- available TimeZones" );
      String[] availableTimeZones = TimeZone.getAvailableIDs();
      for (int i = 0; i<availableTimeZones.length; i++) {
        Logger.info( availableTimeZones[i] );
      }

      Logger.info( "----- available Locales" );
      Locale[] availableLocales = Locale.getAvailableLocales();
      for (int i=0; i<availableLocales.length; i++) {
        Logger.info( availableLocales[i].toString() );
      }
    }

    if (calendar == null) {
      Logger.warn( "No Calendar instance found for ["+timeZoneString+"/"+localeLanguage+"_"+localeCountry+"]" );
    }
    else if (DEBUG) {
      Logger.info( "Use Calendar ["+calendar.toString()+"]" );
    }
    return calendar;
  }

  public Storage getStorage( String storageName ) {
    if (storageName == null) {
      return this.systemStorage;
    }
    else if (storageName.startsWith( STORAGE_PREFIX_PI )) {
      int pos = storageName.indexOf( ':' );
      String storageType = storageName.substring( 0, pos );
      String storagePath = storageName.substring( pos+1 );
      return getStorage( storageType, storagePath );
    }
    else {
      return getStorage( STORAGE_CLASSPATH, storageName );
    }
  }

  public Storage getStorage( String storageType, String storagePath ) {
    if (storageType.equals( STORAGE_CLASSPATH )) {
      return new IOSFileStorage( storagePath, null, new IOSBundleStorage( storagePath ) );
    }
    else if (storageType.equals( STORAGE_PRIVATE_PICTURES )) {
      return new IOSFileStorage( storagePath, "Images", null );
    }
    else if (storageType.equals( STORAGE_PRIVATE_DOWNLOADS )) {
      return new IOSFileStorage( storagePath, "Download", null );
    }
    else {
      throw new IllegalArgumentException( "Unknown storage type="+storageType );
    }
  }

  @Override
  public boolean sendMail( String mailAddr, String title, String body, List<FileOwner> attachmentProviderList ) {
    try {

      // NATIVE MAIL SOURCE HERE:
      if (mailAddr != null) {
        String[] toAddrs = new String[1];
        toAddrs[0] = mailAddr;
      }
      if (title != null) {

      }
      if (body != null) {

      }
      if (attachmentProviderList != null) {
        List<String> reportFiles= new ArrayList<>();
        for (FileOwner fileOwner : attachmentProviderList) {
          reportFiles.addAll( fileOwner.getOwnedFiles() );
        }
        if (reportFiles.size() > 0) {
          for (String logFilePath : reportFiles) {

          }
        }
      }
      return true;
    }
    catch (Exception ex) {
      return false;
    }
  }

  @Override
  public WifiManager getWifiManager() {
    if (wifiManager == null) {
      wifiManager = new WifiManagerIOS();
    }
    return wifiManager;
  }

  @Override
  public void clearWebCache() {
    //   NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    //  [sharedCache removeAllCachedResponses];
  }
}
