package de.pidata.system.ios;

import de.pidata.system.base.WifiConnectionInfo;
import de.pidata.system.base.WifiConnectionState;
import de.pidata.system.base.WifiManager;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class WifiManagerIOS implements WifiManager {

  public WifiManagerIOS() {
  }

  WifiConnectionInfo currentConnectionInfo;

  @Override
  public List<String> scanWiFi() {
    ArrayList<String> ssidList = new ArrayList<>();
    int x = 0;
    for (int i = 0; i < x; i++) {
      String ssid = "X";
      ssidList.add( ssid );
    }
    return ssidList;
  }

  @Override
  public void enableWifi() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public boolean checkConnectionState() {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public String setActiveWifi( String ssid ) {
    // TODO Not yet implemented
    throw new RuntimeException( "TODO" );
  }

  @Override
  public WifiConnectionInfo getWiFiConnectionInfo() throws IOException {
    if (currentConnectionInfo == null) {
      currentConnectionInfo = new WifiConnectionInfo( WifiConnectionState.INITIALIZING, "", 0, 0, 0, "" );
    }
    return currentConnectionInfo;
  }

  @Override
  public URLConnection openWiFiConnection( URL url ) throws IOException {
    return url.openConnection();
  }
}
