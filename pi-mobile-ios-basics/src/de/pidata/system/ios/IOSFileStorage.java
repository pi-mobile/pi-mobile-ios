/*
 * This file is part of PI-Mobile iOS (https://gitlab.com/pi-mobile/pi-mobile-ios).
 * Copyright (C) 2005-2020 PI-Data, Germany (https://www.pi-data.de).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package de.pidata.system.ios;

import de.pidata.system.base.Storage;
import de.pidata.system.filebased.AbstractFileStorage;

import java.io.File;
import java.io.IOException;

import static de.pidata.system.filebased.FilebasedStorage.getStoragePath;

public class IOSFileStorage extends AbstractFileStorage {

  public IOSFileStorage( String storageName, String basePath, Storage altReadOnlyStorage ) {
    super( storageName, getStoragePath( storageName, basePath ), altReadOnlyStorage );
  }

  @Override
  protected File getFile( String fileName ) {
    if (fileName == null) {
      if ((path == null) || (path.length() == 0)) {
        fileName = ".";
      }
      else {
        fileName = path;
      }
    }
    else if ((path != null) && (path.length() > 0)) {
      if (path.endsWith( "/" ) || path.endsWith("\\")) {
        fileName = path + fileName;
      }
      else {
        fileName = path + "/" + fileName;
      }
    }
    return new File(fileName);
  }

  @Override
  protected File getStorageFile() {
    // On iOS at least isDirectory() and list() only work on File returned by getAbsoluteFile() !?
    return new File( path ).getAbsoluteFile();
  }

  @Override
  public Storage getSubDirectory( String childName ) throws IOException {
    File testFile = getFile(childName);
    if (testFile.isDirectory()) {
      Storage altStorage = getAltReadOnlyStorage();
      Storage newAltStorage = null;
      if (altStorage != null) {
        newAltStorage = new IOSBundleStorage( getStoragePath( childName, name ));
      }
      return new IOSFileStorage( childName, testFile.getPath(), newAltStorage );
    }
    else {
      throw new IOException( "File is not a directory, name="+childName );
    }
  }
}
